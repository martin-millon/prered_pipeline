'''
We finally apply the masterflats to the science images
'''

from __future__ import print_function
import os
import sys
import glob
from astropy.time import Time
from astropy.io.fits import getheader
import numpy as np
from ECAMUtils import auxfcts


def main(dates, myrawdirrestruct, productsciencedir, productdir, redo=True, log_stream=None):
    if log_stream is None:
        outstream = sys.stdout
    else:
        outstream = log_stream

    failed_images = []

    for ind, date in enumerate(dates):
        print("=" * 15, date, "=" * 15, file=outstream)
        datedir = os.path.join(myrawdirrestruct, date)
        logline = []

        science_dirs = [os.path.join(datedir, o) for o in os.listdir(datedir) if
                        os.path.isdir(os.path.join(datedir, o)) and 'science' in os.path.join(datedir,
                                                                                              o)]  # select the science directories

        for sdir in science_dirs:
            print("=" * 15, sdir.split('science')[1], "=" * 15, file=outstream)
            images = glob.glob(os.path.join(sdir, "*_bs.fits"))  # select the bias subtracted images.

            couldnotflatfield = []
            for i, im in enumerate(images):
                basename = os.path.basename(im)
                basename_out = basename.split("_bs.fits")[0] + ".fits"
                header = getheader(im)
                target = str(header["OBJECT"])
                filt = str(header["FILTER"])
                try:
                    ampname = str(header["HIERARCH OGE DET OUT NAME"])
                except KeyError:
                    ampname = str(header["HIERARCH OGE DET OUT RNAME"])

                flattype = filt + ampname + "4"  # historical naming convention, to make the pipeline compatible with the previous file system
                destdir = os.path.join(productsciencedir, target + "_" + filt + "_" + ampname + "_" + flattype)

                if os.path.isdir(destdir):
                    print("The destination %s directory exists. I will complete it." % destdir, file=outstream)
                else:
                    os.mkdir(destdir)

                print("=== %4i/%4i : %s ===" % (i + 1, len(images), target), file=outstream)

                # We find the masterflat correpsonding to the night :
                masterflatfilepath = os.path.join(productdir, "MF_" + flattype, "MF_%s.fits" % date)

                inputfilepath = im
                outputfilepath = os.path.join(destdir, basename_out)

                if os.path.isfile(outputfilepath) and redo is False:
                    print('Already flatfielded, going on...', file=outstream)
                    continue
                elif os.path.isfile(outputfilepath) and redo is True:
                    print('Already flatfielded, I will remove it', file=outstream)
                    os.remove(outputfilepath)

                if not os.path.isfile(inputfilepath):
                    print("WARNING : Cannot find input file %s" % inputfilepath, file=outstream)
                    logline.append("WARNING : Cannot find input file %s \n" % inputfilepath)
                    couldnotflatfield.append(im)
                    continue

                (a, h) = auxfcts.fromfits(inputfilepath, hdu=0, verbose=False)

                if not os.path.isfile(masterflatfilepath):
                    print("WARNING : No masterflat available for this night... I will not flatfield the image %s" % im,
                          file=outstream)
                    logline.append(
                        "WARNING : No masterflat available for this night... I will not flatfield the image %s \n" % im)
                    couldnotflatfield.append(im)
                    continue

                else:
                    (masterflat, mh) = auxfcts.fromfits(masterflatfilepath, hdu=0, verbose=False)
                    n_flats = mh['NCOMBINE']
                    mf_filter = mh['FILTER']
                    mf_ampmode = mh['PR_FORMA']
                    flatfieldeda = a / masterflat
                    h['PR_FLATF'] = ("Yes", "pypr has flatfielded this image.")

                    # We add some infos to the header
                    h['PR_NIGHT'] = (date, "pypr night")
                    h['PR_NFLAT'] = (n_flats, "pypr number of flatfields in masterflat")
                    h['PR_MFNAM'] = (os.path.basename(masterflatfilepath), "pypr masterflatname")
                    h['PR_MFFIL'] = (mf_filter, "pypr masterflat filter")
                    h['PR_MFAMP'] = (mf_ampmode, "pypr masterflat ampmode")
                    h['PR_PROUD'] = (":-)", "pypr proudly prereduced this image !")

                    flatfieldeda = flatfieldeda[300:-300, 300:-300].clip(min=-66000, max=66000)  # border of 300 pixels

                    if ampname == "ALL":
                        flatfieldeda = np.fliplr(flatfieldeda)
                    elif ampname == "LL":
                        flatfieldeda = np.fliplr(flatfieldeda)
                    elif ampname == "UL":
                        pass

                    # We write the fits file
                    auxfcts.tofits(outputfilepath, flatfieldeda, hdr=h, verbose=True, dtype='>f4')
                    print("Done !", file=outstream)

            print("I could not flatfield %i images in %s:" % (len(couldnotflatfield), sdir), file=outstream)
            logline.append("I could not flatfield %i images in %s: \n" % (len(couldnotflatfield), sdir))
            for img in couldnotflatfield:
                print(img, file=outstream)
                logline.append(img + '\n')
                failed_images.append(date + ' ' + img)

        logtxt = "".join(logline)
        log_night = open(os.path.join(datedir, "Euler_ECAM_flatfielding.log"), "w")
        log_night.write(logtxt)
        log_night.close()

    print('Done with masterflat correction.', file=outstream)
    print("#" * 10, file=outstream)

    return failed_images

if __name__ == '__main__':
    # myrawdirrestruct = "./test_data_ECAM/"
    # myrawdirrestruct = "/Users/martin/Desktop/lc_run/data_raw/ECAM_test/"
    myrawdirrestruct = "/obs/lenses_EPFL/RAW/ECAM_1/"
    # productsciencedir = "/Users/martin/Desktop/lc_run/data_raw/ECAM_test/reduced/science"
    productsciencedir = "/obs/lenses_EPFL/PRERED/ECAM/reduc"
    # productdir = "/Users/martin/Desktop/lc_run/data_raw/ECAM_test/reduced/"
    productdir = "/obs/lenses_EPFL/PRERED/ECAM/"

    datemin = "2021-12-24"
    datemax = "2021-12-26"
    starttime = Time(datemin, format='iso', scale='utc').mjd
    endtime = Time(datemax, format='iso', scale='utc').mjd

    days = np.arange(starttime, endtime + 1, 1)
    dates = [Time(d, format='mjd', scale='utc').iso[:10] for d in days]

    failure = main(dates, myrawdirrestruct, productsciencedir, productdir, redo=True, log_stream=None)
