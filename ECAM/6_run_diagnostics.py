'''
This script contains the diagnostics for some reference objects.
For the moment we will follow the evolution of the flatfields and make pngs, and the average ellipticity over the night.

One can imagine that we could also do the photometry of some standard stars to follow the evolution of the CCD, ect...

Please add here the diagnostics functions.
'''

from __future__ import print_function
import os
import sys
import glob
from astropy.time import Time
from astropy.io.fits import getheader
from astropy.coordinates import angle_utilities
from astropy.coordinates.angles import Angle
import numpy as np
import pandas as pd
pd.plotting.register_matplotlib_converters() #to remove a warning because of a future conflict between pandas and matplotlib
import math
from ECAMUtils import f2n
from ECAMUtils import diagnostics_util
import ephem


def make_MF_png(date, masterflat_dir, png_dir, redo=False):
    '''Create nice pngs of the masterflats and return '''
    masterflatpath = os.path.join(masterflat_dir, "MF_%s.fits" % date)
    if not os.path.isfile(masterflatpath):
        print("Masterflat does not exist.")
        return "No masterflats for %s"%date

    h = getheader(masterflatpath)
    destpath = os.path.join(png_dir, "MF_%s.png" % date)

    if os.path.isfile(destpath) and redo is False:
        print("Masterflat png already exists.")
        return "%s : Masterflat png already exists."%date

    f2nimage = f2n.fromfits(masterflatpath, hdu=0, verbose=True)
    f2nimage.setzscale(0.9, 1.1)
    f2nimage.rebin(8)
    f2nimage.makepilimage(scale="lin", negative=False)
    f2nimage.writetitle("MF " + date)
    f2nimage.writeinfo(['Number of combined flats : %i' % h['NCOMBINE']])
    f2nimage.tonet(destpath)
    return "Png done for %s"%masterflatpath


def make_flats_png(flat_dir, png_dir, redo=False):
    '''Create nice pngs of all the flats contained in flat_dir'''
    flats = glob.glob(os.path.join(flat_dir, "*_bs.fits"))
    for f in flats:
        name = os.path.basename(f).split('_bs.fits')[0]
        destpath = os.path.join(png_dir, "flat_%s.png" % name)
        if os.path.isfile(destpath) and redo is False:
            print("Flat png already exists.")
            continue
        f2nimage = f2n.fromfits(f, hdu=0, verbose=True)
        f2nimage.setzscale(0.9, 1.1)
        f2nimage.rebin(8)
        f2nimage.makepilimage(scale="lin", negative=False)
        f2nimage.writetitle(name)
        f2nimage.tonet(destpath)


def diagnostics_plots(df, diagnostics_dir, lensname):
    plot_dir = os.path.join(diagnostics_dir, "plots")
    if not os.path.isdir(plot_dir):
        os.mkdir(plot_dir)

    mhjds = df[:]['mhjd'].values
    n_im = len(mhjds)
    diffaxes = df[:]['A-B'].values
    windspeeds = df[:]["windspeed"].values
    windangles = df[:]["windangle"].values
    total_speed = df[:]["total_speed"].values
    seeings = df[:]["seeing"].values
    ellipticities = df[:]["ellipticity"].values
    airmasses = df[:]['airmass'].values

    diagnostics_util.plot_fields(mhjds, diffaxes, windspeeds, windangles, total_speed, airmasses, seeings,
                                 ellipticities, plot_dir,
                                 lensname, kick_bad_image=True, show=False, average_per_night=False)

    # average per night:
    average_seeing = []
    average_diffaxes = []
    average_ellipticity = []
    average_windspeeds = []
    average_windangle = []
    average_totalspeed = []
    average_airmasses = []
    mhjd_vec = []
    for m in range(int(min(mhjds)), int(max(mhjds)), 1):
        mhjd_vec.append(m)
        average_seeing.append(np.mean(df[df["mhjd"] == m]['seeing']))
        average_diffaxes.append(np.mean(df[df["mhjd"] == m]['A-B']))
        average_ellipticity.append(np.mean(df[df["mhjd"] == m]['ellipticity']))
        average_windspeeds.append(np.mean(df[df["mhjd"] == m]['windspeed']))
        average_windangle.append(np.mean(df[df["mhjd"] == m]['windangle']))
        average_totalspeed.append(np.mean(df[df["mhjd"] == m]["total_speed"]))
        average_airmasses.append(np.mean(df[df["mhjd"] == m]['airmass']))

    diagnostics_util.plot_fields(mhjd_vec, average_diffaxes, average_windspeeds, average_windangle, average_totalspeed,
                                 average_airmasses, average_seeing,
                                 average_ellipticity, plot_dir,
                                 lensname, kick_bad_image=True, show=False, average_per_night=True)


def run_diagnostics(df, productdir, ref, diagnostics_dir, xephemlens, redo=False, runsex = True, logstream=sys.stdout, pixsize=0.2149,
                    saturlevel=65000, telescope='Euler'):
    images = sorted(glob.glob(os.path.join(productdir, ref, "*.fits")))
    cat_path = os.path.join(diagnostics_dir, "sex_cat_%s" % ref)
    if not os.path.isdir(cat_path):
        os.mkdir(cat_path)

    basenames = [os.path.basename(im) for im in images]
    for i, im in enumerate(images):
        if df.empty:
            entry = []
        else:
            entry = df[df['image'] == basenames[i]]

        if len(entry) == 0:
            print('New image %s' % basenames[i], file=logstream)
            index = -1
        elif len(entry) == 1 and redo is True:
            print('Image %s already exists in the database, I will redo it.' % basenames[i], file=logstream)
            index = df[df['image'] == basenames[i]].index[0]
        elif len(entry) == 1 and redo is False:
            # print('Image %s already exists in the database, I will not redo it.' % basenames[i], file=logstream)
            index = df[df['image'] == basenames[i]].index[0]
            continue
        else:
            raise RuntimeError('Database contains many time the same image !')

        # Core of the functions, we run sextractor and get all the necessary fields :

        catfilename = os.path.join(cat_path, basenames[i].split('.fits')[0] + ".cat")
        dic = diagnostics_util.run_sextractor(im, catfilename, pixsize=pixsize, saturlevel=saturlevel,
                                              checkplots=False, runsex = runsex)

        header = getheader(im)
        dic["derot"] = header["HIERARCH OGE ADA ROTMEASURED"]
        dic["azi"] = header["HIERARCH OGE TEL TARG AZI START"]
        dic["elev"] = header["HIERARCH OGE TEL TARG ELE START"]
        dic["windspeed"] = header["HIERARCH OGE AMBI WINDSP"]
        dic["winddir"] = header["HIERARCH OGE AMBI WINDDIR"]
        dic['image'] = basenames[i]
        dic['date'] = header["PR_NIGHT"]
        dic['mhjd'] = Time(header["PR_NIGHT"], format='iso', scale='utc').mjd
        dic['windangle'] = angle_utilities.angular_separation(Angle(dic["azi"], unit='degree'), 0,
                                                              Angle(dic["winddir"], unit='degree'), 0).value * 360. / (
                                   2 * math.pi)
        dic['elev_speed'] = math.cos(29 * math.pi / 180.) * math.sin(dic["azi"] / 180. * math.pi)
        dic['azi_speed'] = (math.sin(-29 * math.pi / 180.) - (
                math.sin(-47 * math.pi / 180.) * math.cos(dic["azi"] / 180. * math.pi))) / math.sin(
            dic["azi"] / 180. * math.pi) ** 2
        dic['total_speed'] = np.sqrt(dic['azi_speed'] ** 2 + dic['elev_speed'] ** 2)
        dic['A-B'] = (dic['A_axis'] - dic['B_axis']) * pixsize

        if telescope == 'Euler':
            obs = ephem.Observer()
            obs.long = "-70:43:48.00"  # euler longitude
            obs.lat = "-29:15:24.00"  # Euler latitude
            obs.elevation = 2347.0  # Euler altitude
            obs.epoch = ephem.J2000

            # DJD = JD - 2415020 is the Dublin Julian Date used by pyephem.
            djd = float(Time(header['DATE-OBS'], format='isot', scale='utc').jd - 2415020.0)  # jd is a string !
            obs.date = djd

            lens = ephem.readdb(xephemlens)
            lens.compute(obs)
            dic['airmass'] = diagnostics_util.airmass(float(lens.alt))

            # Moon
            moon = ephem.Moon()
            moon.compute(obs)

            moonsep = ephem.separation(moon, lens)
            dic['moondist'] = math.degrees(float(moonsep))
            dic['moonpercent'] = float(moon.phase)

            # Sun
            sun = ephem.Sun()
            sun.compute(obs)
            sunset = obs.previous_setting(sun)
            timefromsunset = (djd - sunset) *24.0 #in hour
            dic['sundist'] = math.degrees(float(ephem.separation(sun, lens)))
            dic['timefromsunset'] = timefromsunset
        

        serie = pd.Series(dic)
        if index == -1 : 
            df=pd.concat([df, serie.to_frame().T])
        else : 
            df[index]=serie.to_frame()

    return df


def main(dates, myrawdirrestruct, productsciencedir, masterflat_dir_list, diagnostics_dir, png_dir_list, reference_ellipticity_object,
         exphemlens_list,
         redo=True, log_stream=None, pixsize=0.2149, saturlevel=65000):
    if log_stream is None:
        outstream = sys.stdout
    else:
        outstream = log_stream

    for png_dir in png_dir_list:
        if not os.path.isdir(png_dir):
            os.mkdir(png_dir)

    logline = ['Starting diagnostics... \n']

    # make the pngs of the masterflats :
    for ind, date in enumerate(dates):
        print("=" * 15, date, "=" * 15, file=outstream)
        datedir = os.path.join(myrawdirrestruct,date)
        logline = ['Starting diagnostics... \n']
        for masterflat_dir, png_dir in zip(masterflat_dir_list, png_dir_list):
            exit_message = make_MF_png(date, masterflat_dir, png_dir, redo=redo)
            logline.append(exit_message)
        logtxt = "".join(logline)
        log_night = open(os.path.join(datedir, "Euler_ECAM_diagnostics.log"), "w")
        log_night.write(logtxt)
        log_night.close()

    # here we list the field we are interrested in :
    database_field = {'image': [],
                      'date': [],
                      'ellipticity': [],
                      'A_axis': [], #size of the PSF along the long axis in pixel
                      'B_axis': [], #size of the PSF along the short axis in pixel
                      'A-B': [], #diferrence between short and long axis in arcsec
                      'Theta': [], #position angle of the PSF
                      'seeing': [], #seeing in arcsec
                      'seeingpixels': [], #seeing in pixel
                      "derot": [], #position of the derotator
                      "azi": [], #azimuthal angle
                      "elev": [], #elevation angle
                      "windspeed": [], #wind speed in m.s-1
                      "winddir": [], #angular direction of the wind
                      "windangle": [],#angle between the wind and the azimuthal angle of the telescope
                      "elev_speed": [], #motion around the elevation axis in rad.s-1
                      "azi_speed": [], #motion around the azimuthal axis in rad.s-1
                      "total_speed": [], #total motion
                      "goodstars": [], #number of stars used by sextractor
                      "mhjd": [], #modified Julian Date of the observation
                      'airmass': [], #airmass of the observation
                      'moondist': [], #separation to the moon (deg)
                      'moonpercent': [], #moon illumination
                      'sundist': [], #separation to the sun in deg
                      'timefromsunset':[], #time from the last sunset in hour
                      }

    for ref, xephem in zip(reference_ellipticity_object, exphemlens_list):
        panda_database = os.path.join(diagnostics_dir, ref + "_database.csv")
        if not os.path.isfile(panda_database):
            df = pd.DataFrame(database_field)
            print('Your database does not exists yet, I will create it.', file=outstream)
            df.to_csv(panda_database, index=True, header=True)
        else:
            print("Reading existing data base : %s" % panda_database, file=outstream)
            df = pd.read_csv(panda_database, index_col=0)

        #to add a new field in the database, suppress the previous csv file, turn redo to True and runsex to False (if you are reusing previous Sextractor catalog)
        df = run_diagnostics(df, productsciencedir, ref, diagnostics_dir, xephem, redo=False, runsex=True, logstream=log_stream,
                             pixsize=pixsize,
                             saturlevel=saturlevel)
        diagnostics_plots(df, diagnostics_dir, ref)
        print(df, file=outstream)
        df.to_csv(panda_database)

    print("Done with the diagnostics", file=outstream)
    print("#" * 10, file=outstream)


if __name__ == '__main__':
    # myrawdirrestruct = "./test_data_ECAM/"
    myrawdirrestruct = "/obs/lenses_EPFL/RAW/ECAM_1/"
    # productsciencedir = "./test_data_ECAM/reduced/reduc"
    productsciencedir = "/obs/lenses_EPFL/PRERED/ECAM/reduc"
    # masterflat_dir = "./test_data_ECAM/reduced/MF_RGUL4"
    # diagnostics_dir = "./test_data_ECAM/reduced/diagnostics"
    diagnostics_dir = "/obs/lenses_EPFL/PRERED/ECAM/diagnostics"
    diagno_masterflat_dir_list = ["/obs/lenses_EPFL/PRERED/ECAM/MF_RGUL4","/obs/lenses_EPFL/PRERED/ECAM/MF_VGUL4"]  #masterflat to create the pngs
    png_dir_list = [os.path.join(diagnostics_dir, "png_flat_RG_UL") , os.path.join(diagnostics_dir, "png_flat_VG_UL")]

    datemin = "2021-12-24"
    datemax = "2021-12-26"
    starttime = Time(datemin, format='iso', scale='utc').mjd
    endtime = Time(datemax, format='iso', scale='utc').mjd

    reference_ellipticity_object = [
                                    "RXJ1131-123_RG_UL_RGUL4", 
                                    "DES2038-4008_RG_UL_RGUL4",
                                    "PSJ0259-2338_RG_UL_RGUL4",
                                    "J0457-7820_RG_UL_RGUL4",
                                    "J0818-2613_RG_UL_RGUL4",
                                    "J0941+0518_RG_UL_RGUL4",
                                    "DES0501-4118_RG_UL_RGUL4", 
                                    ]
    exphemlens_list = [
                       "RXJ1131-123,f|Q,11:31:55.40,-12:31:55.00,19.0,2000",
                      "DES2038-4008,f|Q,20:38:02.60,-40:08:13.2,19.0,2000",
                       "PSJ0259-2338,f|Q,02:59:33.51,-23:38:01.97,19.0,2000",
                       "J0457-7820,f|Q,04:57:23.60,-78:20:48.0,19.0,2000",
                       "J0818-2613,f|Q,08:18:00.00,-26:13:00.0,19.0,2000",
                       "J0941+0518,f|Q,09:41:00.00,05:18:00.0,19.0,2000",
                       "DES0501-4118,f|Q,05:01:00.00,-41:18:00.0,19.0,2000",
                       ]

    Euler_saturlevel = 65000
    Euler_pixsize = 0.2149  # in arcsecond

    days = np.arange(starttime, endtime + 1, 1)
    dates = [Time(d, format='mjd', scale='utc').iso[:10] for d in days]

    main(dates,myrawdirrestruct, productsciencedir, diagno_masterflat_dir_list, diagnostics_dir, png_dir_list, reference_ellipticity_object,
         exphemlens_list,
         redo=True, log_stream=None, pixsize=Euler_pixsize, saturlevel=Euler_saturlevel)
