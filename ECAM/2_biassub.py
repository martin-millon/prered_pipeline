"""
We use pre and overscan to bias subtract images, and then we cut the images (they will all have the same size)
We do update the corresponding dbs, but it is ok to launch me again over the same images.
"""
from __future__ import print_function
import os
import sys
import glob
from astropy.io.fits import getheader
from astropy.time import Time
import numpy as np
from ECAMUtils import ecambias
from ECAMUtils import auxfcts


def main(dates, myrawdirrestruct, redo=False, log_stream=None):
    if log_stream is None:
        outstream = sys.stdout
    else:
        outstream = log_stream

    print("Bias subtraction", file=outstream)
    print("My redofromscratch is set to %s" % redo, file=outstream)

    for ind, date in enumerate(dates):
        print("=" * 15, date, "=" * 15, file=outstream)
        datedir = os.path.join(myrawdirrestruct, date)
        dirs = [os.path.join(datedir, o) for o in os.listdir(datedir) if
                os.path.isdir(os.path.join(datedir, o)) and not 'bias' in os.path.join(datedir, o)]
        nobiassub = []
        logline = []

        for directory in dirs:
            images = sorted(glob.glob(os.path.join(directory, '*.fits')))
            basename_images = [os.path.basename(ima) for ima in images]

            for i, image in enumerate(images):
                basename = os.path.basename(image).split('.fits')[0]  # get only the name of the file without extension
                if "_bs" in basename:
                    continue
                if basename + "_bs.fits" in basename_images and redo is False:
                    print("bias-subtracted image exists already for %s !" % image, file=outstream)
                    logline.append("bias-subtracted image exists already for %s ! \n" % image)
                    continue
                elif basename + "_bs.fits" in basename_images and redo is True:
                    os.remove(os.path.join(directory, basename + "_bs.fits"))
                    print("Removed the previous bias subtraction %s !" % (basename + "_bs.fits"), file=outstream)
                    logline.append("Removed the previous bias subtraction %s ! \n" % (basename + "_bs.fits"))

                header = getheader(image)

                try:
                    ampname = str(header["HIERARCH OGE DET OUT NAME"])
                except KeyError:
                    ampname = str(header["HIERARCH OGE DET OUT RNAME"])
                print("=== %4i/%4i : format %s : %s ===" % (i + 1, len(images), ampname, basename), file=outstream)
        
                try : 
                    (a, h) = auxfcts.fromfits(image, hdu=0, verbose=True)
                except : 
                    print(image)
                    exit()

                # And now, depending on the format, we process this array...
                h['PR_FORMA'] = (ampname, "pypr image format for biassub")

                if ampname in ["UL", "LL", "UR", "LR"]:

                    plotfilepath = os.path.join(directory, basename + "_poplot.png")

                    ecambias.poplot_onequad(basename, a, h, plotfilepath)

                    # In pinciple, we want to use biassub_onequad_poxsimpleramp.
                    # But for flats, there is this CTI issue, hence we do a special processing for them.
                    if "flat" in directory and ampname == "UL":

                        print("FLAT UL : Special treatment !", file=outstream)
                        (a, h) = ecambias.biassub_onequad_poycst(a, h)

                    else:

                        (a, h) = ecambias.biassub_onequad_poxsimpleramp(a, h)

                elif ampname == "ALL":

                    (a, h) = ecambias.biassub_ALL(a, h)

                else:
                    print("ERROR, format unknown: ", ampname, file=outstream)
                    print("Skipping for now...", file=outstream)
                    nobiassub.append(basename)
                    continue

                # We get back to 32 bit arrays :
                a = a.astype(np.float32)

                # We write the fits file
                outfilepath = os.path.join(directory, basename + "_bs.fits")
                auxfcts.tofits(outfilepath, a, hdr=h, verbose=True)

        # Printing faulty formats:
        if not len(nobiassub) == 0:
            print("I failed to subtract the bias on the following images : ", file=outstream)
            for e in nobiassub:
                print("=" * 5, e, "=" * 5, file=outstream)
                logline.append("=" * 5 + e + "=" * 5 + "\n")
            print("You might want to do something with these images...", file=outstream)
        else:
            print("I successfully subtracted the bias of all images.", file=outstream)
            logline.append("I successfully subtracted the bias of all images. \n")

        logtxt = "".join(logline)
        log_night = open(os.path.join(datedir, "Euler_ECAM_biassub.log"), "w")
        log_night.write(logtxt)
        log_night.close()

    print("Done with bias subtraction", file=outstream)
    print("#" * 10, file=outstream)


if __name__ == '__main__':
    myrawdirrestruct = "/obs/lenses_EPFL/RAW/ECAM_1/"
    # myrawdirrestruct = "./test_data_ECAM/"
    datemin = "2021-05-25"
    datemax = "2021-05-25"
    starttime = Time(datemin, format='iso', scale='utc').mjd
    endtime = Time(datemax, format='iso', scale='utc').mjd

    filtername = ["BG"]
    amp_mode = ["UL"]
    flat_mode = [fl + "_" + amp_mode[i] for i, fl in enumerate(filtername)]

    days = np.arange(starttime, endtime + 1, 1)
    dates = [Time(d, format='mjd', scale='utc').iso[:10] for d in days]

    main(dates, myrawdirrestruct, redo=False, log_stream=None)
