import os
import alipy
import glob
import astropy.io.fits as fits
import numpy as np
import astroalign as aa

path = '/Users/Martin/Desktop/lc_run/data_raw/ECAM_test/Saturn/R'

files_object = sorted(glob.glob(os.path.join(path,'ECAM*.fits')))
files_object = [f for f in files_object if not '_rates' in f]  # rmoved the already aligned images
print(files_object)

for f in files_object:
    datas = fits.open(f)[0].data
    header = fits.open(f)[0].header
    exptime = header['EXPTIME']

    datas = datas/exptime

    hdu = fits.PrimaryHDU(datas, header=header)
    hdul = fits.HDUList([hdu])
    hdul.writeto(f.split('.fits')[0]+ '_rates.fits', overwrite=True)


files_object = sorted(glob.glob(os.path.join(path,'ECAM*_rates.fits')))
# identifications = alipy.ident.run(files_object[0], files_object, visu=False, n=5)
# outputshape = alipy.align.shape(files_object[0])
# print(files_object)
# for id in identifications:
#     if id.ok == True:
#         alipy.align.affineremap(id.ukn.filepath, id.trans, shape=outputshape, makepng=True,
#                                 outdir=os.path.join(path, "ALIGNED"))

source = files_object[0]
for f in files_object:
    basename = os.path.basename(f).split('.')[0]
    datas = fits.open(f)[0].data
    source_datas = fits.open(source)[0].data
    if source != f :
        registered_image, footprint = aa.register(source_datas, datas)
    else :
        registered_image = source_datas


    hdu = fits.PrimaryHDU(datas)
    hdul = fits.HDUList([hdu])
    hdul.writeto(os.path.join(path,'ALIGNED', basename + '_aligned.fits'), overwrite=True)



files_object_tostack = sorted(glob.glob(os.path.join(path,'ALIGNED','*.fits')))
stack = []

for f in files_object_tostack:
    datas = fits.open(f)[0].data
    header = fits.open(f)[0].header
    stack.append(datas)

stack = np.median(stack, axis=0)

header['STACK'] = 'True'
hdu = fits.PrimaryHDU(stack, header=header)
hdul = fits.HDUList([hdu])
hdul.writeto(os.path.join(path, 'stack.fits'), overwrite=True)




#skysubtraction
pixsize =0.2149
saturlevel = 65000
skyimagepath = os.path.join(path, 'sky.fits')
cmd = "%s %s -c default_sky_template_smooth.sex -PIXEL_SCALE %.3f -SATUR_LEVEL %.3f -CHECKIMAGE_NAME %s" %('sex', os.path.join(path, 'stack.fits'), pixsize, saturlevel, skyimagepath)
os.system(cmd)


sky = fits.open(skyimagepath)[0].data
skysubimagea = stack - sky

header['SKYSUB'] = 'True'
hdu = fits.PrimaryHDU(skysubimagea, header=header)
hdul = fits.HDUList([hdu])
hdul.writeto(os.path.join(path, 'stack_skysub.fits'), overwrite=True)