import numpy as np
import os
import glob
from astropy.io.fits import getheader
import pandas as pd

productsciencedir = "/obs/lenses_EPFL/PRERED/ECAM/reduc"
diagnostics_dir = "/obs/lenses_EPFL/PRERED/ECAM/diagnostics/observer_stats"
panda_database = os.path.join(diagnostics_dir, 'database.csv')

dirs = os.listdir(productsciencedir)
dirs = [o for o in dirs if os.path.isdir(o)]
observers = []
email = []
object = []

for dir in dirs :
    print(dir)
    files = glob.glob(os.path.join(dir, "*.fits"))
    for fitsfile in files :
        header = getheader(fitsfile)
        observers.append(header['OBSERVER'])
        object.append(header['OBJECT'])
        email.append(header['EMAIL'])

    break

dic = {'observer' : observers,
       'email' : email,
       'object' : object
       }

df = pd.DataFrame(dic)
df.to_csv(panda_database)











