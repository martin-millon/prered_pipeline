'''
Shutter image creation. Based on Zisell 2000 https://www.aavso.org/sites/default/files/webpublications/ejaavso/v28n2/149.pdf

You need long exposure ( the flats does a good job)
'''

__author__ = 'Martin Millon' # as of 18.12.2021

import astropy.io.fits as fits
import numpy as np
import glob, os
import scipy.ndimage

outputpath = "/Users/martin/Desktop/observation_Euler/run2021/"
shutterpath = "/Users/martin/Desktop/observation_Euler/run2021/shutterimage_2021-12-16/"
shutterpathout="/Users/martin/Desktop/observation_Euler/run2021/shutterimage_2021-12-16_corrected/"
biaspath = "/Users/martin/Desktop/observation_Euler/run2021/bias/"
flatpath = "/Users/martin/Desktop/observation_Euler/run2021/flats"

if not os.path.exists(shutterpathout):
    os.mkdir(shutterpathout)

shutterimages = glob.glob(os.path.join(shutterpath, '*.fits'))
biasimages = glob.glob(os.path.join(biaspath, '*.fits'))
flatsimages = glob.glob(os.path.join(flatpath, '*.fits'))

biases = []
for b in biasimages:
    data = fits.open(b)[0].data
    header = fits.open(b)[0].header
    biases.append(data)

masterbias = np.median(biases, axis=0)
masterbias = np.asarray(masterbias).astype(np.float32)

flats = []
for f in flatsimages:
    data = fits.open(f)[0].data
    suba = data[1000:3000, 1000:3000]
    subamed = np.median(suba.ravel())
    data = data / subamed
    flats.append(data)

masterflat = np.median(flats, axis=0)
masterflat = np.asarray(masterflat).astype(np.float32)

hdu = fits.PrimaryHDU(masterflat)
hdul = fits.HDUList([hdu])
hdul.writeto(os.path.join(outputpath, "masterflat.fits"), overwrite=True)

shutters =[]
for s in shutterimages:
    print(s)
    basename = os.path.basename(s)
    datas = fits.open(s)[0].data
    exptimes = fits.open(s)[0].header['EXPTIME']
    headers = fits.open(s)[0].header
    dataf = fits.open(flatsimages[0])[0].data
    exptimef = fits.open(flatsimages[0])[0].header['EXPTIME']

    datas = np.asarray(datas).astype(np.float32)
    dataf = np.asarray(dataf).astype(np.float32)

    datas = (datas - masterbias)/masterflat
    dataf = (dataf - masterbias)/masterflat

    center_region = np.median(datas[2050:2150, 2050:2150])
    # Correct for the different illumination of the long and short exposure. This is absolutely necessary to respect the first equation of Zisell 2000.
    # so we normalise the incoming photon flux for the short and long exposure, Assuming that the central region is not affected by the shutter.

    correction_rate = (np.median(dataf)/exptimef)*(exptimes/center_region)

    datas = correction_rate*datas

    shutter_map = (datas*exptimef - dataf*exptimes) / (datas-dataf) #zisell 2000 https://www.aavso.org/sites/default/files/webpublications/ejaavso/v28n2/149.pdf
    shutters.append(shutter_map)

    headers['NORMALIZED'] = 'True'
    hdu = fits.PrimaryHDU(shutter_map, header=headers)
    hdul = fits.HDUList([hdu])
    hdul.writeto(os.path.join(shutterpathout, "shutter_corrected_%s"%basename), overwrite=True)


mastershutter = np.median(shutters, axis=0)
mastershutter= mastershutter[50:-50,50:-50] #to fit the ecam baissub images
hdu = fits.PrimaryHDU(mastershutter)
hdul = fits.HDUList([hdu])
hdul.writeto(os.path.join(outputpath, "mastershutter.fits"), overwrite=True)

mastershutter_filtered = scipy.ndimage.gaussian_filter(mastershutter, sigma=2)
hdu = fits.PrimaryHDU(mastershutter_filtered)
hdul = fits.HDUList([hdu])
hdul.writeto(os.path.join(outputpath, "mastershutter_filtered.fits"), overwrite=True)
#1. stack a serie of 1 second exposure

#2. filter the