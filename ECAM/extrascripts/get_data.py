import os
import pyfits
import glob

origdir = "/obs/insdata/ECAM/raw/2019-05-01"
ecamfiles = sorted(glob(os.path.join(origdir, "ECAM*.fits")))

filter = 'OO'

# print ecamfiles

destdir = "./data/"
if not os.path.isdir(destdir):
    print("Making directory %s" % (destdir))
    os.mkdir(destdir)

for ecamfile in ecamfiles:

    # try:
    basename = os.path.basename(ecamfile)
    print
    basename

    destpath = os.path.join(destdir, basename)

    if "FOCUS" in basename:
        # We copy these focus exposures right away, and skip the rest of the loop.
        # Not sure if we need them, but let's keep them anyway.
        print("\tFocus")
        # mycopy(ecamfile, destpath)
        continue

    header = pyfits.getheader(ecamfile)
    obsfilter = str(header["FILTER"])

    if obsfilter == filter:
        os.system('cp %s %s' % (ecamfile, destpath))
