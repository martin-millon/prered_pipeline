import os
import glob
import sys
sys.path.append("../ECAMUtils")
import auxfcts

def main(new_folder,old_folder,output_folder):
    if not os.path.isdir(output_folder):
        os.mkdir(output_folder)

    old_images = sorted(glob.glob(os.path.join(old_folder,'*.fits')))

    for im in old_images :
        basename = os.path.basename(im)
        new_im = os.path.join(new_folder,basename)

        a,h = auxfcts.fromfits(im, hdu=0, verbose=False)
        a_new,h_new = auxfcts.fromfits(new_im, hdu=0, verbose=False)

        diff = a - a_new

        res_name = os.path.join(output_folder,basename)
        auxfcts.tofits(res_name, diff, h_new)



if __name__ == '__main__':
    new_folder = "../test_data_ECAM/new/"
    old_folder =  "../test_data_ECAM/test/"
    output_folder = "../test_data_ECAM/new/residuals"

    main(new_folder,old_folder,output_folder)