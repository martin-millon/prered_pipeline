import sys
sys.path.append('..')
from ECAMUtils import diagnostics_util as du
import glob, os
import numpy as np
import astropy.io.fits as fits
import matplotlib.pyplot as plt


img_folder  = "/Users/martin/Desktop/lc_run/data_raw/ECAM_test/J0457"
imgs = sorted(glob.glob(os.path.join(img_folder, "*.fits")))

print('I found %i images'%len(imgs))
dics = []
dates = []
for im in imgs :

    catfilename = os.path.join(img_folder, os.path.basename(im).split('.fits')[0] + ".cat")
    header = fits.open(im)[0].header
    dics.append(du.run_sextractor(im, catfilename, pixsize=0.2149, saturlevel=65000, sextractor_command='sex', checkplots=False, runsex = True))
    dates.append(header['MJD-OBS'])

seeings = []
ellipticity = []
for i,img in enumerate(imgs):
    print("### %s ###"%os.path.basename(img))
    print(dics[i])
    seeings.append(dics[i]['seeing'])
    ellipticity.append(dics[i]['ellipticity'])

print('Mean seeing : %2.2f'%np.mean(seeings))
print('Mean ellipticity: %2.2f'%np.mean(ellipticity))

plt.figure(1)
plt.plot(dates, ellipticity)
plt.show()