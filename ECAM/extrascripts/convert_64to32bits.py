import sys
sys.path.append('..')
from ECAMUtils import auxfcts
import os
import glob

overwrite = True
folder = '/obs/lenses_EPFL/PRERED/ECAM/reduc/GRAL1651-0417_RG_UL_RGUL4'

images = sorted(glob.glob(os.path.join(folder,'*.fits')))

print('I have %i images in %s'%(len(images), folder))

for i, im in enumerate(images):
    (a, h) = auxfcts.fromfits(im, hdu=0, verbose=False)
    basename = os.path.basename(im).split('.fits')[0]

    if h['BITPIX']==-32:
        print('Image %s already in 32 bits. Skipping !'%basename)
        continue

    if overwrite:
        outputname = im
    else :
        outputname = os.path.join(folder, basename + "_32bits.fits")


    auxfcts.tofits(outputname, a, hdr=h, verbose=True, dtype='>f4')
