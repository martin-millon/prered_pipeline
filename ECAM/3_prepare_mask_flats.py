"""
We now prepare the flats. This requires to compensate for the shutter, compute the flat levels and mask the stars
"""
from __future__ import print_function
import os
import sys
import glob
from astropy.io.fits import getheader
from astropy.io.fits import open as fitsopen
from astropy.time import Time
import numpy as np
from ECAMUtils import auxfcts
from photutils import make_source_mask

def main(dates, myrawdirrestruct, shutterimagepath, redo=False, log_stream=None):
    if log_stream is None:
        outstream = sys.stdout
    else:
        outstream = log_stream

    shutterimage = auxfcts.fromfits(shutterimagepath, hdu=0, verbose=True)[0]
    maxshutter = np.max(shutterimage)

    for ind, date in enumerate(dates):
        print("=" * 15, date, "=" * 15, file=outstream)
        datedir = os.path.join(myrawdirrestruct, date)
        flat_dirs = [os.path.join(datedir, o) for o in os.listdir(datedir) if
                     os.path.isdir(os.path.join(datedir, o)) and 'flat' in os.path.join(datedir, o)]
        logline = []

        for flat_dir in flat_dirs:
            print("Shutter compensation for %s" % os.path.basename(flat_dir), file=outstream)
            flats = glob.glob(os.path.join(flat_dir, '*bs.fits'))

            for i, fl in enumerate(flats):
                print("=== %4i / %4i : %s ===" % (i + 1, len(flats), fl), file=outstream)

                flat_basename = os.path.basename(fl).split('.fits')[0]  # get only the image name, without extension
                h = getheader(fl)

                # apply shutter correction :
                if "PR_SHTCO" in h.keys():
                    print("Was already corrected with %s !" % h["PR_SHTCO"], file=outstream)
                    logline.append("%s already shutter corrected \n" % fl)
                    (cora, h) = auxfcts.fromfits(fl, hdu=0, verbose=False)
                else:
                    (a, h) = auxfcts.fromfits(fl, hdu=0, verbose=False)
                    exptime = float(h['EXPTIME'])
                    print("My exptime : %.3f" % (exptime))
                    print("Maximal correction : %.4f %%" % (100.0 * maxshutter / exptime))
                    cora = a * (1.0 + (shutterimage / exptime))

                    h['PR_SHTCO'] = (os.path.basename(shutterimagepath), "pypr shutter correction applied")
                    logline.append("%s corrected with shutter image \n" % fl)

                # now we measure the flat level
                if "medclean1" in h.keys():
                    print("Flat levels already computed", file=outstream)
                    logline.append("%s has flat levels already computed \n" % fl)
                else:
                    clean1 = cora[500:1500, 500:1500]
                    clean2 = cora[2500:3500, 500:1500]
                    clean3 = cora[2500:3500, 2500:3500]
                    clean4 = cora[500:1500, 2500:3500]

                    h["HIERARCH medclean1"] = np.median(clean1.ravel())
                    h["stdclean1"] = np.std(clean1.ravel())
                    h["meanclean1"] = np.mean(clean1.ravel())

                    h["medclean2"] = np.median(clean2.ravel())
                    h["stdclean2"] = np.std(clean2.ravel())
                    h["meanclean2"] = np.mean(clean2.ravel())

                    h["medclean3"] = np.median(clean3.ravel())
                    h["stdclean3"] = np.std(clean3.ravel())
                    h["meanclean3"] = np.mean(clean3.ravel())

                    h["medclean4"] = np.median(clean4.ravel())
                    h["stdclean4"] = np.std(clean4.ravel())
                    h["meanclean4"] = np.mean(clean4.ravel())

                    h["medcleanall"] = np.mean(
                        [np.median(clean1.ravel()), np.median(clean2.ravel()), np.median(clean3.ravel()),
                         np.median(clean4.ravel())])

                    logline.append("%s : adding flat levels to the header \n" % fl)

                # And we overwrite the bias subtracted flat :
                auxfcts.tofits(fl, cora, h)

                # now we make the mask

                # flatpath = os.path.join(bsdir, image["filename"])
                maskfilename = "mask_" + flat_basename + ".pl"
                maskpath = os.path.join(flat_dir, maskfilename)

                if os.path.isfile(maskpath) and redo is False :
                    print("Mask is already existing for %s, I am not redoing it." % flat_basename, file=outstream)
                    logline.append("Mask is already existing for %s, I am not redoing it. \n" % fl)
                    continue

                elif os.path.isfile(maskpath) and redo is True :
                    print("Mask is already existing for %s, I will delete it and redo it. " % flat_basename, file=outstream)
                    logline.append("Mask is already existing for %s, I will delete it and redo it. \n" % fl)
                    os.remove(maskpath)

                mask = make_source_mask(cora, nsigma=10, npixels=20)

                auxfcts.tofits(maskpath, mask)
                logline.append("Mask created for %s \n" % fl)

        logtxt = "".join(logline)
        log_night = open(os.path.join(datedir, "Euler_ECAM_flats_preparation.log"), "w")
        log_night.write(logtxt)
        log_night.close()
    print("Done with mask preparation.", file=outstream)
    print("#" * 10, file=outstream)


if __name__ == '__main__':
    myrawdirrestruct = "/obs/lenses_EPFL/RAW/ECAM_1/"
    # myrawdirrestruct = "/Users/martin/Desktop/lc_run/data_raw/ECAM_test/"
    shutterimagepath = "shutter_20211216_filtered.fits"
    datemin = "2021-05-25"
    datemax = "2021-05-25"
    starttime = Time(datemin, format='iso', scale='utc').mjd
    endtime = Time(datemax, format='iso', scale='utc').mjd

    filtername = ["BG"]
    amp_mode = ["UL"]
    flat_mode = [fl + "_" + amp_mode[i] for i, fl in enumerate(filtername)]

    days = np.arange(starttime, endtime + 1, 1)
    dates = [Time(d, format='mjd', scale='utc').iso[:10] for d in days]

    main(dates, myrawdirrestruct, shutterimagepath, redo=False, log_stream=None)
