"""
We look for new ECAM* lens images and calibs in the official geneva euler database and copy them.
Existing file are overwritten, if redo == True

Be sure to keep "target_list.txt" up to date, so that no files are forgotten.
This script will tell you about any targets that it "leaves behind".


No problem to lauch it again and again over the same directories.
"""
from __future__ import print_function
import sys, os, shutil
from glob import glob
import numpy as np
from astropy.time import Time
from astropy.io.fits import getheader


def mycopy(fullsrcpath, fulldstpath, redo=False):
    if os.path.exists(fulldstpath) and redo is False:
        os.system('cp -n %s %s' % (fullsrcpath, fulldstpath))
    else:
        os.system('cp %s %s' % (fullsrcpath, fulldstpath))


def main(dates, rawdir, myrawdir, flat_mode, amp_mode, redo=False, log_stream=None):
    # if log stream == None : we print in the terminal.
    if log_stream is None:
        outstream = sys.stdout
    else:
        outstream = log_stream

    f = open("targetslist.txt", "r")
    flines = f.readlines()
    f.close()
    mytargets = [line.rstrip() for line in flines if (line[0] != "#" and len(line) > 2)]
    mytargets = "\n".join(mytargets)

    f = open("skiplist.txt", "r")
    flines = f.readlines()
    f.close()
    skiplist = [line.rstrip() for line in flines if (line[0] != "#" and len(line) > 2)]
    skiplist = "\n".join(skiplist)

    othertargets = []
    no_obs_dates = []

    for ind, date in enumerate(dates):
        print("=" * 15, date, "=" * 15, file=outstream)

        origdir = os.path.join(rawdir, date)
        ecamfiles = sorted(glob(os.path.join(origdir, "ECAM*.fits")))

        # print ecamfiles
        destdir = os.path.join(myrawdir, date)
        if not os.path.isdir(destdir):
            print("Making directory %s" % (date), file=outstream)
            os.mkdir(destdir)

        if len(ecamfiles) == 0:
            no_obs_dates.append(date)
            print("No observation on %s"%date, file=outstream)
            continue

        for ecamfile in ecamfiles:

            # try:
            basename = os.path.basename(ecamfile)
            print(basename, file=outstream)

            if basename in skiplist:
                print("\tIs on donotcopylist !", file=outstream)
                continue

            #check that the file is not in any subfolder of the destpath or in the destpath :
            if redo is False :
                already_existing = sorted(glob(os.path.join(destdir,'*/*.fits')))
                other_file =glob(os.path.join(destdir,'*.fits'))
                for o in other_file : 
                    already_existing.append(o)
                already_existing = [os.path.basename(ex) for ex in already_existing]
                if os.path.basename(ecamfile) in already_existing :
                    print('I already have %s in subfolder and your redo flag is False'%ecamfile, file= outstream)
                    continue

            destpath = os.path.join(destdir, basename)

            if "FOCUS" in basename:
                # We skip the focus exposure
                print("\tFocus", file=outstream)
                continue

            header = getheader(ecamfile)

            target = str(header["OBJECT"])
            obstype = str(header["HIERARCH OGE OBS TYPE"])

            try:
                ampname = str(header["HIERARCH OGE DET OUT NAME"])
            except:
                ampname = str(header["HIERARCH OGE DET OUT RNAME"])

            obsfilter = str(header["FILTER"])
            obsmode = obsfilter + '_' + ampname

            # The calibrations :
            if (obstype == "BIAS" and ampname in amp_mode) or (obstype == "DARK" and ampname in amp_mode):
                # As for the focus exposures, we copy all of them.
                print("\tCalibration : %s" % (obstype), file=outstream)
                #mycopy(ecamfile, destpath, redo=redo) #We are not copying bias anymore as we are using overscan.
                continue

            if (obstype == "FLAT") and (obsmode in flat_mode):
                # As for the focus exposures, we copy all of them.
                print("\tCalibration : %s" % (obstype), file=outstream)
                mycopy(ecamfile, destpath, redo=redo)
                continue

            # And now the actual science exposures :
            if target in mytargets:
                print("\tMine ! (%s)" % (target), file=outstream)
                mycopy(ecamfile, destpath, redo=redo)
            else:
                othertargets.append(target)
                print("\tUnknown target ! (%s)" % (target), file=outstream)

    histlines = ["%30s : %5i" % (target, othertargets.count(target)) for target in set(othertargets)]

    print("Targets I did not copy so far (add them in copytargetslist.txt if you want) :", file=outstream)
    print("\n".join(histlines), file=outstream)
    if len(no_obs_dates) > 0 :
        print("I have no ECAM files on : ", file=outstream)
        for n in no_obs_dates :
            print(n, file=outstream)

    return no_obs_dates


if __name__ == '__main__':
    raw_dir = "/insdata/ECAM/raw"
    myrawdir = "/obs/lenses_EPFL/RAW/ECAM_1/"

    datemin = "2021-12-17"
    datemax = "2021-12-17"
    starttime = Time(datemin, format='iso', scale='utc').mjd
    endtime = Time(datemax, format='iso', scale='utc').mjd

    filtername = ["RG", "VG", "BG"]
    amp_mode = ["UL", "UL","UL"]
    flat_mode = [fl + "_" + amp_mode[i] for i, fl in enumerate(filtername)]

    days = np.arange(starttime, endtime + 1, 1)
    dates = [Time(d, format='mjd', scale='utc').iso[:10] for d in days]

    no_obs = main(dates, raw_dir, myrawdir, flat_mode, amp_mode, redo=False, log_stream=None)

#TODO : don't copy if the file exists in subfolder
