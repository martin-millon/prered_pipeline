#!/bin/sh -l

module add astro
module add anaconda/3-5.2.0

source activate iraf3

export PATH=/home/astro/millon/.local/bin:$PATH

export PYTHONPATH=/home/astro/millon/Desktop/PyCS:$PYTHONPATH
export PYTHONPATH=${PYTHONPATH}:/home/astro/millon/Desktop/Mod_challenge/lenstronomy_extension/
export PYTHONPATH=${PYTHONPATH}:/home/astro/millon/Desktop/Mod_challenge/lenstronomy/
export PYTHONPATH=${PYTHONPATH}:/home/astro/millon/Desktop/tdlmcpipeline/
export ftp_proxy=http://proxy.unige.ch:3128
export http_proxy=http://proxy.unige.ch:3128
export https_proxy=http://proxy.unige.ch:3128

export PYTHONPATH=${PYTHONPATH}:/home/astro/millon/.local/lib/python3.7/site-packages/

echo $PYTHONPATH

python3 /home/astro/millon/Desktop/PRERED/ECAM/crontab_test.py
