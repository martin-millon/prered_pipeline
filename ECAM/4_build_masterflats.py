"""
We now build the master flats. This requires to group the flat from the previous nights. You can control how namy flats are stacked to create the master flats by giving 4 parameters :
    radius_future : number of days to look for flat after the present date
    radius_past : number of days to look for flat before the present date
    n_min_flats : minimal number of flats to stack, if this not matched I will double the radius of search until I match this criterion
    n_max_flats : maximal number of flats. I will take the flats the closest to the present date.
"""
from __future__ import print_function
import os
import sys
import glob
from astropy.time import Time
import numpy as np
from ECAMUtils import auxfcts
from astropy.io.fits import getheader, getdata
from stsci.image.numcombine import num_combine


def search_flat(date, rawdir, filtername, amp_mode, radius_future,
                radius_past):
    '''Return all flats in the given radius for the required amp mode and filter.
     I will add the flats from the closest to the reference date to the furthest.'''

    night = {}
    flats = []
    flats_name = []
    masks = []
    masks_name = []

    r_max = max(radius_past, radius_future)
    starttime = Time(date, format='iso', scale='utc').mjd
    for i in range(2 * r_max):
        index = int(i / 2)
        if i == 0:
            continue
        if i % 2 == 0:
            # we look backward
            d = Time(starttime - index, format='mjd', scale='utc').iso[:10]
            if index > radius_past:
                continue
        else:
            # we look forward
            d = Time(starttime + index, format='mjd', scale='utc').iso[:10]
            if index > radius_future:
                continue

        flat_dir = os.path.join(rawdir, d, "flat_%s_%s" % (filtername, amp_mode))
        flat_night = glob.glob(os.path.join(flat_dir, "*bs.fits"))
        for f in flat_night:
            flat_base = os.path.basename(f).split('.fits')[0]
            m = os.path.join(flat_dir, 'mask_' + flat_base + '.pl')
            if not os.path.isfile(m):
                print('mask %s is not there ! Skipping it...'%m)
                continue
            mean_flatlevel = getheader(f)["medcleanall"]
            mean_flatlevel1 = getheader(f)["medclean1"]
            mean_flatlevel2 = getheader(f)["medclean2"]
            mean_flatlevel3 = getheader(f)["medclean3"]
            mean_flatlevel4 = getheader(f)["medclean4"]
            mean_flatlevel_quadrant = [ mean_flatlevel1, mean_flatlevel2, mean_flatlevel3, mean_flatlevel4]
            exptime = getheader(f)["EXPTIME"]
            data = getdata(f)
            mask = getdata(m)
            if mean_flatlevel < 8000 or mean_flatlevel > 40000 or exptime < 5:
                print("%s : Bad flat level, rejecting." % f)
            elif not check_homogenity(mean_flatlevel, mean_flatlevel_quadrant):
                print("%s : Bad flat level, rejecting because of inhomogenities in the flat." % f)
            else:
                flats_name.append(f)
                flats.append(data)
                masks_name.append(m)
                masks.append(mask)


    night["flats_name"] = flats_name
    night["flats"] = flats
    night["masks_name"] = masks_name
    night["masks"] = masks
    return night

def check_homogenity(mean_flatlevel, mean_flatlevel_quadrant, margin_level =0.2):
    thresh_up = mean_flatlevel + mean_flatlevel*margin_level
    thresh_down = mean_flatlevel - mean_flatlevel*margin_level
    check = True
    for mf in mean_flatlevel_quadrant :
        if mf > thresh_up or mf < thresh_down:
            check = False

    return check

def main(dates, myrawdirrestruct, productdir, filtername, amp_mode, radius_future=0,
         radius_past=3, n_max_flats=10, n_min_flats=6, redo=False, log_stream=None):

    if log_stream is None:
        outstream = sys.stdout
    else:
        outstream = log_stream

    for ind, date in enumerate(dates):
        print("=" * 15, date, "=" * 15, file=outstream)
        datedir = os.path.join(myrawdirrestruct, date)
        logline = []

        for fm, am in zip(filtername, amp_mode):
            print("=" * 15, fm + '_' + am, "=" * 15, file=outstream)

            masterflatdir = os.path.abspath(os.path.join(productdir, "MF_" + fm + am + "4"))
            if not os.path.isdir(masterflatdir):
                print("I'll create the directory %s" % masterflatdir, file=outstream)
                os.mkdir(masterflatdir)

            previousmasterflatpath = os.path.join(masterflatdir, "MF_%s.fits" % date)
            if os.path.isfile(previousmasterflatpath) and redo is True:
                # Just to be sure
                print("%s : masterflat %s %s already exists, deleting this one." % (date, fm, am), file=outstream)
                logline.append("%s : masterflat %s %s already exists, deleting this one. \n" % (date, fm, am))
                os.remove(previousmasterflatpath)

            elif os.path.isfile(previousmasterflatpath) and redo is False:
                print("%s : masterflat %s %s already exists, skipping this one." % (date, fm, am), file=outstream)
                logline.append("%s : masterflat %s %s already exists, skipping this one. \n" % (date, fm, am))
                continue

            try:
                night = search_flat(date, myrawdirrestruct, fm, am, radius_future, radius_past)

                nbrcombi = len(night["flats_name"])

                rf = radius_future
                rp = radius_past
                while nbrcombi < n_min_flats:
                    # We will extent the radius until we have enough flats.
                    rf += 2
                    rp += 2
                    night = search_flat(date, myrawdirrestruct, fm, am, rf, rp)
                    nbrcombi = len(night["flats_name"])
                    if (rp >= 60 or rf >= 60) and nbrcombi < n_min_flats:
                        raise RuntimeError(
                            "I did not find %i flats within +%i days and -%i days around %s, there is a problem here ! \n I have only : %s \n" % (
                                n_min_flats, rf, rp, date, '\n'.join(night["flats_name"] + '\n')))
                    elif nbrcombi > n_min_flats:
                        print(
                            "WARNING : I had to increase the radius of search to +%i -%i days around %s but I found %i flats" % (
                                rf, rp, date, nbrcombi), file=outstream)
                        logline.append(
                            "WARNING : I had to increase the radius of search to +%i -%i days around %s %s %s but I found %i flats \n" % (
                                rf, rp, date, fm, am, nbrcombi))

                if nbrcombi > n_max_flats:
                    # we reduce the number of flats we account for :
                    print("I found %i flats, I will reduce it to %i." % (nbrcombi, n_max_flats), file=outstream)
                    logline.append("I found %i flats for %s %s %s, I will reduce it to %i. \n" % (
                        nbrcombi, date, fm, am, n_max_flats))
                    night["flats"] = night["flats"][:n_max_flats]
                    night["flats_name"] = night["flats_name"][:n_max_flats]
                    night["masks"] = night["masks"][:n_max_flats]
                    night["masks_name"] = night["masks_name"][:n_max_flats]

                nbrcombi = len(night["flats_name"])
                logline.append('Combining these %i flats : \n' % nbrcombi)
                logline.append('\n'.join(night["flats_name"]) + '\n')

                a = num_combine(night["flats"], masks=night["masks"], combination_type="median")
                _, h = auxfcts.fromfits(night["flats_name"][0], hdu=0, verbose=False)

                h['NCOMBINE']=nbrcombi
                for i,tocombine in enumerate(night["flats_name"]):
                    k = "IMCMB%i"%(i+1)
                    h[k]=tocombine

                # # We cannot do anything. Just to be sure, we erase previous flats...
                outputfilepath = os.path.join(masterflatdir, "MF_%s.fits" % date)


                print("Combination of %i flats" % nbrcombi, file=outstream)

                # NORMALIZATION
                if am == "ALL":
                    suba = a[500:1500, 500:1500]  # we use quadrant 1 (= LL)
                else:
                    suba = a[1000:3000, 1000:3000]
                subamed = np.median(suba.ravel())

                medianval = subamed

                a = a / subamed

                #for some reason the masterflat is now transposed...
                a = a.T

                auxfcts.tofits(outputfilepath, a, h)

                print("Done for %s %s." % (fm, am), file=outstream)
                logline.append("Successfully created master flat %s in %s %s \n" % (outputfilepath, fm, am))
            except Exception as e:
                print("Masterflat %s %s %s failed !!!" % (date, fm, am), file=outstream)
                print(e, file=outstream)
                logline.append("ERROR : Masterflat %s %s %s failed !!! \n" % (date, fm, am))
                logline.append(str(e) + "\n")
                exit()

        logtxt = "".join(logline)
        log_night = open(os.path.join(datedir, "Euler_ECAM_masterflat_construction.log"), "w")
        log_night.write(logtxt)
        log_night.close()

    print('Done with masterflats construction.', file=outstream)
    print("#" * 10, file=outstream)


if __name__ == '__main__':
    myrawdirrestruct = "/obs/lenses_EPFL/RAW/ECAM_1/"
    # myrawdirrestruct = "/Users/martin/Desktop/lc_run/data_raw/ECAM_test/"
    # productdir = "/Users/martin/Desktop/lc_run/data_raw/ECAM_test/reduced/"
    productdir = "/obs/lenses_EPFL/PRERED/ECAM/"

    datemin = "2021-12-24"
    datemax = "2021-12-26"
    starttime = Time(datemin, format='iso', scale='utc').mjd
    endtime = Time(datemax, format='iso', scale='utc').mjd

    filtername = ["RG"]
    amp_mode = ["UL"]
    flat_mode = [fl + "_" + amp_mode[i] for i, fl in enumerate(filtername)]

    days = np.arange(starttime, endtime + 1, 1)
    dates = [Time(d, format='mjd', scale='utc').iso[:10] for d in days]

    radius_future = 3
    radius_past = 3
    n_min_flats = 5
    n_max_flats = 20

    main(dates, myrawdirrestruct, productdir, filtername, amp_mode, radius_future=radius_future,
         radius_past=radius_past, n_max_flats=n_max_flats, n_min_flats=n_min_flats, redo=True, log_stream=None)
