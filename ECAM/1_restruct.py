"""
We re-organized the raw data by type, i.e calibration or science image.
"""

#########################################
from __future__ import print_function
import os, sys
from glob import glob
from astropy.io.fits import getheader
from astropy.time import Time
import numpy as np


def main(dates, myrawdir, command='mv', log_stream=None, redo=False):
    if redo == False:
        command = command + ' -n'  # no clobber command (do not overwrite)
    else :
        command = command + ' -f' #do not prompt before overwriting

    # if log stream == None : we print in the terminal.
    if log_stream is None:
        outstream = sys.stdout
    else:
        outstream = log_stream

    f = open("targetslist.txt", "r")
    flines = f.readlines()
    f.close()
    mytargets = [line.rstrip() for line in flines if (line[0] != "#" and len(line) > 2)]
    mytargets = "\n".join(mytargets)

    f = open("skiplist.txt", "r")
    flines = f.readlines()
    f.close()
    skiplist = [line.rstrip() for line in flines if (line[0] != "#" and len(line) > 2)]
    skiplist = "\n".join(skiplist)

    for ind, date in enumerate(dates):
        print("=" * 15, date, "=" * 15, file=outstream)
        datedir = os.path.join(myrawdir, date)

        fitsfiles = sorted(glob(os.path.join(datedir, "*.fits")))
        dirs = [os.path.join(datedir, o) for o in os.listdir(datedir) if os.path.isdir(os.path.join(datedir, o))]
        loglines = []

        if len(fitsfiles) == 0 and len(dirs) == 0:
            print("I have %i images for this night" % len(fitsfiles), file=outstream)
            loglines.append("No images for this night \n")

        elif len(dirs) != 0 and len(fitsfiles) == 0:
            print("Folder was already restructured", file=outstream)
            loglines.append("Folder already restructured. \n")
            for directory in dirs:
                files = glob(os.path.join(directory, "*.fits"))
                loglines.append("I have %i images in folder %s \n" % (len(files), directory))
        elif len(dirs) != 0 and len(fitsfiles) != 0:
            print("Folder was already restructured but I have new files", file=outstream)
            loglines.append("Folder already restructured but I have new files. \n")
            for directory in dirs:
                files = glob(os.path.join(directory, "*.fits"))
                loglines.append("I have %i images in folder %s \n" % (len(files), directory))

        for fitsfile in fitsfiles:
            basename = os.path.basename(fitsfile)

            if "FOCUS" in basename:
                # We skip it.
                print("\tFocus, skipping", file=outstream)
                continue

            if fitsfile in skiplist:
                print("%s is in skiplist, skipping" % fitsfile, file=outstream)
                continue
	    
            try : 
                header = getheader(fitsfile)
            except : 
                print("%s is corrupted. Skipping" % fitsfile, file=outstream)
                loglines.append("%s is corrupted. Skipping" % fitsfile)
                continue

            target = str(header["OBJECT"])
            obstype = str(header["HIERARCH OGE OBS TYPE"])
            exptime = str(header["EXPTIME"])
            obsfilter = str(header["FILTER"])
            try:
                ampname = str(header["HIERARCH OGE DET OUT NAME"])
            except KeyError:
                ampname = str(header["HIERARCH OGE DET OUT RNAME"])

            # Some checks :
            if ampname not in ["ALLL", "ALL", "LL", "LR", "UL", "UR"]:
                raise RuntimeError("Unknown ampname : %s" % (ampname))
            if "/" in target:
                raise RuntimeError("Teach me to handle such targetnames !")

            # We build a directory name
            destdir = "unknown"

            if obstype == "BIAS":
                destdir = "bias_%s" % ampname

            if obstype == "DARK":
                destdir = "dark_%s" % ampname

            if obstype == "FLAT":
                destdir = "flat_%s_%s" % (obsfilter, ampname)
                line = "%s\t%s\t%s\t%s\t%7.2f\n" % ("Euler", obstype, obsfilter + '_'+ampname, date, float(exptime))
                loglines.append(line)

            if target in mytargets:
                destdir = "science_%s_%s_%s" % (target, obsfilter, ampname)
                line = "%s\t%s\t%s\t%s\t%7.2f\n" % ("Euler", target, obsfilter, date, float(exptime))
                loglines.append(line)

            destdir = os.path.join(myrawdir, date, destdir)
            if not os.path.isdir(destdir):
                os.makedirs(destdir)

            destpath = os.path.join(destdir, basename)

            os.system("%s %s %s" % (command, fitsfile, destpath))

        logtxt = "".join(loglines)
        log_night = open(os.path.join(datedir, "Euler_ECAM_night_restruct.log"), "w")
        log_night.write('Exposures taken during the night : \n')
        log_night.write(logtxt)
        log_night.close()

    print("Done with restructuration ! \n", file=outstream)
    print("#"*10, file=outstream)


if __name__ == '__main__':
    myrawdir = "/obs/lenses_EPFL/RAW/ECAM_1/"
    # myrawdir = "./test_data_ECAM/"
    datemin = "2021-12-14"
    datemax = "2021-12-16"
    starttime = Time(datemin, format='iso', scale='utc').mjd
    endtime = Time(datemax, format='iso', scale='utc').mjd

    filtername = ["RG", "VG", "BG"]
    amp_mode = ["UL", "UL", "UL"]
    flat_mode = [fl + "_" + amp_mode[i] for i, fl in enumerate(filtername)]

    days = np.arange(starttime, endtime + 1, 1)
    dates = [Time(d, format='mjd', scale='utc').iso[:10] for d in days]

    main(dates, myrawdir, command='mv', log_stream=None, redo=False)
