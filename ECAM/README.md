## New Pipeline for ECAM reduction.

Each script can be run individually to be able to redo one part of the pipeline. You can select the date in the individual scripts, in the 'main' section of each script.
Otherwise run_all launch the full reduction, parameters can be updated directly in the main fonction. If the redo flag is turned off, we will redo everything from scratch.
The pipeline is currently running on a virtual machine on lesta : `lensesdrs01`

__IMPORTANT__ : This pipeline does not use iraf anymore, but `photutils` and  `stsci.image` python package instead. 

__TO ADD NEW TARGET__ : add the name of the target (as in the OB) in the __targetslist.txt__. Otherwise, it won't be reduced.

The pipeline is composed of 7 steps :

## 0_copy_raw
We copy the images from the observatory folder to our reduction folder

## 1_restruct
We sort the images by type and objects : calibration, flats, science, ect...

## 2_biassub
Remove the bias using the overscan.

## 3_prepare_mask
We build the mask to mask star in the flats and compensate for the shutter image.

## 4_build_masterflats
We group flats from the previous and coming nights. I will search flats between "radius_future" and "radius_past" around the observation date until I have
"n_max_flats". I have find less than "n_min_flats", I will double the searching windows and print a warning message. I will build one masterflat per night and per filter and ampmode. I will reject flats with ADU above 30000 or below 8000 or exptime < 6s.

## 5_apply_masterflats
I divide the science images by the masterflats.

## 6_run_diagnostics
Make the png images of the flats + measure ellipticity, seeing, airmass and many thing else in the standard fields. You can add any diagnostics of the
instrument here. Make some graph with the evolution of the ellipticity. You can play with the parameters to try to find correlation.
You can select the standard fields with : 

    reference_ellipticity_object = ["RXJ1131-123_RG_UL_RGUL4"]
    exphemlens_list = ["RXJ1131-123,f|Q,11:31:55.40,-12:31:55.00,19.0,2000"]
    
__To add a new field in the database__ : 
- add the name of the file in the dictionnary l228
- modify the the run_diagnostic function to fill your new field
- delete previous csv file 
- rerun script 6, with turning the option redo = True at l266 and runsex = False if you don't need to rerun sextractor.

__Current fields__ : 
- 'image': name of the file,
- 'date': date of the observation in UT time,
- 'ellipticity': ellipticity of the PSF,
- 'A_axis':  size of the PSF along the long axis in pixel,
- 'B_axis':  size of the PSF along the short axis in pixel,
- 'A-B':  diferrence between short and long axis in arcsec,
- 'Theta':  position angle of the PSF,
- 'seeing':  seeing in arcsec,
- 'seeingpixels':  seeing in pixel,
- "derot":  position of the derotator,
- "azi":  azimuthal angle,
- "elev":  elevation angle,
- "windspeed":  wind speed in m.s-1,
- "winddir":  angular direction of the wind,
- "windangle": angle between the wind and the azimuthal angle of the telescope,
- "elev_speed":  motion around the elevation axis in rad.s-1,
- "azi_speed":  motion around the azimuthal axis in rad.s-1,
- "total_speed":  total motion,
- "goodstars":  number of stars used by sextractor,
- "mhjd":  modified Julian Date of the observation,
- 'airmass':  airmass of the observation,
- 'moondist':  separation to the moon (deg),
- 'moonpercent':  moon illumination,
- 'sundist':  separation to the sun in deg,
- 'timefromsunset': time from the last sunset in hour
    
## 7_cleanup 
Remove all the intermediate product to save disk space
It also create a summary of all the logs in the pipe_log directory. All the prints are saved in the 'print_out' directory in the pipe_log folder. 

## run_all 
You can also run all the scrit at once, by specifying the start and end date with the --start and --end optionnal argument. Date format is YYYY-MM-DD. Without argument the pipeline will run from 3 days ago until today for the 3 first steps. It will prepare the flats until today but construct the masterflats only for the day, 3 days ago. This is meant to wait for the flats taken up to 3 day after the observation date. The reduction is in fact done 3 days after the observation

## execute_all.sh 
This is wrapper in bash to call for run_all.py. It loads all the necessary module on lesta and execute the python scripts. It can be call with crontab for automatic run everyday. To edit the crontab command : 
    
    export EDITOR=nano
    crontab -e 
    

 The image on which the pipeline has failed are written in error_flatfielding.log in the pipe_log directory. We are not using bias callibration, the overscan is doing the job.
 
 ## Outputs : 
  __Reduced files__ : /obs/lenses_EPFL/PRERED/ECAM/reduc/
 __Diagnostics Plots__ : /obs/lenses_EPFL/PRERED/ECAM/diagnostics/plots/ 
__Raw images__ : /obs/lenses_EPFL/RAW/ECAM_1/
 __Masterflats pngs__ : /obs/lenses_EPFL/PRERED/ECAM/diagnostics/png_flat_RG_UL/
 
 __IMPORTANT__ : 
 /obs/lenses_EPFL/RAW/ECAM_1_restruct/ contains only symlink and is not used anymore.
 








