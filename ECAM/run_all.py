#!/usr/bin/python3

import os, datetime
from astropy.time import Time
import numpy as np
import argparse as ap
import sys 
copy_raw = __import__('0_copy_raw')
restruct_raw = __import__('1_restruct')
biassub = __import__('2_biassub')
prepare_mask = __import__('3_prepare_mask_flats')
build_flats = __import__('4_build_masterflats')
apply_flats = __import__('5_apply_masterflats')
diagnostics = __import__('6_run_diagnostics')
cleanup= __import__('7_cleanup')

def main(dates, dates_flats):
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    #           GENERAL CONFIGURATION
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

    # The scripts and configuration :
    scriptsdir = "/home/astro/millon/Desktop/PRERED/ECAM"

    #Raw data :
    raw_dir = "/insdata/ECAM/raw"
    myrawdir = "/obs/lenses_EPFL/RAW/ECAM_1/" #where to copy the data, sorted by date
    myrawdirrestruct = "/obs/lenses_EPFL/RAW/ECAM_1/" #where to copy the data, sorted by object, this can be same folder as myrawdir

    # The products (will get large !) :
    productdir = "/obs/lenses_EPFL/PRERED/ECAM/"
    productsciencedir = "/obs/lenses_EPFL/PRERED/ECAM/reduc"

    #The diagnostics :
    diagnostics_dir = "/obs/lenses_EPFL/PRERED/ECAM/diagnostics"
    diagno_masterflat_dir_list = ["/obs/lenses_EPFL/PRERED/ECAM/MF_RGUL4","/obs/lenses_EPFL/PRERED/ECAM/MF_VGUL4"]  #masterflat to create the pngs
    png_dir_list = [os.path.join(diagnostics_dir, "png_flat_RG_UL") , os.path.join(diagnostics_dir, "png_flat_VG_UL")]

    #The log file :
    now = datetime.datetime.now()
    log_directory = '/home/astro/millon/Desktop/PRERED/ECAM/pipe_log/'
    log_file = os.path.join(log_directory, 'print_out/log_%s.txt' % now.strftime("%Y-%m-%d_%H:%M"))
    error_log =  os.path.join(log_directory, 'error_flatfielding.log') #log file to record the images that failed during flatfielding
    e = open(error_log, 'a')
    f = open(log_file, 'a')
    f.write('################# \n')
    f.write('Reduction starting on %s: \n'%now.strftime("%Y-%m-%d %H:%M"))
    # f = sys.stdout

    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    #   Flat setting
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    #select here the readout mode and filter you need for the flatfielding
    filtername = ["RG","VG"]
    amp_mode = ["UL","UL"]
    flat_mode = [fl + "_" + amp_mode[i] for i,fl in enumerate(filtername)]

    #shutter image for flat correction
    shutterimagepath = "shutter_20211216_filtered.fits"

    #parameters for the flat construction, radius in days around the observation date to look for flats
    radius_future = 3
    radius_past = 3
    n_min_flats = 5
    n_max_flats = 20

    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    #           Pipeline
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    redo = True # redo already reduced files ?
    Euler_saturlevel = 65000 #euler information
    Euler_pixsize = 0.2149  # in arcsecond

    #object to use for the standard fields
    reference_ellipticity_object = [
                                    "RXJ1131-123_RG_UL_RGUL4", 
                                    "DES2038-4008_RG_UL_RGUL4",
                                    "PSJ0259-2338_RG_UL_RGUL4",
                                    "J0457-7820_RG_UL_RGUL4",
                                    "J0818-2613_RG_UL_RGUL4",
                                    "J0941+0518_RG_UL_RGUL4",
                                    "DES0501-4118_RG_UL_RGUL4", 
                                    ]
    exphemlens_list = [
                       "RXJ1131-123,f|Q,11:31:55.40,-12:31:55.00,19.0,2000",
                      "DES2038-4008,f|Q,20:38:02.60,-40:08:13.2,19.0,2000",
                       "PSJ0259-2338,f|Q,02:59:33.51,-23:38:01.97,19.0,2000",
                       "J0457-7820,f|Q,04:57:23.60,-78:20:48.0,19.0,2000",
                       "J0818-2613,f|Q,08:18:00.00,-26:13:00.0,19.0,2000",
                       "J0941+0518,f|Q,09:41:00.00,05:18:00.0,19.0,2000",
                       "DES0501-4118,f|Q,05:01:00.00,-41:18:00.0,19.0,2000",
                       ]


    print ('Running from %s to %s' % (dates[0], dates[-1]))
    print ('Preparing flats from %s to %s' % (dates_flats[0], dates_flats[-1]))
    # copy the raw image from repository :
    no_obs_date = copy_raw.main(dates_flats, raw_dir, myrawdir,flat_mode,amp_mode, log_stream = f)
    #update the dates with the telescope was closed
    dates = [d for d in dates if d not in no_obs_date]
    dates_flats = [d for d in dates_flats if d not in no_obs_date]

    #restructure the raw directory by object, calibrations,ect... :
    restruct_raw.main(dates_flats, myrawdir, command='mv', log_stream=f, redo=redo)
    #Subtract the bias from the flats and science images :
    biassub.main(dates_flats, myrawdirrestruct, redo=redo, log_stream=f)
    #prepare the flat masks :
    prepare_mask.main(dates_flats, myrawdirrestruct, shutterimagepath, redo=redo, log_stream=f)
    #stack flats to create the masterflat :
    build_flats.main(dates, myrawdirrestruct, productdir, filtername, amp_mode, radius_future=radius_future,
         radius_past=radius_past, n_max_flats=n_max_flats, n_min_flats=n_min_flats, redo=redo, log_stream=f)
    #apply the masterflats to the science images :
    failure = apply_flats.main(dates, myrawdirrestruct, productsciencedir, productdir, redo=redo, log_stream=f)
    e.write("\n".join(failure) + '\n')
    #run the diagnostics (if you want to redo sextractor on all images turn the flag directly in the script 6, otherwise I am just redoing the master flats pngs) :
    diagnostics.main(dates, myrawdirrestruct, productsciencedir, diagno_masterflat_dir_list, diagnostics_dir, png_dir_list, reference_ellipticity_object,exphemlens_list,
         redo=redo, log_stream=f, pixsize=Euler_pixsize, saturlevel=Euler_saturlevel)
    #cleaning up :
    cleanup.main(dates, myrawdirrestruct, log_directory, log_stream=f)

    print("Pipeline ended succesfully.", file=f)
    f.close()
    e.close()

if __name__ == '__main__':
    parser = ap.ArgumentParser(prog="python {}".format(os.path.basename(__file__)),
                               description="Run the ECAM pipeline. No argument will run for the previous nigth.",
                               formatter_class=ap.RawTextHelpFormatter)
    help_startnight = "first night to reduce in the format YYYY-MM-DD"
    help_endnight = "last night to reduce in the format YYYY-MM-DD"

    parser.add_argument('--start', dest='startnight', type=str,
                        metavar='', action='store', default=None,
                        help=help_startnight)
    parser.add_argument('--end', dest='endnight', type=str,
                        metavar='', action='store', default=None,
                        help=help_endnight)

    args = parser.parse_args()
    if args.startnight is None or args.endnight is None:
        now = datetime.datetime.now()
        startnight = now.strftime("%Y-%m-%d")
        starttime = Time(startnight, format='iso', scale='utc').mjd - 3  # we reduce until the three days before, to use the flats taken after observation date.
        endtime = starttime  # only one night
        endtime_flats = Time(startnight, format='iso', scale='utc').mjd # we prepare the flats until today

    else:
        args.startnight = args.startnight + " 01:00:00"
        args.endnight = args.endnight + " 01:00:00"
        starttime = Time(args.startnight, format='iso', scale='utc').mjd
        endtime = Time(args.endnight, format='iso', scale='utc').mjd
        endtime_flats = endtime #if we give in argument the dates, we are not looking for flats further

    days = np.arange(starttime, endtime + 1, 1)
    days_flats = np.arange(starttime, endtime_flats + 1, 1)
    dates = [Time(d, format='mjd', scale='utc').iso[:10] for d in days]
    dates_flats = [Time(d, format='mjd', scale='utc').iso[:10] for d in days_flats]

    main(dates, dates_flats)
