#!/home/weber/anaconda3/bin/python
import astropy.io.fits as pyfits
import os

from astropy.time import Time
from astropy.coordinates import SkyCoord, EarthLocation, AltAz, Angle, ICRS
import pathlib
import astropy.units as u
import os, time
import sys
import argparse
import signal
from math import pi
import subprocess

# elevation above sea level, m
LONGITUDE = -1.234522901093557  # site longitude, +W rads
LATITUDE = -0.51067457448673  # site latitude, +N rads
ELEVATION = 2378

la_silla = EarthLocation.from_geodetic(LONGITUDE * 180 / pi, LATITUDE * 180 / pi, ELEVATION)

stop = False


def signal_handler(sig, frame):
    global stop
    stop = True


BLUE = "34m"
RED = "31m"
GREEN = "32m"


def display_colored_text(color, text):
    """
	Display colored text in a terminal, color rendring depends on the terminal settings
	"""
    colored_text = f"\033[{color}{text}\033[00m"
    return colored_text


def astrometry_file(f):
    """
	Proceed to the astrometry of a given fits file.
	it copy it in the current directory and proceed to its analysis.
	uses astrometry.net
	"""
    print("Starting ASTROMETRY SESSION")
    print("Start Analysis of : %s"%f)
    p = pathlib.Path(f)
    p = args.path / p
    subprocess.run(["cp", f"{p}", "."])
    hdu = pyfits.open(f)
    if args.keyword != None:
        if not (args.keyword_value in hdu[0].header[args.keyword]):
            return False
    if args.lazy:
        if pathlib.Path(f).with_suffix(".new").exists():
            return False

    ra = hdu[0].header["RA"]
    dec = hdu[0].header["DEC"]
    obtt = Time(hdu[0].header["MJD-OBS"], format="mjd")
    print(obtt.iso)
    obtt = Time(float(hdu[0].header["OGE OBS TUNIX MID"]), format="unix")
    print(obtt.iso)

    subprocess.run(["solve-field", "--overwrite", "--no-verify", "--ra", f"{str(ra)}", "--dec", f"{str(dec)}",
                    '--scale-units', 'app', "--scale-low", "0.214", "--scale-high", "0.216",
                    "--crpix-center", "--radius", "5", "--no-plots", f"{p.name}"]
                   )
    hdu.close()
    try:
        hdu = pyfits.open(p.with_suffix(".new").name)
        ram = hdu[0].header["CRVAL1"]
        decm = hdu[0].header["CRVAL2"]
        E = SkyCoord(Angle(ra * u.deg), Angle(dec * u.deg))
        print(E)
        P = SkyCoord(Angle(ram * u.deg), Angle(decm * u.deg))
        print(P)
        # obtt=Time(hdu[0].header["MJD-OBS"],format="utc")
        # print(obtt.iso)
        Ealaz = E.transform_to(AltAz(obstime=obtt, location=la_silla))
        Palaz = P.transform_to(AltAz(obstime=obtt, location=la_silla))
        print(display_colored_text(GREEN, f"Results for {f}"))
        print(f"Asked : {Ealaz.alt}, {Ealaz.az}, Measured {Palaz.alt}, {Palaz.az}")
        print(f"{(Ealaz.alt-Palaz.alt)}, {Ealaz.az-Palaz.az}")
        print(
            display_colored_text(
                RED,
                f" Delta El : {(Ealaz.alt-Palaz.alt).arcsec} arcsec, Delta Az : {(Ealaz.az-Palaz.az).arcsec} arcsec",
            )
        )

    except:
        print(display_colored_text(RED, "Pas d'astrometrie"))
    print(display_colored_text(GREEN, f"Finished the analysis of : {f}"))
    return False


parser = argparse.ArgumentParser(
    description="Do the astrometry of fits images inside a given directory, either live or not"
)
parser.add_argument(
    "action",
    choices=["watch", "batch"],
    help="watch to watch a directory or batch to loop through existing images",
)
parser.add_argument("path", help="directory to deal with")
parser.add_argument("-k", "--keyword", help="to filter by fits keyword")
parser.add_argument("-v", "--keyword-value", help="keyword value to select")
parser.add_argument(
    "-l",
    "--lazy",
    action="store_true",
    help="non redo astrometry for images already solved",
)

args = parser.parse_args()
print(args)

if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal_handler)
    if args.action == "watch":
        # Wath mode where astrometry is performed on the go

        before = dict([(f, None) for f in os.listdir(args.path)])

        print(f"Start Watching directory {args.path}")
        while 1:
            time.sleep(1)
            after = dict([(f, None) for f in os.listdir(args.path)])
            added = [f for f in after if not f in before]
            removed = [f for f in before if not f in after]
            if added:
                print("Added: ", ", ".join(added))
            if removed:
                print("Removed: ", ", ".join(removed))
            before = after
            for f in added:
                if "ECAM" in f:
                    fs = os.path.getsize(os.path.join(args.path, f))
                    lfs = 0
                    while fs != lfs:
                        lfs = fs
                        time.sleep(0.2)
                        fs = os.path.getsize(os.path.join(args.path, f))

                    astrometry_file(f)
            if stop:
                print(display_colored_text(RED, "Ctrl-C stop watching"))
                sys.exit()

    else:
        # batch mode all file in a directory are done once.
        lst = sorted(pathlib.Path(args.path).glob("ECAM*.fits"))
        for f in lst:
            astrometry_file(f)
            if stop:
                print(display_colored_text(RED, "Ctrl-C stop watching"))
                sys.exit()
