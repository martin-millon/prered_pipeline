#
#   Calculates the seeing etc of the images and updates the database.
#   You can turn on the flag "checkplots" to check how I do this.
#   In this case I will not update the database.
#

from . import star
import numpy as np
import os
import math
import matplotlib
import matplotlib.pyplot as plt
import datetime


def airmass(radalt):
    #   We calculate the airmass (radalt is altitude in radians)
    #   Rozenberg's empirical relation :
    #   X = 1 / [sin ho + 0.025 exp(-11 sin ho)]
    #   where ho is the apparent altitude of the object. This formula can be used all down to the horizon (where it gives X = 40).

    if radalt < 0.0:
        return -1.0
    elif radalt > math.pi / 2.0:
        return -2.0
    else:
        return 1.0 / (math.sin(radalt) + 0.025 * math.exp(-11.0 * math.sin(radalt)))

def datetimefromjd(JD):
    """
    Copy and past from cosmouline.
    Can be of use here to plot lightcurves with nice dates.

    Returns the Gregorian calendar (i.e. our "normal" calendar)
    Based on wikipedia:de and the interweb :-)


    :type JD: float
    :param JD: julian date

    :rtype: datetime object
    :returns: corresponding datetime

    """

    if JD < 0:
        raise ValueError('Julian Day must be positive')

    dayofwk = int(math.fmod(int(JD + 1.5),7))
    (F, Z) = math.modf(JD + 0.5)
    Z = int(Z)

    if JD < 2299160.5:
        A = Z
    else:
        alpha = int((Z - 1867216.25)/36524.25)
        A = Z + 1 + alpha - int(alpha/4)


    B = A + 1524
    C = int((B - 122.1)/365.25)
    D = int(365.25 * C)
    E = int((B - D)/30.6001)

    day = B - D - int(30.6001 * E) + F
    nday = B-D-123
    if nday <= 305:
        dayofyr = nday+60
    else:
        dayofyr = nday-305
    if E < 14:
        month = E - 1
    else:
        month = E - 13

    if month > 2:
        year = C - 4716
    else:
        year = C - 4715


    # a leap year?
    leap = 0
    if year % 4 == 0:
        leap = 1

    if year % 100 == 0 and year % 400 != 0:
        leap = 0
    if leap and month > 2:
        dayofyr = dayofyr + leap

    # Convert fractions of a day to time
    (dfrac, days) = math.modf(day/1.0)
    (hfrac, hours) = math.modf(dfrac * 24.0)
    (mfrac, minutes) = math.modf(hfrac * 60.0)
    seconds = round(mfrac * 60.0) # seconds are rounded

    if seconds > 59:
        seconds = 0
        minutes = minutes + 1
    if minutes > 59:
        minutes = 0
        hours = hours + 1
    if hours > 23:
        hours = 0
        days = days + 1

    return datetime.datetime(year,month,int(days),int(hours),int(minutes),int(seconds))


def run_sextractor(image, catfilename, pixsize=0.2149, saturlevel=65000, sextractor_command='sex', checkplots=False, runsex = True):
    forceseeingpixels = True

    # We need to run Sextractor
    cmd = "%s %s -c default_see_template.sex -PIXEL_SCALE %.3f -SATUR_LEVEL %.3f -CATALOG_NAME %s" % (
        sextractor_command, image, pixsize, saturlevel, catfilename)
    if runsex :
        os.system(cmd)

    # We read and sort the sextractor catalog
    goodsexstars = star.readsexcat(catfilename, maxflag=2, posflux=True,
                                   propfields=["THETA_IMAGE", "B_IMAGE", "A_IMAGE"])
    nbrstars = len(goodsexstars)
    sortedsexstars = star.sortstarlistby(goodsexstars, 'fwhm')

    # Just to make the optional plot work in any case :
    peakpos = -5.0

    # We make an array to measure the seeing
    fwhms = np.array([s.fwhm for s in sortedsexstars])

    if len(fwhms) > 10:

        # We want a crude guess at what kind of range we have to look for stars
        # The goal here is to have a "nice-looking" histogram with a well defined
        # peak somewhere inside the range.

        minfwhm = 1.5
        medfwhm = np.median(fwhms)
        if medfwhm < minfwhm:
            medfwhm = minfwhm

        # stdfwhm = np.std(fwhms)
        # widestars = medfwhm + 2.0 * stdfwhm

        widestars = 3.0 * medfwhm

        maxfwhm = 30.0
        if widestars < maxfwhm:
            maxfwhm = widestars

        # At this point the true seeing should be between minfwhm and maxfwhm.

        # We build a first histogram :

        (hist, edges) = np.histogram(fwhms, bins=10,
                                     range=(minfwhm, maxfwhm))  # I removed new=True, depreciated since python 1.4
        # Note that points outside the range are not taken into account at all, they don't fill the side bins !

        # We find the peak, and build a narrower hist around it
        maxpos = np.argmax(hist)
        if maxpos == 0:
            # print "FWHMs ="
            # print "\n".join(["%.3f" % (fwhm) for fwhm in fwhms])
            # raise mterror("This FWHM distribution is anormal (many cosmics). Something is wrong with sextractor... Problematic img: " + image['imgname'])
            print("This image has many low-FWHM objects (cosmics ?)")
            seeingpixels = np.median(fwhms)
            if forceseeingpixels:
                if seeingpixels < 2:
                    seeingpixels = 2.01  # HAAAAAAX
            seeing = seeingpixels * pixsize

        elif maxpos == len(hist) - 1:
            print("This image if funny, it seems to have many high-FWHM objects.")
            print("I can only make a crude guess ...")
            seeingpixels = np.median(fwhms)
            if forceseeingpixels:
                if seeingpixels < 2:
                    seeingpixels = 2.01  # HAAAAAAX
            seeing = seeingpixels * pixsize


        else:  # the normal situation :
            peakpos = 0.5 * (edges[maxpos] + edges[maxpos + 1])

            # We build a second histogram around this position, with a narrower range :
            (hist, edges) = np.histogram(fwhms, bins=10, range=(
                peakpos - 2.0, peakpos + 2.0))  # I removed new=True, depreciated since python 1.4
            maxpos = np.argmax(hist)
            peakpos = 0.5 * (edges[maxpos] + edges[maxpos + 1])

            # We take the median of values around this peakpos :
            starfwhms = fwhms[np.logical_and(fwhms > peakpos - 1.0, fwhms < peakpos + 1.0)]
            if len(starfwhms) > 0:
                seeingpixels = np.median(starfwhms)
                if forceseeingpixels:
                    if seeingpixels < 2:
                        seeingpixels = 2.01  # HAAAAAAX
            else:
                seeingpixels = peakpos
                if forceseeingpixels:
                    if seeingpixels < 2:
                        seeingpixels = 2.01  # HAAAAAAX
            seeing = seeingpixels * pixsize

    elif len(fwhms) > 0:
        print("Only %i stars, using the median ..." % (len(fwhms)))
        seeingpixels = np.median(fwhms)
        if forceseeingpixels:
            if seeingpixels < 2:
                seeingpixels = 2.01  # HAAAAAAX
        seeing = seeingpixels * pixsize

    else:
        print("Are you kidding ? No stars at all !")
        seeing = -1.0
        seeingpixels = -1.0

    print("Measured seeing [pixels] :", seeingpixels)
    print("Measured seeing [arcsec] :", seeing)

    if checkplots:
        import matplotlib.pyplot as plt
        plt.hist(fwhms, bins=np.linspace(np.min(fwhms), np.max(fwhms), 50), facecolor='green')
        plt.axvline(x=seeingpixels, linewidth=2, color='red')
        plt.axvline(x=peakpos - 1.0, linewidth=2, color='blue')
        plt.axvline(x=peakpos + 1.0, linewidth=2, color='blue')
        plt.xlabel('FWHM [pixels]')
        plt.title('Histogram of FWHM')
        plt.grid(True)
        plt.show()

    # And we measure the ellipticity of the images, by looking at sources with similar width then our seeingpixels
    # Now look at this beauty : :-)
    ells = np.array([s.ell for s in sortedsexstars])
    starells = np.array([s.ell for s in sortedsexstars if abs(s.fwhm - seeingpixels) < 1.0])

    print("I found", len(ells), "stars for ellipticity measure.")

    if len(starells) > 0:
        ell = np.median(starells)
    else:
        print("Bummer ! No stars for ellipticity measure.")
        ell = -1.0

    print("Measured ellipticity :", ell)
    if checkplots:
        plt.hist(ells, bins=np.linspace(0, 1, 50), facecolor='grey')
        plt.hist(starells, bins=np.linspace(0, 1, 50), facecolor='green')
        plt.axvline(x=ell, linewidth=2, color='red')
        plt.xlabel('Ellipticity')
        plt.title('Histogram of ellipticity')
        plt.grid(True)
        plt.show()

    # New thing, we also measure the position angle of the ellipcitity
    pas = np.array([s.props["THETA_IMAGE"] for s in sortedsexstars])
    starpas = np.array([s.props["THETA_IMAGE"] for s in sortedsexstars if abs(s.fwhm - seeingpixels) < 1.0])

    if len(starpas) > 0:
        pa = np.median(starpas)
        pastd = np.std(starpas)
    else:
        pa = -1.0
        pastd = 0.0
    print("Measured position angle :", pa, pastd)

    # same for the minor and major axis
    bimgs = np.array([s.props["B_IMAGE"] for s in sortedsexstars])
    starbimgs = np.array([s.props["B_IMAGE"] for s in sortedsexstars if abs(s.fwhm - seeingpixels) < 1.0])

    aimgs = np.array([s.props["A_IMAGE"] for s in sortedsexstars])
    staraimgs = np.array([s.props["A_IMAGE"] for s in sortedsexstars if abs(s.fwhm - seeingpixels) < 1.0])

    if len(starbimgs) > 0:
        bimage = np.median(starbimgs)
    else:
        bimage = -1
    print("Measured minor axis :", bimage)

    if len(starbimgs) > 0:
        aimage = np.median(staraimgs)
    else:
        aimage = -1
    print("Measured major axis :", aimage)

    return {'seeing': float(seeing), 'ellipticity': float(ell), 'goodstars': nbrstars,
            'seeingpixels': float(seeingpixels), 'Theta': float(pa), 'B_axis': float(bimage),
            'A_axis': float(aimage)}


def plot_fields(mhjds, diffaxes, windspeeds, windangles, total_speed, airmasses, seeings, ellipticities, plot_dir,
                lensname, kick_bad_image=False, show=True, average_per_night=False, showdates = True):

    # Create an index of the good images (kick bad seeing, bad airmass, crazy wind)

    if kick_bad_image:
        good_inds = [True if (0 < ws < 100 and am < 2.0 and see < 3.0)
                     else False
                     for ws, am, see in zip(windspeeds, airmasses, seeings)]
    else:
        good_inds = [True
                     for ws, am, see in zip(windspeeds, airmasses, seeings)]

    if average_per_night :
        suffix = "_average"
    else :
        suffix = ""

    marker_size = 10

    # Ok, so now we have a bit of everything. Let's explore the correlations. Change the first value in the zip functions below to chose what you want
    xs = [x for x, v in zip(total_speed, good_inds) if v is True]
    ys = [y for y, v in zip(diffaxes, good_inds) if v is True]
    cs = [c for c, v in zip(seeings, good_inds) if v is True]

    # finally, the plot
    cm = plt.cm.get_cmap('RdYlBu_r')
    fig1, ax = plt.subplots()
    sc = plt.scatter(xs, ys, c=cs, cmap=cm, vmin=0.5, vmax=2.5, s=marker_size)
    plt.colorbar(sc, label='seeing')
    plt.title(lensname)
    plt.xlabel("total speed", fontsize=12)
    plt.ylabel("A-B", fontsize=12)

    fig1.savefig(os.path.join(plot_dir, lensname+"_rotation_speed_vs_diffaxis%s.png"%suffix))

    xs = [x for x, v in zip(mhjds, good_inds) if v is True]
    ys = [y for y, v in zip(ellipticities, good_inds) if v is True]
    cs = [c for c, v in zip(seeings, good_inds) if v is True]

    fig2, ax = plt.subplots()
    sc = plt.scatter(xs, ys, c=cs, cmap=cm, vmin=0.5, vmax=2.5, s=marker_size)
    plt.colorbar(sc, label='seeing')
    plt.title(lensname)
    plt.xlabel("mhjds", fontsize=12)
    plt.ylabel("ellipticity", fontsize=12)

    if showdates:
        # This showdates stuff should come at the very end
        minjd = ax.get_xlim()[0]
        maxjd = ax.get_xlim()[1]
        # axes.set_xlim(minjd, maxjd)
        yearx = ax.twiny()
        yearxmin = datetimefromjd(minjd + 2400000.5)
        yearxmax = datetimefromjd(maxjd + 2400000.5)
        yearx.set_xlim(yearxmin, yearxmax)
        yearx.xaxis.set_minor_locator(matplotlib.dates.MonthLocator())
        yearx.xaxis.set_major_locator(matplotlib.dates.YearLocator())
        yearx.xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%Y'))
        yearx.xaxis.tick_top()

    fig2.savefig(os.path.join(plot_dir, lensname+"_evolution_ellipticity%s.png"%suffix))

    xs = [x for x, v in zip(mhjds, good_inds) if v is True]
    ys = [y for y, v in zip(diffaxes, good_inds) if v is True]
    cs = [c for c, v in zip(seeings, good_inds) if v is True]

    fig3, ax = plt.subplots()
    sc = plt.scatter(xs, ys, c=cs, cmap=cm, vmin=0.5, vmax=2.5, s=marker_size)
    plt.colorbar(sc, label='seeing')
    plt.title(lensname)
    plt.xlabel("mhjds", fontsize=12)
    plt.ylabel("A-B", fontsize=12)
    if showdates:
        # This showdates stuff should come at the very end
        minjd = ax.get_xlim()[0]
        maxjd = ax.get_xlim()[1]
        # axes.set_xlim(minjd, maxjd)
        yearx = ax.twiny()
        yearxmin = datetimefromjd(minjd + 2400000.5)
        yearxmax = datetimefromjd(maxjd + 2400000.5)
        yearx.set_xlim(yearxmin, yearxmax)
        yearx.xaxis.set_minor_locator(matplotlib.dates.MonthLocator())
        yearx.xaxis.set_major_locator(matplotlib.dates.YearLocator())
        yearx.xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%Y'))
        yearx.xaxis.tick_top()

    fig3.savefig(os.path.join(plot_dir, lensname+"_evolution_diffaxis%s.png"%suffix))

    xs = [x for x, v in zip(diffaxes, good_inds) if v is True]
    ys = [y for y, v in zip(windspeeds, good_inds) if v is True]
    cs = [c for c, v in zip(seeings, good_inds) if v is True]

    fig4, ax = plt.subplots()
    sc = plt.scatter(xs, ys, c=cs, cmap=cm, vmin=0.5, vmax=2.5, s=marker_size)
    plt.colorbar(sc, label='seeing')
    plt.title(lensname)
    plt.xlabel("A-B", fontsize=12)
    plt.ylabel("windspeed", fontsize=12)

    fig4.savefig(os.path.join(plot_dir, lensname+"_wind_speed_vs_diffaxis%s.png"%suffix))

    xs = [x for x, v in zip(diffaxes, good_inds) if v is True]
    ys = [y for y, v in zip(windangles, good_inds) if v is True]
    cs = [c for c, v in zip(seeings, good_inds) if v is True]

    fig5, ax = plt.subplots()
    sc = plt.scatter(xs, ys, c=cs, cmap=cm, vmin=0.5, vmax=2.5, s=marker_size)
    plt.colorbar(sc, label='seeing')
    plt.title(lensname)
    plt.xlabel("A-B", fontsize=12)
    plt.ylabel("windangles", fontsize=12)

    fig5.savefig(os.path.join(plot_dir, lensname+"_wind_angles_vs_diffaxis%s.png"%suffix))

    if show:
        plt.show()
	
    plt.close()

