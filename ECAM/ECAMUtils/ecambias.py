"""
Stuff for the overscan and prescan processing
"""

import numpy as np
import os

if not 'DISPLAY' in os.environ : 
	print("No Display environement ! ")
	import matplotlib
	matplotlib.use('Agg')
	import matplotlib.pyplot as plt
else : 
	import matplotlib.pyplot as plt
import scipy.ndimage

def getsec(a, sec):
	"""
	sec is a string like '[101:2148,101:2156]' as given in the ECAM headers,
	and we cut this part from a.
	"""
	(xa, xb, ya, yb) = map(int, sec[1:-1].replace(",", ":").split(":"))
	cuta = a[xa-1:xb,ya-1:yb]
	return cuta


def putsec(a, sec, data):
	"""
	writes data into a, at position specified by sec.
	sizes must match, of course.
	"""
	(xa, xb, ya, yb) = map(int, sec[1:-1].replace(",", ":").split(":"))
	#print xa, xb, ya, yb
	a[xa-1:xb,ya-1:yb] = data
	



def getsubarrays_multiquad(a, h, channel):
	"""
	This one is soft coded for now, let's see how it works.
	I suspect problems with the header indications of flats in the first months of eulercam.
	"""
	
	data = getsec(a, h["OGE DET OUT%i DATASEC" % channel])
	ox = getsec(a, h["OGE DET OUT%i OVSCXSEC" % channel])
	px = getsec(a, h["OGE DET OUT%i PRSCXSEC" % channel])
	oy = getsec(a, h["OGE DET OUT%i OVSCYSEC" % channel])
	py = getsec(a, h["OGE DET OUT%i PRSCYSEC" % channel])
	
	#print px.shape, ox.shape, py.shape, oy.shape
	return {"data":data, "px":px, "ox":ox, "py":py, "oy":oy}




def getsubarrays_onequad(a, h):
	"""
	Hard coded. Dom and Gilles confirmed me (may 2011) that this has never changed.
	Safer, as there might be errors in the headers... 
	"""
	
	data = getsec(a, "[51:4146,51:4162]")
	ox = getsec(a, "[4147:4196,51:4162]")
	px = getsec(a, "[1:50,51:4162]")
	oy = getsec(a, "[51:4146,4163:4212]")
	py = getsec(a, "[51:4146,1:50]")
	
	return {"data":data, "px":px, "ox":ox, "py":py, "oy":oy}


def poplot_onequad(basename, a, h, filename):
	"""
	We plot the overscan and prescan, a and  h are not changed.
	"""
	
	subarrays = getsubarrays_onequad(a, h)
	
	#print subarrays["px"].shape
	#pxstds = np.std(subarrays["px"], axis=0)
	#pxmaxs = np.max(subarrays["px"], axis=0)
	#pxmins = np.min(subarrays["px"], axis=0)
	#pxups = pxmeds + pxstds
	#pxdowns = pxmeds - mp
	
	px_meds = np.median(subarrays["px"], axis=0)
	px_meds = scipy.ndimage.filters.gaussian_filter1d(px_meds, 50.0, mode='reflect')
	px_x = np.arange(len(px_meds))
	
	ox_meds = np.median(subarrays["ox"], axis=0)
	ox_meds = scipy.ndimage.filters.gaussian_filter1d(ox_meds, 50.0, mode='reflect')
	ox_x = np.arange(len(ox_meds))
	
	py_meds = np.median(subarrays["py"], axis=1)
	py_meds = scipy.ndimage.filters.gaussian_filter1d(py_meds, 50.0, mode='reflect')
	py_x = np.arange(len(py_meds))
	
	oy_meds = np.median(subarrays["oy"], axis=1)
	oy_meds = scipy.ndimage.filters.gaussian_filter1d(oy_meds, 50.0, mode='reflect')
	oy_x = np.arange(len(oy_meds))
	
	fig1 = plt.figure(figsize=(6,4))
	
	plt.plot(px_x, px_meds, "-", label = "px")
	plt.plot(ox_x, ox_meds, "-", label = "ox")
	plt.plot(py_x, py_meds, "-", label = "py")
	plt.plot(oy_x, oy_meds, "-", label = "oy")
	
	#print subarrays["ox"]
	
	reflevel = np.median(subarrays["px"].ravel())
	plt.ylim(reflevel-30, reflevel+30)
	
	plt.title("%s" % basename)
	plt.figtext(0.15, 0.75, "%s" %basename)
	plt.legend()
	fig1.savefig(filename)
	#plt.show()
	plt.close('all')
	
	

def biassub_onequad_poxcst(a, h):
	"""
	constant value, mean/median of px and ox
	UL seems pretty flat (that's why I choose it)
	For LL we need to subtract a ramp in px and ox.
	"""
	#print a.shape

	subarrays = getsubarrays_onequad(a, h)

	px_med = np.median(subarrays["px"].ravel())
	ox_med = np.median(subarrays["ox"].ravel())
	
	simplebiaslevel = (ox_med + px_med) / 2.0 # ok so this is a float
	blstd = np.std(np.concatenate((subarrays["px"].ravel(), subarrays["ox"].ravel())))
	
	bsdata = subarrays["data"] - simplebiaslevel
	
	h['PR_BSUBM']= ("biassub_onequad_poxcst", "pypr bias subtraction method")
	h['PR_BLEVL']= ("%.6f" % simplebiaslevel, "pypr median subtracted bias level")
	h['PR_BLSTD'] = ("%.6f" % blstd, "pypr standard deviation subtracted bias level")
	
	 		
	#print bsdata.dtype # was float32
	return (bsdata, h)
	

def biassub_onequad_poxsimpleramp(a, h):
	"""
	projected median and mean of px and ox
	"""
	
	subarrays = getsubarrays_onequad(a, h)

	ppx = np.median(subarrays["px"], axis = 0)
	pox = np.median(subarrays["ox"], axis = 0)
	# these are two vectors
	
	biasvect = (ppx + pox) / 2.0
	
	bsdata = subarrays["data"] - biasvect # array - column, this is done line by line.
	
	h['PR_BSUBM'] = ("biassub_onequad_poxsimpleramp", "pypr bias subtraction method")
	h['PR_BLEVL'] = ("%.6f" % np.median(biasvect), "pypr median subtracted bias level")		
	h['PR_BLSTD'] = ("%.6f" % np.std(biasvect), "pypr standard deviation subtracted bias level")
	
	#print bsdata.dtype # was float32
	return (bsdata, h)
	

def biassub_onequad_poycst(a, h):
	"""
	The simple mean of the median of prescan and overscan Y (!!!)
	This is for flats, where the X pre and overscans are affected by something like CTI
	It is used for all single quadrant flats in UL mode. For the others, one would have to check again how to do it best.
	"""
	
	subarrays = getsubarrays_onequad(a, h)

	py_med = np.median(subarrays["py"].ravel())
	oy_med = np.median(subarrays["oy"].ravel())
	
	simplebiaslevel = (oy_med + py_med) / 2.0 # ok so this is a float
	blstd = np.std(subarrays["py"].ravel()) # we skip oy here, as it has many bad pixel  rows
	
	bsdata = subarrays["data"] - simplebiaslevel
	
	h['PR_BSUBM'] = ( "biassub_onequad_poycst", "pypr bias subtraction method")
	h['PR_BLEVL'] = ("%.6f" % simplebiaslevel, "pypr median subtracted bias level")
	h['PR_BLSTD'] = ("%.6f" % blstd, "pypr standard deviation subtracted bias level")
	
	 		
	#print bsdata.dtype # was float32
	return (bsdata, h)
	


def biassub_ALL(a, h):
	"""
	The solution for ampname = ALL
	Warning we do change a !!!
	"""
	
	outarray = np.zeros(a.shape)
	
	for i in [1,2,3,4]:

		subarrays = getsubarrays_multiquad(a, h, i)
		
		ppx = np.median(subarrays["px"], axis=0)
		pox = np.median(subarrays["ox"], axis=0)
	
		biasx = (ppx + pox) / 2.0 # this is a vector containing the ramp
	
		bsdata = subarrays["data"] - biasx
		putsec(outarray, h["OGE DET OUT%i DATASEC" % i], bsdata)
			
	
	bsdata = outarray[100:4196, 100:4212] # we get the 4 datasecs = center of image.
	h['PR_BSUBM'] =( "biassub_ALL", "pypr bias subtraction method")
	h['PR_BLEVL'] = ("%.6f" % 0.0, "pypr median subtracted bias level")		
	h['PR_BLSTD'] = ( "%.6f" % 0.0, "pypr standard deviation subtracted bias level")
	
	
	#print bsdata.dtype # was float64
	return (bsdata, h)
	

"""
def hardgetsubarraysLL(a, h):
	
	#Special version for LL : overscan taken from header seems unreliable for some imagse, we hard code the region ???
	
	#This is a good image example :
	#HIERARCH OGE DET OUT1 DATASEC = '[51:4146,51:4162]' / d.datas1 / Data reg. out1
	#HIERARCH OGE DET OUT1 GAIN = 2.71000004 / d.gain1 / Output 1 meas. gain [e-/ADU]
	#HIERARCH OGE DET OUT1 ID = 'LL      ' / d.outid1 / Output1 ID [LL|LR|UL|UR]
	#HIERARCH OGE DET OUT1 OVSCXSEC = '[4147:4196,51:4162]' / d.ovscxs1 / X overscan
	#HIERARCH OGE DET OUT1 OVSCYSEC = '[51:4146,4163:4212]' / d.ovscys1 / Y overscan
	#HIERARCH OGE DET OUT1 PRSCXSEC = '[1:50,51:4162]' / d.prscxs1 / X prescan reg. o
	#HIERARCH OGE DET OUT1 PRSCYSEC = '[51:4146,1:50]' / d.prscys1 / Y prescan reg. o
	#HIERARCH OGE DET OUT1 X =   51 / d.outx1 / X Location of output 1
	#HIERARCH OGE DET OUT1 Y =   51 / d.outy1 / Y Location of output 1

	
	
	data = cutsec(a, "[51:4146,51:4162]")
	ox = cutsec(a, "[4147:4196,51:4162]")
	px = cutsec(a, "[1:50,51:4162]")
	oy = cutsec(a, "[51:4146,4163:4212]")
	py = cutsec(a, "[51:4146,1:50]")
	
	return {"data":data, "px":px, "ox":ox, "py":py, "oy":oy}
"""	
"""
def biassub(a, h, channel, hard=False):

	if hard:
		subarrays = hardgetsubarraysLL(a, h)
	else:
		subarrays = getsubarrays(a, h, channel)
		 
	ppx = np.median(subarrays["px"], axis=0)
	pox = np.median(subarrays["ox"], axis=0)
	
	biasx = (ppx + pox) / 2.0
	#print biasx.shape
	#print subarrays["data"].shape
	
	bsdata = subarrays["data"] - biasx
	
	biasmed = np.median(biasx)
	biasmean = np.mean(biasx)
	biasstd = np.std(biasx)
	
	if hard:
		putsec(a, "[51:4146,51:4162]", bsdata)
	else:
		putsec(a, h["OGE DET OUT%i DATASEC" % channel], bsdata)
	
	return 
"""
