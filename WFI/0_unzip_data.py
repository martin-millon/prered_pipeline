import os
import sys
from astropy.time import Time
import numpy as np

def main(dates, datapath, log_stream=None):
	if log_stream is None:
		outstream = sys.stdout
	else:
		outstream = log_stream

	print("#" * 15, " 0 Unzip ", "#" * 15, file=outstream)
	for date in dates:
		print("="*15, date, "="*15, file=outstream)
		datadir = os.path.join(datapath, date)
		os.chdir(datadir)
		os.system("gzip -d *.Z")

if __name__ == '__main__':
	# datapath = '/obs/lenses_EPFL/RAW/WFI_lenses/data_raw/'
	datapath = "./test_data_WFI/"

	datemin = "2019-10-23"
	datemax = "2019-10-23"
	starttime = Time(datemin, format='iso', scale='utc').mjd
	endtime = Time(datemax, format='iso', scale='utc').mjd

	days = np.arange(starttime, endtime + 1, 1)
	dates = [Time(d, format='mjd', scale='utc').iso[:10] for d in days]


	main(dates, datapath)





