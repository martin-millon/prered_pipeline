import os, sys
sys.path.append('..')
from astropy.io import fits
import numpy as np
from astropy.time import Time
from astropy.io.fits import getheader
from TelescopeUtils import f2n

"""
Generate the pngs of the flats.
"""

def make_MF_png(date, masterflat_dir, png_dir, redo=False):
    '''Create nice pngs of the masterflats and return '''

    destpath = os.path.join(png_dir, "masterflat_%s.png" % date)
    if os.path.isfile(destpath) and redo is True :
        print('Deleting previous png')
        os.remove(destpath)

    masterdomeflatpath = os.path.join(masterflat_dir,date, "masterdomeflat.fits")
    masterskyflatpath = os.path.join(masterflat_dir,date, "masterskyflat.fits")
    if os.path.isfile(masterskyflatpath):
        flat_type = 'Skyflat'
        masterflatpath = masterskyflatpath
    elif os.path.isfile(masterdomeflatpath):
        flat_type = 'Domeflat'
        masterflatpath = masterdomeflatpath
    else :
        print("Masterflat does not exist.")
        return "No masterflats for %s"%date

    h = getheader(masterflatpath)
    destpath = os.path.join(png_dir, "masterflat_%s.png" % date)

    if os.path.isfile(destpath) and redo is False:
        print("Masterflat png already exists.")
        return "%s : Masterflat png already exists."%date

    f2nimage = f2n.fromfits(masterflatpath, hdu=0, verbose=True)
    f2nimage.setzscale(0.9, 1.1)
    f2nimage.rebin(8)
    f2nimage.makepilimage(scale="lin", negative=False)
    f2nimage.writetitle("MF " + date)
    if 'NCOMBINE' in h.keys() :
        f2nimage.writeinfo(['Number of combined flats : %i'%(h['NCOMBINE']), flat_type])
    else :
        f2nimage.writeinfo(['Number of combined flats : ? ', flat_type])
    f2nimage.tonet(destpath)
    return "Png done for %s"%masterflatpath

def main(dates, workpath, pngpath, log_stream=None, redo = True):
    if log_stream is None:
        outstream = sys.stdout
    else:
        outstream = log_stream

    print("#"*15, " 3 Running diagnostics ", "#"*15, file = outstream)
    for date in dates:
        print("=" * 15, date, "=" * 15, file=outstream)
        s = make_MF_png(date, workpath, pngpath, redo=redo)
        print(s, file=outstream)





if __name__ == '__main__':
    datapath = '/obs/lenses_EPFL/RAW/WFI_lenses/data_raw/'
    #datapath = "./test_data_WFI/"
    workpath = '/obs/lenses_EPFL/PRERED/WFI/reduc/'
    #workpath = "./test_data_WFI/reduced/"
    pngpath = '/obs/lenses_EPFL/PRERED/WFI/diagnostics/'
    #pngpath = './test_data_WFI/pngs/'

    datemin = "2019-10-01"
    datemax = "2019-10-15"
    starttime = Time(datemin, format='iso', scale='utc').mjd
    endtime = Time(datemax, format='iso', scale='utc').mjd

    days = np.arange(starttime, endtime + 1, 1)
    dates = [Time(d, format='mjd', scale='utc').iso[:10] for d in days]

    main(dates, workpath, pngpath, redo = True)
