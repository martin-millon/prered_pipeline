import os
import shutil
import sys
import numpy as np
from astropy.time import Time
from astropy.io import fits

def check_flat(data, header):
    mean_level = np.mean(data)
    exp_time = header['EXPTIME']
    type = header['OBJECT']

    if mean_level >27000 or mean_level <12000 :
        return False

    if type == "FLAT,SCREEN" and exp_time >= 3.0 :
        return False

    return True

def check_bias(data,header, sizex_min = 2000, sizey_min = 2000 ):
    if np.shape(data)[0] < sizex_min :
        return False
    if np.shape(data)[1] < sizey_min :
        return False
    return True


def get_all_dates(start="2018-01-01", stop="2018-01-01"):
    '''
    return a list containing all the date from the start date
    :param start: string containing the start date
    :param stop: string containing the stop date
    :return: list of all the date between start and end date.
    '''

    starttime = Time(start, format='iso', scale='utc').mjd
    endtime = Time(stop, format='iso', scale='utc').mjd

    days = np.arange(starttime, endtime + 1, 1)
    dates = [Time(d, format='mjd', scale='utc').iso[:10] for d in days]
    return dates


def main(dates, datapath, workpath, observation_log, redo=True, log_stream=None, error_stream=None):
    if log_stream is None:
        outstream = sys.stdout
    else:
        outstream = log_stream

    if error_stream is None:
        errstream = sys.stderr
    else:
        errstream = error_stream

    failed_date=[]
    obslogline = []

    print("#" * 15, " 1 Reduction ", "#" * 15, file=outstream)
    for date in dates:
        print("=" * 15, date, "=" * 15, file=outstream)
        datadir = os.path.join(datapath, date)
        workdir = os.path.join(workpath, date)
        biasesdir = os.path.join(workdir, 'biases')
        skyflatsdir = os.path.join(workdir, 'skyflats')
        domeflatsdir = os.path.join(workdir, 'domeflats')
        sciencesdir = os.path.join(workdir, 'sciences')
        othersdir = os.path.join(workdir, 'others')

        alldirs = [workdir, biasesdir, skyflatsdir, domeflatsdir, sciencesdir, othersdir]

        if redo:
            if os.path.isdir(workdir):
                shutil.rmtree(workdir)

        for dirpath in alldirs:
            if not os.path.isdir(dirpath):
                os.mkdir(dirpath)

        filepaths = [f.split('\n')[0] for f in os.popen('ls %s/*.fits' % datadir).readlines()]

        print("Isolating chosen chip...", file=outstream)
        n_science = 0
        n_skyflat = 0
        n_domeflat = 0
        n_other = 0
        n_bias = 0
        obj_list = []
        for filepath in filepaths:

            filename = os.path.basename(filepath)

            print(filename, file=outstream)
            try :
                hdulist = fits.open(filepath)
                data = hdulist[3].data

            except Exception as e :
                print("Corrupted file %s : %s" %(filename, e), file=outstream)
                print("Corrupted file %s : %s"%(filename, e,), file=errstream)
                continue

            header = hdulist[0].header
            gender = header["OBJECT"]
            if "FOCUS" in gender or "DARK" in gender:
                # we skip
                continue

            # Science
            # if gender in sciencelist:
            # fits.writeto(os.path.join(sciencesdir, filename), data, header)
            if gender == "FLAT,SKY":
                fits.writeto(os.path.join(skyflatsdir, filename), data, header)
                n_skyflat +=1
            elif gender == "FLAT,SCREEN":
                n_domeflat +=1
                fits.writeto(os.path.join(domeflatsdir, filename), data, header)
            elif gender == "BIAS":
                n_bias +=1
                fits.writeto(os.path.join(biasesdir, filename), data, header)
            else:
                n_science +=1
                obj_list.append(gender)
                fits.writeto(os.path.join(sciencesdir, filename), data, header)

        obslogline.append("%s :  %i science images - %s \n"%(date,n_science,",".join(obj_list)))
        obslogline.append("%s :  %i sky flat images - %s \n" % (date, n_skyflat, ",".join(obj_list)))
        obslogline.append("%s :  %i dome flat images - %s \n" % (date, n_domeflat, ",".join(obj_list)))
        obslogline.append("%s :  %i bias images - %s \n" % (date, n_bias, ",".join(obj_list)))
        obslogline.append("%s :  %i other images - %s \n" % (date, n_other, ",".join(obj_list)))
        biasespaths = [f.split('\n')[0] for f in os.popen('ls %s/*.fits' % biasesdir).readlines()]
        skyflatspaths = [f.split('\n')[0] for f in os.popen('ls %s/*.fits' % skyflatsdir).readlines()]
        domeflatspaths = [f.split('\n')[0] for f in os.popen('ls %s/*.fits' % domeflatsdir).readlines()]
        sciencespaths = [f.split('\n')[0] for f in os.popen('ls %s/*.fits' % sciencesdir).readlines()]
        otherspaths = [f.split('\n')[0] for f in os.popen('ls %s/*.fits' % othersdir).readlines()]

        # get a list of all dates until the today :
        all_dates = get_all_dates(start='2018-01-01', stop=date)


        ### MASTERBIASES ###
        print("Create masterbias...", file=outstream)
        biasarrays = []
        masterbiasdata = None
        if len(biasespaths) >= 6:
            masterbiasheader = {}
            biascount = 0
            for i,biaspath in enumerate(biasespaths):
                try : 
                    bias = fits.open(biaspath)
                except Exception as e :
                    print("Problem with bias %s : %s" %(biaspath, e), file=outstream)
                    print("Problem with bias %s : %s" %(biaspath, e), file=errstream)
                    continue
                if check_bias(bias[0].data,bias[0].header):
                    biasarrays.append(bias[0].data)
                    biascount +=1
                    if biascount == 1:
                        masterbiasheader = bias[0].header
                        masterbiasheader['BIAS1'] = os.path.basename(biaspath)
                    else:
                        masterbiasheader['BIAS%i' % (i + 1)] = os.path.basename(biaspath)

            masterbiasheader['NCOMBINE'] = biascount
            masterbiasheader['OBJECT'] = 'MASTERBIAS'

            if biascount >=5 :
                masterbiasdata = np.median(biasarrays, axis=0)
                masterbias = fits.PrimaryHDU(data=masterbiasdata, header = masterbiasheader)
                masterbias.writeto(os.path.join(workdir, 'masterbias.fits'))
            else :
                print("\tNot enough exposures !", file=outstream)
        else:
            print("\tNot enough exposures !", file=outstream)

        print("Apply masterbias...", file=outstream)
        if os.path.isfile(os.path.join(workdir, 'masterbias.fits')):
            pass
        else:
            print("\tNo biases for tonight !", file=outstream)
            print("\tI look in previous nights folders...", file=outstream)
            for pdate in all_dates[::-1]:
                if os.path.isfile(os.path.join(workpath, pdate, 'masterbias.fits')):
                    masterbiasdata = fits.open(os.path.join(workpath, pdate, 'masterbias.fits'))[0].data
                    print("\t\tI found a masterbias in %s" % pdate, file=outstream)
                    break

        if masterbiasdata is None :
            print("No masterbias was found. Skipping %s"%date, file=outstream)
            print("No masterbias was found. Skipping %s"%date, file=errstream)
            failed_date.append(date)
            continue

        for subpaths in [skyflatspaths, domeflatspaths, sciencespaths, otherspaths]:
            for filepath in subpaths:
                try :
                    file = fits.open(filepath)  # we use readonly mode because it's much faster.
                    newdata = file[0].data - masterbiasdata
                    header = file[0].header
                    os.remove(filepath)
                    fits.writeto(filepath, newdata, header)
                except Exception as e :
                    print("ERROR in fbias subtraction : %s"%e, file=outstream)
                    print(e, file=errstream)
                    failed_date.append(date)

        ### SKYFLATS  ###
        print("Create skyflats...", file=outstream)
        flatsarrays = []
        masterskyflatdata = None
        if len(skyflatspaths) >= 3:
            masterskyheader = {}
            skyflatcount = 0
            for i,flatpath in enumerate(skyflatspaths):
                flat = fits.open(flatpath)
                flatdata = flat[0].data
                flatheader = flat[0].header
                if check_flat(flatdata, flatheader):
                    flatsarrays.append(flatdata / np.median(flatdata))
                    skyflatcount +=1
                    if skyflatcount == 1:
                        masterskyheader = flatheader
                        masterskyheader['FLAT1'] = os.path.basename(flatpath)
                    else :
                        masterskyheader['FLAT%i'%(i+1)] =os.path.basename(flatpath)
                else :
                    print("Rejecting sky flat %s"%flatpath, file=outstream)

            if skyflatcount >= 3 :
                masterskyheader['NCOMBINE'] = skyflatcount
                masterskyheader['OBJECT'] = 'MASTERFLAT,SKY'
                masterskyflatdata = np.median(flatsarrays, axis=0)
                masterskyflat = fits.PrimaryHDU(data=masterskyflatdata, header=masterskyheader )
                masterskyflat.writeto(os.path.join(workdir, 'masterskyflat.fits'))
            else :
                print("\tNot enough exposures !", file=outstream)
        else:
            print("\tNot enough exposures !", file=outstream)

        ### DOMEFLATS ###
        print("Create domeflats...", file=outstream)
        flatsarrays = []
        masterdomeflatdata = None
        if len(domeflatspaths) >= 3:
            masterdomeheader ={}
            domeflatcount = 0
            for i, flatpath in enumerate(domeflatspaths):
                flat = fits.open(flatpath)
                flatdata = flat[0].data
                flatheader = flat[0].header
                if check_flat(flatdata,flatheader):
                    flatsarrays.append(flatdata / np.median(flatdata))
                    domeflatcount +=1
                    if domeflatcount == 1 :
                        masterdomeheader = flatheader
                        masterdomeheader['FLAT1'] = os.path.basename(flatpath)
                    else :
                        masterdomeheader['FLAT%i'%(i+1)] =os.path.basename(flatpath)
                else :
                    print("Rejecting dome flat %s"%flatpath, file=outstream)

            if domeflatcount >= 3 :
                masterdomeheader['NCOMBINE'] = domeflatcount
                masterdomeheader['OBJECT'] = 'MASTERFLAT,SCREEN'
                masterdomeflatdata = np.median(flatsarrays, axis=0)
                masterdomeflat = fits.PrimaryHDU(data=masterdomeflatdata, header=masterdomeheader)
                masterdomeflat.writeto(os.path.join(workdir, 'masterdomeflat.fits'))
            else :
                print("\tNot enough exposures !", file=outstream)
        else:
            print("\tNot enough exposures !", file=outstream)

        ### APPLY MASTERFLAT ###
        print("Apply masterflat...", file=outstream)
        masterflat = None
        if os.path.isfile(os.path.join(workdir, 'masterskyflat.fits')):
            masterflat = masterskyflatdata
        elif os.path.isfile(os.path.join(workdir, 'masterdomeflat.fits')):
            masterflat = masterdomeflatdata
        else:
            print("\tNo flats for tonight !", file=outstream)
            print("\tI look in previous nights folders...", file=outstream)
            for pdate in all_dates[::-1]:
                if os.path.isfile(os.path.join(workpath, pdate, 'masterskyflat.fits')):
                    masterflat = fits.open(os.path.join(workpath, pdate, 'masterskyflat.fits'))[0].data
                    print("\t\tI found a sky flat in %s" % pdate, file=outstream)
                    break
                elif os.path.isfile(os.path.join(workpath, pdate, 'masterdomeflat.fits')):
                    masterflat = fits.open(os.path.join(workpath, pdate, 'masterdomeflat.fits'))[0].data
                    print("\t\tI found a dome flat in %s" % pdate, file=outstream)
                    break

        if masterflat is None :
            print("No masterflat was found. Skipping %s"%date, file=outstream)
            print("No masterflat was found. Skipping %s"%date, file=errstream)
            failed_date.append(date)
            continue

        for subpaths in [sciencespaths, otherspaths]:
            for filepath in subpaths:
                try :
                    img = fits.open(filepath)
                    imgdata = img[0].data
                    imgheader = img[0].header
                    ffimgdata = imgdata / masterflat

                    # now crop the data
                    ffimgdata = ffimgdata[60:]  # cut the bottom
                    ffimgdata = np.transpose(ffimgdata)
                    ffimgdata = ffimgdata[60:-60]  # cut left and right
                    ffimgdata = np.transpose(ffimgdata)

                    os.remove(filepath)
                    fits.writeto(filepath, ffimgdata, imgheader)
                except Exception as e :
                    print("ERROR in flat-fielding : %s"%e, file=outstream)
                    print(e, file=errstream)
                    failed_date.append(date)

    logtxt = "".join(obslogline)
    log_night = open(observation_log, "a")
    log_night.write(logtxt)
    log_night.close()

    print("Done !", file=outstream)
    if len(failed_date) > 0 :
        print("I failed for the folowing date : ", failed_date, file=outstream)



if __name__ == '__main__':
    datapath = '/obs/lenses_EPFL/RAW/WFI_lenses/data_raw/'
    # datapath = "./test_data_WFI/"
    workpath = '/obs/lenses_EPFL/PRERED/WFI/reduc/'
    # workpath = "./test_data_WFI/reduced/"
    # observation_log = './test_data_WFI/observation_log.log'
    log_directory = '/home/astro/millon/Desktop/PRERED/WFI/pipe_log/'
    observation_log = os.path.join(log_directory, 'observation_log.log')

    datemin = "2021-01-03"
    datemax = "2021-03-24"
    starttime = Time(datemin, format='iso', scale='utc').mjd
    endtime = Time(datemax, format='iso', scale='utc').mjd

    days = np.arange(starttime, endtime + 1, 1)
    dates = [Time(d, format='mjd', scale='utc').iso[:10] for d in days]

    fail_date = main(dates, datapath, workpath, observation_log, redo=True)
