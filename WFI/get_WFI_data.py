import os
import sys
import shutil
from astropy.time import Time
import numpy as np
import glob
import datetime


def main(dates, datapath, redo=False, username='martinM', password=None, log_stream=None, error_stream = None):
    if log_stream is None:
        outstream = sys.stdout
    else:
        outstream = log_stream

    if error_stream is None:
        errstream = sys.stderr
    else:
        errstream = error_stream

    if password is None:
        raise RuntimeError('Please provide your ESO logins.')


    print("#" * 15, "  Downloading data ", "#" * 15, file=outstream)
    rundir = os.path.dirname(__file__)
    msg = "Problem with the following folders:"

    for date in dates:
        print("=" * 3, date, "=" * 3)

        nightpath = os.path.join(datapath, date)

        if os.path.isdir(nightpath) and redo is True:
            shutil.rmtree(nightpath)
            print("Deleting existing reduction...")
            os.mkdir(nightpath)
        elif os.path.isdir(nightpath) and redo is False:
            print("Already existing reduction, skipping this night.")
            continue
        else : 
            os.mkdir(nightpath)
        os.system('cp my_prog_access.sh %s/' % nightpath)
        os.chdir(nightpath)
        os.system("./my_prog_access.sh -User '%s' -Passw '%s' -Night '%s'" %(username, password, date))

        #check the downloaded files :
        try:
            # how many files according to the sh script ?
            lines = open(glob.glob("*downloadRequest*.sh")[0], "r").readlines()
            nfiles = len([l for l in lines if "https" in l])
        except:
            nfiles = 0

        # how many ddl files ?
        nddl = len(glob.glob("*.fits*"))
        if nddl != nfiles:
            msg += "\n" + nightpath + ": " + str(nddl) + "/" + str(nfiles)

        os.chdir(rundir)

    if msg != "Problem with the following folders:":
        print("Reduction starting on ", datetime.datetime.now(), file=errstream)
        print(msg, file=errstream)


if __name__ == '__main__':
    datapath = '/obs/lenses_EPFL/RAW/WFI_lenses/data_raw/'
    # datapath = "./test_data_WFI/"

    datemin = "2020-03-01"
    datemax = "2020-03-01"
    starttime = Time(datemin, format='iso', scale='utc').mjd
    endtime = Time(datemax, format='iso', scale='utc').mjd

    days = np.arange(starttime, endtime + 1, 1)
    dates = [Time(d, format='mjd', scale='utc').iso[:10] for d in days]

    main(dates, datapath, redo=True)
