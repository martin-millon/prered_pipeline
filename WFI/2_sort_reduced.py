import os, sys
from astropy.io import fits
import numpy as np
from astropy.time import Time

"""
Sort the reduced data - simply put the reduced images in a single directory per lens, ready to be imported in cosmouline.
"""


def main(dates, workpath, reducpath, log_stream=None):
    if log_stream is None:
        outstream = sys.stdout
    else:
        outstream = log_stream


    print("#" * 15, " 2 Sorting ", "#" * 15, file=outstream)
    for date in dates:
        print("=" * 15, date, "=" * 15, file=outstream)
        print("Sorting files...", file=outstream)
        workdir = os.path.join(workpath, date)
        sciencesdir = os.path.join(workdir, 'sciences')
        othersdir = os.path.join(workdir, 'others')

        scifiles = [d.split('\n')[0] for d in os.popen('ls %s/*.fits' % sciencesdir).readlines()]
        othersfiles = [d.split('\n')[0] for d in os.popen('ls %s/*.fits' % othersdir).readlines()]

        for allfiles in [scifiles, othersfiles]:
            for file in allfiles:
                header = fits.open(file)[0].header
                gender = header["OBJECT"]

                genderdir = os.path.join(reducpath, gender)
                if not os.path.isdir(genderdir):
                    os.mkdir(genderdir)
                os.system('cp %s %s' % (file, os.path.join(genderdir, os.path.basename(file))))

if __name__ == '__main__':
    # workpath = '/obs/lenses_EPFL/PRERED/WFI/reduc/'
    workpath = "./test_data_WFI/reduced/"
    # reducpath = '/obs/lenses_EPFL/PRERED/WFI/sorted/'
    reducpath = "./test_data_WFI/sorted/"

    datemin = "2019-10-10"
    datemax = "2019-10-10"
    starttime = Time(datemin, format='iso', scale='utc').mjd
    endtime = Time(datemax, format='iso', scale='utc').mjd

    days = np.arange(starttime, endtime + 1, 1)
    dates = [Time(d, format='mjd', scale='utc').iso[:10] for d in days]

    main(dates, workpath, reducpath)
