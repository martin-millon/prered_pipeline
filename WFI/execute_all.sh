#!/bin/sh -l

module add astro

export PATH=/home/astro/millon/.local/bin:$PATH

export PYTHONPATH=/home/astro/millon/Desktop/PyCS:$PYTHONPATH
export PYTHONPATH=${PYTHONPATH}:/home/astro/millon/Desktop/Mod_challenge/lenstronomy_extension/
export PYTHONPATH=${PYTHONPATH}:/home/astro/millon/Desktop/Mod_challenge/lenstronomy/
export PYTHONPATH=${PYTHONPATH}:/home/astro/millon/Desktop/tdlmcpipeline/
export ftp_proxy=http://proxy.unige.ch:3128
export http_proxy=http://proxy.unige.ch:3128
export https_proxy=http://proxy.unige.ch:3128

export PYTHONPATH=${PYTHONPATH}:/home/astro/millon/.local/lib/python3.7/site-packages/

date
echo 'Runing shell script'

cd /home/astro/millon/Desktop/PRERED/WFI/

python3 run_all.py --start 2021-10-26 --end 2022-01-06
