# Pipeline for WFI reduction.

Pipeline for WFI reduction. We keep only the reduced frames, the masterflats and the masterbias. The rest can be easily re-downloaded from the ESO archive. 

## Get WFI data from ESO server
SCript to query the ESO database. It will download ANy change in the reuested files should be done in the `my_prog_access.sh` script. 

## 0_unzip_data
unzip the downloaded data with `gzip` command.

## 1_reduc_data 
Masterbias and Masterflat construction. Also apply the masterbais and master flat to the data. Skyflats are used in priority and then domeflats. If there is no calibration this night, it will look for masterflats in the previous nights. 

## 2_sort_reduced
Sort the data per target

## 3_diagnostics 
MAke the pngs of the masterflat. Other diagnostics test can be implemented here. 

## 4_cleanup
Remove the unecessary files to save disk space. We keep only the reduced frames, the masterflats and the masterbias. 

## run_all 
You can also run all the scrit at once, by specifying the start and end date with the --start and --end optionnal argument. Date format is YYYY-MM-DD. Without argument the pipeline will run for the previous night. 

## execute_all.sh 
This is wrapper in bash to call for run_all.py. It loads all the necessary module on lesta and execute the python scripts. It can be call with crontab for automatic run everyday. To edit the crontab command : 
    
    export EDITOR=nano
    crontab -e 