import os
import sys
import shutil
from astropy.time import Time
import numpy as np

def main(dates, datapath, redo=False, log_stream=None, error_stream=None):
    if log_stream is None:
        outstream = sys.stdout
    else:
        outstream = log_stream

    if error_stream is None:
        errstream = sys.stderr
    else:
        errstream = error_stream


    print("#" * 15, "  Downloading data ", "#" * 15, file=outstream)

    for date in dates:
        print("=" * 3, date, "=" * 3, file=outstream)

        nightpath = os.path.join(datapath, date)

        if os.path.isdir(nightpath) and redo is True:
            shutil.rmtree(nightpath)
            print("Deleting existing downloaded data...", file=outstream)
        elif os.path.isdir(nightpath) and redo is False:
            print("Already existing downloaded files, skipping this night.", file=outstream)
            continue
        else :
            os.mkdir(nightpath)

        os.system('scp -r vattobs@vatt.as.arizona.edu:/mnt/TBArray/images/cosmograil/%s %s'%(date, datapath))
        #TODO : download the log and check that the download was complete !!!
        print('Download complete for the night %s'%date)

if __name__ == '__main__':
    # datapath = '/obs/lenses_EPFL/RAW/VATT/'
    datapath =  "/Users/martin/Desktop/COSMOULINE/data_raw/VATT_test/"

    datemin = "2019-11-19"
    datemax = "2019-11-19"
    starttime = Time(datemin, format='iso', scale='utc').mjd
    endtime = Time(datemax, format='iso', scale='utc').mjd

    days = np.arange(starttime, endtime + 1, 1)
    dates = [Time(d, format='mjd', scale='utc').iso[:10] for d in days]

    main(dates, datapath, redo=True)