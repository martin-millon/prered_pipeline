import os, sys
from astropy.io import fits
import numpy as np
from astropy.time import Time

"""
Remove the unecessary files to save disk space. We keep only the reduced frames, the masterflats and the masterbias. 
"""


def main(dates, datapath, workpath, log_stream=None):
    if log_stream is None:
        outstream = sys.stdout
    else:
        outstream = log_stream

    print("#" * 15, " 4 Cleaning ", "#" * 15, file=outstream)
    for date in dates:
        print("=" * 15, date, "=" * 15, file=outstream)
        # We keep raw
        # raw_data = os.path.join(datapath, date, "*")
        # os.system("rm %s"%raw_data)

        workdir = os.path.join(workpath, date)
        biases = os.path.join(workdir, 'biases', '*')
        skyflats = os.path.join(workdir, 'flats', '*')
        sciences = os.path.join(workdir, 'science', '*')
        others = os.path.join(workdir, 'other', '*')

        os.system("rm %s" % biases)
        os.system("rm %s" % skyflats)
        os.system("rm %s" % sciences)
        os.system("rm %s" % others)


if __name__ == '__main__':
    # datapath = '/obs/lenses_EPFL/RAW/VATT/'
    datapath = "/Users/martin/Desktop/COSMOULINE/data_raw/VATT_test/"
    # workpath = '/obs/lenses_EPFL/PRERED/VATT/reduc'
    workpath = "/Users/martin/Desktop/COSMOULINE/data_raw/VATT_test/reduced"

    datemin = "2019-11-19"
    datemax = "2019-11-19"
    starttime = Time(datemin, format='iso', scale='utc').mjd
    endtime = Time(datemax, format='iso', scale='utc').mjd

    days = np.arange(starttime, endtime + 1, 1)
    dates = [Time(d, format='mjd', scale='utc').iso[:10] for d in days]

    main(dates, datapath, workpath)
