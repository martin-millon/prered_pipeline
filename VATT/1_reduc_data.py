import os
import shutil
import sys
sys.path.append('..')
import numpy as np
from astropy.time import Time
from astropy.io import fits
from TelescopeUtils.utils import check_flat
from TelescopeUtils.utils import get_all_dates
from TelescopeUtils.diagnostics_util import run_sextractor

def main(dates, workpath, pixsize=0.188, saturlevel=65000, sextractor_command='sex', reduction_log=None, redo=True, log_stream=None, error_stream=None):
    if log_stream is None:
        outstream = sys.stdout
    else:
        outstream = log_stream

    if error_stream is None:
        errstream = sys.stderr
    else:
        errstream = error_stream

    if reduction_log is None:
        pipedir = os.path.dirname(__file__)
        if not os.path.exists(os.path.join(pipedir, 'pipe_log')):
            os.mkdir(os.path.join(pipedir, 'pipe_log'))
        reduction_log = os.path.join(pipedir, 'pipe_log', 'reduction_log.txt')

    failed_date = []

    print("#" * 15, " 1 Reduction ", "#" * 15, file=outstream)
    for date in dates:
        workdir = os.path.join(workpath, date)
        biasdir = os.path.join(workdir, 'biases')
        flatdir = os.path.join(workdir, 'flats')
        sciencedir = os.path.join(workdir, 'science')
        otherdir = os.path.join(workdir, 'other')

        biasespaths = [f.split('\n')[0] for f in os.popen('ls %s/*.fits' % biasdir).readlines()]
        flatspaths = [f.split('\n')[0] for f in os.popen('ls %s/*.fits' % flatdir).readlines()]
        sciencespaths = [f.split('\n')[0] for f in os.popen('ls %s/*.fits' % sciencedir).readlines()]
        otherspaths = [f.split('\n')[0] for f in os.popen('ls %s/*.fits' % otherdir).readlines()]

        all_dates = get_all_dates(start='2018-01-01', stop=date)

        ### MASTERBIASES ###
        if os.path.isfile(os.path.join(workdir, 'masterbias.fits')) and redo is False :
            print("Masterbias already exists", file=outstream)
            masterbiasdata = fits.getdata(os.path.join(workdir, 'masterbias.fits'))
            masterbiasheader = fits.getheader(os.path.join(workdir, 'masterbias.fits'))
        else :
            # TODO : implement overscan
            print("Create masterbias...", file=outstream)
            biasarrays = []
            masterbiasdata = None
            if len(biasespaths) >= 4:
                masterbiasheader = []
                for i, biaspath in enumerate(biasespaths):
                    bias = fits.open(biaspath)
                    biasarrays.append(bias[0].data)
                    if i == 0:
                        masterbiasheader = bias[0].header
                        masterbiasheader['BIAS1'] = os.path.basename(biaspath)
                    else:
                        masterbiasheader['BIAS%i' % (i + 1)] = os.path.basename(biaspath)

                masterbiasheader['NCOMBINE'] = len(biasespaths)
                masterbiasheader['OBJECT'] = 'MASTERBIAS'

                masterbiasdata = np.median(biasarrays, axis=0)
                # masterbiasdata = np.flip(masterbiasdata , axis=0)
                # masterbiasdata = remove_bad_margins(masterbiasdata, num_pixels=20)
                masterbias = fits.PrimaryHDU(data=masterbiasdata, header=masterbiasheader)
                masterbias.writeto(os.path.join(workdir, 'masterbias.fits'), overwrite=redo)
            else:
                print("\tNot enough exposures !", file=outstream)

        ### APPLY MASTERBIAS ###
        print("Apply masterbias...", file=outstream)
        if os.path.isfile(os.path.join(workdir, 'masterbias.fits')):
            pass
        else:
            print("\tNo biases for tonight !", file=outstream)
            print("\tI look in previous nights folders...", file=outstream)
            for pdate in all_dates[::-1]:
                if os.path.isfile(os.path.join(workpath, pdate, 'masterbias.fits')):
                    masterbiasdata = fits.open(os.path.join(workpath, pdate, 'masterbias.fits'))[0].data
                    print("\t\tI found a masterbias in %s" % pdate, file=outstream)
                    break

        if masterbiasdata is None:
            print("No masterbias was found. Skipping %s" % date, file=outstream)
            print("No masterbias was found. Skipping %s" % date, file=errstream)
            failed_date.append(date)
            continue

        for subpaths in [flatspaths, sciencespaths, otherspaths]:
            for filepath in subpaths:
                try:
                    file = fits.open(filepath)  # we use readonly mode because it's much faster.
                    newdata = file[0].data - masterbiasdata
                    header = file[0].header
                    os.remove(filepath)
                    fits.writeto(filepath, newdata, header, overwrite=redo)
                except Exception as e:
                    print("ERROR in fbias subtraction : %s" % e, file=outstream)
                    print(e, file=errstream)
                    failed_date.append(date)

        ### FLATS  ###
        if os.path.isfile(os.path.join(workdir, 'masterflat.fits')) and redo is False :
            print("Masterflat already exists", file=outstream)
            mastersflatdata = fits.getdata(os.path.join(workdir, 'masterflat.fits'))
        else :
            print("Create masterflats...", file=outstream)
            flatsarrays = []
            mastersflatdata = None
            if len(flatspaths) >= 3:
                masterheader = []
                flatcount = 0
                for i, flatpath in enumerate(flatspaths):
                    flat = fits.open(flatpath)
                    flatdata = flat[0].data
                    flatheader = flat[0].header
                    if check_flat(flatdata, flatheader):
                        flatsarrays.append(flatdata / np.median(flatdata))
                        flatcount += 1
                        if flatcount == 1:
                            masterheader = flatheader
                            masterheader['FLAT1'] = os.path.basename(flatpath)
                        else:
                            masterheader['FLAT%i' % (i + 1)] = os.path.basename(flatpath)
                    else:
                        print("Rejecting sky flat %s" % flatpath, file=outstream)

                if flatcount > 0:
                    masterheader['NCOMBINE'] = flatcount
                    masterheader['OBJECT'] = 'MASTERFLAT,SKY'
                    mastersflatdata = np.median(flatsarrays, axis=0)
                    # mastersflatdata = np.flip(mastersflatdata, axis=0)
                    # mastersflatdata = remove_bad_margins(mastersflatdata, num_pixels=20)
                    mastersflat = fits.PrimaryHDU(data=mastersflatdata, header=masterheader)
                    mastersflat.writeto(os.path.join(workdir, 'masterflat.fits'), overwrite=redo)
                else:
                    print("\tNot enough exposures !", file=outstream)
            else:
                print("\tNot enough exposures !", file=outstream)

        ### APPLY MASTERFLAT ###
        print("Apply masterflat...", file=outstream)
        masterflat = None
        if os.path.isfile(os.path.join(workdir, 'masterflat.fits')):
            masterflat = mastersflatdata
        else:
            print("\tNo flats for tonight !", file=outstream)
            print("\tI look in previous nights folders...", file=outstream)
            for pdate in all_dates[::-1]:
                if os.path.isfile(os.path.join(workpath, pdate, 'mastersflat.fits')):
                    masterflat = fits.open(os.path.join(workpath, pdate, 'mastersflat.fits'))[0].data
                    print("\t\tI found a sky flat in %s" % pdate, file=outstream)
                    break

        if masterflat is None:
            print("No masterflat was found. Skipping %s" % date, file=outstream)
            print("No masterflat was found. Skipping %s" % date, file=errstream)
            failed_date.append(date + ", ")
            continue

        for subpaths in [sciencespaths, otherspaths]:
            for filepath in subpaths:
                try:
                    img = fits.open(filepath)
                    imgdata = img[0].data
                    imgheader = img[0].header
                    if 'FLATFIELD' in imgheader : 
                        print("%s is already flatfielded"%filepath, file=outstream)
                        continue
			
                    ffimgdata = imgdata / masterflat

                    # now crop the data
                    ffimgdata = np.flip(ffimgdata, axis=0)
                    ffimgdata = np.nan_to_num(ffimgdata)
                    # ffimgdata = remove_bad_margins(ffimgdata, num_pixels=20)

                    catfilename = os.path.join(os.path.dirname(filepath), os.path.basename(filepath) + ".cat")
                    sexdic = run_sextractor(filepath, catfilename, pixsize=pixsize, saturlevel=saturlevel,
                                            sextractor_command=sextractor_command, checkplots=False, runsex=True)

                    imgheader['SEEING'] = sexdic['seeing']
                    imgheader['ELL'] = sexdic['ellipticity']
                    imgheader['GOODSTARS'] = sexdic['goodstars']
                    imgheader['B_AXIS'] = sexdic['B_axis']
                    imgheader['A_AXIS'] = sexdic['A_axis']
                    imgheader['THETA'] = sexdic['Theta']
                    imgheader['FLATFIELD'] = 'Done'
                    os.remove(filepath)
                    fits.writeto(filepath, ffimgdata, imgheader, overwrite=redo)

                except Exception as e:
                    print("ERROR in flat-fielding : %s" % e, file=outstream)
                    print(e, file=errstream)
                    failed_date.append(date + ", ")

    print("Done !", file=outstream)
    if len(failed_date) > 0:
        print("I failed for the folowing date : ", failed_date, file=outstream)
        logtxt = "I failed for the folowing date : " + "".join(failed_date) + "\n"
        log_night = open(reduction_log, "a")
        log_night.write(logtxt)
        log_night.close()


if __name__ == '__main__':
    workpath = '/obs/lenses_EPFL/PRERED/VATT/reduc'
    #workpath = "/Users/martin/Desktop/COSMOULINE/data_raw/VATT_test/reduced"
    #reduction_log = './test_data_WFI/reduction_log.log'
    log_directory = '/Users/martin/Desktop/prered_pipeline/VATT/pipe_log/'
    reduction_log = os.path.join(log_directory, 'reduction_log.log')

    datemin = "2019-12-22"
    datemax = "2019-12-22"
    starttime = Time(datemin, format='iso', scale='utc').mjd
    endtime = Time(datemax, format='iso', scale='utc').mjd

    days = np.arange(starttime, endtime + 1, 1)
    dates = [Time(d, format='mjd', scale='utc').iso[:10] for d in days]

    fail_date = main(dates, workpath, reduction_log=reduction_log, redo=False)
