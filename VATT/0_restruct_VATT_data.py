import os
import sys
import shutil
from astropy.time import Time
import numpy as np
import glob
from astropy.io import fits


def gather_fits_file(data_list, margin=0):
    """
    Gather data frame along the x axis
    :param data_list: list of array containing the images
    :param margin : gap with zeros between the frame
    :return: a numpy array containing all the data
    """
    x_arr = []
    x_index = [0]
    y_arr = []
    lx = 0
    ly = 0
    for i, d in enumerate(data_list):
        x, y = np.shape(d)
        x_arr.append(x)
        x_index.append(x_index[i] + x + margin)
        y_arr.append(y)
        if y > ly:
            ly = y
        lx += x
        if i > 0:
            lx += margin

    data = np.zeros((lx, ly))
    for i, d in enumerate(data_list):
        data[x_index[i]:x_index[i] + x_arr[i], 0:y_arr[i]] = d

    print("Image output shape : ", np.shape(data))

    return data


def invert_y(data):
    # TODO : write this

    return data


def main(dates, datapath, workpath, redo=False, log_stream=None, error_stream=None, log_dir=None):
    if log_stream is None:
        outstream = sys.stdout
    else:
        outstream = log_stream

    if error_stream is None:
        errstream = sys.stderr
    else:
        errstream = error_stream

    if log_dir is None:
        pipedir = os.path.dirname(__file__)
        log_dir = os.path.join(pipedir, 'pipe_log')

    if not os.path.exists(log_dir):
        os.mkdir(log_dir)

    for date in dates:
        observation_log = os.path.join(log_dir, 'night_log_%s.txt' % date)
        print("=" * 3, date, "=" * 3)
        obslogline = []
        obslogline.append("=" * 3 + date + "=" * 3 + "\n")

        nightpath = os.path.join(datapath, date)
        nightworkpath = os.path.join(workpath, date)
        biasdir = os.path.join(nightworkpath, 'biases')
        flatdir = os.path.join(nightworkpath, 'flats')
        sciencedir = os.path.join(nightworkpath, 'science')
        otherdir = os.path.join(nightworkpath, 'other')
        n_science = 0
        n_flat = 0
        n_bias = 0
        n_other = 0
        obj_list = []

        print(nightworkpath)

        if os.path.isdir(nightworkpath) and redo is True:
            shutil.rmtree(nightworkpath)
            print("Deleting existing reduction...")
        elif os.path.isdir(nightworkpath) and redo is False:
            print("Already existing reduction, skipping this night.")
            continue

        if not os.path.isdir(nightworkpath):
            os.mkdir(nightworkpath)

        filepaths = glob.glob(os.path.join(nightpath, "*.fits"))

        for filepath in filepaths:
            if not os.path.isdir(biasdir):
                os.mkdir(biasdir)
            if not os.path.isdir(flatdir):
                os.mkdir(flatdir)
            if not os.path.isdir(sciencedir):
                os.mkdir(sciencedir)
            if not os.path.isdir(otherdir):
                os.mkdir(otherdir)

            filename = os.path.basename(filepath)
            if filename == "test.fits":
                continue

            print(filename, file=outstream)

            hdulist = fits.open(filepath)
            header = hdulist[0].header
            data1 = hdulist[1].data
            data2 = hdulist[2].data
            data = gather_fits_file([data1, data2], margin=20)
            data = invert_y(data)
            gender = header["IMAGETYP"]

            if gender == "flat" or "flat" in filename :
                n_flat += 1
                fits.writeto(os.path.join(flatdir, filename), data, header)
            elif gender == "bias" or gender == "zero":
                n_bias += 1
                fits.writeto(os.path.join(biasdir, filename), data, header)
            elif gender == "object":
                n_science += 1
                fits.writeto(os.path.join(sciencedir, filename), data, header)
            else:
                n_other += 1
                obj_list.append(gender)
                fits.writeto(os.path.join(otherdir, filename), data, header)

        obslogline.append("%s :  %i science images - %s \n" % (date, n_science, ",".join(obj_list)))
        obslogline.append("%s :  %i flat images - %s \n" % (date, n_flat, ",".join(obj_list)))
        obslogline.append("%s :  %i bias images - %s \n" % (date, n_bias, ",".join(obj_list)))
        obslogline.append("%s :  %i other images - %s \n" % (date, n_other, ",".join(obj_list)))

        logtxt = "".join(obslogline)
        log_night = open(observation_log, "a")
        log_night.write(logtxt)
        log_night.close()


if __name__ == '__main__':
    datapath = '/obs/lenses_EPFL/RAW/VATT/'
    #datapath = "/Users/martin/Desktop/COSMOULINE/data_raw/VATT_test"
    workpath = '/obs/lenses_EPFL/PRERED/VATT/reduc'
    #workpath = "/Users/martin/Desktop/COSMOULINE/data_raw/VATT_test/reduced"

    datemin = "2019-12-21"
    datemax = "2019-12-21"
    starttime = Time(datemin, format='iso', scale='utc').mjd
    endtime = Time(datemax, format='iso', scale='utc').mjd

    days = np.arange(starttime, endtime + 1, 1)
    dates = [Time(d, format='mjd', scale='utc').iso[:10] for d in days]

    main(dates, datapath, workpath, redo=True)
