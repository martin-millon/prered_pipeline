import os
import datetime
from astropy.time import Time
import numpy as np
import argparse as ap

get_data = __import__('download_VATT')
restruct = __import__('0_restruct_VATT_data')
reduc = __import__('1_reduc_data')
sort = __import__('2_sort_reduced')
diag = __import__('3_diagnostics')
cleanup = __import__('4_clean_up')


def main(dates):
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    #           GENERAL CONFIGURATION
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

    datapath = '/obs/lenses_EPFL/RAW/VATT/'  # where the raw data are downloaded
    workpath = '/obs/lenses_EPFL/PRERED/VATT/reduc'  # where the intermediate steps of the reduction are stored
    reducpath = '/obs/lenses_EPFL/PRERED/VATT/sorted/'  # where the reduced files are stored
    pngpath = '/obs/lenses_EPFL/PRERED/VATT/diagnostics/'  # where the masterflat png images are saved
    redo = True
    save_disk = False

    pixsize = 0.188
    saturlevel = 65000
    sextractor_command = 'sex'
    object_list = ['J0659+1629'] #object list for the diagnostics (stacked images by night).

    # The log file :
    now = datetime.datetime.now()
    log_directory = '/home/astro/millon/Desktop/PRERED/VATT/pipe_log/'
    log_file = os.path.join(log_directory, 'print_out/log_%s.txt' % now.strftime("%Y-%m-%d_%H:%M"))
    error_log = os.path.join(log_directory,
                             'error_flatfielding.log')  # log file to record the images that failed during flatfielding
    red_log = os.path.join(log_directory,
                           'reduction_log.log')  # log file to record the images that failed during flatfielding
    e = open(error_log, 'a')
    f = open(log_file, 'w')
    f.write('################# \n')
    f.write('Reduction starting on %s: \n' % now.strftime("%Y-%m-%d %H:%M"))

    #get_data.main(dates, datapath, redo=redo, log_stream=f, error_stream=e)
    #restruct.main(dates, datapath, workpath, redo=redo, log_stream=f, error_stream=e, log_dir=log_directory)
    #reduc.main(dates, workpath, pixsize=pixsize, saturlevel=saturlevel, sextractor_command=sextractor_command, reduction_log=red_log, redo=redo, log_stream=f, error_stream=e)
    #sort.main(dates, workpath, reducpath, log_stream=f, redo=redo)
    diag.main(dates, object_list, workpath, pngpath, reducpath, log_stream=f, redo=redo)
    if save_disk :
        cleanup.main(dates, datapath, workpath, log_stream=f)


if __name__ == '__main__':
    parser = ap.ArgumentParser(prog="python {}".format(os.path.basename(__file__)),
                               description="Run the VATT pipeline. No argument will run for the previous nigth.",
                               formatter_class=ap.RawTextHelpFormatter)
    help_startnight = "first night to reduce in the format YYYY-MM-DD"
    help_endnight = "last night to reduce in the format YYYY-MM-DD"

    parser.add_argument('--start', dest='startnight', type=str,
                        metavar='', action='store', default=None,
                        help=help_startnight)
    parser.add_argument('--end', dest='endnight', type=str,
                        metavar='', action='store', default=None,
                        help=help_endnight)

    args = parser.parse_args()
    if args.startnight is None or args.endnight is None:
        now = datetime.datetime.now()
        startnight = now.strftime("%Y-%m-%d")
        starttime = Time(startnight, format='iso',
                         scale='utc').mjd - 3  # we reduce until the three days before, to use the flats taken after observation date.
        endtime = starttime  # only one night

    else:
        args.startnight = args.startnight + " 01:00:00"
        args.endnight = args.endnight + " 01:00:00"
        starttime = Time(args.startnight, format='iso', scale='utc').mjd
        endtime = Time(args.endnight, format='iso', scale='utc').mjd

    days = np.arange(starttime, endtime + 1, 1)
    dates = [Time(d, format='mjd', scale='utc').iso[:10] for d in days]

    main(dates)
