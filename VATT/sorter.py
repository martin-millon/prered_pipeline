import time
import os
import glob

now = time.strftime("%Y-%m-%d")
basedir = '/mnt/TBArray/images/cosmograil'
pipelogdir = '/mnt/TBArray/images/cosmograil/log'
# basedir = '.'
datedir = os.path.join(basedir,now)

if not os.path.isdir(datedir):
    os.mkdir(datedir)
if not os.path.isdir(pipelogdir):
    os.mkdir(pipelogdir)

reduction_log = os.path.join(pipelogdir,'pipe_log_%s.log'%now)

files = glob.glob(os.path.join(basedir,"*.fits"))

log_night = open(reduction_log, "w")
log_night.write("Sorting started on %s \n"%now)
log_night.write("Number of files : %i \n"%len(files))
log_night.write("\n".join(files))
log_night.close()

os.system('mv %s %s'%(os.path.join(basedir,'*.fits'),datedir))
