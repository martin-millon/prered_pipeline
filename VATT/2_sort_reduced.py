import os, sys
sys.path.append('..')
from astropy.io import fits
import numpy as np
from astropy.time import Time
from lens_list import xephemlens_list
import astroalign
from TelescopeUtils.utils import remove_bad_margins
from TelescopeUtils.utils import find_object_from_coordinate
from TelescopeUtils.utils import get_best_seeing_index
from TelescopeUtils.diagnostics_util import remove_sky


"""
Sort the reduced data - simply put the reduced images in a single directory per lens, ready to be imported in cosmouline.
"""

def main(dates, workpath, reducpath, log_stream=None, redo=False, seeing_stack =3, ellipticity_stack = 0.3):
    command = 'cp'
    if redo == False:
        command = command + ' -n'  # no clobber command (do not overwrite)

    if log_stream is None:
        outstream = sys.stdout
    else:
        outstream = log_stream

    print("#" * 15, " 2 Sorting ", "#" * 15, file=outstream)
    for date in dates:
        print("=" * 15, date, "=" * 15, file=outstream)
        print("Sorting files...", file=outstream)
        workdir = os.path.join(workpath, date)
        sciencesdir = os.path.join(workdir, 'science')
        othersdir = os.path.join(workdir, 'other')

        scifiles = [d.split('\n')[0] for d in os.popen('ls %s/*.fits' % sciencesdir).readlines()]
        othersfiles = [d.split('\n')[0] for d in os.popen('ls %s/*.fits' % othersdir).readlines()]
        allfiles = scifiles + othersfiles

        #delete old files
        to_delete = []
        for i,f in enumerate(allfiles):
            if '_sky.fits' in f or '_skysub.fits' in f:
                print('Removing :%s'%f, file=outstream)
                os.remove(f)
                to_delete.append(i)
                                     
        allfiles = [i for j, i in enumerate(allfiles) if j not in to_delete]
        dic = {'files': [], 'object': []}
        objkey_list = []
        
        if len(allfiles) == 0:
            print("No science images on %s"%date, file = log_stream) 
            continue
    
        for f in allfiles:
            print(f, file = outstream )
            dic['files'].append(f)
            header = fits.open(f)[0].header

            object = find_object_from_coordinate(header['RA'], header['DEC'], header['DATE-OBS'], xephemlens_list, tolerance = 2)
            dic['object'].append(object)
            key = object
            if not key in dic.keys():
                dic[key] = [f]
                objkey_list.append(key)
            else:
                dic[key].append(f)

            objdir = os.path.join(reducpath, object)
            if not os.path.isdir(objdir):
                os.mkdir(objdir)

            os.system('%s %s %s' % (command, f, os.path.join(objdir, os.path.basename(f).split(".fits")[0] +"_"+ date + ".fits")))

        # create a stack of the images:
        for o in objkey_list:
            # first we find the best seeing image
            print('Stacking %s' % o, file=outstream)
            images_to_align = dic[o]
            ind = get_best_seeing_index(images_to_align)
            image_1 = remove_sky(images_to_align[ind])
            #image_1 = np.array(image_1).byteswap().newbyteorder() #this a solve a known bug in pandas when opening fits files.
            header = fits.getheader(images_to_align[ind])
            image_1 = remove_bad_margins(image_1, num_pixels=60)
            image_1 = np.nan_to_num(image_1)
            aligned = [image_1]
            seeing_list=[header['SEEING']]
            for i,im in enumerate(images_to_align) :
                if i == ind :
                    continue
                h = fits.getheader(im)
                if h['SEEING']>seeing_stack or h['ELL'] > ellipticity_stack :
                    print('Discarding %s for the stack, seeing : %2.2f, ellipiticity : %2.2f'%(im,h['SEEING'],h['ELL']))
                    continue
                try:
                    seeing_list.append(h['SEEING'])
                    image_2 = remove_sky(im)
                    image_2 = remove_bad_margins(image_2, num_pixels=60)
                    image_2 = np.nan_to_num(image_2)
                    #image_2 = np.array(image_2).byteswap().newbyteorder()
                    image_aligned, footprint = astroalign.register(image_1, image_2)

                except Exception as e:
                    print("I cannot align image %s : %s"%(im,e), file = log_stream)
                else :
                    aligned.append(image_aligned)
            #Stack image :
            stack_dir = os.path.join(reducpath, o , "stack")
            n_image = len(aligned)
            if not os.path.isdir(stack_dir):
                os.mkdir(stack_dir)

            stacked = np.median(aligned,axis = 0)
            print("%s : combining %i images."%(o, n_image))
            header['NCOMBINE'] = n_image
            header['SEEING'] = np.mean(seeing_list)
            hdu = fits.PrimaryHDU(data=stacked, header=header)
            hdu.writeto(os.path.join(stack_dir, 'stack_%s_%s.fits')%(o,date), overwrite=True)



if __name__ == '__main__':
    workpath = '/obs/lenses_EPFL/PRERED/VATT/reduc'
    #workpath = "/Users/martin/Desktop/COSMOULINE/data_raw/VATT_test/reduced"
    reducpath = '/obs/lenses_EPFL/PRERED/VATT/sorted/'
    #reducpath = "/Users/martin/Desktop/COSMOULINE/data_raw/VATT_test/sorted"

    datemin = "2020-01-20"
    datemax = "2020-01-20"
    starttime = Time(datemin, format='iso', scale='utc').mjd
    endtime = Time(datemax, format='iso', scale='utc').mjd

    days = np.arange(starttime, endtime + 1, 1)
    dates = [Time(d, format='mjd', scale='utc').iso[:10] for d in days]

    object_list = main(dates, workpath, reducpath, redo=True)
