import os, sys
sys.path.append('..')
from astropy.io import fits
import numpy as np
from astropy.time import Time
from astropy.io.fits import getheader
from TelescopeUtils.utils import make_MF_png
from TelescopeUtils.utils import make_stack_png

"""
Remove the unecessary files to save disk space. We keep only the reduced frames, the masterflats and the masterbias. 
"""

def main(dates, object_list, workpath, pngpath, reducpath, log_stream=None, redo=True):
    if log_stream is None:
        outstream = sys.stdout
    else:
        outstream = log_stream

    if not os.path.isdir(pngpath):
        os.mkdir(pngpath)

    print("#"*15, " 3 Running diagnostics ", "#"*15, file = outstream)
    for date in dates:
        print("=" * 15, date, "=" * 15, file=outstream)
        s = make_MF_png(date, workpath, pngpath, redo=redo)
        print(s, file=outstream)

        for o in object_list :
            stack_png_dir = os.path.join(pngpath, "stack_%s"%o)
            if not os.path.isdir(stack_png_dir):
                os.mkdir(stack_png_dir)
            stack_file = os.path.join(reducpath, o, "stack", 'stack_%s_%s.fits'%(o, date))
            if os.path.isfile(stack_file): 
                s = make_stack_png(stack_file, date, stack_png_dir, redo=redo, rebin =2)


if __name__ == '__main__':
    workpath = '/obs/lenses_EPFL/PRERED/VATT/reduc'
    #workpath = "/Users/martin/Desktop/COSMOULINE/data_raw/VATT_test/reduced"
    pngpath = '/obs/lenses_EPFL/PRERED/VATT/diagnostics/'
    #pngpath = "/Users/martin/Desktop/COSMOULINE/data_raw/VATT_test/diagnostics"
    reducpath = '/obs/lenses_EPFL/PRERED/VATT/sorted/'
    #reducpath = "/Users/martin/Desktop/COSMOULINE/data_raw/VATT_test/sorted"

    datemin = "2020-01-18"
    datemax = "2020-01-18"
    starttime = Time(datemin, format='iso', scale='utc').mjd
    endtime = Time(datemax, format='iso', scale='utc').mjd

    object_list = ['J0659+1629']

    days = np.arange(starttime, endtime + 1, 1)
    dates = [Time(d, format='mjd', scale='utc').iso[:10] for d in days]

    main(dates,object_list, workpath, pngpath,reducpath, redo = True)
