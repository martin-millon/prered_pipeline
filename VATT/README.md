# Pipeline for VATT reduction.

Pipeline for VATT reduction. 

## Get VATT data
Script to download the VATT data from the vattobs server. From a new machine, you need to create a new ssh key with : 
```ssh-keygen -t rsa -b 2048``` and then ```ssh-copy-id vattobs@vatt.as.arizona.edu``` and type the password only once. 

## 1_reduc_data 
Masterbias and Masterflat construction. Also apply the masterbais and master flat to the data. We use domeflats. If there is no calibration this night, it will look for masterflats in the previous nights. 

## 2_sort_reduced
Sort the data per target

## 3_diagnostics 
Make the pngs of the masterflat and of the science images. It will do a stacked image of the night. Other diagnostics test can be implemented here. 

## 4_cleanup
Remove the unecessary files to save disk space. We keep only the reduced frames, the masterflats and the masterbias. 

## run_all 
You can also run all the scrit at once, by specifying the start and end date with the --start and --end optionnal argument. Date format is YYYY-MM-DD. Without argument the pipeline will run for the previous night. 

## execute_all.sh 
This is wrapper in bash to call for run_all.py. It loads all the necessary module on lesta and execute the python scripts. It can be call with crontab for automatic run everyday. To edit the crontab command : 
    
    export EDITOR=nano
    crontab -e 