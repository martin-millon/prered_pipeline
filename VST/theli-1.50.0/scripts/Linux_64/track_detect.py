#!/usr/bin/env python3

# This script identifies satellite tracks in astronomical
# images and modifies associated weight and flag images.
# Also a plot showing found tracks can be created.
#
# The used algorithms and methods are described in a jupyter notebook
# at https://github.com/terben/Python-cheat-sheets/blob/master/scipy_optical_object_detection.ipynb

__version__ = "Pipeline Version: 1.50.0"

# necessary modules:
import argparse
import sys
import os.path as op
import numpy as np
import scipy.ndimage.measurements as snm
import scipy.ndimage.filters as snf
import scipy.ndimage.morphology as snmo
import scipy.linalg as sl
import astropy.io.fits as aif
import astropy.stats as ast
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import THELIImage as TI

# settings for nicer plots:
# font size of labels etc,
matplotlib.rcParams['font.size'] = 18
# line width of coordinate axes
matplotlib.rcParams['axes.linewidth'] = 2.0

# Handle command line arguments:
parser = argparse.ArgumentParser(
    formatter_class=argparse.RawDescriptionHelpFormatter,
    epilog="""
SCRIPT NAME:
  track_detect - detect satellite tracks in optical images

DESCRIPTION:
  The script tries to detect satellite tracks in optical astronomical
  FITS images. If tracks are found, corresponding weight and flag images
  are modified to indicate the found track. The science image remains
  unmodified. If tracks are found, a plot showing the found track and
  the masked pixels can be created.

  The used algorithms and methods are described in a Jupyter notebook:
  https://github.com/terben/Python-cheat-sheets/blob/master/scipy_optical_object_detection.ipynb

AUTHOR:
  Thomas Erben (terben@astro.uni-bonn.de)
""")

parser.add_argument('-i', '--input_science',
                    help='input science image to detect tracks')
parser.add_argument('--input_weight',
                    help='input weight image')
parser.add_argument('--input_flag',
                    help='input flag image')
parser.add_argument('--output_weight',
                    help='output weight image')
parser.add_argument('--output_flag',
                    help='output flag image')
parser.add_argument('-p', '--output_plot',
                    help='name of the plot showing satellite tracks')
parser.add_argument('-m', '--mask_value',
                    help='mask (flag) value for pixels polluted by a track',
                    type=int, default=32)

args = parser.parse_args()

input_science = args.input_science
input_weight = args.input_weight
input_flag = args.input_flag
output_weight = args.output_weight
output_flag = args.output_flag
output_plot = args.output_plot
mask_value = args.mask_value

if input_science == None:
    raise parser.error("A science image must be provided")

# test for existence of files given as command-line arguments:
for data in [ input_science, input_weight, input_flag ]:
    if data != None:
        if not op.isfile(data):
            print("File %s does not exist!" % (data), file=sys.stderr)
            sys.exit(1)

# Here the main scripts task start:
k = 1.0                         # we detect objects having pixels
                                # with k * sigma_sky
min_object_size = 300           # minimum size (extension in either x or y)
                                # an object must have to be included in track
                                # analysis

# load image
science = TI.THELIImage(input_science)

# if a weight is present, we set to zero in the science image
# all pixels having a zero weight. Probably we can do better than
# that but it should be fine for our purposes of track detection
if input_weight != None:
    weight = TI.THELIImage(input_weight)
    science.data[np.isclose(weight.data, 0.0)] = 0.0

# obtain robust image statistics:
# obtain 'robust statistics' around the sky-background of the image.
data_mean, data_median, data_sigma = \
  ast.sigma_clipped_stats(science.data[::10,::10])

# smooth image for object detection:
# pixels exceeding 1 \sigma_sky without any smoothing:
sigma_gauss = 1.0 # smoothing sigma
data_smooth = snf.gaussian_filter(science.data, sigma=sigma_gauss)
det_smooth = data_smooth > k * data_sigma

# label connected features in the detection image
objects, n_objects = snm.label(det_smooth)

# obtain slices from the connected features (sources) identified:
slices = snm.find_objects(objects)
track_pixels = np.zeros(objects.shape, bool)
n_tracks = 0

print("Analysing %d candidate objects in %s" % (n_objects, input_science))

# now check whether there are tracks among the objects:
for i, slc in enumerate(slices):
    # Note that we only work with the detection image, not on the original
    # pixel data here!
    det_pixels = objects[slc]

    # check whether the object has a significant size from the outset:
    slice_height, slice_width = det_pixels.shape

    if slice_height > min_object_size or slice_width > min_object_size:
        det_pixels = (det_pixels == i + 1)
        x = np.arange(slice_width)
        y = np.arange(slice_height)
        flux = np.sum(det_pixels)
        x_cen = np.sum(x[np.newaxis,:] * det_pixels) / flux
        y_cen = np.sum(y[:,np.newaxis] * det_pixels) / flux
        c_xx = np.sum(((x - x_cen)**2)[np.newaxis,:] * det_pixels) / flux
        c_yy = np.sum(((y - y_cen)**2)[:,np.newaxis] * det_pixels) / flux
        c_xy = np.sum((x - x_cen)[np.newaxis,:] *
                      (y - y_cen)[:,np.newaxis] * det_pixels) / flux
        C = np.array([[c_xx, c_xy], [c_xy, c_yy]])
        w, v = sl.eig(C)
        a, b = np.max(2.0 * np.sqrt(np.real(w))), \
               np.min(2.0 * np.sqrt(np.real(w)))

        # The final check whether we have a track:
        if a > min_object_size and b / a < 0.1:
            n_tracks = n_tracks + 1
            track_pixels[slc] = det_pixels

# modify weight and flag if available and do plotting if asked for:
if n_tracks > 0:
    print('%d track(s) found in %s' % (n_tracks, input_science))

    # widen found tracks by 5 pixels to also catch up faint track pixels
    # at the track broders.
    newtrack_pixels = snmo.binary_dilation(track_pixels,
                                         structure=np.ones((5,5)))

    if input_weight != None:
        print('Modifying weight %s' % (input_weight))

        weight = TI.THELIImage(input_weight)
        weight.data[newtrack_pixels == True] = 0.0
        weight.add_history('Weight corrected for satellite track(s) in %s' %
                           (op.basename(input_science)),
                           program='track_detect.py',
                           program_version=__version__)

        if output_weight == None:
            output_weight = input_weight

        print('Storing new weight in %s' % (output_weight))
        weight.writeto(output_weight, overwrite=True)

    if input_flag != None:
        print('Modifying flag %s' % (input_flag))

        flag = TI.THELIImage(input_flag)
        # we implicitely assume that the flag image has integer type:
        flag.data[newtrack_pixels == True] |= mask_value
        flag.add_history('Flag corrected for satellite track(s) in %s' %
                           (op.basename(input_science)),
                           program='track_detect.py',
                           program_version=__version__)

        if output_flag == None:
            output_flag = input_flag

        print('Storing new flag in %s' % (output_flag))
        flag.writeto(output_flag, overwrite=True)

    if output_plot != None:
        print('Creating overview plot')

        min_val = data_median - 3.0 * data_sigma
        max_val = data_median + 10.0 * data_sigma

        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 4))
        ax1.imshow(science.data, origin='lower', cmap='gray_r',
                   vmin=min_val, vmax=max_val)
        ax1.set_xlabel('x [pix]')
        ax1.set_ylabel('y [pix]')
        ax1.set_title('original data')
        ax2.imshow(science.data, origin='lower', cmap='gray_r',
                   vmin=min_val, vmax=max_val)

        track_image = np.zeros(track_pixels.shape + (4,), np.uint8)
        track_image[:,:,2] = (newtrack_pixels > 0) * 255
        track_image[:,:,3] = (newtrack_pixels > 0) * 255
        ax2.imshow(track_image, origin='lower')
        ax2.set_xlabel('x [pix]')
        ax2.set_ylabel('y [pix]')
        ax2.set_title('detected track(s)')

        plt.tight_layout()

        print('Storing plot in %s' % (output_plot))
        plt.savefig(output_plot, dpi=400)
else:
    print('No track found in %s.' % (input_science))
