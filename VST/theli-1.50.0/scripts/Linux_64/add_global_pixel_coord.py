import sys
import numpy as np
import THELIWCS as TW
import wcsutil
import ldac
import bashreader

# The script adds 'global' mosaic-pixel coordinates to an LDAC-catalogue.
# The global coordinates are defined w.r.t. the CRVAL header keywords
# of a 'complete' multi-chip mosaic. The script is primarily used for
# the photometric illumination correction task.
#
# The new keys have the names Xpos_global and Ypos_global

# $1: file with catalogues to process. Each line contains
#     - LDAC catalogue to which we want to add global mosaic coordinates
#     - LDAC table
#     - scamp header corresponding to $1
#     - output catalogue name (OPTIONAL; if not given, the old catalogue
#                              is overwritten)

# HISTORY:
# ========
#
# 18.05.2018:
# substituted clobber argument with overwrite (astropy deprecation
# warning)

catalog_file = open(sys.argv[1])

for line in catalog_file:
    line = line.strip().split()

    ldaccat = ldac.LDACCat(line[0])
    ldactable = ldaccat[line[1]]

    # Note that scamp headers do not contain NAXIS1 and NAXIS2 keywords
    # which are necessary to define a valid WCS.
    # Note that scamp headers do not contain NAXIS1/2 keywords. The actual
    # values are unimportant for the operations done within this script.
    # We therefore put 'arbitraty values' of NAXIS1=2000 and NAXIS2=4000.
    # The global coordinates are defined w.r.t. CRPIX1 and CRPIX2 in the lower
    # left corner of each chip:
    wcs = TW.WCS_from_txt(line[2],
                          NAXIS1=2000, NAXIS2=4000,
                          CRPIX1=0, CRPIX2=0)

    x, y = wcs.sky2image(ldactable['Ra'], ldactable['Dec'])

    # add x and y to the LDACtable and write back the catalogue
    ldactable['Xpos_global'], ldactable['Ypos_global'] = x, y

    if len(line) > 3:
        ldaccat.saveas(line[3])
    else:
        ldaccat.saveas(line[0], overwrite=True)
