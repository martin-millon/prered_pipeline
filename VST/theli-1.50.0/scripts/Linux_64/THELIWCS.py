# class to administrate a THELI WCS system. This is a TAN-projection
# WCS with the required keyowrds:
# CRPIX1/2, CRVAL1/2, NAXIS1/2, CDi_j. CTYPE1/2 must be
# 'RA---TAN' and 'DEC--TAN'.
#
# The system is based on Erin Sheldons wcsutil WCS system.
# In contrast to the astropy WCS-system, it already supports
# the scamp/swarp distortion model.

# 15-02-2018:
# integration of scamp distortions. We now support FITS headers
# with scamp PV distortion coefficients

import re
import astropy.io.fits as aif
import numpy as np
import wcsutil

class WCS:
    # The initialisation function just initialises a wcsutil
    # WCS object:
    def __init__(self, crval1, crval2, crpix1, crpix2,
                 cd1_1, cd1_2, cd2_1, cd2_2, naxis1, naxis2,
                 ctype1='RA---TAN', ctype2='DEC--TAN',
                 **kwargs):

        WCS_dict = {}
        WCS_dict['crval1'] = crval1
        WCS_dict['crval2'] = crval2
        WCS_dict['crpix1'] = crpix1
        WCS_dict['crpix2'] = crpix2
        WCS_dict['cd1_1'] = cd1_1
        WCS_dict['cd1_2'] = cd1_2
        WCS_dict['cd2_1'] = cd2_1
        WCS_dict['cd2_2'] = cd2_2
        WCS_dict['naxis1'] = naxis1
        WCS_dict['naxis2'] = naxis2
        WCS_dict['ctype1'] = ctype1
        WCS_dict['ctype2'] = ctype2

        # we implicitely assume that kwargs only contains scamp PV-coefficients
        # with corresponding float values.
        # If this is not ensured, we could add some tests on the keys
        # in the future:
        for key, value in kwargs.items():
            WCS_dict[key.lower()] = value

        self.WCS=wcsutil.WCS(WCS_dict)

        # get the Ra and Dec-values of the WCS center:
        self.ra_center, self.dec_center = \
            self.WCS.image2sky(naxis1 / 2., naxis2 / 2.)

    def __getitem__(self, key):
        return self.WCS[key]

    def __setitem__(self, key, val):
        self.WCS[key] = val

    def keys(self):
        return self.WCS.keys()

    def radius(self):
        """
        The 'radius' of a WCS is defined as the distance from its
        center to one of its corners.

        return: The radius of the WCS in degrees.
        """

        r = np.sqrt(np.abs(self.WCS['cd1_1'] * self.WCS['cd2_2'] -
                           self.WCS['cd1_2'] * self.WCS['cd2_1'])) * \
            np.hypot(self.WCS['naxis2'], self.WCS['naxis1']) / 2.

        return r

    def overlap(self, awcs):
        """
        input:
        a WCS system

        returns:
        True if the two WCS overlap and False otherwise

        Note:
        This is only a quick, not exact test whether two WCS-systems
        overlap. It can (and often will) produce false positives but
        never false negatives!
        """

        rad = self.radius() + awcs.radius()

        overlap = False
        if np.abs(self.dec_center - awcs.dec_center) < rad:
            sep = getAngDist(self.ra_center, self.dec_center,
                             awcs.ra_center, awcs.dec_center)
            if sep < rad:
                overlap = True

        return overlap

    def sky2image(self, longitude, latitude, find=True):
        """
        input:
        lomgitude and latitude (probably Ra and Dec; can be arrays)

        returns:
        The x and y coordinates according to the WCS
        """

        # Just pass the job to the correspondignwcsutil function:
        return self.WCS.sky2image(longitude, latitude, find=True)

    def image2sky(self, x, y):
        """
        input:
        x and y pixel coordinates (can be arrays)

        returns:
        The Ra and Dec coordinates according to the WCS
        """

        # Just pass the job to the correspondignwcsutil function:
        return self.WCS.image2sky(x, y)

# some helper functions:
def is_number(s):
    """
    Tests whether a string 's' can be converted to a float number.
    Returns True if yes and false otherwise
    """

    try:
        float(s)

        return True
    except:
        return False

def WCS_from_fits(filename, ext=0, **kwargs):
    """
    Extract a THELI WCS system from a FITS file.

    inputs:
    - A THELI processed image (THELI processed
      images are ensured to carry necessary keywords
    - The optional argument 'ext' gives the FITS
      extension to extract the WCS from.
    - any header keywords from the file can be overriden with kwargs.
      (The same for keys that might not be present in the file)

    return:
    A THELI WCS structure.
    """

    head = aif.getheader(filename, ext=ext)

    # convert header keys to lower-case to be in sync with the conventions
    # in this module.
    head = { key.lower():value for key, value in head.items() }

    # extract PV-coefficients from the header if any.
    PV_entries = { key:value for key, value in head.items()
                   if key.startswith("pv") == True and \
                      is_number(value) == True }

    # add header keywords (or overwerite them) with command
    # line parameters:
    for name, value in kwargs.items():
        head[name.lower()] = float(value)

    return WCS(head['crval1'], head['crval2'],
               head['crpix11'], head['crpix2'],
               head['cd1_1'], head['cd1_2'],
               head['cd_1'], head['cd2_2'],
               head['naxis1'], head['naxis2'],
               **PV_entries)

def WCS_from_txt(filename, **kwargs):
    """
    Extract a THELI WCS system from an ASCII version of an image
    header.

    inputs:
    - The text file from which to extract the WCS
    - any header keywords from the file can be overriden with kwargs.
      (The same for keys that might not be present in the file)

    return:
    A THELI WCS structure.
    """

    # buid a dictionary from all relevant, numeric header-entries:
    WCS_dict = {}

    f = open(filename)

    for line in f:
        # skip comment lines and other non key-value lines in the txt-file:
        if line.find('=') != -1:
            res = re.split('=', line)
            name = res[0].strip()
            res = re.split('/', res[1])
            value = res[0].strip()

            if is_number(value) == True:
                WCS_dict[name.lower()] = float(value)

    f.close()

    # extract PV-coefficients from the header if any.
    # If there are none, we obtain an empty dictionary
    PV_entries = { key:value for key, value in WCS_dict.items()
                   if key.startswith("pv") == True and \
                      is_number(value) == True }

    # add header keywords (or overwerite them) with command
    # line parameters:
    for name, value in kwargs.items():
        WCS_dict[name.lower()] = float(value)

    return WCS(WCS_dict['crval1'], WCS_dict['crval2'],
               WCS_dict['crpix1'], WCS_dict['crpix2'],
               WCS_dict['cd1_1'], WCS_dict['cd1_2'],
               WCS_dict['cd2_1'], WCS_dict['cd2_2'],
               WCS_dict['naxis1'], WCS_dict['naxis2'],
               **PV_entries)

def getAngDist(ra1, dec1, ra2, dec2):
  """
    Calculate the angular distance between two (Ra, Dec) coordinates.

    Parameters
    ----------
    ra1 : float, array
        Right ascension of the first object in degrees.
    dec1 : float, array
        Declination of the first object in degrees.
    ra2 : float, array
        Right ascension of the second object in degrees.
    dec2 : float, array
        Declination of the second object in degrees.

    Returns
    -------
    Angle : float, array
        The angular distance in DEGREES between the first
        and second coordinate in the sky.

  """

  delt_lon = (ra1 - ra2) * np.pi/180.
  delt_lat = (dec1 - dec2) * np.pi/180.
  # Haversine formula
  dist = 2.0 * np.arcsin(np.sqrt(np.sin(delt_lat / 2.0)**2 + \
                                 np.cos(dec1 * np.pi / 180.) * \
                                 np.cos(dec2 * np.pi / 180.) *
                                 np.sin(delt_lon/2.0)**2))

  return dist * 180.0 / np.pi
