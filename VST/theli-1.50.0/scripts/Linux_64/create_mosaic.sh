#!/bin/bash -u

# This script illustrates the basic usage of the THELI pipeline. The
# commands below process a OMEGACAM test data set from raw image to a
# final co-added image.

# This script also serves as an example for possible 'superscripts'
# collecting various pipeline tasks.

# Note that the script (as most of the individual THELI subtasks) does
# NOT react intelligently on errors during image reduction!!!!

#
# Edit the following two lines to point to the MD of your data and
# your reduction directory (The directory where all your reduction
# scripts are located and where you will execute individual processing
# steps):
while [ $# -gt 0 ]; do
        case $1 in
        -MD )                 shift
                md=$1
		;;
	    -REDDIR )                shift
                reddir=$1
		;;
		-PHOTCAT )				shift
	    		photcat=$1
		;;
		-FILTER )				shift
	    		filter=$1
		;;
		-SCRIPTDIR )				shift
	    		scriptdir=$1
		;;
	esac
			shift
done

export MD=$md
export REDDIR=$reddir
export PHOTCAT=$photcat
export FILTER=$filter

cd $scriptdir

. ./progs.ini

# The catalogue OMEGACAM_SDSS_overlap_01_2019.cat contains all sources
# from the OMEGACAM archive that overlap with Sloan up to 01/2019
export PHOTCAT=/Users/martin/Desktop/observation_VST/OMEGACAM_SDSS_overlap_01_2019.cat

# setup THELI logging:
: ${THELI_LOGGING:="Y"}
: ${THELI_LOGLEVEL="2"} # Log level 2 means 'extensive output logging from
                        # all the scripts
export THELI_LOGGING
export THELI_LOGLEVEL

#
# THELI uses the INSTRUMENT environment variable to identify the
# instrument it is working on. Configuration files named
# ${INSTRUMENT}.ini are usually loaded by processing scripts to load
# necessary, instrument specific information:
export INSTRUMENT=OMEGACAM
#
# The following is just used in this script to identify
# data directories.

# Do astrometric calibration with SCAMP
# We exclusively use SCMAP now and do not support ASTROMETRIC
# anymore. You should have scamp V2.7.7 (or higher) installed.
# It needs to suuport the GAIA-DR2 reference catalogue for
# astrometric calibration
ASTROMMETHOD=SCAMP
ASTROMADD="_scamp_2MASS"

#./parallel_manager.sh ./process_science_eclipse_para.sh ${MD}/ BIAS \
                      #DOMEFLAT_${FILTER} \
                      #SCIENCE_${FILTER} NORESCALE
#./parallel_manager.sh ./process_superflat_eclipse_para.sh ${MD}/ \
                      #SCIENCE_${FILTER} OFC NOFRINGE NOSUPERTEST
##./parallel_manager.sh ./create_illumfringe_para.sh ${MD}/ SCIENCE_${FILTER}
#./parallel_manager.sh ./process_science_illum_eclipse_para.sh ${MD}/ \
                      #SCIENCE_${FILTER} OFC RESCALE ILLUM
		      
./create_binnedmosaics_exposurepara.sh \
    ${MD} SCIENCE_${FILTER} OMEGA OFC 2 -32
    
#./distribute_sets.sh ${MD} SCIENCE_${FILTER} OFCS 1000

