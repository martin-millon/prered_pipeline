while [ $# -gt 0 ]; do
        case $1 in
        -MD )                 shift
                md=$1
		;;
	    -REDDIR )                shift
                reddir=$1
		;;
		-PHOTCAT )				shift
	    		photcat=$1
		;;
		-FILTER )				shift
	    		filter=$1
		;;
		-SCRIPTDIR )				shift
	    		scriptdir=$1
		;;
		-SET )				shift
	    		set=$1
		;;
	esac
			shift
done

export MD=$md
export REDDIR=$reddir
export PHOTCAT=$photcat
export FILTER=$filter
export SET=$set

cd $scriptdir

. ./progs.ini

# setup THELI logging:
: ${THELI_LOGGING:="Y"}
: ${THELI_LOGLEVEL="1"} # Log level 2 means 'extensive output logging from
                        # all the scripts
export THELI_LOGGING
export THELI_LOGLEVEL

export INSTRUMENT=OMEGACAM
ASTROMMETHOD=SCAMP
ASTROMADD="_scamp_2MASS"

export THELI_LOGDIR=`pwd`/logs_TESTDATA_SET

#
# Here, SET processing is performed:
# ==================================
# We only process set_1. To process set_2, just change the following variable: (1000 is the number of overlapping pixels to be in the same sets)
./distribute_sets.sh ${MD} SCIENCE_${FILTER} OFCS 1000

