#!/bin/bash -u

# This script illustrates the basic usage of the THELI pipeline. The
# commands below process a OMEGACAM test data set from raw image to a
# final co-added image.

# This script also serves as an example for possible 'superscripts'
# collecting various pipeline tasks.

# Note that the script (as most of the individual THELI subtasks) does
# NOT react intelligently on errors during image reduction!!!!

#
# Edit the following two lines to point to the MD of your data and
# your reduction directory (The directory where all your reduction
# scripts are located and where you will execute individual processing
# steps):

. ./progs.ini

export MD=/vol/theli/raidssd/terben/THELI_OMEGACAM_TESTDATA
export REDDIR=/vol/theli/data1/terben/reduce_1.50.0

# The catalogue OMEGACAM_SDSS_overlap_01_2019.cat contains all sources
# from the OMEGACAM archive that overlap with Sloan up to 01/2019
export PHOTCAT=/vol/theli/data1/terben/reduce_1.50.0/OMEGACAM_SDSS_overlap_01_2019.cat

# setup THELI logging:
: ${THELI_LOGGING:="Y"}
: ${THELI_LOGLEVEL="2"} # Log level 2 means 'extensive output logging from
                        # all the scripts
export THELI_LOGGING
export THELI_LOGLEVEL

#
# THELI uses the INSTRUMENT environment variable to identify the
# instrument it is working on. Configuration files named
# ${INSTRUMENT}.ini are usually loaded by processing scripts to load
# necessary, instrument specific information:
export INSTRUMENT=OMEGACAM
#
# The following is just used in this script to identify
# data directories.
export FILTER=r_SDSS

# Do astrometric calibration with SCAMP
# We exclusively use SCMAP now and do not support ASTROMETRIC
# anymore. You should have scamp V2.7.7 (or higher) installed.
# It needs to suuport the GAIA-DR2 reference catalogue for
# astrometric calibration
ASTROMMETHOD=SCAMP
ASTROMADD="_scamp_2MASS"

#
# Here, RUN processing is performed:
# ==================================
#

# We log run and set processing to different directories:
export THELI_LOGDIR=`pwd`/logs_TESTDATA_RUN

# R1: Split the MEF data into individual chips
# ============================================
#
# split data and update headers. This steps takes data
# from ${MD}/BIAS/ORIGINALS (accordingly for other species),
# splits the MEF files into single chips, updates the
# headers to the THELI format and puts resulting files
# to  ${MD}/BIAS etc.
./process_split_OMEGACAM_eclipse.sh -md ${MD} -sd BIAS
./process_split_OMEGACAM_eclipse.sh -md ${MD} -sd DARK
./process_split_OMEGACAM_eclipse.sh -md ${MD} -sd SKYFLAT_${FILTER}
./process_split_OMEGACAM_eclipse.sh -md ${MD} -sd SCIENCE_${FILTER}
./process_split_OMEGACAM_eclipse.sh -md ${MD} -sd STANDARD_${FILTER}
#
# Here you could do a first data check and verify that the modes of
# raw images are within predefined values. A typical script call to
# only accept for further processing only skyflats with a mode
# between 5000 and 40000 would look like:
# ./check_files.sh ${MD}/ SKYFLAT_${FILTER} 5000 40000
#
# we omit this step as the data were chosen to be good !!

#
# R2: process BIAS frames
# =======================
#
# This step processed BIAS images (overscan correction and
# stacking). Final results can be found in ${MD}/BIAS and
# the produced final BIAS frames are called BIAS_i.fits (i being
# chip number)
./parallel_manager.sh ./process_bias_eclipse_para.sh ${MD}/ BIAS
#
# R3: process DARK frames
# =======================
#
# The same as the previous step for DARK frames
./parallel_manager.sh ./process_bias_eclipse_para.sh ${MD}/ DARK
#
# R4: process FLAT frames
# ======================
#
# This step processes FLAT frames (overscan correction, BIAS subtraction,
# stacking). Final results can be found in ${MD}/SKYFLAT_r_SDSS and the
# produced final SKYFLAT frames are called SKYFLAT_${FILTER}_i.fits
./parallel_manager.sh ./process_flat_eclipse_para.sh ${MD}/ BIAS \
                      SKYFLAT_${FILTER}
#
# R5: process SCIENCE frames
# ==========================
#
# OMEGACAM data suffer from crosstalk in several chips (25, 26 and 27).
# We need to correct for it explicitely.
# NOTES: (1) The correction is applied on an OB (ESO oberving block basis)
#        (2) The coefficients are stored in a file (here
#            ct_coeffs_SCIENCE_r_SDSS_TESTDATA). If you process data multiple
#            times, cou can reuse (store) the file and only do the
#            apply_crosstalk step
./create_crosstalk_coefficients_exposurepara.sh ${MD} \
            SCIENCE_r_SDSS OBSTART \
            ${REDDIR}/ct_coeffs_SCIENCE_r_SDSS_TESTDATA

./apply_crosstalk_exposurepara.sh ${MD} SCIENCE_r_SDSS \
            ${REDDIR}/ct_coeffs_SCIENCE_r_SDSS_TESTDATA \
            ${MD}/SCIENCE_r_SDSS/CT ""

if [ -d ${MD}/SCIENCE_r_SDSS/CT ]; then
  mv ${MD}/SCIENCE_r_SDSS/CT/*fits ${MD}/SCIENCE_r_SDSS
  rmdir ${MD}/SCIENCE_r_SDSS/CT
fi

# (overscan correction, bias subtraction,
# flatfielding, superflatfielding). r_SDSS OMEAGCAM images
# do not need defringing. Please ask me if you have i_SDSS or
# z_SDSS images:
./parallel_manager.sh ./process_science_eclipse_para.sh ${MD}/ BIAS \
                      SKYFLAT_${FILTER} \
                      SCIENCE_${FILTER} NORESCALE
./parallel_manager.sh ./process_superflat_eclipse_para.sh ${MD}/ \
                      SCIENCE_${FILTER} OFC NOFRINGE NOSUPERTEST
./parallel_manager.sh ./create_illumfringe_para.sh ${MD}/ SCIENCE_${FILTER}
./parallel_manager.sh ./process_science_illum_eclipse_para.sh ${MD}/ \
                      SCIENCE_${FILTER} OFC RESCALE ILLUM
#
# R6: create postage stamps of SCIENCE images
# ===========================================
#
# we create 8x8 binned mosaic images showing the prereduced images
# The result goes to ${MD}/SCIENCE_${FILTER}/BINNED
./create_binnedmosaics_exposurepara.sh \
    ${MD} SCIENCE_${FILTER} OMEGA OFCS 8 -32

#
# you should take a look at the created mosaics at this stage. Usually
# you would use them to identify whether preprocessing steps should
# be iterated or whether masks should be created (for instance to mark
# satellite tracks). For the example set the only necessary masks
# are WFI.2000-12-26T07:30:37.238_[5-8].reg. Make sure they are in the
# ${MD}/SCIENCE_${FILTER}/reg directory before proceeding.
#
# R7: create weight frames
# ========================
#
# first rescale SKYFLATs and SUPER FLATS to a mode of '1'.
# Outputs are 8 files in ${MD}/ SKYFLAT_R_norm  and
# ${MD}/SCIENCE_R_norm/:
./parallel_manager.sh ./create_norm_para.sh ${MD} SKYFLAT_${FILTER}
./parallel_manager.sh ./create_norm_para.sh ${MD} SCIENCE_${FILTER}

# produce global weights and flags:
# outputs: 8 globalflags and globalweights in ${MD}/WEIGHTS
./parallel_manager.sh ./create_global_weights_flags_para.sh ${MD} NOLINK\
                      SKYFLAT_${FILTER}_norm 0.7 1.3 DARK -9 9 \
                      SCIENCE_${FILTER}_norm 0.9 1.1

# finally create weight images for all individual images:
./parallel_manager.sh ./create_weights_flags_para.sh ${MD} \
                      SCIENCE_${FILTER} OFCS WEIGHTS_FLAGS HIGHSN

#
# R8: Photometric calibration
# ===========================
#
# first process standardstar frames in exactly the same way as the
# science observations.
./parallel_manager.sh ./process_science_eclipse_para.sh ${MD} BIAS \
                        SKYFLAT_${FILTER} STANDARD_${FILTER} NORESCALE
./parallel_manager.sh ./create_illumfringe_para.sh ${MD} STANDARD_${FILTER} \
                        SCIENCE_${FILTER}
./parallel_manager.sh ./process_science_illum_eclipse_para.sh ${MD} \
                        STANDARD_${FILTER} OFC RESCALE ILLUM

# R8.1: create weights and flags for the standard star fields:
# ------------------------------------------------------------
./parallel_manager.sh ./create_weights_flags_para.sh ${MD} \
                      STANDARD_${FILTER} OFCS WEIGHTS_FLAGS HIGHSN

# R8.2 source extraction from the standardstar fields
# ---------------------------------------------------
./parallel_manager.sh ./create_astromcats_weights_para.sh \
                      ${MD} STANDARD_${FILTER} OFCS cat \
                      WEIGHTS OFCS.weight OFCS.flag 5

# R8.3 astrometric calibration for the standard star fields
# ---------------------------------------------------------
#
# we use the 2MASS catalogue. It does not really matter but
# you could also use GAIA of course.
# Note that we depend on SCAMP as astrometric tool for this step!
./create_scamp_astrom_photom_multiinst.sh \
       -i ${INSTRUMENT} -d ${MD} STANDARD_${FILTER} OFCS -p 4.0 \
       -k SHORT -c 2MASS

# merge the extracted sources with those from a standard star catalogue:
./parallel_manager.sh ./create_stdphotom_prepare_para.sh ${MD} \
                        STANDARD_${FILTER} OFCS 2MASS

# The following command adds 'global coordinates' to the catalogues.
# We will not need those quantities here but we need to call the
# script to ensure the pipeline to run correctly:
./parallel_manager.sh ./create_global_mosaic_coord_para.sh ${MD} \
          STANDARD_${FILTER} OFCS 2MASS
./create_stdphotom_merge_exposurepara.sh ${MD} STANDARD_${FILTER} \
       OFCS ${PHOTCAT}

# determine an absolute photometric solution and update headers
# of science frames:
#
./create_abs_photo_info.sh ${MD} STANDARD_${FILTER} \
       OFCS r_SDSS r gmr -0.1 0.05 RUNCALIB

./create_zp_correct_header_exposurepara.sh ${MD} SCIENCE_${FILTER} OFCS \
         ${MD}/STANDARD_${FILTER}/calib/night_0_${FILTER}_result.asc 2 RUN

# include the following calls if you checked your results up to now
# and if you want to clean up disk space
## ./cleanfiles.sh ${MD}/BIAS WFI "."
## ./cleanfiles.sh ${MD}/BIAS WFI "OC."
## ./cleanfiles.sh ${MD}/DARK WFI "."
## ./cleanfiles.sh ${MD}/DARK WFI "OC."
## ./cleanfiles.sh ${MD}/SKYFLAT_${FILTER} WFI "."
## ./cleanfiles.sh ${MD}/SKYFLAT_${FILTER} WFI "OC."
## ./cleanfiles.sh ${MD}/SCIENCE_${FILTER}/SPLIT_IMAGES WFI "."
## rmdir ${MD}/SCIENCE_${FILTER}/SPLIT_IMAGES
## ./cleanfiles.sh ${MD}/SCIENCE_${FILTER}/OFC_IMAGES WFI "OFC."
## rmdir ${MD}/SCIENCE_${FILTER}/OFC_IMAGES
## ./clesnfiles.sh ${MD}/SCIENCE_${FILTER}/SUB_IMAGES WFI "OFC_sub."
## rmdir ${MD}/SCIENCE_${FILTER}/SUB_IMAGES
## ./cleanfiles.sh ${MD}/STANDARD_${FILTER}/SPLIT_IMAGES WFI "."
## rmdir ${MD}/STANDARD_${FILTER}/SPLIT_IMAGES
## ./cleanfiles.sh ${MD}/STANDARD_${FILTER}/OFC_IMAGES WFI "OFC."
## rmdir ${MD}/STANDARD_${FILTER}/OFC_IMAGES
##
##
# R9: RUN -> SET data structure
# =============================
#
# set new logging directory for set processing:
export THELI_LOGDIR=`pwd`/logs_TESTDATA_SET

# sort the SCIENCE frames in different sets; in our example, two sets are
# present and all images are moved to ${MD}/set_1 and  ${MD}/set_2.
# The distribute_sets script pts the set directories below the same main
# directory as the RUNS.
./distribute_sets.sh ${MD} SCIENCE_${FILTER} OFCS 1000

#
# Here, SET processing is performed:
# ==================================
#

# We only process set_1. To process set_2, just change the following variable:
SET=set_1

# S1: Perform astrometric calibration of science data
# ===================================================


# S1.1: create catalogues for astrometric and photometric calibration
# -------------------------------------------------------------------
./parallel_manager.sh ./create_astromcats_weights_para.sh ${MD} ${SET} \
                        OFCS cat WEIGHTS OFCS.weight NONE 5

# S1.2: perform astrometric calibration
# -------------------------------------
# alternatively to the previous two scripts you
# can perform astrometry/relative photometry with scamp.
# In this case the following script call is necessary:
./create_scamp_astrom_photom_multiinst.sh \
        -i OMEGACAM -d ${MD} ${SET} OFCS -p 4.0 -k LONG -c 2MASS

# S2: estimate some statistics on images:
# =======================================
./create_stats_table.sh ${MD} ${SET} OFCS headers${ASTROMADD}

# S3: estimate final photometric zeropoint for a co-added stack
# (This is based on quantities from the previous statistics step)
# ===============================================================
./create_absphotom_photometrix.sh ${MD} ${SET}

#
# S4: create diagnostic plots with SuperMongo (SM)
# ================================================
./make_checkplot_stats.sh ${MD} ${SET} chips.cat5 STATS ${SET}

# S5: subtract the sky from science images and detect satellite tracks
# ====================================================================
./parallel_manager.sh ./create_skysub_para.sh \
                      ${MD} ${SET} OFCS ".sub" TWOPASS

# perform simplistic satellite track detection; this step needs
# to be donw on sky-subtratced images. Hence, it is done here.
./parallel_manager.sh ./create_track_detect_para.sh \
      ${MD} ${SET} WEIGHTS OFCS.sub OFCS \
      ${MD}/${SET}/track_plots

# S6: Image Co-addition
# =====================
#
# perform final image co-addition; if you want to use the experimental
# header update feature after co-addition (see below) you ALWAYS
# have to provide a coaddition condition (-l option) to the following
# script.

# S6.1: some preparation, file creation for co-addition
# -----------------------------------------------------
./prepare_coadd_swarp.sh -m ${MD} -s ${SET} -e "OFCS.sub" -n TEST \
                         -w ".sub" -eh headers${ASTROMADD} \
                         -l ${MD}/${SET}/cat/chips.cat5 STATS\
                            "(SEEING<2.0);" \
                            ${MD}/${SET}/cat/TEST.cat

# S6.2: Resample single images
# ----------------------------
./parallel_manager.sh ./resample_coadd_swarp_para.sh ${MD} ${SET} \
                      "OFCS.sub" TEST ${REDDIR}

# we noticed that resampled images have a slight bias in the
# overall sky-background value. We again estimate a background
# level for the whole image and subtract it:
#./parallel_manager.sh ./create_modesub_para.sh ${MD}/${SET} \
#                      coadd_TEST ? \
#                      OFCS.sub.TEST.resamp.

# S6.3:  Final image co-addition
# ------------------------------
./perform_coadd_swarp.sh ${MD} ${SET} TEST OFCS.sub.TEST.resamp. \
                     "" ${REDDIR} MAP_WEIGHT FLAG WEIGHTED

#
# HERE THE FORMAL RESPONSIBILITY OF THE PIPELINE ENDS
# THE STEPS DESCRIBED IN THE FOLLOWING ARE EXPERIMENTAL
# AND NOT YET CLEANLY IMPLEMENTED IN THE PIPELINE FLOW.

#
# update the header of the co-added image with information on
# total exposure time, effective gain, magnitude zeropoint information

#
# first get the magnitude zeropoint for the co-added image:
if [ -f ${MD}/${SET}/cat/chips_phot.cat5 ]; then
  MAGZP=$(${P_LDACTOASC} -i ${MD}/${SET}/cat/chips_phot.cat5 -t ABSPHOTOM \
                   -k COADDZP | awk 'END {print $0}')
else
  MAGZP=-1.0
fi
#
# now do the actual header update
./update_coadd_header.sh ${MD} ${SET} TEST STATS coadd ${MAGZP} AB \
                         "(SEEING<2.0);" 2MASS OMEGACAM "VST"
##
##
## END OF PROCESSING SCRIPT
##
