# class to administrate a THELI image. A THELI image is a FITS image
# with a primary HDU and imaging data. It does not have any extensions!

# HISTORY COMMENTS:
# =================
#
# 24.08.2018:
# I added a boolean checksum keyword to the image save-method. If set to True
# CHECKSUM and DATASUM keywords will be added to the header before the
# data is written.

import re
import os
import datetime as dt
import numpy as np
import astropy.io.fits as aif

# exception classes for this module:
class THELIImageError(Exception):
    """
    The exception is thrown for errors within the THELIImage class
    """

    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

class BadFITSSectionError(Exception):
    """
    The exception is thrown if we pass a bad section string to
    function section2slice
    """

    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

# utility functions
def section2slice(section):
    """
    Parse an IRAF/FITS section specification string and convert to a
    numpy slice specification (2-element tuple of slices). Converts
    from 1-indexed to 0-indexed, end from last element to 1-past-last
    element, and swaps index order.

    input:
     - section: string-valued IRAF/FITS section specification,
      e.g. '[1:1024,1:4096]'

    output:
     - 2-element tuple of slices, which can index numpy arrays

    """

    values = re.findall(r"\w+", section)
    if len(values) != 4:
        raise BadFITSSectionError("%s" % section)

    values = [ int(s) for s in values ]

    return (slice(values[2]-1,values[3]),
            slice(values[0]-1,values[1]))


class THELIImage(object):
    """
    A THELI image is a 2-D astronomical FITS image with a primary
    header and imaging data. It has no extensions.

    The class is primarily a convenience wrapper around astropy-functions.
    """

    def __init__(self, name=None, shape=(2048, 4096),
                 dtype=np.float32):
        """
        A THELI image  can be instantiated either as an empty image
        or with an existing FITS image on disk.

        >>> a = THELIimage.THELIimage('test.fits')
            # reads the image 'test.fits' into the variable 'a'.
        """

        if name is None:
            self.data = np.zeros(shape=shape, dtype=dtype)
            self.hdu = [aif.PrimaryHDU(self.data)]
            self.header = self.hdu[0].header
        else:
            self.hdu = aif.open(name)

            if len(self.hdu) > 1:
                raise THELIImageError('Too many image extensions in %s' %
                                      (name))

            self.header = self.hdu[0].header
            self.data = self.hdu[0].data

    # indexing a THEIimage means that we are after a header
    # keyword:
    def __getitem__(self, key):
        """
        Returns the value of the specified header keyword

        >>> print(a['NAXIS1'])
            # prints the value of the NAXIS1 header keyword
        """
        return self.header[key]

    def __setitem__(self, key, value):
        """
        Sets the header keyord 'key' to the specified value

        >>> a['TEST'] = 1
            # sets the header keyword 'TEST' to an int of 1.
        """
        self.header[key] = value

    def add_history(self, history, program=None, program_version=None):
        """
        Adds history comments to the header of a THELI image. Besides
        the history string itself, a data/time string and, if required,
        the programname and program version are written to the history.

        >>> a.addhistory('Image corrected for B/F effect',
                         program='bfcorrect.py')
        """

        curr_date = dt.datetime.now().strftime("%Y-%m-%dT%H:%M:%S")

        entry = "history entry added at: %s" % (curr_date)

        if program != None:
            entry = "%s added history entry at: %s" % (program, curr_date)

        self.header['history'] = ""
        self.header['history'] = entry

        if program != None and program_version != None:
            entry = "%s: Version %s" % (program, program_version)
            self.header['history'] = entry

        self.header['history'] = history

    def writeto(self, file_name, checksum=False, **kwargs):
        # Just pass the writing of a THELI image to the corresponsing
        # astropy wirteo function:
        # If the checksum keyowrd is True, we add CHECKSUM and
        # DAATSUM keywords before the data is written:
        hdu = aif.PrimaryHDU(header=self.header, data=self.data)

        if checksum == True:
            hdu.add_checksum()

        hdu.writeto(file_name, **kwargs)
