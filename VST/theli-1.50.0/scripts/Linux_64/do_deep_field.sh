while [ $# -gt 0 ]; do
        case $1 in
        -MD )                 shift
                md=$1
		;;
	    -REDDIR )                shift
                reddir=$1
		;;
		-PHOTCAT )				shift
	    		photcat=$1
		;;
		-FILTER )				shift
	    		filter=$1
		;;
		-SCRIPTDIR )				shift
	    		scriptdir=$1
		;;
		-SET )				shift
	    		set=$1
		;;
		-SEEING )				shift
	    		seeing=$1
		;;
	esac
			shift
done

export MD=$md
export REDDIR=$reddir
export PHOTCAT=$photcat
export FILTER=$filter
export SET=$set
export SEEING=$seeing

cd $scriptdir

. ./progs.ini

# setup THELI logging:
: ${THELI_LOGGING:="Y"}
: ${THELI_LOGLEVEL="1"} # Log level 2 means 'extensive output logging from
                        # all the scripts
export THELI_LOGGING
export THELI_LOGLEVEL

export INSTRUMENT=OMEGACAM
ASTROMMETHOD=SCAMP
ASTROMADD="_scamp_2MASS"

export THELI_LOGDIR=`pwd`/logs_TESTDATA_SET


##S1: Perform astrometric calibration of science data
##===================================================


# S1.1: create catalogues for astrometric and photometric calibration
# -------------------------------------------------------------------
./parallel_manager.sh ./create_astromcats_weights_para.sh ${MD} ${SET} \
                        OFCS cat WEIGHTS OFCS.weight NONE 5

# S1.2: perform astrometric calibration (aclient needed)
# -------------------------------------
# alternatively to the previous two scripts you
# can perform astrometry/relative photometry with scamp.
# In this case the following script call is necessary:
./create_scamp_astrom_photom_multiinst.sh \
        -i OMEGACAM -d ${MD} ${SET} OFCS -p 4.0 -k LONG -c 2MASS

# S2: estimate some statistics on images:(aclient needed)
# =======================================
./create_stats_table.sh ${MD} ${SET} OFCS headers${ASTROMADD}

## S3: estimate final photometric zeropoint for a co-added stack(aclient needed)
## (This is based on quantities from the previous statistics step)
## ===============================================================
./create_absphotom_photometrix.sh ${MD} ${SET}

##
## S4: create diagnostic plots with SuperMongo (SM)
## ================================================
./make_checkplot_stats.sh ${MD} ${SET} chips.cat5 STATS ${SET}

## S5: subtract the sky from science images and detect satellite tracks
#### ====================================================================
./parallel_manager.sh ./create_skysub_para.sh \
                      ${MD} ${SET} OFCS ".sub" TWOPASS

 ##perform simplistic satellite track detection; this step needs
 ##to be donw on sky-subtratced images. Hence, it is done here.
./parallel_manager.sh ./create_track_detect_para.sh \
      ${MD} ${SET} WEIGHTS OFCS.sub OFCS \
      ${MD}/${SET}/track_plots

 #S6: Image Co-addition
 #=====================

 #perform final image co-addition; if you want to use the experimental
 #header update feature after co-addition (see below) you ALWAYS
 #have to provide a coaddition condition (-l option) to the following
 #script.

 #S6.1: some preparation, file creation for co-addition
# -----------------------------------------------------
./prepare_coadd_swarp.sh -m ${MD} -s ${SET} -e "OFCS.sub" -n TEST \
                         -w ".sub" -eh headers${ASTROMADD} \
                         -l ${MD}/${SET}/cat/chips.cat5 STATS\
                            "(SEEING<$seeing);" \
                            ${MD}/${SET}/cat/TEST.cat

#move the header file : 			    
TMPNAME_1=`echo ${MD} | sed -e 's!/\{2,\}!/!g' -e 's!/*$!!' |\
           ${P_GAWK} -F/ '{print $NF}'`
TMPNAME_2=`echo ${SET} | sed -e 's!/\{2,\}!/!g' -e 's!/*$!!' |\
           ${P_GAWK} -F/ '{print $NF}'`
COADDFILENAME=${TMPNAME_1}_${TMPNAME_2}_TEST
mv coadd_${COADDFILENAME}.head ${REDDIR}/coadd_${COADDFILENAME}.head


## S6.2: Resample single images
## ----------------------------
./parallel_manager.sh ./resample_coadd_swarp_para.sh ${MD} ${SET} \
                      "OFCS.sub" TEST ${REDDIR}

# we noticed that resampled images have a slight bias in the
# overall sky-background value. We again estimate a background
#level for the whole image and subtract it:
./parallel_manager.sh ./create_modesub_para.sh ${MD}/${SET} \
                      coadd_TEST ? \
                      OFCS.sub.TEST.resamp.

### S6.3:  Final image co-addition
### ------------------------------
./perform_coadd_swarp.sh ${MD} ${SET} TEST OFCS.sub.TEST.resamp. \
                     "" ${REDDIR} MAP_WEIGHT FLAG WEIGHTED

#
# HERE THE FORMAL RESPONSIBILITY OF THE PIPELINE ENDS
# THE STEPS DESCRIBED IN THE FOLLOWING ARE EXPERIMENTAL
# AND NOT YET CLEANLY IMPLEMENTED IN THE PIPELINE FLOW.

#
# update the header of the co-added image with information on
# total exposure time, effective gain, magnitude zeropoint information

##
# first get the magnitude zeropoint for the co-added image:
if [ -f ${MD}/${SET}/cat/chips_phot.cat5 ]; then
  MAGZP=$(${P_LDACTOASC} -i ${MD}/${SET}/cat/chips_phot.cat5 -t ABSPHOTOM \
                   -k COADDZP | awk 'END {print $0}')
else
  MAGZP=-1.0
fi
#
# now do the actual header update
./update_coadd_header.sh ${MD} ${SET} TEST STATS coadd ${MAGZP} AB \
                         "(SEEING<$seeing);" 2MASS OMEGACAM "VST"
###
###
### END OF PROCESSING SCRIPT
###
