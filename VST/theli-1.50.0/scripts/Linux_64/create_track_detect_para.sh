#!/usr/bin/bash -u

# -----------------------------------------------------------------------
# File Name:           create_track_detect_para.sh
# Author:              Thomas Erben (terben@astro.uni-bonn.de)
# Last modified on:    29.03.2019
# Description:         Performs simplistic satellite track detection
# -----------------------------------------------------------------------

# wrapper script for track_detect.py. The script is a more simplistic version
# of satellite track detection than the hough-transform implementation of
# Patrick. However, the running time of this script is considerably shorter
# and we use the techinuqe for short exposures such as the ATLAS survey.

# SCRIPT HISTORY
# ==============
#
# 29.03.2018:
# if plots should be created we create the necessary output directory
# if it does not yet exist.

#$1: MD (main directory)
#$2: SD (science directory)
#$3: WD (weight directory containing weight and flag images; give NONE if
#        weights/flags should not be considered)
#$4: image ending
#$5: weight ending
#%6: output plot directory (give NONE if you do not want to store plots)
#$7: chips to work on

# File inclusions:
. ./${INSTRUMENT:?}.ini
. ./bash_functions.include
theli_start "$*" "${!#}"

if [ $# -ne 7 ]; then
  echo "SYNOPSIS: $0 MD SD WD ENDING WENDING PLOTDIR CHIPS"
  exit 1;
fi

MD=${1}
SD=${2}
WD=${3}
ENDING=${4}
WENDING=${5}
PLOT_DIR=${6}

for CHIP in ${!#}
do
  IMAGES=$(${P_FIND} ${MD}/${SD} -name \*_${CHIP}${ENDING}.fits)

  for IMAGE in ${IMAGES}
  do
    BASE=$(basename ${IMAGE} ${ENDING}.fits)
    DIR=$(dirname ${IMAGE})

    PLOTARG=""
    if [ "${PLOT_DIR}" != "NONE" ]; then
      if [ ! -d "${PLOT_DIR}" ]; then
        theli_warn "${PLOT_DIR} does not exist! I create it."
        mkdir -p ${PLOT_DIR}
      fi
      PLOTARG="-p ${PLOT_DIR}/${BASE}.png"
    fi

    WEIGHTARG=""
    if [ "${WD}" != "NONE" ]; then
      WEIGHT=${MD}/${WD}/${BASE}${WENDING}.weight.fits
      FLAG=${MD}/${WD}/${BASE}${WENDING}.flag.fits
      WEIGHTARG="--input_weight ${WEIGHT} --input_flag ${FLAG}"
    fi

    ${P_PYTHON3} ./track_detect.py -i ${IMAGE} ${WEIGHTARG} ${PLOTARG}
  done
done

theli_end "${!#}"
exit 0;
