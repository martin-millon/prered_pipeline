#!/bin/bash -u

# This script illustrates the basic usage of the THELI pipeline. The
# commands below process a OMEGACAM test data set from raw image to a
# final co-added image.

# This script also serves as an example for possible 'superscripts'
# collecting various pipeline tasks.

# Note that the script (as most of the individual THELI subtasks) does
# NOT react intelligently on errors during image reduction!!!!

#
# Edit the following two lines to point to the MD of your data and
# your reduction directory (The directory where all your reduction
# scripts are located and where you will execute individual processing
# steps):
while [ $# -gt 0 ]; do
        case $1 in
        -MD )                 shift
                md=$1
		;;
	    -REDDIR )                shift
                reddir=$1
		;;
		-PHOTCAT )				shift
	    		photcat=$1
		;;
		-FILTER )				shift
	    		filter=$1
		;;
		-SCRIPTDIR )				shift
	    		scriptdir=$1
		;;
	esac
			shift
done

export MD=$md
export REDDIR=$reddir
export PHOTCAT=$photcat
export FILTER=$filter

cd $scriptdir
. ./progs.ini


# setup THELI logging:
: ${THELI_LOGGING:="Y"}
: ${THELI_LOGLEVEL="2"} # Log level 2 means 'extensive output logging from
                        # all the scripts
export THELI_LOGGING
export THELI_LOGLEVEL

#
# THELI uses the INSTRUMENT environment variable to identify the
# instrument it is working on. Configuration files named
# ${INSTRUMENT}.ini are usually loaded by processing scripts to load
# necessary, instrument specific information:
export INSTRUMENT=OMEGACAM


# Do astrometric calibration with SCAMP
# We exclusively use SCMAP now and do not support ASTROMETRIC
# anymore. You should have scamp V2.7.7 (or higher) installed.
# It needs to suuport the GAIA-DR2 reference catalogue for
# astrometric calibration
ASTROMMETHOD=SCAMP
ASTROMADD="_scamp_2MASS"

#
# Here, RUN processing is performed:
# ==================================
#
# We log run and set processing to different directories:
export THELI_LOGDIR=`pwd`/logs_MONITORING

# R1: Split the MEF data into individual chips
# ============================================
#
# split data and update headers. This steps takes data
# from ${MD}/BIAS/ORIGINALS (accordingly for other species),
# splits the MEF files into single chips, updates the
# headers to the THELI format and puts resulting files
# to  ${MD}/BIAS etc.
./process_split_OMEGACAM_eclipse.sh -md ${MD} -sd BIAS
#./process_split_OMEGACAM_eclipse.sh -md ${MD} -sd DARK
./process_split_OMEGACAM_eclipse.sh -md ${MD} -sd SKYFLAT_${FILTER}
./process_split_OMEGACAM_eclipse.sh -md ${MD} -sd SCIENCE_${FILTER}
./process_split_OMEGACAM_eclipse.sh -md ${MD} -sd STANDARD_${FILTER}
#
# Here you could do a first data check and verify that the modes of
# raw images are within predefined values. A typical script call to
# only accept for further processing only skyflats with a mode
# between 5000 and 40000 would look like:
# ./check_files.sh ${MD}/ SKYFLAT_${FILTER} 5000 40000
#
# we omit this step as the data were chosen to be good !!

#
# R2: process BIAS frames
# =======================
#
# This step processed BIAS images (overscan correction and
# stacking). Final results can be found in ${MD}/BIAS and
# the produced final BIAS frames are called BIAS_i.fits (i being
# chip number)
./parallel_manager.sh ./process_bias_eclipse_para.sh ${MD}/ BIAS
#
# R3: process DARK frames
# =======================
#
# The same as the previous step for DARK frames
#./parallel_manager.sh ./process_bias_eclipse_para.sh ${MD}/ DARK
#
# R4: process FLAT frames
# ======================
#
# This step processes FLAT frames (overscan correction, BIAS subtraction,
# stacking). Final results can be found in ${MD}/SKYFLAT_${FILTER} and the
# produced final SKYFLAT frames are called SKYFLAT_${FILTER}_i.fits
./parallel_manager.sh ./process_flat_eclipse_para.sh ${MD}/ BIAS \
                      SKYFLAT_${FILTER}
#
 #R5: process SCIENCE frames
 #==========================

 #OMEGACAM data suffer from crosstalk in several chips (25, 26 and 27).
 #We need to correct for it explicitely.
 #NOTES: (1) The correction is applied on an OB (ESO oberving block basis)
        #(2) The coefficients are stored in a file (here
            #ct_coeffs_SCIENCE_$filter_TESTDATA). If you process data multiple
            #times, cou can reuse (store) the file and only do the
            #apply_crosstalk step
./create_crosstalk_coefficients_exposurepara.sh ${MD} \
            SCIENCE_${FILTER} OBSTART \
            ${MD}/ct_coeffs_SCIENCE_${FILTER}

./apply_crosstalk_exposurepara.sh ${MD} SCIENCE_${FILTER} \
            ${MD}/ct_coeffs_SCIENCE_${FILTER} \
            ${MD}/SCIENCE_${FILTER}/CT ""

if [ -d ${MD}/SCIENCE_${FILTER}/CT ]; then
  mv ${MD}/SCIENCE_${FILTER}/CT/*fits ${MD}/SCIENCE_${FILTER}
  rmdir ${MD}/SCIENCE_${FILTER}/CT
fi

# (overscan correction, bias subtraction,
# flatfielding, superflatfielding). ${FILTER} OMEAGCAM images
# do not need defringing. Please ask me if you have i_SDSS or
# z_SDSS images:
./parallel_manager.sh ./process_science_eclipse_para.sh ${MD}/ BIAS \
                      SKYFLAT_${FILTER} \
                      SCIENCE_${FILTER} NORESCALE
./parallel_manager.sh ./process_superflat_eclipse_para.sh ${MD}/ \
                      SCIENCE_${FILTER} OFC NOFRINGE NOSUPERTEST
./parallel_manager.sh ./create_illumfringe_para.sh ${MD}/ SCIENCE_${FILTER}
./parallel_manager.sh ./process_science_illum_eclipse_para.sh ${MD}/ \
                      SCIENCE_${FILTER} OFC RESCALE ILLUM
#
# R6: create postage stamps of SCIENCE images
# ===========================================
#
# we create 8x8 binned mosaic images showing the prereduced images
# The result goes to ${MD}/SCIENCE_${FILTER}/BINNED
./create_binnedmosaics_exposurepara.sh \
    ${MD} SCIENCE_${FILTER} OMEGA OFCS 1 -32
