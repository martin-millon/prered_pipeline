#!/bin/bash -u

# This script illustrates the basic usage of the THELI pipeline. The
# commands below process a OMEGACAM test data set from raw image to a
# final co-added image.

# This script also serves as an example for possible 'superscripts'
# collecting various pipeline tasks.

# Note that the script (as most of the individual THELI subtasks) does
# NOT react intelligently on errors during image reduction!!!!

#
# Edit the following two lines to point to the MD of your data and
# your reduction directory (The directory where all your reduction
# scripts are located and where you will execute individual processing
# steps):
while [ $# -gt 0 ]; do
        case $1 in
        -MD )                 shift
                md=$1
		;;
	    -REDDIR )                shift
                reddir=$1
		;;
		-PHOTCAT )				shift
	    		photcat=$1
		;;
		-FILTER )				shift
	    		filter=$1
		;;
		-SCRIPTDIR )				shift
	    		scriptdir=$1
		;;
	esac
			shift
done

export MD=$md
export REDDIR=$reddir
export PHOTCAT=$photcat
export FILTER=$filter

cd $scriptdir

. ./progs.ini

# The catalogue OMEGACAM_SDSS_overlap_01_2019.cat contains all sources
# from the OMEGACAM archive that overlap with Sloan up to 01/2019
export PHOTCAT=/Users/martin/Desktop/observation_VST/OMEGACAM_SDSS_overlap_01_2019.cat

# setup THELI logging:
: ${THELI_LOGGING:="Y"}
: ${THELI_LOGLEVEL="2"} # Log level 2 means 'extensive output logging from
                        # all the scripts
export THELI_LOGGING
export THELI_LOGLEVEL

#
# THELI uses the INSTRUMENT environment variable to identify the
# instrument it is working on. Configuration files named
# ${INSTRUMENT}.ini are usually loaded by processing scripts to load
# necessary, instrument specific information:
export INSTRUMENT=OMEGACAM
#
# The following is just used in this script to identify
# data directories.

# Do astrometric calibration with SCAMP
# We exclusively use SCMAP now and do not support ASTROMETRIC
# anymore. You should have scamp V2.7.7 (or higher) installed.
# It needs to suuport the GAIA-DR2 reference catalogue for
# astrometric calibration
ASTROMMETHOD=SCAMP
ASTROMADD="_scamp_2MASS"


##
## R6: create postage stamps of SCIENCE images
## ===========================================
##
## we create 8x8 binned mosaic images showing the prereduced images
## The result goes to ${MD}/SCIENCE_${FILTER}/BINNED
#./create_binnedmosaics_exposurepara.sh \
    #${MD} SCIENCE_${FILTER} OMEGA OFCS 8 -32

#
# you should take a look at the created mosaics at this stage. Usually
# you would use them to identify whether preprocessing steps should
# be iterated or whether masks should be created (for instance to mark
# satellite tracks). For the example set the only necessary masks
# are WFI.2000-12-26T07:30:37.238_[5-8].reg. Make sure they are in the
# ${MD}/SCIENCE_${FILTER}/reg directory before proceeding.
#
# R7: create weight frames
# ========================

 #first rescale SKYFLATs and SUPER FLATS to a mode of '1'.
 #Outputs are 8 files in ${MD}/ DOMEFLAT_R_norm  and
 #${MD}/SCIENCE_R_norm/:
#./parallel_manager.sh ./create_norm_para.sh ${MD} DOMEFLAT_${FILTER}
#./parallel_manager.sh ./create_norm_para.sh ${MD} SCIENCE_${FILTER}

 #produce global weights and flags:
 ##outputs: 8 globalflags and globalweights in ${MD}/WEIGHTS
#./parallel_manager.sh ./create_global_weights_flags_para.sh ${MD} NOLINK\
                      #DOMEFLAT_${FILTER}_norm 0.7 1.3 \
                      #SCIENCE_${FILTER}_norm 0.9 1.1

## finally create weight images for all individual images:
#./parallel_manager.sh ./create_weights_flags_para.sh ${MD} \
                      #SCIENCE_${FILTER} OFCS WEIGHTS_FLAGS HIGHSN
		      

## R8: Photometric calibration
## ===========================
##
## first process standardstar frames in exactly the same way as the
## science observations.
#./parallel_manager.sh ./process_science_eclipse_para.sh ${MD} BIAS \
                        #DOMEFLAT_${FILTER} STANDARD_${FILTER} NORESCALE
##./parallel_manager.sh ./create_illumfringe_para.sh ${MD} STANDARD_${FILTER} \
                        ###SCIENCE_${FILTER}
##./parallel_manager.sh ./process_science_illum_eclipse_para.sh ${MD} \
                        ##STANDARD_${FILTER} OFC RESCALE ILLUM

### R8.1: create weights and flags for the standard star fields:
### ------------------------------------------------------------
#./parallel_manager.sh ./create_weights_flags_para.sh ${MD} \
                      #STANDARD_${FILTER} OFCS WEIGHTS_FLAGS HIGHSN

## R8.2 source extraction from the standardstar fields
## ---------------------------------------------------
#./parallel_manager.sh ./create_astromcats_weights_para.sh \
                      #${MD} STANDARD_${FILTER} OFCS cat \
                      #WEIGHTS OFCS.weight OFCS.flag 5

## R8.3 astrometric calibration for the standard star fields
## ---------------------------------------------------------
##
## we use the 2MASS catalogue. It does not really matter but
## you could also use GAIA of course.
## Note that we depend on SCAMP as astrometric tool for this step!
#./create_scamp_astrom_photom_multiinst.sh \
       #-i ${INSTRUMENT} -d ${MD} STANDARD_${FILTER} OFCS -p 4.0 \
       #-k SHORT -c 2MASS

## merge the extracted sources with those from a standard star catalogue:
#./parallel_manager.sh ./create_stdphotom_prepare_para.sh ${MD} \
                        #STANDARD_${FILTER} OFCS 2MASS

## The following command adds 'global coordinates' to the catalogues.
## We will not need those quantities here but we need to call the
## script to ensure the pipeline to run correctly:
#./parallel_manager.sh ./create_global_mosaic_coord_para.sh ${MD} \
          #STANDARD_${FILTER} OFCS 2MASS
#./create_stdphotom_merge_exposurepara.sh ${MD} STANDARD_${FILTER} \
       #OFCS ${PHOTCAT}

## determine an absolute photometric solution and update headers
## of science frames:
##
#./create_abs_photo_info.sh ${MD} STANDARD_${FILTER} \
       #OFCS r_SDSS r gmr -0.1 0.05 RUNCALIB

#./create_zp_correct_header_exposurepara.sh ${MD} SCIENCE_${FILTER} OFCS \
         #${MD}/STANDARD_${FILTER}/calib/night_0_${FILTER}_result.asc 2 RUN
