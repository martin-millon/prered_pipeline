#!/usr/bin/bash -u

# The script writes photometric keywords to images that do not have
# them. As fourth argument we can provide an ASCII file with
# photometric information as produced by photo_abs.py.  If no file
# provided 'dummy' keywords indicating 'no photometric inforation
# available' are written.  This is to bring headers of frames for
# which no standardstar observations are available in accordance with
# photometrically calibrated frames. The script should be called after
# create_abs_phot_info.sh

# This is a parallel version of 'create_zp_correct_header.sh'

# SCRIPT HISTORY:
#
# 10.12.2014:
# script started

# $1: main dir
# $2: science dir (the cat dir is a subdirectory of this)
# $3: extension
# $4: file with photometric information (OPTIONAL)
# $5: ZPCHOICE (OPTIONAL; only useful if fourth argument
#     is provided as well)
# $6: PHOTCALM (NIGHT or RUN; only useful if also fourth and
#     fifth argument are provided)

. ./${INSTRUMENT:?}.ini
. ./bash_functions.include
theli_start "$*"

if [ $# -lt 3 ] && [ $# -gt 6 ]; then
  theli_error "Wrong number of command line arguments!"
  exit 1;
fi

if [ $# -gt 3 ]; then
  PHOTFILE=$4
  if [ ! -f ${PHOTFILE} ]; then
    echo "File ${PHOTFILE} with photometric information not present!"
    exit 1;
  fi

  ZPCHOICE=$5
  if [ ${ZPCHOICE} -lt 0 ] || [ ${ZPCHOICE} -gt 3 ]; then
    echo "error: ZPCHOICE must be between 0 and 3."
    exit 1;
  fi

  # read the values for zeropoints and exdtinction coefficients into
  # shell arrays (their indices start with 0!)
  ZP_READ=`${P_GAWK} '{print $1}' ${PHOTFILE}`
  ZP=(${ZP_READ})

  COEFFS_READ=`${P_GAWK} '{print $2}' ${PHOTFILE}`
  COEFFS=(${COEFFS_READ})

  PHOTCALM="'$6'"

  MODE=REPLACE
else
  ZPCHOICE=0
  ZP=(-1.0 -1.0 -1.0)
  COEFFS=(-1.0 -1.0 -1.0)

  PHOTCALM="'NONE'"

  MODE=NOREPLACE
fi

${P_FIND} /$1/$2 -name \*$3.fits > ${TEMPDIR}/images_$$

# check whether we need to do something at all:
if [ -s ${TEMPDIR}/images_$$ ]; then
  # construct the argument list for the 'writekey' command:
  value "${ZPCHOICE}"
  WRITEKEYARG=(ZPCHOICE "${VALUE}" ${MODE})

  # In case we have a valid photometric calibration we write the ZP
  # and COEFF keywords:
  if [ $# -gt 3 ] && [ ${ZPCHOICE} -gt 0 ]; then
    value "${ZP[${ZPCHOICE}]}"
    WRITEKEYARG+=(ZP "${VALUE} / Magnitude zeropoint" ${MODE})
    value "${COEFFS[${ZPCHOICE}]}"
    WRITEKEYARG+=(COEFF "${VALUE} / Extinction coefficient" ${MODE})
  fi

  i=1
  while [ ${i} -le 3 ]
  do
    j=$(( $i - 1 ))  # array indices start at 0, not at 1.
    value "${ZP[$j]}"
    WRITEKEYARG+=(ZP$i "${VALUE} / Magnitude zeropoint" ${MODE})
    value "${COEFFS[$j]}"
    WRITEKEYARG+=(COEFF${i} "${VALUE} / Extinction coefficient" ${MODE})
    i=$(( $i + 1 ))
  done

  value "${PHOTCALM}"
  WRITEKEYARG+=(PHOTCALM
               "${VALUE} / photometric calibration mode (RUN or NIGHT)"
               ${MODE})


  # do the file updating in parallel:
  i=0
  while [ ${i} -lt ${NPARA} ]
  do
    FILES[$i]=`${P_GAWK} '(NR%'${NPARA}' == '${i}')' ${TEMPDIR}/images_$$`
    NFILES[$i]=`echo ${FILES[$i]} | wc -w`

    if [ ${NFILES[$i]} -gt 0 ]; then
      {
        for FILE in ${FILES[$i]}
        do
          writekey ${FILE} "${WRITEKEYARG[@]}"
        done
      } &
    fi
    ((i++))
  done
else
  theli_error "No files to process; Nothing to do!"
  test -f ${TEMPDIR}/images_$$ && rm ${TEMPDIR}/images_$$
  exit 1;
fi

# clean up and bye:
test -f ${TEMPDIR}/images_$$ && rm ${TEMPDIR}/images_$$

theli_end
exit 0;
