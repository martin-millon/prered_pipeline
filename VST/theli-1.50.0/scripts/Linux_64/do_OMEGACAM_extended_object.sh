#!/bin/bash -u

# This script illustrates the basic usage of the THELI pipeline. The
# commands below process a OMEGACAM test data set from raw image to a
# final co-added image.

# This script also serves as an example for possible 'superscripts'
# collecting various pipeline tasks.

# Note that the script (as most of the individual THELI subtasks) does
# NOT react intelligently on errors during image reduction!!!!

#
# Edit the following two lines to point to the MD of your data and
# your reduction directory (The directory where all your reduction
# scripts are located and where you will execute individual processing
# steps):
while [ $# -gt 0 ]; do
        case $1 in
        -MD )                 shift
                md=$1
		;;
	    -REDDIR )                shift
                reddir=$1
		;;
		-PHOTCAT )				shift
	    		photcat=$1
		;;
		-FILTER )				shift
	    		filter=$1
		;;
		-SCRIPTDIR )				shift
	    		scriptdir=$1
		;;
	esac
			shift
done

export MD=$md
export REDDIR=$reddir
export PHOTCAT=$photcat
export FILTER=$filter

cd $scriptdir
. ./progs.ini


# setup THELI logging:
: ${THELI_LOGGING:="N"}
: ${THELI_LOGLEVEL="1"} # Log level 2 means 'extensive output logging from
                        # all the scripts
export THELI_LOGGING
export THELI_LOGLEVEL

#
# THELI uses the INSTRUMENT environment variable to identify the
# instrument it is working on. Configuration files named
# ${INSTRUMENT}.ini are usually loaded by processing scripts to load
# necessary, instrument specific information:
export INSTRUMENT=OMEGACAM


# Do astrometric calibration with SCAMP
# We exclusively use SCMAP now and do not support ASTROMETRIC
# anymore. You should have scamp V2.7.7 (or higher) installed.
# It needs to suuport the GAIA-DR2 reference catalogue for
# astrometric calibration
ASTROMMETHOD=SCAMP
ASTROMADD="_scamp_2MASS"

#
# Here, RUN processing is performed:
# ==================================
#
# We log run and set processing to different directories:
export THELI_LOGDIR=`pwd`/logs_MONITORING

# R1: Split the MEF data into individual chips
# ============================================
#
# split data and update headers. This steps takes data
# from ${MD}/BIAS/ORIGINALS (accordingly for other species),
# splits the MEF files into single chips, updates the
# headers to the THELI format and puts resulting files
# to  ${MD}/BIAS etc.
./process_split_OMEGACAM_eclipse.sh -md ${MD} -sd BIAS
#./process_split_OMEGACAM_eclipse.sh -md ${MD} -sd DARK
./process_split_OMEGACAM_eclipse.sh -md ${MD} -sd SKYFLAT_${FILTER}
./process_split_OMEGACAM_eclipse.sh -md ${MD} -sd SCIENCE_${FILTER}
./process_split_OMEGACAM_eclipse.sh -md ${MD} -sd STANDARD_${FILTER}
#
# Here you could do a first data check and verify that the modes of
# raw images are within predefined values. A typical script call to
# only accept for further processing only skyflats with a mode
# between 5000 and 40000 would look like:
# ./check_files.sh ${MD}/ SKYFLAT_${FILTER} 5000 40000
#
# we omit this step as the data were chosen to be good !!

#
# R2: process BIAS frames
# =======================
#
# This step processed BIAS images (overscan correction and
# stacking). Final results can be found in ${MD}/BIAS and
# the produced final BIAS frames are called BIAS_i.fits (i being
# chip number)
./parallel_manager.sh ./process_bias_eclipse_para.sh ${MD}/ BIAS
#
# R3: process DARK frames
# =======================
#
# The same as the previous step for DARK frames
#./parallel_manager.sh ./process_bias_eclipse_para.sh ${MD}/ DARK
#
# R4: process FLAT frames
# ======================
#
# This step processes FLAT frames (overscan correction, BIAS subtraction,
# stacking). Final results can be found in ${MD}/SKYFLAT_${FILTER} and the
# produced final SKYFLAT frames are called SKYFLAT_${FILTER}_i.fits
./parallel_manager.sh ./process_flat_eclipse_para.sh ${MD}/ BIAS \
                      SKYFLAT_${FILTER}

 #R5: process SCIENCE frames
 #==========================

 #OMEGACAM data suffer from crosstalk in several chips (25, 26 and 27).
 #We need to correct for it explicitely.
 #NOTES: (1) The correction is applied on an OB (ESO oberving block basis)
        #(2) The coefficients are stored in a file (here
            #ct_coeffs_SCIENCE_$filter_TESTDATA). If you process data multiple
            #times, cou can reuse (store) the file and only do the
            #apply_crosstalk step
./create_crosstalk_coefficients_exposurepara.sh ${MD} \
            SCIENCE_${FILTER} OBSTART \
            ${REDDIR}/ct_coeffs_SCIENCE_${FILTER}_TESTDATA

./apply_crosstalk_exposurepara.sh ${MD} SCIENCE_${FILTER} \
            ${REDDIR}/ct_coeffs_SCIENCE_${FILTER}_TESTDATA \
            ${MD}/SCIENCE_${FILTER}/CT ""

if [ -d ${MD}/SCIENCE_${FILTER}/CT ]; then
  mv ${MD}/SCIENCE_${FILTER}/CT/*fits ${MD}/SCIENCE_${FILTER}
  rmdir ${MD}/SCIENCE_${FILTER}/CT
fi

# (overscan correction, bias subtraction,
# flatfielding, superflatfielding). ${FILTER} OMEAGCAM images
# do not need defringing. Please ask me if you have i_SDSS or
# z_SDSS images:
./parallel_manager.sh ./process_science_eclipse_para.sh ${MD}/ BIAS \
                      SKYFLAT_${FILTER} \
                      SCIENCE_${FILTER} NORESCALE
#./parallel_manager.sh ./process_superflat_eclipse_para.sh ${MD}/ \
                      SCIENCE_${FILTER} OFC NOFRINGE NOSUPERTEST
#./parallel_manager.sh ./create_illumfringe_para.sh ${MD}/ SCIENCE_${FILTER}
#./parallel_manager.sh ./process_science_illum_eclipse_para.sh ${MD}/ \
                      SCIENCE_${FILTER} OFC RESCALE ILLUM

 #R6: create postage stamps of SCIENCE images
 #===========================================

 #we create 8x8 binned mosaic images showing the prereduced images
 #The result goes to ${MD}/SCIENCE_${FILTER}/BINNED
#./create_binnedmosaics_exposurepara.sh \
    ${MD} SCIENCE_${FILTER} OMEGA OFC 8 -32


 #you should take a look at the created mosaics at this stage. Usually
 #you would use them to identify whether preprocessing steps should
 #be iterated or whether masks should be created (for instance to mark
 #satellite tracks). For the example set the only necessary masks
 #are WFI.2000-12-26T07:30:37.238_[5-8].reg. Make sure they are in the
 #${MD}/SCIENCE_${FILTER}/reg directory before proceeding.

# R7: create weight frames
# ========================
#
# first rescale SKYFLATs and SUPER FLATS to a mode of '1'.
# Outputs are 8 files in ${MD}/ SKYFLAT_R_norm  and
# ${MD}/SCIENCE_R_norm/:
#./parallel_manager.sh ./create_norm_para.sh ${MD} SKYFLAT_${FILTER}
#./parallel_manager.sh ./create_norm_para.sh ${MD} SCIENCE_${FILTER}

# produce global weights and flags:
# outputs: 8 globalflags and globalweights in ${MD}/WEIGHTS
#./parallel_manager.sh ./create_global_weights_flags_para.sh ${MD} NOLINK\
                      SKYFLAT_${FILTER}_norm 0.7 1.3 \
                      SCIENCE_${FILTER}_norm 0.9 1.1

# finally create weight images for all individual images:
#./parallel_manager.sh ./create_weights_flags_para.sh ${MD} \
                      SCIENCE_${FILTER} OFCS WEIGHTS_FLAGS HIGHSN

#
# R8: Photometric calibration (will only chnage the header of the OFCS files)
# ===========================
#
# first process standardstar frames in exactly the same way as the
# science observations.
#./parallel_manager.sh ./process_science_eclipse_para.sh ${MD} BIAS \
                        SKYFLAT_${FILTER} STANDARD_${FILTER} NORESCALE
#./parallel_manager.sh ./create_illumfringe_para.sh ${MD} STANDARD_${FILTER} \
                        SCIENCE_${FILTER}
#./parallel_manager.sh ./process_science_illum_eclipse_para.sh ${MD} \
                        STANDARD_${FILTER} OFC RESCALE ILLUM

# R8.1: create weights and flags for the standard star fields:
# ------------------------------------------------------------
#./parallel_manager.sh ./create_weights_flags_para.sh ${MD} \
                      STANDARD_${FILTER} OFCS WEIGHTS_FLAGS HIGHSN

# R8.2 source extraction from the standardstar fields
# ---------------------------------------------------
#./parallel_manager.sh ./create_astromcats_weights_para.sh \
                      ${MD} STANDARD_${FILTER} OFCS cat \
                      WEIGHTS OFCS.weight OFCS.flag 5

 #R8.3 astrometric calibration for the standard star fields
 #---------------------------------------------------------

 #we use the 2MASS catalogue. It does not really matter but
 #you could also use GAIA of course.
# Note that we depend on SCAMP as astrometric tool for this step!
#./create_scamp_astrom_photom_multiinst.sh \
       -i ${INSTRUMENT} -d ${MD} STANDARD_${FILTER} OFCS -p 4.0 \
       -k SHORT -c 2MASS

# merge the extracted sources with those from a standard star catalogue:
#./parallel_manager.sh ./create_stdphotom_prepare_para.sh ${MD} \
                        STANDARD_${FILTER} OFCS 2MASS

# The following command adds 'global coordinates' to the catalogues.
# We will not need those quantities here but we need to call the
## script to ensure the pipeline to run correctly:
#./parallel_manager.sh ./create_global_mosaic_coord_para.sh ${MD} \
          STANDARD_${FILTER} OFCS 2MASS
#./create_stdphotom_merge_exposurepara.sh ${MD} STANDARD_${FILTER} \
       OFCS ${PHOTCAT}

## determine an absolute photometric solution and update headers
## of science frames:
##
#./create_abs_photo_info.sh ${MD} STANDARD_${FILTER} \
       OFCS ${FILTER} r gmr -0.1 0.05 RUNCALIB

#./create_zp_correct_header_exposurepara.sh ${MD} SCIENCE_${FILTER} OFCS \
         ${MD}/STANDARD_${FILTER}/calib/night_0_${FILTER}_result.asc 2 RUN

## include the following calls if you checked your results up to now
## and if you want to clean up disk space
### ./cleanfiles.sh ${MD}/BIAS WFI "."
### ./cleanfiles.sh ${MD}/BIAS WFI "OC."
### ./cleanfiles.sh ${MD}/DARK WFI "."
### ./cleanfiles.sh ${MD}/DARK WFI "OC."
### ./cleanfiles.sh ${MD}/SKYFLAT_${FILTER} WFI "."
### ./cleanfiles.sh ${MD}/SKYFLAT_${FILTER} WFI "OC."
### ./cleanfiles.sh ${MD}/SCIENCE_${FILTER}/SPLIT_IMAGES WFI "."
### rmdir ${MD}/SCIENCE_${FILTER}/SPLIT_IMAGES
### ./cleanfiles.sh ${MD}/SCIENCE_${FILTER}/OFC_IMAGES WFI "OFC."
### rmdir ${MD}/SCIENCE_${FILTER}/OFC_IMAGES
### ./clesnfiles.sh ${MD}/SCIENCE_${FILTER}/SUB_IMAGES WFI "OFC_sub."
### rmdir ${MD}/SCIENCE_${FILTER}/SUB_IMAGES
### ./cleanfiles.sh ${MD}/STANDARD_${FILTER}/SPLIT_IMAGES WFI "."
### rmdir ${MD}/STANDARD_${FILTER}/SPLIT_IMAGES
### ./cleanfiles.sh ${MD}/STANDARD_${FILTER}/OFC_IMAGES WFI "OFC."
### rmdir ${MD}/STANDARD_${FILTER}/OFC_IMAGES
###
