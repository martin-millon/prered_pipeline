#!/usr/bin/bash -u

# The script calculates global mosaic coordiantes w.r.t. the mosaics
# astrometric system provided in scamp header files.
# We need a 'global pixel coordinate system' for the illumination correction
# task. The script needs to be called after 'create_stdphotom_prepare_para.sh'
# and before 'create_stdphotom_merge_exposurepara.sh'.

# HISTORY:
# ========
#
# 20.07.2017:
# I forgot to enable THELI logging within the script - fixed.

#$1: MD
#$2: SD
#$3: ending
#$4: astrometric catalogue (scamp headers are assumed to be in
#                          $1/$2/headers_scamp_$4)
#$5: chips to work on

. ./${INSTRUMENT:?}.ini
. ./bash_functions.include
theli_start "$*" "${!#}"

if [ $# -ne 5 ]; then
  echo "SYNOPSIS: $0 MD SD ENDING ASTROCAT CHIPS_TO_PROCESS"
  exit 1;
fi

MD=$1
SD=$2
ENDING=$3
STANDARDCAT=$4

for CHIP in ${!#}
do
  CATS=$(find ${MD}/${SD}/cat/*_${CHIP}${ENDING}_photprep.cat)

  test -f ${TEMPDIR}/tmp.txt_$$ && rm ${TEMPDIR}/tmp.txt_$$
  for CAT in ${CATS}
  do
    BASE=$(basename ${CAT} ${ENDING}_photprep.cat)
    HEADER=${MD}/${SD}/headers_scamp_${STANDARDCAT}/${BASE}.head

    echo "${CAT} STDTAB ${HEADER}" >> ${TEMPDIR}/tmp.txt_$$
  done
  ${P_PYTHON3} add_global_pixel_coord.py ${TEMPDIR}/tmp.txt_$$
done

# clean up and bye:
rm ${TEMPDIR}/*_$$
theli_end "${!#}"
exit 0;
