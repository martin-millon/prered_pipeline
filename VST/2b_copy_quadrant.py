import os, sys, shutil, glob
from astropy.io import fits
import numpy as np

def main(date, datapath, workpath, command, chip, filter, redo = False, copy_extra_quadrant = True):
    if redo == False :
        command = command + ' -n' #no clobber command (do not overwrite)

    origin_folder = 'SCIENCE_%s'%filter
    #origin_folder = 'set_1'

    print("=" * 15, date, "=" * 15)

    filenames_chip = glob.glob(os.path.join(datapath,date,origin_folder,'*_%iOFCS.fits'%chip))
    if len(filenames_chip) >= 1 :
        success = True
        for file in filenames_chip : 
            print('Successfully found the reduced quadrant')
            filename = file.split('/')[-1]
            prefix = filename.split('_')[0]
            hdulist = fits.open(file)
            header = hdulist[0].header
            object_name = header["OBJECT"]
            reddir = os.path.join(workpath,object_name)
        
            if not os.path.isdir(reddir):
                os.mkdir(reddir)
        
            os.system('%s %s %s'%(command, file, os.path.join(reddir,filename)))
    else :
        print('Reduced quadrant does not exit. Problem occured in THELI.')
        success = False
        return success

    if copy_extra_quadrant :
        filenames_all = glob.glob(os.path.join(datapath,date,origin_folder,'*_*OFCS.fits'))
        for file in filenames_all :
            filename = file.split('/')[-1]
            prefix = filename.split('_')[0]
            chip_num = filename.split('_')[1]
            chip_num = chip_num.split('OFCS')[0]
            if int(chip_num) != chip :  #only if it is not the chip where the lens is
                hdulist = fits.open(file)
                header = hdulist[0].header
                object_name = header["OBJECT"]
                reddir = os.path.join(workpath,object_name+'_wide_field')

                if not os.path.isdir(reddir):
                    os.mkdir(reddir)

                os.system('%s %s %s'%(command, file, os.path.join(reddir,filename)))

    return success

if __name__ == '__main__':
    datapath = '/obs/lenses_EPFL/PRERED/VST/sorted/'
    workpath = '/obs/lenses_EPFL/PRERED/VST/reduced/'

    dates = [e.split('\n')[0] for e in os.popen('ls %s'%datapath).readlines()]
    dates = [d for d in dates if d >= '2019-05-03' and d <= '2019-05-03']
    filter = 'r_SDSS'

    chip = 13 #76
    command = 'cp' #cp to copy or mv to move the images to the sorted directory

    redo = False
    copy_extra_quadrant = True
    for date in dates :
        success = main(date, datapath, workpath, command, chip, filter, redo = redo, copy_extra_quadrant = copy_extra_quadrant)
	
    print (success)

