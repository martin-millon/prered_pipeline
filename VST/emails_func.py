import email
import smtplib
import ssl

def send_email(sender_email, receiver_email,path_pwd, message):
    smtp_server = "mailhost.astro.unige.ch"
    port = 25 # For starttls
    with open(path_pwd,'r') as f:
        password=f.read()

    # Create a secure SSL context

    # context = ssl.create_default_context()
    with smtplib.SMTP(smtp_server, port) as server:
        # server.ehlo()  # Can be omitted
        # server.starttls(context=context)
        # server.ehlo()  # Can be omitted
        # server.login(sender_email, password)
        server.sendmail(sender_email, receiver_email, message)
