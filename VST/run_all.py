#!/usr/bin/python3
try : 
    import os, sys, glob, datetime
    import get_VST_data 
    sort_data = __import__('1_sort_data')
    patch = __import__('patch')
    uncompress = __import__('0_uncompress_data')
    reduce_data = __import__('2_reduce_data')
    copy_quadrant = __import__('2b_copy_quadrant')
    distribute_sets = __import__('3_distribute_sets')
    deep_field = __import__('4_do_deep_field')
    copy_mosaic = __import__('5_copy_mosaic')
    clean_up = __import__('6_clean_up')
    from astropy.time import Time
    import numpy as np
    import argparse as ap
    from emails_func import send_email

except Exception as e : 
    g = open('/home/astro/millon/Desktop/PRERED/VST/pipe_log/cron_error.err', 'a')
    g.write('IMPORT ERROR : %s'%e)
    print('IMPORT ERROR : %s'%e)
    g.close()
    exit()

def main(dates): 
    rawpath = '/obs/lenses_EPFL/RAW/VST'
    workpath = '/obs/lenses_EPFL/PRERED/VST/sorted/'
    reducpath = '/obs/lenses_EPFL/PRERED/VST/reduced/'

    eso_login_path = os.path.join('/home/astro/millon/Desktop/PRERED', 'eso_login.txt')
    
    filter = "r_SDSS"
    program_id_list = ['1103.A-0801(D)','106.216P.001','106.216P.002','108.21Z4.001']#first program ID is used to download the calibration
    chip = 13
    
    redo = True
    clean = True
    download = True
    
    script_dir = '/home/astro/millon/Desktop/PRERED/VST/theli-1.50.0/scripts/Linux_64/'
    THELI_script = os.path.join(script_dir, 'do_OMEGACAM_monitoring.sh')
    THELI_script_distrib = os.path.join(script_dir, 'do_distribute_set.sh')
    THELI_script_deep_field = os.path.join(script_dir, 'do_deep_field.sh')
    photcat='/home/astro/millon/Desktop/PRERED/VST/OMEGACAM_SDSS_overlap_01_2019.cat'
    
    now = datetime.datetime.now()
    log_file = '/home/astro/millon/Desktop/PRERED/VST/pipe_log/log_%s.txt'%now.strftime("%Y-%m-%d_%H:%M")
    log_file_ap = '/home/astro/millon/Desktop/PRERED/VST/pipe_log/VSTglobal.txt'

    f = open(log_file, 'a')
    f.write('################# \n')
    f.write('Reduction starting on %s: \n'%now.strftime("%Y-%m-%d %H:%M"))

    fglob = open(log_file_ap, 'a')
    
    for date in dates : 
        print ("Starting reduction for nights %s"%date)
        f.write("=" * 15 + date + "=" * 15 + '\n')
        success_glob = True
        
        if download : 
            #Download the data :
            with open(eso_login_path, 'r') as feso:
                logins = feso.readlines()
            username=logins[0].split('\n')[0]
            pwd=logins[0].split('\n')[1]
            get_VST_data.main(date, rawpath, program_id_list, filter, username=username, pwd=pwd)
            
            #Unpack the data : 
            uncompress.main(date,rawpath, 'funpack',keep_compressed = False)
            
            #sort the data : 
            n_science = sort_data.main(date, rawpath,workpath, 'mv', filter, redo = redo)
            
            # temporary fix for dead CCD #77 
            f.write('WARNING : DATA ON CHIP #77 have been modified due to a problem of the CCD. \n')
            patch.main(date, workpath, filter)
        
        else : 
            n_science = 8 #arbitrary
        
        f.write('Science images : %i \n'%n_science)

        if n_science > 0 : #observation took place on that night
            #Use Theli to reduce the data : 
            reduce_data.main(date, workpath, script_dir, THELI_script, photcat, filter)
            
            #Save the outputs of the pipeline : 
            sucess_red = copy_quadrant.main(date, workpath, reducpath, 'cp', chip, filter, redo = redo, copy_extra_quadrant = True)
            
            if sucess_red : 
                f.write('Sucessfully reduced the night images \n')
                #Distribute between sets : 
                sets = distribute_sets.main(date, workpath, script_dir, THELI_script_distrib, photcat, filter)
                
                #Compute the night co-added image : 
                print ('I found the following sets : ', sets) 
                
                for set in sets : 
                    f.write('I found the following set : %s\n'%set) 
                    deep_field.main(date, workpath, set, script_dir, THELI_script_deep_field, photcat, filter)
            
                    #Save the coadded images : 
                    success_df = copy_mosaic.main(date, workpath, reducpath, 'cp', set, redo = redo)
                    
                    if success_df :
                        f.write('Sucessfully created the co-added image for set %s. \n'%set)
                    else :
                        success_glob = False
                        f.write('ERROR : problem occured when creating the co-added image for set %s. \n'%set)
                
                if clean :
                    clean_up.main(date, workpath, sets, filter)
                    for set in sets :
                        f.write('Cleaning up set %s... Done. \n'%set)
            else :
                success_glob = False
                f.write('ERROR : problem occured when reducing the data. \n')
            
        else : 
            print("No science images were taken on %s. Stopping there."%date)

        if success_glob:
            if n_science > 0:
                fglob.write("%s, Reduction %s : SUCCESS \n" %(now.strftime("%Y-%m-%d_%H:%M"), date))
            else :
                fglob.write("%s, Reduction %s : NO SCIENCE IMAGE \n" % (now.strftime("%Y-%m-%d_%H:%M"), date))
        else :
            fglob.write("%s, Reduction %s : FAILED \n"%(now.strftime("%Y-%m-%d_%H:%M"), date))
            path_pwd = os.path.join('/home/astro/millon/Desktop/PRERED','pwd.txt')
            message = """From: From reduction COSMOGRAIL <reduction.cosmograil@gmail.com>
            To: To Martin Millon <martin.millon@epfl.ch>
            Subject: VST reduction failure.

            The VST reduction pipeline has failed on %s.
            """%date

            send_email('reduction.cosmograil@gmail.com', 'martin.millon@epfl.ch', path_pwd, message)
    
    finish = datetime.datetime.now()
    f.write('Reduction finishing at %s: \n'%finish.strftime("%Y-%m-%d %H:%M"))
    f.close()
    fglob.close()
    
if __name__ == '__main__':
    parser = ap.ArgumentParser(prog="python {}".format(os.path.basename(__file__)),
                               description="Run the VST pipeline. No argument will run for the previous nigth.",
                               formatter_class=ap.RawTextHelpFormatter)
    help_startnight = "first night to reduce in the format YYYY-MM-DD"
    help_endnight = "last night to reduce in the format YYYY-MM-DD"

    parser.add_argument('--start', dest='startnight', type=str,
                            metavar='', action='store', default=None,
                            help=help_startnight)
    parser.add_argument('--end', dest='endnight', type=str,
                            metavar='', action='store', default=None,
                            help=help_endnight)
                            
    args = parser.parse_args()
    if args.startnight is None or args.endnight is None : 
        now = datetime.datetime.now()
        startnight = now.strftime("%Y-%m-%d")
        starttime = Time(startnight, format='iso', scale='utc').mjd - 1 #we reduce the previous night
        endtime = starttime #only one night
    
    else : 
        args.startnight = args.startnight + " 01:00:00"
        args.endnight = args.endnight + " 01:00:00"
        starttime = Time(args.startnight, format='iso', scale='utc').mjd
        endtime = Time(args.endnight, format='iso', scale='utc').mjd
        
    days = np.arange(starttime, endtime+1, 1)
    dates = [Time(d, format='mjd', scale='utc').iso[:10] for d in days]
    print ('Running from %s to %s'%(dates[0],dates[-1]))
    
    main(dates)
    
