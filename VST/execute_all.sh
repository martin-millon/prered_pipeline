#!/bin/sh -l 
#SBATCH -o /home/astro/millon/Desktop/PRERED/VST/pipe_log/slurm/slurm_%j.out  # STDOUT
#SBATCH -e /home/astro/millon/Desktop/PRERED/VST/pipe_log/slurm/slurm_%j.err  # STDERR

module add astro
module add python/3.7.1

export PATH=/home/astro/millon/.local/bin:$PATH
export PATH=~/.local/share/junest/bin:$PATH

export PYTHONPATH=${PYTHONPATH}:/home/astro/millon/Desktop/Mod_challenge/lenstronomy_extension/
export PYTHONPATH=${PYTHONPATH}:/home/astro/millon/Desktop/Mod_challenge/lenstronomy/
export PYTHONPATH=${PYTHONPATH}:/home/astro/millon/Desktop/tdlmcpipeline/
export ftp_proxy=http://proxy.unige.ch:3128
export http_proxy=http://proxy.unige.ch:3128
export https_proxy=http://proxy.unige.ch:3128

export PYTHONPATH=${PYTHONPATH}:/home/astro/millon/.local/lib/python3.7/site-packages/

date
echo 'Runing shell script'

cd /home/astro/millon/Desktop/PRERED/VST/

python3 run_all.py --start $YESTERDAY --end $YESTERDAY
