#!/usr/bin/python3

import os, sys, glob, datetime
from astropy.time import Time
import numpy as np
import argparse as ap


def main(date, datapath, rawpath, filter, sets, f,keep_science=True):
    f.write("=" * 15 + date + "=" * 15 + '\n')
    print("=" * 15, date, "=" * 15)

    # cleaning the sorted directory
    parent = os.path.join(datapath, date)
    try :
        directories = os.listdir(parent)
    except Exception as e :
        print('ERROR on %s'%date)
        f.write('ERROR on %s \n'%date)
        f.write('RERUN THE PIPELINE FOR THAT DATE !!! \n')
        f.write(e + '\n')
        return

    n_science = count_science(os.path.join(parent, 'SCIENCE_%s' % filter, 'ORIGINALS'))
    if keep_science: 
        science_folder = os.path.join(parent, 'SCIENCE_%s' % filter, 'ORIGINALS')
    else :
        science_folder = None

    if n_science > 0:
        if check_set(directories, sets):
            message = 'I had %i science images and I did the deep field properly \n' % n_science
            delete_dir(directories, parent, message = message, keep_folder = science_folder)
            f.write(message)
        else:
            f.write('I had a problem with folder %s. Sience images exists but not deep field was detected. \n' % date)
            f.write('I did not delete it \n')
    else:
        message = 'No science images in directory %s \n' % date
        delete_dir(directories, parent, message = message)
        f.write(message)

    # cleaning rawdir (but this should not contain any .fits file anymore):
    raw_dir = os.path.join(rawpath, date)
    os.system('rm %s'%os.path.join(raw_dir,'*.fits'))


def count_science(path):
    return len(glob.glob('*.fits'))


def check_set(list_dir, sets):
    for set in sets:
        if set in list_dir:
            return True
    return False


def delete_dir(list_dir, parent, message='', keep_folder = None):
    for directory in list_dir:
        if directory == keep_folder : 
            continue 
        else : 
            os.system('rm -r %s' % directory)

    now = datetime.datetime.now()
    with open(os.path.join(parent, 'cleaned.txt'), 'w') as handle:
        handle.write('cleaned on %s \n' % now.strftime("%Y-%m-%d_%H:%M"))
        handle.write(message)


if __name__ == '__main__':
    parser = ap.ArgumentParser(prog="python {}".format(os.path.basename(__file__)),
                               description="Run the VST pipeline. No argument will run for the revious nigth.",
                               formatter_class=ap.RawTextHelpFormatter)
    help_startnight = "first night to reduce in the format YYYY-MM-DD"
    help_endnight = "last night to reduce in the format YYYY-MM-DD"

    parser.add_argument('--start', dest='startnight', type=str,
                        metavar='', action='store', default=None,
                        help=help_startnight)
    parser.add_argument('--end', dest='endnight', type=str,
                        metavar='', action='store', default=None,
                        help=help_endnight)

    args = parser.parse_args()
    last_cleaning_file = '/home/astro/millon/Desktop/PRERED/VST/pipe_log/cleaning_log/last_cleaning.txt'
    now = datetime.datetime.now()

    if args.startnight is None or args.endnight is None:
        with open(last_cleaning_file, 'r') as g:
            starttime = Time(g.readline(), format='iso', scale='utc').mjd + 1

        print('Starting from :', Time(starttime, format='mjd', scale='utc').iso[:10])
        today = now.strftime("%Y-%m-%d")
        endtime = Time(today, format='iso',
                       scale='utc').mjd - 30  # by default we delete everything from the cleaning until 30 days ago

    else:
        args.startnight = args.startnight + " 01:00:00"
        args.endnight = args.endnight + " 01:00:00"
        starttime = Time(args.startnight, format='iso', scale='utc').mjd
        endtime = Time(args.endnight, format='iso', scale='utc').mjd

    datapath = '/obs/lenses_EPFL/PRERED/VST/sorted/'
    rawpath = '/obs/lenses_EPFL/RAW/VST/'
    log_file = '/home/astro/millon/Desktop/PRERED/VST/pipe_log/cleaning_log/cleaning_log_%s.txt' % now.strftime(
        "%Y-%m-%d_%H:%M")
    filter = 'r_SDSS'
    sets = ['set_1', 'set_2', 'set_3', 'set_4', 'set_5', 'set_6', 'set_7']
    f = open(log_file, 'a')
    f.write('Cleaning starting on %s: \n' % now.strftime("%Y-%m-%d %H:%M"))

    if starttime > endtime:
        print('I have nothing to do, your start time is after your end time')
        f.write("Starting on %s \n" % Time(starttime, format='mjd', scale='utc').iso[:10])
        f.write("Ending on %s \n" % Time(endtime, format='mjd', scale='utc').iso[:10])
        f.write("I had nothing to do ! \n")
    else:
        days = np.arange(starttime, endtime + 1, 1)
        dates = [Time(d, format='mjd', scale='utc').iso[:10] for d in days]

        for date in dates:
            print(date)
            main(date, datapath, rawpath, filter, sets, f, keep_science = True)

        with open(last_cleaning_file, 'w') as g:
            g.write(Time(endtime, format='mjd', scale='utc').iso[:10])

        print('Running from %s to %s' % (dates[0], dates[-1]))
        f.write('Ran from %s to %s' % (dates[0], dates[-1]))

    f.close()
