import os, sys, shutil, glob
from astropy.io import fits
import numpy as np

def mkdir_recursive(path):
    sub_path = os.path.dirname(path)
    if not os.path.exists(sub_path):
        mkdir_recursive(sub_path)
    if not os.path.exists(path):
        os.mkdir(path)

def main(date, datapath,workpath, command, filter, redo = False): 
    if redo == False : 
        command = command + ' -n' #no clobber command (do not overwrite)
    
    print(("=" * 15, date, "=" * 15))
    datadir = os.path.join(datapath, date)
    workdir = os.path.join(workpath, date)

    if redo:
        if os.path.isdir(workdir):
            shutil.rmtree(workdir)

    filepaths = [f.split('\n')[0] for f in os.popen('ls %s/*.fits' % datadir).readlines()]
    if len(filepaths) == 0 : 
        print("No images taken on night %s"%date)
        mkdir_recursive(workdir)
        return 0 
    
    n_bias = 0
    n_science = 0 
    n_flat = 0 
    n_std = 0

    for filepath in filepaths:

        filename = os.path.basename(filepath)

        print(filename)

        hdulist = fits.open(filepath)
        header = hdulist[0].header
        gender = header["OBJECT"]
        filter_header = header["HIERARCH ESO INS FILT1 NAME"]

        biasesdir = os.path.join(workdir, 'BIAS/ORIGINALS')
        skyflatsdir = os.path.join(workdir, 'SKYFLAT_%s/ORIGINALS'%(filter_header))
        sciencesdir = os.path.join(workdir, 'SCIENCE_%s/ORIGINALS'%filter_header)
        stddir = os.path.join(workdir, 'STANDARD_%s/ORIGINALS'%filter_header)

        #alldirs = [workdir, biasesdir, domeflatsdir, sciencesdir,stddir]
        alldirs = [workdir, biasesdir, skyflatsdir, sciencesdir,stddir]

        for dirpath in alldirs:
            if not os.path.isdir(dirpath):
                mkdir_recursive(dirpath)
        
        if gender == "FOCUS":
            continue
        
        if gender == "FLAT,SKY":
            if filter_header == filter :  
                n_flat +=1
                os.system('%s %s %s' % (command, filepath, skyflatsdir))
            else : 
                print(('Flat %s do not have the correct filter(%s)'%(filepath,filter_header)))
        elif gender == "BIAS":
            if n_bias >= 6 : 
                print ('I already have enough bias, I skip and delete this one.')
                os.system('rm %s'%filepath)
            else : 
                n_bias +=1 
                os.system('%s %s %s'%(command, filepath, biasesdir))
        elif gender == "STD,ZEROPOINT":
            if filter_header == filter : 
                n_std +=1
                os.system('%s %s %s'%(command, filepath, stddir))           
            else : 
                print(('Standard field %s do not have the correct filter (%s)'%(filepath,filter_header)))
        else:
            n_science +=1
            os.system('%s %s %s'%(command, filepath, sciencesdir))
    
    dates_all = [e.split('\n')[0] for e in os.popen('ls %s'%workpath).readlines()]
    
    if n_flat < 6 and n_science > 0: 
        print ('Not enough flats, I am looking for flats in the previous nights')
        nind = dates_all.index(date)
        for pdate in dates_all[:nind][::-1]: #list in reverse order
            if n_flat < 6 : 
                pflats = glob.glob(os.path.join(workpath, pdate, 'SKYFLAT_%s/ORIGINALS/*.fits'%filter))
                count_flats = 0 
                for pf in pflats :
                    if n_flat < 6 :  
                        link = pf.split('/')[-1]
                        link = os.path.join(skyflatsdir,link)
                        os.system('cp %s %s'%(pf, link))
                        n_flat +=1 
                        count_flats +=1
                    else : 
                        print ('Ok, I have enough flats')
                        break 
                        
                print(("\t\tI found %i flats in %s, %i taken" %(len(pflats),pdate, count_flats)))
            else : 
                break 
                    
    if n_bias < 6 and n_science > 0:
        print ('Not enough bias, I am looking for bias in the previous nights')
        nind = dates_all.index(date)
        for pdate in dates_all[:nind][::-1]: #list in reverse order
            if n_bias < 6 : 
                pbias = glob.glob(os.path.join(workpath, pdate, 'BIAS/ORIGINALS/*.fits'))
                count_bias = 0
                for pb in pbias : 
                    if n_bias < 6 : 
                        link = pb.split('/')[-1]
                        link = os.path.join(biasesdir,link)
                        os.system('cp %s %s'%( pb, link))
                        n_bias +=1 
                        count_bias +=1 
                    else : 
                        print ('Ok, I have enough bias')
                        break 
                print(("\t\tI found %i bias in %s, %i taken" %(len(pbias),pdate, count_bias)))
            else : 
                break 
                    
                
    if n_std < 1 and n_science > 0:
        print ('Not enough standard fields, I am looking for standard fields in the previous nights')
        nind = dates_all.index(date)
        for pdate in dates_all[:nind][::-1]: #list in reverse order
            if n_std > 1 : 
                pstd = glob.glob(os.path.join(workpath, pdate, 'STANDARD_%s/ORIGINALS/*.fits'%filter))
                count_std = 0 
                for ps in pstd : 
                    if n_std < 1 : 
                        link = ps.split('/')[-1]
                        link = os.path.join(stddir,link)
                        os.system('cp %s %s'%(ps, link))
                        n_std +=1 
                        count_std +=1 
                    else : 
                        print ('Ok, I have enough standard fields')
                        break
                
                print(("\t\tI found %i bias in %s, %i taken" %(len(pstd),pdate, count_std)))
            else : 
                break 
                    
    print("Sorting Done !")
    return n_science
 
if __name__ == '__main__':
    datapath = '/obs/lenses_EPFL/RAW/VST/'
    workpath = '/obs/lenses_EPFL/PRERED/VST/sorted/'
    
    dates_all = [e.split('\n')[0] for e in os.popen('ls %s' % datapath).readlines()]
    dates = [d for d in dates_all if d >= '2021-08-23' and d <= '2021-08-23']
    
    command = 'cp' #cp to copy or mv to move the images to the sorted directory
    
    filter = 'r_SDSS'
    redo = False
    
    for date in dates:
        n_science = main(date, datapath,workpath, command, filter, redo = redo)
