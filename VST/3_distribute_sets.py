import os, sys, shutil
from astropy.io import fits
import numpy as np
import glob

def main(date, datapath, script_dir, THELI_script, photcat, filter):
    print("=" * 15, date, "=" * 15)

    md=os.path.join(datapath,date)
    reddir=os.path.join(md, 'reduced')

    if not os.path.isdir(reddir):
        os.mkdir(reddir)

    os.system('%s -MD %s -REDDIR %s -PHOTCAT %s -FILTER %s -SCRIPTDIR %s'%(THELI_script, md, reddir, photcat, filter, script_dir))
    sets = glob.glob(os.path.join(datapath,date,'set_*'))
    sets = [s.split('/')[-1] for s in sets]
    return sets

if __name__ == '__main__':
    datapath = '/obs/lenses_EPFL/PRERED/VST/sorted/'

    dates = [e.split('\n')[0] for e in os.popen('ls %s'%datapath).readlines()]
    dates = [d for d in dates if d >= '2022-01-01' and d <= '2022-01-01']
    filter = 'r_SDSS'

    script_dir = '/home/astro/millon/Desktop/PRERED/VST/theli-1.50.0/scripts/Linux_64/'
    photcat='/home/astro/millon/Desktop/PRERED/VST/OMEGACAM_SDSS_overlap_01_2019.cat'

    THELI_script = os.path.join(script_dir, 'do_distribute_set.sh')
    for date in dates :
        sets = main(date, datapath, script_dir, THELI_script, photcat, filter)

