import os, sys
import glob


folders = sorted([f.split('\n')[0] for f in glob.glob("/obs/lenses_EPFL/RAW/VST/????-??-??")])
msg = "Problem with the following folders:"
for f in folders:
	os.chdir(f)
	nfiles_tot = 0 

	try:
		# how many files according to the sh script ?
		download_requestfiles = glob.glob("*downloadRequest*.sh")
		for dfile in download_requestfiles :
			lines = open(dfile , "r").readlines()
			nfiles = len([l for l in lines if ".fits.fz" in l])
			nfiles_tot += nfiles
	except:
		nfiles = 0	

	# how many ddl files ?
	nddl = len(glob.glob("*.fits*"))

	
	#print "="*20
	#print f
	#print nddl, "/", nfiles

	if nddl != nfiles_tot:
		msg += "\n"+f+": "+str(nddl)+"/"+str(nfiles_tot)


if msg != "Problem with the following folders:":
	print msg
