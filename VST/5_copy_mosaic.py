import os, sys, shutil, glob
from astropy.io import fits
import numpy as np
sort_data = __import__('1_sort_data')

def main(date, datapath, workpath, command, set, redo = False):
    if redo == False :
        command = command + ' -n' #no clobber command (do not overwrite)

    print("=" * 15, date," ", set, "=" * 15)

    file = os.path.join(datapath,date,set,'coadd_TEST','coadd.fits')
    file_weight = os.path.join(datapath,date,set,'coadd_TEST','coadd.weight.fits')
    try :
        hdulist = fits.open(file)
        header = hdulist[0].header
        object_name = header["OBJECT"]
        reddir = os.path.join(workpath,object_name,'mosaic')
        reddir_weight = os.path.join(workpath,object_name,'weight_maps')

        if not os.path.isdir(reddir):
            sort_data.mkdir_recursive(reddir)
        
        if not os.path.isdir(reddir_weight):
            sort_data.mkdir_recursive(reddir_weight)

        os.system('%s %s %s'%(command, file, os.path.join(reddir,'OMEGACAM_%s_%s_mosaic.fits'%(date,object_name))))
        os.system('%s %s %s'%(command, file_weight, os.path.join(reddir_weight,'OMEGACAM_%s_%s_mosaic_weight.fits'%(date,object_name))))
        #os.system('fpack -r -s 0 -F %s'%os.path.join(reddir,'OMEGACAM_%s_mosaic.fits'%object_name))
    except Exception as e:
        print(e)
        print('Co-added image was not created. Problem occured in THELI.')
        success = False
    else :
        success = True
    return success

if __name__ == '__main__':
    datapath = '/obs/lenses_EPFL/PRERED/VST/sorted/'
    workpath = '/obs/lenses_EPFL/PRERED/VST/reduced/'

    dates = [e.split('\n')[0] for e in os.popen('ls %s'%datapath).readlines()]
    dates = [d for d in dates if d >= '2019-05-23' and d <= '2019-05-23']
    filter = 'r_SDSS'

    command = 'cp' #cp to copy or mv to move the images to the sorted directory
    sets = ['set_1','set_2']

    redo = False
    for date in dates :
        for set in sets : 
            success = main(date, datapath, workpath, command, set, redo = False)
            
            print ('Succeed %s? '%set, success)




