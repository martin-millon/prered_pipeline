import os, sys, shutil
from astropy.time import Time
import numpy as np

def main(night, datapath, program_id_list, filter, username=None, pwd=None):
	if pwd is None :
		raise RuntimeError('Please provide your ESO logins.')

	print ("="*3, night,"="*3)
	nightpath = os.path.join(datapath,night)
	if os.path.isdir(nightpath):
		shutil.rmtree(nightpath)

	os.mkdir(nightpath)
	os.system('cp my_prog_access_science.sh %s/' % nightpath)
	os.system('cp my_prog_access_bias.sh %s/' % nightpath)
	os.system('cp my_prog_access_standard.sh %s/' % nightpath)
	os.system('cp my_prog_access_flat.sh %s/' % nightpath)
	os.chdir(nightpath)
	for program_id in program_id_list :
		os.system("./my_prog_access_science.sh -User '%s' -Passw '%s' -Night '%s' -Filter '%s' -Program_ID '%s' " %(username, pwd, night,filter, program_id))
	os.system("./my_prog_access_bias.sh -User '%s' -Passw '%s' -Night '%s' -Filter '%s' -Program_ID '%s' " %(username, pwd, night,filter, program_id_list[0]))
	os.system("./my_prog_access_standard.sh -User '%s' -Passw '%s' -Night '%s' -Filter '%s' -Program_ID '%s' " %(username, pwd, night,filter, program_id_list[0]))
	os.system("./my_prog_access_flat.sh -User '%s' -Passw '%s' -Night '%s' -Filter '%s' -Program_ID '%s' " %(username, pwd, night,filter, program_id_list[0]))



if __name__ == '__main__':
	datapath = '/obs/lenses_EPFL/RAW/VST'

	startnight = "2021-08-23 01:00:00"
	endnight = "2021-08-23 01:00:00"
	
	filter = "R_SDSS"
	program_id_list = ['1103.A-0801(D)','106.216P.001','106.216P.002'] #first program ID is used to download the calibration
	
	starttime = Time(startnight, format='iso', scale='utc').mjd
	endtime = Time(endnight, format='iso', scale='utc').mjd
	
	days = np.arange(starttime, endtime+1, 1)
	allnights = [Time(d, format='mjd', scale='utc').iso[:10] for d in days]
	for night in allnights:
		main(night, datapath, program_id_list, filter)
	
