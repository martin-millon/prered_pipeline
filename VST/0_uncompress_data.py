import os, sys, glob

def main(date,datapath,funpack_command, keep_compressed = False):
    print("="*15, date, "="*15)
    datadir = os.path.join(datapath, date)
    print (datadir)
    os.chdir(datadir)
    files = glob.glob("*.fits.fz")
    
    for file in files : 
        print ("Unpacking ", os.path.join(datadir,file))
        if not keep_compressed :
            os.system("%s -F %s"%(funpack_command,os.path.join(datadir,file)))
        else :
            os.system("%s %s"%(funpack_command,os.path.join(datadir,file)))


if __name__ == '__main__':
    datapath = '/obs/lenses_EPFL/RAW/VST'
    
    dates = [e.split('\n')[0] for e in os.popen('ls %s'%datapath).readlines()]
    dates = [d for d in dates if d >= '2021-08-23' and d <= '2021-08-23']
    
    keep_compressed = False
    funpack_command = '/home/astro/millon/Desktop/PRERED/VST/cfitsio/bin/funpack' #need absolute path here
    for date in dates:
        main(date,datapath,funpack_command)
        
