import os, sys, shutil
from astropy.io import fits
import numpy as np

def main(date, datapath, set, script_dir, THELI_script, photcat, filter, seeing = 2.0):
    print("=" * 15, date, "=" * 15)

    md=os.path.join(datapath,date)
    reddir=os.path.join(md, 'reduced')

    if not os.path.isdir(reddir):
        os.mkdir(reddir)

    os.system('%s -MD %s -REDDIR %s -PHOTCAT %s -FILTER %s -SCRIPTDIR %s -SET %s -SEEING %s'%(THELI_script, md, reddir, photcat, filter, script_dir, set, seeing))

if __name__ == '__main__':
    datapath = '/obs/lenses_EPFL/PRERED/VST/sorted/'

    dates = [e.split('\n')[0] for e in os.popen('ls %s'%datapath).readlines()]
    dates = [d for d in dates if d >= '2022-01-01' and d <= '2022-01-01']
    filter = 'r_SDSS'
    seeing = 0.8
    
    script_dir = '/home/astro/millon/Desktop/PRERED/VST/theli-1.50.0/scripts/Linux_64/'
    
    THELI_script = os.path.join(script_dir, 'do_deep_field.sh')
    #sets = ['set_1','set_2']
    sets = ['set_1']
    photcat='/home/astro/millon/Desktop/PRERED/VST/OMEGACAM_SDSS_overlap_01_2019.cat'
    
    for date in dates :
        for set in sets : 
            main(date, datapath, set, script_dir, THELI_script, photcat, filter, seeing = seeing)
