import os, sys, shutil, glob
from astropy.io import fits
import numpy as np


def main(date, datapath,set_list, filter):
    print("=" * 15, date, "=" * 15)

    os.system('rm -r %s'%os.path.join(datapath,date,'WEIGHTS'))
    for set in set_list :
        os.system('rm -r %s'%os.path.join(datapath,date,set))
        #os.system('rm -d %s'%os.path.join(datapath,date,set))
    os.system('rm -r %s'%os.path.join(datapath,date,'SCIENCE_%s_norm'%filter))
    os.system('rm -r %s'%os.path.join(datapath,date,'SKYFLAT_%s_norm'%filter))
    if os.path.isdir(os.path.join(datapath,date,'DOMEFLAT_%s'%filter)):
            os.system('rm -r %s'%os.path.join(datapath,date,'DOMEFLAT_%s'%filter))

    os.system('rm %s'%os.path.join(datapath,date,'SKYFLAT_%s/*.fits'%filter))
    os.system('rm %s'%os.path.join(datapath,date,'SCIENCE_%s/*.fits'%filter))
    os.system('rm %s'%os.path.join(datapath,date,'STANDARD_%s/*.fits'%filter))
    os.system('rm %s'%os.path.join(datapath,date,'BIAS/*.fits'))

    os.system('rm -r %s'%os.path.join(datapath,date,'SCIENCE_%s/OFC_IMAGES'%filter))
    os.system('rm -r %s'%os.path.join(datapath,date,'SCIENCE_%s/SPLIT_IMAGES'%filter))
    os.system('rm -r %s'%os.path.join(datapath,date,'SCIENCE_%s/SUB_IMAGES'%filter))
    os.system('rm -r %s'%os.path.join(datapath,date,'STANDARD_%s/OFC_IMAGES'%filter))
    os.system('rm -r %s'%os.path.join(datapath,date,'STANDARD_%s/SPLIT_IMAGES'%filter))
    os.system('rm -r %s'%os.path.join(datapath,date,'SCIENCE_%s/BINNED'%filter))

if __name__ == '__main__':
    datapath = '/obs/lenses_EPFL/PRERED/VST/sorted/'

    dates = [e.split('\n')[0] for e in os.popen('ls %s'%datapath).readlines()]
    dates = [d for d in dates if d >= '2021-02-13' and d <= '2021-02-13']
    filter = 'r_SDSS'

    set_list = ['set_1','set_2','set_3','set_4']
    #set_list = ['set_1']
    for date in dates :
        main(date, datapath,set_list, filter)




