import os, sys, shutil, glob
from astropy.io import fits
import numpy as np

def mkdir_recursive(path):
    sub_path = os.path.dirname(path)
    if not os.path.exists(sub_path):
        mkdir_recursive(sub_path)
    if not os.path.exists(path):
        os.mkdir(path)

def main(date, datapath,workpath, command, filter, redo = False): 
    if redo == False : 
        command = command + ' -n' #no clobber command (do not overwrite)
    
    print(("=" * 15, date, "=" * 15))
    datadir = os.path.join(datapath, date)
    workdir = os.path.join(workpath, date)

    if redo:
        if os.path.isdir(workdir):
            shutil.rmtree(workdir)
    
    science_dir = os.path.join(datapath, date, 'SCIENCE_%s/ORIGINALS'%filter)
    out_science_dir = os.path.join(workpath, 'SCIENCE_%s/ORIGINALS'%filter)
    
    if not os.path.isdir(out_science_dir):
        mkdir_recursive(out_science_dir)
            
    science_files = glob.glob(os.path.join(science_dir, '*.fits'))
    
    n_science = 0
    for file in science_files : 
            os.system('%s %s %s'%(command, file, out_science_dir))
            print(file)
            n_science +=1
    
    return n_science
    

 
if __name__ == '__main__':
    datapath = '/obs/lenses_EPFL/PRERED/VST/sorted/'
    workpath = '/obs/lenses_EPFL/PRERED/VST/sorted/2022-01-01/'
    
    dates_all = [e.split('\n')[0] for e in os.popen('ls %s' % datapath).readlines()]
    dates = [d for d in dates_all if d >= '2019-04-04' and d <= '2019-06-11']
    
    command = 'cp' #cp to copy or mv to move the images to the sorted directory
    
    filter = 'r_SDSS'
    redo = False
    
    n_image = 0 
    for date in dates:
        n_image += main(date, datapath,workpath, command, filter, redo = redo)
    
    print("Copied %i images in %s."%(n_image,workpath))

