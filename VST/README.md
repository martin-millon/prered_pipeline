# VST REDUCTION PIPELINE # 
Author : Martin Millon

Date : April 11th 2019

## Requirements
funpack /fpack : from the cfitsio package, need to specifically compile funpack with the make funpack command.

scamp : compile and move the executable in \home\astro\millon\.local\bin

sm (SuoperMongo) : optionnal for theli 

aclient : compile for the cdsclient-3.84 and move the executable
 
theli : version 1.50.0, need to be compiled last.

Python package : astropy, numpy, shutil, argparse, ect ... 

## Description
 
This pipeline automatically download  the data from ESO database and reduce it with the THELI pipeline. It keeps the bias subtracted, 
flatfielded, superflatfielded, illumination corrected images for each chip for each image. It also create a mosaic co-added image for each night. 
It is designed to be run automatically every night with cron. 
The main executable scripts (run_all.py) run the following scripts : 

get_VST_data.py : download the data from ESO database, the program ID and filter of the calibration and the folder to store the raw images must be given in argument. Username and password are hardcoded in the file. Works better from lesta.
0_uncompress_data : decompress the .fits.fz files using funpack 

1_sort_data : sort the downloaded file according to THELI requirements. Files are now moved to your workdir. 

2_reduce_data : run the THeli pipeline for basic flatfielding, bias, illumination, operation
 
2b_copy_quadrant : save the reduced individual chip image in your reducpath

3_distributes_set : split the data per observed target (according to THELI standard)

4_do_deep_field : co-add the different exposure in one single co-added mosaic. 

5_copy_mosaic : save your mosaic image in your reduc path 

6_clean_up : delete all the THELI reduction files (this can be up to 180 GB per night !)


## Usage 
run_all.py --start 2019-02-04 --end 2019-02-04 to run the reduction of the night starting on 4th of Feb 2019

run_all.py --> reduce the previous night

Modify this file according to your filter choice, program_ID, path, ect... 
All individual scripts can be run individually when executed as main. Modify the date and relevant paremeter in the indivdual script.  

**THELI path should be set in the theli-1.50.0/scripts/Linux_64/progs.ini file.**
 
## Extrascript
1b --> gather all the science images between two dates in a "fake date" : 2022-01-01. This is usefull if you want to generate a deep image with THELI.

clean_last_month --> cleaning all the raw images in the PRERED and RAW diretory in addition to the bias, flat, ect from the previous month. 

patch --> WARNING : Chip number 20 is dead since November 2020, we replace it with noise to avoid failure when building the masterflats, ect... 

