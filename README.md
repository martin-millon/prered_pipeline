This the prereduction pipeline for the COSMOGRAIL program. 
All reduction scripts for the different telescopes are gathered here. Please go in the different folders to have access the readme of each instruments : 

[ECAM](./ECAM/README.md) 

[VST](./VST/README.md)

[WFI](./WFI/README.md)

[NOT](./NOT/README.md)

[TELESTO](./TELESTO/README.md)


