"""
We do not use pyraf here
we divide by the masterflat if available, crop and flip the image into natural orientation.

"""


execfile("../global.py")

for targetname in alltargetnames:
  print "targetname : %s" % targetname
  print "masterflatname : %s" % masterflatname
  print "biassubdir : %s" % biassubdir
  print "reducdir : %s" % reducdir
  #print "filterkeyword : %s" % filterkeyword
  auxfcts.proquest(askquestions)

  destdir = os.path.join(reducdir, targetname+"_"+masterflatname)

  if os.path.isdir(destdir):
    print "The destination directory exists. I will complete it."
    print destdir
    auxfcts.proquest(askquestions)
  else:
    os.mkdir(destdir)


  # We read the databases

  # We read the science object database :
  db = auxfcts.readpickle(os.path.join(dbdir, targetname + ".pkl"))
  db = sorted(db, key=lambda item: item["datetime"])

  print "%i images in database" % len(db)
  print "filtername : %s" % filtername 
  dbgo = [img for img in db if img["filter"] == filtername]
  print "Among those, %i have the right filter." % len(dbgo)
  print "processfromhere : %s" % processfromhere
  dbgo = [img for img in dbgo if img["nightdatetime"] > processfromhere]
  print "I will now flatfield %i images." % len(dbgo)
  if not flatredofromscratch:
    print "flatredofromscratch is set to False"
    print "I skip the flats already done..."


  masterflatdb = auxfcts.readpickle(os.path.join(dbdir, "MF_" + masterflatname + ".pkl"))
  auxfcts.proquest(askquestions)


  couldnotflatfield = []

  for i, image in enumerate(dbgo):
    print "=== %4i/%4i : format %i : %s ===" % (i+1, len(db), image["format"], image["filename"])

    print "Night : %s" % image["nightdatetimestr"]

    # We find the corresponding entry in the masterflatdb
    # This is used only for decoration ... 
    masterflatdbentry = [entry for entry in masterflatdb if entry["nightdatetimestr"] == image["nightdatetimestr"]]
    if len(masterflatdbentry) != 1:
	    print "Arg, something is wrong with the masterflat database !"
	    print "I cannot find a unique entry for this night."
	    sys.exit()
    masterflatdbentry = masterflatdbentry[0]


    masterflatfilepath = os.path.join(masterflatdir, "MF_%s.fits" % image["nightdatetimestr"])
    inputfilepath = os.path.join(biassubdir, targetname, image["filename"])
    outputfilepath = os.path.join(destdir, image["filename"])

    if os.path.isfile(outputfilepath) and flatredofromscratch:
      print "Output file exists, I remove it."
      os.remove(outputfilepath)
    elif os.path.isfile(outputfilepath) and not flatredofromscratch:
      print "Already flatfielded, I skip..."	    
      continue

    if not os.path.isfile(inputfilepath):
	    print "Cannot find input file..."
	    sys.exit()

    (a, h) = auxfcts.fromfits(inputfilepath, hdu = 0, verbose = True)


    if not os.path.isfile(masterflatfilepath):
	    print "No masterflat available for this night... I will not flatfield the image"
	    flatfieldeda = a
	    h.update('PR_FLATF', "NO !", "pypr has not flatfielded this image !")
	    couldnotflatfield.append(image)
	    ########################
	    # Better idea : let's simply skip those very few frames
	    ########################
	    continue


    else:
	    (masterflat, mh) = auxfcts.fromfits(masterflatfilepath, hdu = 0, verbose = True)	
	    flatfieldeda = a / masterflat
	    h.update('PR_FLATF', "Yes", "pypr has flatfielded this image.")


    # We add some infos to the header
    h.update('PR_NIGHT', image["nightdatetimestr"], "pypr night")
    h.update('PR_NFLAT', len(masterflatdbentry["flats"]), "pypr number of flatfields in masterflat")
    h.update('PR_MFNAM', masterflatname, "pypr masterflatname")
    h.update('PR_PROUD', ":-)", "pypr proudly prereduced this image !")



    # TEST : get the rms time-distance from the individual flats used in the masterflat to this night.
    timedists = [flat["datetime"] - image["datetime"] for flat in masterflatdbentry["flats"]]
    timedists = map(lambda delta: (3600*24*delta.days + delta.seconds)/(3600.0*24.0), timedists) # we convert this into days.
    timedists = np.array(timedists)
    flatspan = np.max(timedists) - np.min(timedists)
    stddist = np.sqrt(np.sum(timedists * timedists))
    print "flatspan", flatspan
    print "stddist", stddist

    h.update('PR_FSPAN', flatspan, "pypr span of flats [days]")
    h.update('PR_FDISP', stddist, "pypr RMS of flats around exposure [days]")



    # Now we want to further crop and flip the image into the natural orientation.
    # The cropping is to further cut an ugly region in Euler images
    flatfieldeda = flatfieldeda[:,0:2000]
    flatfieldeda = np.rot90(np.rot90(flatfieldeda.transpose())).clip(min = -66000, max = 66000)


    # We write the fits file
    outfilepath = os.path.join(destdir, image["filename"])
    auxfcts.tofits(outputfilepath, flatfieldeda, hdr = h, verbose = True)


  print "Done !"

  print "I could not flatfield %i images :" % len(couldnotflatfield)
  for img in couldnotflatfield:
    print img["filename"], image["nightdatetimestr"]

