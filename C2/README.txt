
global.py
---------

All the settings, directory paths, etc.
Executed by every script.

Important directories to specify :
	- scriptsdir : where this global.py is.
	- productdir : where all the pipeline products should be written.
		(This will get massive !)

config
------

Non-python "input" to the scripts. Not very clean : much input is hardcoded in the scripts anyway...


dbs
---

Contains the "databases". A database is a pickle file, containing a list of dictionaries, where each dictionary represents an image. Contains all the filenames, dates, etc.
This will stay reasonnably small, no image files here !


modules
-------

Some special purpose modules, notable auxfcts.py (auxiliary functions).




1_prepare_raw
-------------

Essentially this is to build the initial databases, with as much information as possible taken from headers, filenames, etc.
"Database" = pickle of a python list containing one dict per image.

Here we look only at the headers of the files, no further calculations.
Will have to change for other telescopes.
Corrupted files will be detected here and should not remain in the database.

Build one database for every target (mix the filters)
Build one separate database for the flats of every filter
Build one database for the bias exposures
And one for the darks


Mandatory database keywords we will need from this point on :
(CAPITALS = directly from FITS header)
(lowercase = made by these scripts, safe and clean)

filepath
filename
basename

datetime : This is the most important : a python datetime object of the beginning of the exposure in UTC.

format (at least for Euler)
filter
exptime (as a float in seconds)
object (this is not important at all (put "hello" if you want), but nice)

dbname (this is more important : the name of the db, will be used as directory name. Only makes sense once the dbs are merged)

2_inspect_raw
-------------
Here we do some checks on the the actual data content of the raw fits files.

All kind of plots, manual checks, overscan inspection, etc.
We decide "how" to procede.

Then,
build_bias-overscan_db.py
plot_bias-overscan.py
to inspect a bit the bias exposures


build_joined_db.py
build_test_db.py
To make a test database, containing various formats and targets.


3_bias_sub
----------

We perform the acutal bias subtraction with
bias_sub.py

This has to be run once on every object, and on the flats.
New dbs are created in the dbs dir, and the images are put in the productdir.
Briefly :
	- bias is estimated from overscan or prescan, according to the formats
	- the images are cropped, according to formats, so that they all have the same size, and that identical pixels don't move.
	

plot_biaslevel.py 
Allows to compare the subtracted bias levels from what was measured on the bias frames.
This is just for checks, not calculations done.


4_flatfield
-----------


choose_flats.py
The heart of the prereduction : chooses how to combine the flats, individually for each image (or night).
-> writes a new database containing all this info.

build_masterflats.py
Reads the previous database, and builds masterflats according to it.

flatfield.py
Applies these masterflats to the actual science exposures.








Workflow
--------


1
	build or add images to bias, dark, flat, and science dbs.
	Do this by running the build_db stuff.
	You will have to run them on
	Euler_prered/raw and Euler_prered/raw2
	Build flat for every filter
	
	attribute_formats.py
		to be run on all these dbs
		can be run anytime.
		In principle everything in attribute_formats.py could be done
		directly in the build_db stuff ...
	
2
	plot_*.py
		harmless, don't change anything
	make_raw_pngs
		make pngs from the raw frames, for a first check.
		respect redofromscratch (but cannot see this in the db, 
		instead they simply look if the file already exists...
		As formats might be different, you might want to specify a crop.
		
	measure_bias-overscan.py
		updates the bias db, respects redofromscratch
		then plot it ...
		
	build_joined_db.py
		combines all the science targets into one db.
		will be used by the masterflat combination (as read-only).
	build_test_db.py combines *some* science targets into a testdb.
		
	
3	
	biassub.py on flats, darks, and science
		updates the dbs, respects redofromscratch

4	
	measure_flatlevel.py
		uses flatsname
		updates the flat db, respects redofromscratch
	inspect_flatlevel.py
		write bad flats into the flatskiplist by hand !
	plot_flatlevel.py
	
	group_flats.py
		uses flatname, flatskiplist, flatcutlist, allsciexpdbname
		filtername, masterflatname
		always runs on all images of flatname
		writes a dedicated masterflatdb, does not change other dbs
		the masterflatdb contains only nightdatetimestr and a list
		this masterflatdb will not be modified by further scripts, only read !
	
	inspect_flat_groups.py
		gives some stats about the masterflatdb
	
	make_masterflats.py
		uses flatname and masterflatname
		reads only the masterflatdb
		does not care about science targets at all
		respects processfromhere
		does not update the masterflatdb, but writes a small pickle aside of the masterflat images !
		(updatting the masterflatdb would be problematic as group_flats overwrites it each time)

	make_superflats.py
		reads only the masterflatdb (yes, the science exps of each night are included in here)
		respects processfromhere
		otherwise very similar to make_masterflats.py -- you can run them in parallel.



Some remarks.
About the redofromscratch ... it looks a bit wreird to no only apply the loop on a subset of the full db,
but remember that we want to write all this back into the db. In principle a sublist is mutable and would be fine, but if copies are made somewhere ... this seems safer to me to do it like I did on the full db.









Euler Runs up to 2010-01-29
===========================


flatRG biassub variations :

mask1
-----
iraf.nproto.objmasks.hsigma = 16.0 # yes, this looks huge, but it works. We only want to mask stars after all, nothing else.
iraf.nproto.objmasks.hdetect = "yes"
iraf.nproto.objmasks.ldetect = "no"
iraf.nproto.objmasks.minpix = 5
iraf.nproto.objmasks.ngrow = 3
iraf.nproto.objmasks.agrow = 5.0

With minflats = 12, on some rare ocasions (+ defoc flats) shadows remain, so I will try to use a lower threshold


mask2
-----
Same as mask1, but we use :
iraf.nproto.objmasks.hsigma = 9.0


mask3
-----
Same as mask2, but
iraf.nproto.objmasks.hsigma = 5.0
Conclusion : this is too sensitive, would only be possible when first flatfielding the individual flats before building the masks.


mask4
-----
Nearly the same as mask2, but
iraf.nproto.objmasks.hsigma = 10.0




RGjoined3
---------

uses mask1
minflats = 12
maxsearch = 30


RGjoined4
---------
Same as RGjoined3, but
uses mask2
updated cutlist



RGjoined5
---------
uses mask3
minflats = 3
maxsearch = 30


------------------------- Stop playing, start producing ---------------------

RGjoined6
---------
uses mask4
minflats = 3
maxsearch = 30
As a test : this could be the "minimal number of flats" to be used for prereduction.
I do skip the defocused frames according to skiplist.

make masterflats done
make superflats done
build pages done
build nav done
make plots done
make masterflatpng done
makd flatcomppng done
make superflatpng done
make nrejmaskpng done
ALL DONE


RGjoined7
---------
uses mask4
minflats = 15
maxsearch = 30
As a test : this could be the "large number of flats" to be used for prereduction.
I do skip the defocused frames according to skiplist.

make masterflats done
build pages done
build nav done
make masterflatpng done
makd flatcomppng done
make nrejmask done
make plots done
ALL DONE



RGjoined8
---------
uses mask5 = no masks
minflats = 5
maxsearch = 30
As a test : to check influence of the masking procedure.
I do skip the defocused frames according to skiplist.

make masterflats done
build pages done
build nav done
make plots done
make masterflatpng done
makd flatcomppng done
make nrejmask done
ALL DONE



RGjoined9
---------
uses mask4
minflats = 5
maxsearch = 30
Not sure if minflats 3 is useable, so here are minflats 5.
I do skip the defocused frames according to skiplist.

make masterflats done
build pages done
build nav done
make plots done
make 3 pngs done
ALL DONE



Reduction for test light curves :

1)
RGjoined7 (15 flats, with mask)

2)
RGjoined9 (5 flats, with mask)

3)
RGjoined8 (5 flats, no masks)

4)
RGjoined7 (wrong bias subtraction)



HE0047-1756 1) done
HE0047-1756 2) done
HE0047-1756 3) done
HE0047-1756 4) done


J1226-0006 1) done
RXJ1131-123 1) done
HE2149-274 1) i guess it's done


RGjoined10
----------
Updated on 2010-06-03
We imitate RGjoined7, the most plausible test until now. But we use another mask :
iraf.nproto.objmasks.hsigma = 10.0 # We only want to mask stars after all, nothing else.
iraf.nproto.objmasks.hdetect = "yes" # WARNING : SHOULD BE YES
iraf.nproto.objmasks.ldetect = "no"
iraf.nproto.objmasks.minpix = 30
iraf.nproto.objmasks.ngrow = 4
iraf.nproto.objmasks.agrow = 10.0
minflats = 15
maxsearch = 30
Updated skip and cuts for the newly added images.


RGjoined11
----------
same as 10, but
minflats = 5
maxsearch = 30

I update this one with the last images, quick for J0158-4325



RGjoined12
----------

Updated on 2011-01-19, contains the full C2 set.
Maybe we want to change something to the biassub, but otherwise the selection is definitive.
otherwise same as 10
minflats = 10
maxsearch = 30

2011-01-30 : no the biassub is fine, we leave it like this.
so just restart the full flat construction from scratch, one final time,
and eventually iterate on cutlist or so.
I finally also change group_flats, so that it gets symmetric.

In joined, 19491 have the right filter, that's ok. The others are some few VG images.

final masks done

first quick look at the html masterflats, looks ok.
I will use this for the urgent RXJ1131.
come back later to see if the cutlist needs some updates.

No, everything is fine -- this is the final set of flats !





