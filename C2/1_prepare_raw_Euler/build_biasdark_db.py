#
#	We build the bias db.


execfile("../global.py")


#########

sourcedir = "/obs/lenses_EPFL/RAW/Euler_prered/raw/bias/all"
#sourcedir = "/obs/lenses_EPFL/RAW/Euler_prered/raw2/bias"
#sourcedir = "/obs/lenses_EPFL/RAW/Euler_prered/raw2/dark"

dbname = "bias"
#dbname = "dark"


headerkeywords = ["BITPIX", "NAXIS", "NAXIS1", "NAXIS2",
"OBJECT", "UNSEQ", "FILTRE", "FILTER", "CCD_FILT", "CCD_GAIN", "CCD_TYPE",
"EXPTIME", "DATE-OBS", "NIGHT", "STARTTIM", "TUNIX_DP", "OBSERVER"]


#########



dbfilepath = os.path.join(dbdir, dbname + ".pkl")
if os.path.isfile(dbfilepath):
		
	db = auxfcts.readpickle(dbfilepath)
	db = sorted(db, key=lambda item: item["datetime"])
	print "Database with %i images already exists, I would update it." % len(db)
else:
	db = []
	print "No previous db found, I would create a new db."
	
	
filelist = sorted(glob.glob(os.path.join(sourcedir, "*.fits")))
print "I have found %i FITS files." % len(filelist)
	
	
knownfilenames = [entry["filename"] for entry in db]
newfiles = [filepath for filepath in filelist if os.path.split(filepath)[-1] not in knownfilenames]
print "Among those, %i are not yet in the db and will thus be added." % len(newfiles)
	
if len(newfiles) == 0:
	print "No new files, nothing to do."
	sys.exit()
		
	
# The final question, if everything was normal until here :

print "Final question."
print "Database : %s" % dbname
print "Sourcedir . %s" % sourcedir

auxfcts.proquest(askquestions)


for i,filepath in enumerate(newfiles):
	(filedir, filename) = os.path.split(filepath)
	print "%4i / %4i : %s" % (i+1,len(newfiles), filename)
	
	entry = {} # A new empty dict
	entry["filepath"] = filepath
	entry["filename"] = filename
	entry["basename"] = os.path.splitext(filename)[0]
	entry["datetime"] = None
	
	entry["filesize"] = os.path.getsize(filepath)

	header = pyfits.getheader(filepath)
	
	
	availablekeywords = header.ascardlist().keys()
	
	# We put the big bunch of keywords in the database :
	for kw in headerkeywords:
		if kw in availablekeywords:
			entry[kw] = header[kw]
		else:
			entry[kw] = None
			#print "%10s not available" % kw


	# Now we put the datetime for "normal" images :
	
	if entry["DATE-OBS"] != None:
		entry["datetime"] = datetime.datetime.strptime(entry["DATE-OBS"][0:19], "%Y-%m-%dT%H:%M:%S")
	
	elif entry["STARTTIM"] != None and entry["EXPTIME"] != None:
		tunix = float(entry["STARTTIM"])# + float(entry["EXPTIME"])/2.0
		entry["datetime"] = datetime.datetime.utcfromtimestamp(tunix)
	
	elif entry["TUNIX_DP"] != None:
		tunix = float(entry["TUNIX_DP"])
		entry["datetime"] = datetime.datetime.utcfromtimestamp(tunix)
	
	#print keyworddict["datetime"].strftime("%A, %d. %B %Y %H:%M")
	#print keyworddict["datetime"].strftime("%Y%m%d-%H%M")
	
	
	if entry["datetime"] == None:
		print "WARNING : no datetime..."
		sys.exit()
	
	db.append(entry)

# We sort the db :
db = sorted(db, key=lambda item: item["datetime"])


print "Number of times a keyword was not in the header :"
for kw in headerkeywords:
	valuelist = [entry[kw] for entry in db]
	nbrnones = valuelist.count(None)
	print "%10s : %i" % (kw, nbrnones)

print "I will now write the db."
auxfcts.proquest(askquestions)

# We do a backup as we now know that the file will be modified for sure.
if os.path.isfile(dbfilepath):
	auxfcts.backupfile(dbfilepath, backupdir, "newfiles")
		
auxfcts.writepickle(db, dbfilepath)



