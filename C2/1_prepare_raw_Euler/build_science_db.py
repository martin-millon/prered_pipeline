#
#	We build or update the science image dbs.
#	This script runs on "alltargetnames" (see global.py)
#	- we get a list of all the files for a target
#	- read the current db of the target
#	- add new entries into this db only for the files that are not yet known.
#

execfile("../global.py")

##################

sourcedir = "/obs/lenses_EPFL/RAW/Euler_prered/raw/science/"
#sourcedir = "/obs/lenses_EPFL/RAW/Euler_prered/raw2/lens/"

#alltargetnames = ["test"]


headerkeywords = ["BITPIX", "NAXIS", "NAXIS1", "NAXIS2",
"OBJECT", "CODE", "UNSEQ", "FILTRE", "CCD_FILT", "FILTER", "CCD_GAIN", "CCD_TYPE",
"EXPTIME", "DATE-OBS", "NIGHT", "STARTTIM", "TUNIX_DP",
"F_DATE", "FUSEAU", "OBSERVER"]


##################


print "I will try to create/update the databases of :"
print "\n".join(alltargetnames)


auxfcts.proquest(askquestions)



for targetname in alltargetnames:
	
	print "============= %s ============" % targetname
	dbfilepath = os.path.join(dbdir, targetname + ".pkl")
	if os.path.isfile(dbfilepath):
		
		db = auxfcts.readpickle(dbfilepath)
		db = sorted(db, key=lambda item: item["datetime"])
		print "Database with %i images already exists, I would update it." % len(db)
	else:
		db = []
		print "No previous db found, I would create a new db."
	
	fitsfiledir = os.path.join(sourcedir, "%s" % targetname, "all")
	if not os.path.isdir(fitsfiledir):
		print "WARNING : I can't find the directory with new images for %s" % targetname
		print fitsfiledir
		print "I will thus skip this target !"
		auxfcts.proquest(askquestions)
		continue
	
	
	filelist = sorted(glob.glob(os.path.join(fitsfiledir, "*.fits")))
	print "I have found %i FITS files." % len(filelist)
	
	
	knownfilenames = [entry["filename"] for entry in db]
	newfiles = [filepath for filepath in filelist if os.path.split(filepath)[-1] not in knownfilenames]
	print "Among those, %i are not yet in the db and will thus be added." % len(newfiles)
	
	if len(newfiles) == 0:
		print "No new files, I will skip this target."
		auxfcts.proquest(askquestions)
		continue
		
	
	# The final question, if everything was normal until here :
	auxfcts.proquest(askquestions)



	for i,filepath in enumerate(newfiles):
		(filedir, filename) = os.path.split(filepath)
		print "%4i / %4i : %s" % (i+1,len(newfiles), filename)		

		entry = {} # A new empty dict
		entry["filepath"] = filepath
		entry["filename"] = filename
		entry["basename"] = os.path.splitext(filename)[0]
		entry["datetime"] = None
	
		entry["filesize"] = os.path.getsize(filepath)
	
		header = pyfits.getheader(filepath)


		availablekeywords = header.ascardlist().keys()

		# We put the big bunch of keywords in the database :
		for kw in headerkeywords:
			if kw in availablekeywords:
				entry[kw] = header[kw]
			else:
				entry[kw] = None
				#print "%10s not available" % kw


		# Now the datetime
		if entry["DATE-OBS"] != None: # THIS IS UT !! format should be 2005-09-27T07:25:38.785
			entry["datetime"] = datetime.datetime.strptime(entry["DATE-OBS"][0:19], "%Y-%m-%dT%H:%M:%S")
			#entry["datetime"] = datetime.datetime.strptime(entry["DATE-OBS"][0:16], "%Y-%m-%dT%H:%M")
	
			# We check this with TUNIX_DP just to be sure :
			if entry["TUNIX_DP"] != None:
				testdatetime = datetime.datetime.utcfromtimestamp(float(entry["TUNIX_DP"]))
				diff = testdatetime - entry["datetime"]
	
				diffseconds = diff.seconds + 3600.0*24.0*diff.days	
	
				#print diffseconds
				if diffseconds > 1.0 :
					print "TIME ERROR"
					sys.exit()

			# AS this error never happens, it seems that TUNIX_DP is UT as well, and so we are safe !

		if entry["datetime"] == None: # Ok, then we use TUNIX_DP (and check with FDATE)
	
			if entry["TUNIX_DP"] != None:
				entry["datetime"] = datetime.datetime.utcfromtimestamp(float(entry["TUNIX_DP"]))
				print "I have to use TUNIX_DP"

			#if entry["datetime"] = 
	
			# Mon Dec 16 23:07:38 2002





		"""
		if entry["STARTTIM"] != None and entry["EXPTIME"] != None:
			tunix = float(entry["STARTTIM"]) + float(entry["EXPTIME"])/2.0
			entry["datetime"] = datetime.datetime.utcfromtimestamp(tunix)
	
		elif entry["TUNIX_DP"] != None:
			tunix = float(entry["TUNIX_DP"])
			entry["datetime"] = datetime.datetime.utcfromtimestamp(tunix)
	
		#print keyworddict["datetime"].strftime("%A, %d. %B %Y %H:%M")
		#print keyworddict["datetime"].strftime("%Y%m%d-%H%M")
		"""
	
		if entry["datetime"] == None:
			print "WARNING : no datetime, exiting."
			sys.exit()

		db.append(entry)
	
	# We sort the db :
	db = sorted(db, key=lambda item: item["datetime"])
	
	print "Number of times a keyword is NOT in the header :"
	for kw in headerkeywords:
		valuelist = [entry[kw] for entry in db]
		nbrnones = valuelist.count(None)
		print "%10s : %i" % (kw, nbrnones)
	
	print "I will now write the db."
	auxfcts.proquest(askquestions)

	# We do a backup as we now know that the file will be modified for sure.
	if os.path.isfile(dbfilepath):
		auxfcts.backupfile(dbfilepath, backupdir, "newfiles")
		
	auxfcts.writepickle(db, dbfilepath)


