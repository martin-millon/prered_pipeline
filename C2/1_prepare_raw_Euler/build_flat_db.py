#
#	We build or update the flat db.
#	Main difficulty here is to get the date from different sources...
#


execfile("../global.py")

##################

#sourcedir = "/obs/lenses_EPFL/RAW/Euler_prered/raw/flats/RG"
sourcedir = "/obs/lenses_EPFL/RAW/Euler_prered/raw2/flatRG"


dbname = "flatRG"

headerkeywords = ["BITPIX", "NAXIS", "NAXIS1", "NAXIS2",
"OBJECT", "UNSEQ", "FILTRE", "CCD_FILT", "FILTER", "CCD_GAIN", "CCD_TYPE",
"EXPTIME", "DATE-OBS", "NIGHT", "STARTTIM", "TUNIX_DP", "OBSERVER"]

####################



dbfilepath = os.path.join(dbdir, dbname + ".pkl")
if os.path.isfile(dbfilepath):
		
	db = auxfcts.readpickle(dbfilepath)
	db = sorted(db, key=lambda item: item["datetime"])
	print "Database with %i images already exists, I would update it." % len(db)
else:
	db = []
	print "No previous db found, I would create a new db."
	
	
	
filelist = sorted(glob.glob(os.path.join(sourcedir, "*.fits")))
print "I have found %i FITS files." % len(filelist)
	
	
knownfilenames = [entry["filename"] for entry in db]
newfiles = [filepath for filepath in filelist if os.path.split(filepath)[-1] not in knownfilenames]
print "Among those, %i are not yet in the db and will thus be added." % len(newfiles)
	
if len(newfiles) == 0:
	print "No new files, nothing to do."
	sys.exit()
		
	
# The final question, if everything was normal until here :

print "Final question."
print "Database : %s" % dbname
print "Sourcedir . %s" % sourcedir

auxfcts.proquest(askquestions)


for i,filepath in enumerate(newfiles):
	(filedir, filename) = os.path.split(filepath)
	
	print "%4i / %4i : %s" % (i+1,len(newfiles), filename)
	
	entry = {} # A new empty dict
	entry["filepath"] = filepath
	entry["filename"] = filename
	entry["basename"] = os.path.splitext(filename)[0]
	entry["datetime"] = None
	
	entry["filesize"] = os.path.getsize(filepath)

	header = pyfits.getheader(filepath)
	
	
	availablekeywords = header.ascardlist().keys()
	
	# We put the big bunch of keywords in the database :
	for kw in headerkeywords:
		if kw in availablekeywords:
			entry[kw] = header[kw]
		else:
			entry[kw] = None
			#print "%10s not available" % kw


	# Now first some specialities
	if ".20081208T" in entry["filename"]:
		entry["datetime"] = datetime.datetime(2008, 12, 8, 20, 50)

	# We get the date from the filename or elsewhere, will be used for a check...
	filenamedatestring = entry["filename"].split(".")[1]
	testdate = None
	if len(filenamedatestring) == 15:
		# 20040809T065124, the normal situation
		testdate = filenamedatestring[0:8] # 20040809
	if len(filenamedatestring) == 19:
		# 2009-04-11T22:55:45 , for the new images since april 2009
		testdate = filenamedatestring[0:4] + filenamedatestring[5:7]  + filenamedatestring[8:10] 
		
	if len(filenamedatestring) == 20:
		# ThuJul1707:04:052003, the ugly thing
		#print entry["TUNIX_DP"]
		#testdate = filenamedatestring[16:20] + filenamedatestring[8:10] + filenamedatestring[6:8]
		#testdatetime = datetime.datetime.utcfromtimestamp(float(entry["TUNIX_DP"]))
		#testdate = testdatetime.strftime("%Y%m%d")
	
		testdatetime = datetime.datetime.strptime(filenamedatestring, "%a%b%d%H:%M:%S%Y")
		testdate = testdatetime.strftime("%Y%m%d")
		
	if testdate == None:
		print "WARNING : I cannot get the test date ! %i" % len(filenamedatestring)
		print filenamedatestring
		testdate = None
	

	# Now we put the datetime for "normal" images :
	if entry["datetime"] == None and entry["STARTTIM"] != None and entry["EXPTIME"] != None:
		#tunix = float(entry["STARTTIM"]) + float(entry["EXPTIME"])/2.0
		# beginning of exposure is ok, we do the same for the images anyawy :
		tunix = float(entry["STARTTIM"])
		
		entry["datetime"] = datetime.datetime.utcfromtimestamp(tunix)
		
		# We check this agains our testdate :
		# Ok I did this so now I deactivate it :
		if entry["datetime"].strftime("%Y%m%d") != testdate:
			print "WARNING : testdate and tunix disagree (timezone ?) !"
			print "testdate : %s" % testdate
			print "tunix    : %s" % entry["datetime"].strftime("%Y%m%d %H:%M")
		
	else:
		print "Datetime : no tunix -> I use the filename !"
		if testdate != None:
			entry["datetime"] = datetime.datetime.strptime(testdate, "%Y%m%d")
			
		# And we can do even better :
		if len(filenamedatestring) == 20:
		# ThuJul1707:04:052003, the ugly thing
			precisedatetime = datetime.datetime.strptime(filenamedatestring, "%a%b%d%H:%M:%S%Y")
			entry["datetime"] = precisedatetime
		
		if entry["datetime"] == None:
			print "Ok, no date ... I stop here."
			sys.exit()
	
	#print entry["datetime"]
	
	
	
	#print keyworddict["datetime"].strftime("%A, %d. %B %Y %H:%M")
	#print keyworddict["datetime"].strftime("%Y%m%d-%H%M")
	
	db.append(entry)

# We sort the db :
db = sorted(db, key=lambda item: item["datetime"])

print "Number of times a keyword was not in the header :"
for kw in headerkeywords:
	valuelist = [entry[kw] for entry in db]
	nbrnones = valuelist.count(None)
	print "%10s : %i" % (kw, nbrnones)



	
print "I will now write the db."
auxfcts.proquest(askquestions)

# We do a backup as we now know that the file will be modified for sure.
if os.path.isfile(dbfilepath):
	auxfcts.backupfile(dbfilepath, backupdir, "newfiles")
		
auxfcts.writepickle(db, dbfilepath)

