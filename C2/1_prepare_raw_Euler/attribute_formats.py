"""
We add some new fields to all the images, that will be mandatory for further processing.
nightdatetime : datetime object of the night (to find images of a same night etc)
nightdatetimestr : same, but as a string like 2009-12-24
format : 1 to 6, categorizes the formats of the images
filter : a clean and uniform designation of the filter. ( = None if no filter is known (dark, bias ...)

"""

execfile("../global.py")

#alldbnames = ["HE0435-1223"]
#alldbnames = ["dark", "bias"]


"""
Formats and number of images :	
2047 x 2047 :   146	-> 4
2149 x 2047 :   443	-> 6
2085 x 2045 :     4	-> 3	only one science exp !
2228 x 2047 :  8915	-> 5	normal since 2007
2147 x 2047 :  7574	-> 2	normal before 2007
2087 x 2047 :    81	-> 1

Christel had this :
(iraf conventions for the image regions ...)
format	readnoise	gain	format		biassecX	trimsec (iraf conventions)
1	9.5		2.2	2087,2047	2070:2085	20:2046,2:2046
2	9.5		2.2	2147,2047	2075:2135	20:2046,2:2046
3	9.5		2.2	2085,2045	2070:2085	20:2046,2:2046
4	9.5		2.2	2047,2047	none		1:2027,1:2045
5	14.0		2.2	2228,2047	2080:2200	23:2049,2:2046	NOUVEAU CONTROLEUR
6	14.0		2.2	2149,2047	2075:2135	20:2046,2:2046	NOUVEAU CONTROLEUR

This way all images where 2027 x 2045 pixels (before flatfielding)
Same trim is applied to flats.

mixed formats are a few images of 4 in 2
a few of 6 in 5

My research
-------------


From now on I'm using ds9 and numpy conventions :

format1
first overscan : 2067
bias x 2070:2085, y = all if local, 5:2040 if median
dead pixel : 1101, 1498
last prescan line (white) : 19
trimsec 25:2045,5:2040 -> 2020 x 2035

format2
first overscan : 2067
bias x 2075:2135
dead pixel : 1101, 1498(?)
trimsec 25:2045,5:2040

format3
dead pixel : 1100(!), 1496 or 1497
first overscan : 2066
bias x 2069:2084
trimsec 24:2044,5:2040

format4
pas d'overscan... -> bias frames !
dead pixel : 1101, 1498
last prescan at 19, same as format 1
-> trimsec 25:2045,5:2040


format5
last prescan line : 22
dead pixel : 1104, 1498
ok so ther is a 3 pixel shift with respect to format1
-> trimsec 28:2048,5:2040
bias x 2100:2200


format6
last prescan line : 22, dead pixel 1104
-> trimsec 28:2048,5:2040
bias x 2080:2140

"""

def obsnight(dt):
	"""
	Give me a python datetime of the observation (in UTC !), and I return the python
	datetime (in UTC !) of the afternoon
	before the night at 18:00 (this 18:00 has no particular meaning).
	This is only valid for Euler !!!
	"""
	
	# We cut at 15:00 UTC (i.e. 11:00 or 12:00 (noon) local time)
	cutseconds = 15*3600.0
	
	# The time of day to return :
	rettime = datetime.time(18,0,0)
	#print rettime
	
	seconds = dt.hour*3600.0 + dt.minute*60.0 + dt.second
	
	if seconds > cutseconds :# Then we are in the afternoon or evening:
		return datetime.datetime.combine(dt.date(), rettime)
	else : # Then we are in the morning and need to get the day before:
		return datetime.datetime.combine((dt.date() - datetime.timedelta(1)), rettime)
	
	# example usage : .strftime("%Y%m%d")
	


print "I will run on :"
print alldbnames
auxfcts.proquest(askquestions)

for dbname in alldbnames:
	dbpath = os.path.join(dbdir, dbname + ".pkl")
	db = auxfcts.readpickle(dbpath)
	
	for image in db:
	
		image["nightdatetime"] = obsnight(image["datetime"])
		image["nightdatetimestr"] = image["nightdatetime"].strftime('%Y-%m-%d')
	
		# For a maximum of flexibility, let's do it the pedestrian way !
		
		image["format"] = 0 # means unknown ...
		
		if image["NAXIS1"] == 2047 and image["NAXIS2"] == 2047:
			image["format"] = 4
		
		if image["NAXIS1"] == 2149 and image["NAXIS2"] == 2047:
			image["format"] = 6
		
		if image["NAXIS1"] == 2085 and image["NAXIS2"] == 2045:
			image["format"] = 3
		
		if image["NAXIS1"] == 2228 and image["NAXIS2"] == 2047:
			image["format"] = 5
		
		if image["NAXIS1"] == 2147 and image["NAXIS2"] == 2047:
			image["format"] = 2
		
		if image["NAXIS1"] == 2087 and image["NAXIS2"] == 2047:
			image["format"] = 1
	
		# should never be needed (put in in the build_db.py)
		#if "FILTER" not in image.keys():
		#	image["FILTER"] = None
	
		image["filter"] = "Unknown"
		
		if image["FILTRE"] == "RG" or image["CCD_FILT"] == "RG" or image["FILTER"] == "RG":
			image["filter"] = "RG"
		if image["FILTRE"] == "VG" or image["CCD_FILT"] == "VG" or image["FILTER"] == "VG":
			image["filter"] = "VG"
		if image["FILTRE"] == "BG" or image["CCD_FILT"] == "BG" or image["FILTER"] == "BG":
			image["filter"] = "BG"
			
			
		image["object"] = "Somewhere ..."
		if image["OBJECT"] != None:
			image["object"] = image["OBJECT"]
			
		
		image["exptime"] = -1.0
		if image["EXPTIME"] != None:
			image["exptime"] = float(image["EXPTIME"])
		
		
		image["dbname"] = dbname
		
	outdbpath = dbpath
	if os.path.isfile(outdbpath):
		auxfcts.backupfile(outdbpath, backupdir, "attributeformats")
	auxfcts.writepickle(db, outdbpath, verbose=True)

