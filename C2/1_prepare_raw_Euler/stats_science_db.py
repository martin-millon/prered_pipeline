execfile("../global.py")


#sciencename = "J1226-0006"
#dbname = sciencename

#db = auxfcts.readpickle(os.path.join(dbdir, dbname + ".pkl"))


#dbnames = ["HE0435-1223"]
dbnames = alltargetnames

db = []
for dbname in dbnames:
	thisdb = auxfcts.readpickle(os.path.join(dbdir, dbname + ".pkl"))
	db.extend(thisdb)	


#db = [image for image in db if image["format"] == 4]


#for image in db:
#	print image["filename"], image["datetime"], image["format"]

"""
l = [entry["format"] for entry in db]
h = ["%10s : %5i" % (c, l.count(c)) for c in set(l)]
for l in h:
	print l
"""


#types = set([entry["NAXIS1"] for entry in db])
#print types

"""
l = [entry["CCD_GAIN"] for entry in db]
h = ["%10s : %5i" % (c, l.count(c)) for c in set(l)]
for l in h:
	print l
"""

"""
l = [entry["filesize"] for entry in db]
h = ["%10s : %5i" % (c, l.count(c)) for c in set(l)]
for l in h:
	print l
"""

"""
l = ["%5i x %5i" % (entry["NAXIS1"], entry["NAXIS2"]) for entry in db]
h = ["%20s : %5i" % (c, l.count(c)) for c in set(l)]
for l in h:
	print l
"""
l = ["'%s', '%s', '%s' -> %s" % (entry["FILTRE"], entry["FILTER"], entry["CCD_FILT"], entry["filter"]) for entry in db]
h = ["%30s : %5i" % (c, l.count(c)) for c in set(l)]
for l in h:
	print l

"""
print int(db[1]["datetime"].strftime("%j"))

h = int(db[1]["datetime"].strftime("%H"))
m = int(db[1]["datetime"].strftime("%m"))
"""


for image in db:
	if image["FILTRE"] == "BG":
		print image["filename"]

l = [entry["filesize"] for entry in db]
h = ["%10s : %5i" % (c, l.count(c)) for c in set(l)]
for l in h:
	print l
	
l = [entry["object"] for entry in db]
h = ["%30s : %5i" % (c, l.count(c)) for c in set(l)]
for l in h:
	print l
