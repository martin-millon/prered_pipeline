"""
We compare the contents of two directories to see
if there are some files that we do not yet have in our database.


"""

execfile("../global.py")


mydir = "/obs/lenses_EPFL/Euler_prered/raw/flats/RG" 

newdir = "/obs/lenses_EPFL/Euler_prered/raw/flats/RG_photomall"

#wheretoputnew = "/obs/lenses_EPFL/Euler_prered/raw/flats/RG_new"

mylist = sorted(glob.glob(os.path.join(mydir, "*")))
mybasenames = set([os.path.splitext(os.path.basename(myfile))[0] for myfile in mylist])

newlist = sorted(glob.glob(os.path.join(newdir, "*")))
newbasenames = set([os.path.splitext(os.path.basename(newfile))[0] for newfile in newlist])


print "%i files in mydir" % len(mylist)
print "%i files in newdir" % len(newlist)


#unknodifference(other, ...)


#auxfcts.proquest(askquestions)

unknownfiles = []

for (newfile, newbasename) in zip(newlist, newbasenames):
	
	if newbasename not in mybasenames:
		unknownfiles.append(newfile)
		print newbasename
	
	
print "%i files in newdir that I don't have yet." % len(unknownfiles)
