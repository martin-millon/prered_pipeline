
#
#	For a given science object, we compare the db made from the fits files with the cosmolog made at Euler.
#

execfile("../global.py")




stringtimeformat = "%Y%m%d" # i.e. we only check up to a precision of the day.
#stringtimeformat = "%Y%m%d-%H%M"


print targetname
auxfcts.proquest(askquestions)

# We read the database (sorting is not important)
db = auxfcts.readpickle(os.path.join(dbdir, targetname + ".pkl"))
#db = sorted(db, key=lambda item: item["datetime"])

# A list of strings of dates :
#dbstringtimes = [img["datetime"].strftime("%Y%m%d-%H%M") for img in db]
dbstringtimes = [img["datetime"].strftime(stringtimeformat) for img in db]

# We read the cosmolog :
logfile = "../config/cosmolog_Euler_1.log"

f = open(logfile, 'r')
lines = f.readlines()
f.close()

logs = []		
for i, line in enumerate(lines):
	fields = line.split()
	if len(fields) == 5:
		
		target = fields[1]
		filter = fields[2]
		Tdatetime = fields[3]
		exptime = fields[4]
							
		pythondt = datetime.datetime.strptime(Tdatetime[0:19], "%Y-%m-%dT%H:%M:%S")
		
		#stringtime = pythondt.strftime("%Y%m%d-%H%M")
		stringtime = pythondt.strftime(stringtimeformat)
		
		logs.append({"target":target, "filter":filter,
		"pythondt":pythondt, "exptime":exptime, "stringtime":stringtime, "rawtime":Tdatetime})
		

# We filter out the unwanted lenses:
logs = [log for log in logs if log["target"] == targetname]		

print "Number of exposures in cosmolog : %i" % len(logs)

print "Number of exposures in db : %i" % len(db)

#print "Images in cosmolog that are not in db :"

nodblogs = []
for log in logs:
	if log["stringtime"] not in dbstringtimes:
		#print log["rawtime"], log["filter"], log["exptime"]
		nodblogs.append(log)

print "Number of exposures of cosmolog that are not in db : %i" % len(nodblogs)
for log in nodblogs:
	print log["rawtime"], log["filter"], log["exptime"]

logstringtimes = [log["stringtime"] for log in logs]
nologdbs = []
for image in db:
	if image["datetime"].strftime(stringtimeformat) not in logstringtimes:
		nologdbs.append(image)


print "Number of exposures in db that are not in cosmolog : %i" % len(nologdbs)
for image in nologdbs:
	print image["datetime"]


