execfile("../global.py")


zipdir = "/obs/lenses_EPFL/Euler_prered/raw/flats/RG_photomall/gz" 


inlist = sorted(glob.glob(os.path.join(zipdir, "*.gz")))
print "%i *.gz files." % len(inlist)

auxfcts.proquest(askquestions)

for i, infile in enumerate(inlist):
	print "%i : %s" % (i+1, infile)
	
	outname = os.path.splitext(os.path.basename(infile))[0]
	outpath = os.path.join(zipdir, outname)
	
	cmd = "gunzip -c %s > %s" % (infile, outpath)
	#print cmd
		
	os.system(cmd)
	
print "Done."


