
execfile("../global.py")
import matplotlib.pyplot as plt
import matplotlib.dates as mpldates



print "masterflatname : %s" % masterflatname

flatviewdirname = masterflatname + "_html"
flatviewdir = os.path.join(productdir, flatviewdirname)

masterflatdb = auxfcts.readpickle(os.path.join(dbdir, "MF_" + masterflatname + ".pkl"))
masterflatdbgo = [night for night in masterflatdb if night["nightdatetime"] > processfromhere]

bsflatdir = os.path.join(biassubdir, flatname)
print "bsflatdir : %s" % bsflatdir
print "flatname : %s" % flatname
print "processfromhere : %s" % processfromhere

print "I will build plots for %i nights" % (len(masterflatdbgo))

auxfcts.proquest(askquestions)

# WE GET ALL THE FLATS
flatdb = auxfcts.readpickle(os.path.join(dbdir, flatname + ".pkl"))
allflatdates = np.array([image["datetime"] for image in flatdb])
allflatheights = 0.5*np.ones(allflatdates.shape)

# WE GET ALL THE GROUPS
flatcuts = auxfcts.readdatelist(os.path.join(configdir, flatcutlistfilename))
# And sort this :
flatcuts = sorted(flatcuts, key=lambda item: item["pythondt"])
# flatcuts is now a list of dicts  {"pythondt", "comment"}


# We get all the nights
scistarts = [night["firstexpdt"] for night in masterflatdbgo]
sciends = [night["lastexpdt"] for night in masterflatdbgo]

#masterflatdbgo = masterflatdbgo[::-1]
for i, night in enumerate(masterflatdbgo):
		
	print "============ %4i / %4i : %s ==========" % (i+1, len(masterflatdbgo), night["nightdatetimestr"])

	nightdir = os.path.join(flatviewdir, night["nightdatetimestr"])

	fig = plt.figure(figsize=(14,0.6))
	#fig.subplots_adjust(left = 0.007, right = 0.993, bottom=0.04, top=0.96)
	fig.subplots_adjust(left = 0.007, right = 0.993, bottom=0.34, top=0.90)
	ax = fig.add_subplot(111)

	for flatcut in flatcuts:	
		plt.axvline(flatcut["pythondt"], color="black")
	
	thisflatdates = np.array([flat["datetime"] for flat in night["flatsinclnotok"]])
	thisflatdatesok = np.array([flat["datetime"] for flat in night["flats"]])
	thisflatheights = 0.5*np.ones(thisflatdates.shape)
	thisflatheightsok = 0.5*np.ones(thisflatdatesok.shape)
	
	ax.plot(allflatdates, allflatheights, marker = "o", color = "black", linestyle="None", markeredgewidth=0.5)
	ax.plot(thisflatdates, thisflatheights, marker = "o", color = "red", linestyle="None", markeredgewidth=0.5)
	ax.plot(thisflatdatesok, thisflatheightsok, marker = "o", color = "#55FF55", linestyle="None", markeredgewidth=0.5)
	
	plt.axvline(night["firstexpdt"], color="blue")
	plt.axvline(night["lastexpdt"], color="blue")
	
	for scistart in scistarts:
		plt.axvline(scistart, color="blue", ls = "--")
	for sciend in sciends:
		plt.axvline(sciend, color="blue", ls = "--")
		
		
	plt.axvline(night["groupstart"]["pythondt"], color="red")
	plt.axvline(night["groupend"]["pythondt"], color="red")
	ax.text(night["groupstart"]["pythondt"]+datetime.timedelta(0.1), 1.0, night["groupstart"]["comment"], color="red", fontsize=10)
	ax.text(night["groupend"]["pythondt"]+datetime.timedelta(0.1), 1.0, night["groupend"]["comment"], color="red", fontsize=10)
	
	# format the ticks
	#months   = mpldates.MonthLocator()  # every month
	#days   = mpldates.DayLocator()  # every day
	#daysFmt = mpldates.DateFormatter('%d')
	#noFmt = mpldates.DateFormatter('')
	#yearsFmt = mpldates.DateFormatter('%Y')
	#monthsFmt = mpldates.DateFormatter('%b') # Jan Feb ...
	#ax.xaxis.set_major_locator(days)
	#ax.xaxis.set_minor_locator(days)
	#ax.xaxis.set_major_formatter(daysFmt)
	#ax.xaxis.set_minor_formatter(daysFmt)
	
	
	xmin = night["nightdatetime"] - datetime.timedelta(30)
	xmax = night["nightdatetime"] + datetime.timedelta(30)
	ax.set_xlim(xmin, xmax)
	
	#dateFmt = mpldates.DateFormatter('%Y-%m-%d')
	dateFmt = mpldates.DateFormatter('%d')
	ax.xaxis.set_major_formatter(dateFmt)
	daysLoc = mpldates.DayLocator()
	hoursLoc = mpldates.HourLocator(interval=6)
	ax.xaxis.set_major_locator(daysLoc)
	ax.xaxis.set_minor_locator(hoursLoc)
	#fig.autofmt_xdate(bottom=0.18)
	
	
	ax.set_yticklabels([])
	ax.set_yticks([])
	ax.set_ylim(0,2)
	#ax.set_xticklabels([])
	
	ax.set_xlabel("")
	
	destpath = os.path.join(nightdir, "plot.png")
	
	#plt.show()
	#sys.exit()
	
	plt.savefig(destpath, transparent=True)
	plt.close()
	plt.clf()
	del fig
	#sys.exit()
