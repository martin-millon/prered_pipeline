
execfile("../global.py")
import f2n


print "masterflatname : %s" % masterflatname

flatviewdirname = masterflatname + "_html"
flatviewdir = os.path.join(productdir, flatviewdirname)

masterflatdb = auxfcts.readpickle(os.path.join(dbdir, "MF_" + masterflatname + ".pkl"))
masterflatdbgo = [night for night in masterflatdb if night["nightdatetime"] > processfromhere]

bsflatdir = os.path.join(biassubdir, flatname)

print "processfromhere : %s" % processfromhere

print "I will build pngs of %i nrejmasks" % (len(masterflatdbgo))

auxfcts.proquest(askquestions)



for i, night in enumerate(masterflatdbgo):
		
	print "============ %4i / %4i : %s ==========" % (i+1, len(masterflatdbgo), night["nightdatetimestr"])

	nightdir = os.path.join(flatviewdir, night["nightdatetimestr"])
	nrejmaskfitsfilepath = os.path.join(masterflatdir, "MFnrejmask_%s.fits" % night["nightdatetimestr"])
	
	if not os.path.isfile(nrejmaskfitsfilepath):
		print "No mask, skipping this one ..."
		continue
	
	f2nimage = f2n.fromfits(nrejmaskfitsfilepath, hdu=0, verbose=True)
	
	f2nimage.setzscale(0.1, 3.9)
	f2nimage.rebin(3, method="max")
	f2nimage.makepilimage(scale="clin", negative=False)
	f2nimage.z2 = 100000.0 # to avoid black cutoff
	f2nimage.showcutoffs(redblue = False) # this will give us a white background
	f2nimage.z2 = 3.9 # we reset the original
	f2nimage.z1 = -1.0
	f2nimage.showcutoffs(redblue = True) # this will give us red cutoffs
	f2nimage.z1 = 0.1
	
	
	
	f2nimage.writetitle("Stacked masks " + night["nightdatetimestr"], colour=(0,0,0))
	
	"""
	infolist = ["", "", image["datetime"].strftime("  %Y-%m-%d   %H:%M")]
	infolist.extend(["%12s : %s" % (key, image[key]) for key in keywords])
	infolist.append("%12s : %5i %5i " % ("Cuts", f2nimage.z1, f2nimage.z2))
	#infolist.append(image["datetime"].strftime("%Y-%m-%d %H:%M"))
	
	f2nimage.writeinfo(infolist)
	"""
	
	destpath = os.path.join(nightdir, "nrejmask.png")
	f2nimage.tonet(destpath)
	
	
	
