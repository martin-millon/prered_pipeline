execfile("../global.py")


print "masterflatname : %s" % masterflatname

flatviewdirname = masterflatname + "_html"
flatviewdir = os.path.join(productdir, flatviewdirname)

if os.path.isdir(flatviewdir):
	print "The flatviewdir exists. I will complete it."
	print flatviewdir
	auxfcts.proquest(askquestions)
else:
	os.mkdir(flatviewdir)

masterflatdb = auxfcts.readpickle(os.path.join(dbdir, "MF_" + masterflatname + ".pkl"))
masterflatdbgo = [night for night in masterflatdb if night["nightdatetime"] > processfromhere]
# This time we can safely use this subset for our loop, as we will not update the database !

bsflatdir = os.path.join(biassubdir, flatname)
print "bsflatdir : %s" % bsflatdir
print "processfromhere : %s" % processfromhere

print "%i nights in total" % len(masterflatdb)
print "I will build html pages for %i nights" % (len(masterflatdbgo))

auxfcts.proquest(askquestions)

# We read the template
pagetemplatefile = open("page_template.html", "r")
pagetemplate = pagetemplatefile.read()
pagetemplatefile.close()

for i, night in enumerate(masterflatdbgo):
		
	print "============ %4i / %4i : %s ==========" % (i+1, len(masterflatdbgo), night["nightdatetimestr"])


	# WE PREPARE THE DIRECTORY
	nightdir = os.path.join(flatviewdir, night["nightdatetimestr"])
	# We check if the nightdir exists
	#if os.path.isdir(nightdir):
	#	print "Deleting the nightdir ..."
	#	shutil.rmtree(nightdir)	
	if not os.path.isdir(nightdir):
		os.mkdir(nightdir)
	
	# WE BUILD A TITLE STRING CONTAINING THE NUMBER OF FLATS
	night["title"] = "%s : %2i flats in night, %2i in masterflat" % (night["nightdatetimestr"], len(night["thisnightflatsok"]), len(night["flats"]))

	# WE BUILD A STRING ABOUT THE SCIENCE OBJECTS
	sciexpobjectlist = [exp["object"] for exp in night["sciexps"]]
	sciexphisttext = ["%s (%i)" % (obj, sciexpobjectlist.count(obj)) for obj in sorted(list(set(sciexpobjectlist)))]
	night["sciexphisttext"] = ", ".join(sciexphisttext)

	# WE BUILD A STRING ABOUT THE NIGHT GROUP
	night["groupstarttxt"] = "%s (%s)" % (night["groupstart"]["pythondt"].strftime("%Y-%m-%dT%H:%M:%S"), night["groupstart"]["comment"])
	night["groupendtxt"] = "%s (%s)" % (night["groupend"]["pythondt"].strftime("%Y-%m-%dT%H:%M:%S"), night["groupend"]["comment"])
	groupdelta = night["groupend"]["pythondt"] - night["groupstart"]["pythondt"]
	groupdeltadays = (3600*24*groupdelta.days + groupdelta.seconds)/(3600.0*24.0)
	night["groupspantxt"] = "%3.1f" % (groupdeltadays)


	# WE BUILD A TEXT FIELD WITH INFOS ABOUT THE FLATS (including not-ok flats)
	for flat in night["flatsinclnotok"]:
		if flat["ok"] == False:
			flat["nookmessage"] = "(X) %s" % flat["kocomment"]
		else:
			flat["nookmessage"] = ""
			
		delta = (flat["datetime"] - night["firstexpdt"])
		flat["deltadays"] = (3600*24*delta.days + delta.seconds)/(3600.0*24.0)
			
	flatstext = ["%35s : %s, %3.0f s : %+5.1f days %s\n" % (flat["filename"], flat["datetime"].strftime("%Y-%m-%dT%H:%M:%S"), flat["exptime"], flat["deltadays"], flat["nookmessage"]) for flat in night["flatsinclnotok"]]
	night["flatstext"] = "".join(flatstext)

	# WE BUILD A TEXT FIELD WITH THE IRAF combination OUTPUT for these flats
	
	irafblabla = [""]
	nrejblabla = [""]
	irafpklpath = os.path.join(masterflatdir, "MF_%s.pkl" % night["nightdatetimestr"])
	
	if os.path.isfile(irafpklpath): # So that you can run it even without masterflats.
		irafinfo = auxfcts.readpickle(irafpklpath)
		irafblabla.extend(irafinfo["combitxtlines"])
	
		nrejblabla.append("%10i /%10i /%10i /%10i " % (irafinfo["fullrejpixels"], irafinfo["halfrejpixels"], irafinfo["twoormorerejpixels"], irafinfo["partrejpixels"])) 
		
		
	
	irafblabla = [line for line in irafblabla if len(line) > 4]
	
	night["irafblabla"] = "\n".join(irafblabla)
	night["nrejblabla"] = "".join(nrejblabla)
	

	# WE BUILD THE SKIM EFFECT
	#width = 505
	#height = 508
	width = 673
	height = 678

	images = ["masterflat.png", "superflat.png"]
	images.extend(["compa_%s.png" % img["basename"] for img in night["flats"]])
	images.append("nrejmask.png")
	nimages = len(images)
	
	
	skimtext = ["""<img src='%s' width='%i' height='%i' usemap='#skimmap' border='0' name='skim' alt=''>
	<map name="skimmap">""" % (images[0], width, height)]
	for i, image in enumerate(images):
		xmin = i * int(width/len(images))
		ymin = 0
		xmax = (i+1)*int(width/len(images))
		ymax = height
	
		skimtext.append("""
		<area shape="rect" COORDS="%i,%i,%i,%i" href="" onClick="return false" onmouseover="skim.src='%s';" alt="">
		""" % (xmin, ymin, xmax, ymax,image))
	skimtext.append("</map>")
	night["skimtext"] = "".join(skimtext)

	# We build the html page
	filldict = night
	page = pagetemplate % filldict # You love python, don't you ?
	
	# We write the filled template into an index.html file
	htmlpage = open(os.path.join(nightdir, "index.html"), "w")
	htmlpage.write(page)
	htmlpage.close()

	



