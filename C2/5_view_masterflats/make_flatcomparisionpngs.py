
execfile("../global.py")
import f2n


print "masterflatname : %s" % masterflatname

flatviewdirname = masterflatname + "_html"
flatviewdir = os.path.join(productdir, flatviewdirname)

masterflatdb = auxfcts.readpickle(os.path.join(dbdir, "MF_" + masterflatname + ".pkl"))
masterflatdbgo = [night for night in masterflatdb if night["nightdatetime"] > processfromhere]

bsflatdir = os.path.join(biassubdir, flatname)
print "bsflatdir : %s" % bsflatdir
print "flatname : %s" % flatname
print "processfromhere : %s" % processfromhere

print "I will build pngs for %i nights" % (len(masterflatdbgo))

auxfcts.proquest(askquestions)



for i, night in enumerate(masterflatdbgo):
		
	print "============ %4i / %4i : %s ==========" % (i+1, len(masterflatdbgo), night["nightdatetimestr"])

	nightdir = os.path.join(flatviewdir, night["nightdatetimestr"])
	masterflatpath = os.path.join(masterflatdir, "MF_%s.fits" % night["nightdatetimestr"])
	
	if not os.path.isfile(masterflatpath):
		print "No masterflat, skipping this one ..."
		continue
	
	
	f2nimage = f2n.fromfits(masterflatpath, hdu=0, verbose=True)
	f2nimage.rebin(3)
	
	
	
	mfa = f2nimage.numpyarray # So this is our masterflat, binned 4x4
	
	
	
	for flat in night["flats"]:
		print flat["filename"]
		
		flatpath = os.path.join(bsflatdir, flat["filename"])
		
		f2nimage = f2n.fromfits(flatpath, hdu=0, verbose=True)
		f2nimage.rebin(3)
		
		fa = f2nimage.numpyarray # This is the flat
		
		fa = fa / np.median(fa.ravel())
		
		diff = fa - mfa
		
		f2nimage = f2n.f2nimage(diff)
		f2nimage.setzscale(-0.02, 0.02)
		#f2nimage.rebin(4)
		f2nimage.makepilimage(scale="lin", negative=False)
	
		#f2nimage.showcutoffs(redblue = True)
		f2nimage.writeinfo([flat["filename"]])
		
		delta = (flat["datetime"] - night["firstexpdt"])
		flat["deltadays"] = (3600*24*delta.days + delta.seconds)/(3600.0*24.0)
		
		if flat["nightdatetimestr"] == night["nightdatetimestr"]:
			# Then we draw a border
			f2nimage.drawrectangle(1, f2nimage.numpyarray.shape[0]-1, 1, f2nimage.numpyarray.shape[1]-1)	
			f2nimage.drawrectangle(2, f2nimage.numpyarray.shape[0]-2, 2, f2nimage.numpyarray.shape[1]-2)
		f2nimage.writetitle("%s : %+5.1f d" % (flat["datetime"].strftime("%Y-%m-%dT%H:%M:%S"), flat["deltadays"]))
	
		destpath = os.path.join(nightdir, "compa_%s.png" % flat["basename"])
		f2nimage.tonet(destpath)
	
	
	
