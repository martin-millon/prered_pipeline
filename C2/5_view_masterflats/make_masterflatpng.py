
execfile("../global.py")
import f2n


print "masterflatname : %s" % masterflatname

flatviewdirname = masterflatname + "_html"
flatviewdir = os.path.join(productdir, flatviewdirname)

masterflatdb = auxfcts.readpickle(os.path.join(dbdir, "MF_" + masterflatname + ".pkl"))
masterflatdbgo = [night for night in masterflatdb if night["nightdatetime"] > processfromhere]

bsflatdir = os.path.join(biassubdir, flatname)

print "processfromhere : %s" % processfromhere

print "I will build pngs for %i masterflats" % (len(masterflatdbgo))

auxfcts.proquest(askquestions)



for i, night in enumerate(masterflatdbgo):
		
	print "============ %4i / %4i : %s ==========" % (i+1, len(masterflatdbgo), night["nightdatetimestr"])

	nightdir = os.path.join(flatviewdir, night["nightdatetimestr"])
	masterflatpath = os.path.join(masterflatdir, "MF_%s.fits" % night["nightdatetimestr"])
	
	if not os.path.isfile(masterflatpath):
		print "No masterflat, skipping this one ..."
		continue
	
	f2nimage = f2n.fromfits(masterflatpath, hdu=0, verbose=True)
	
	f2nimage.setzscale(0.9, 1.1)
	f2nimage.rebin(3)
	f2nimage.makepilimage(scale="lin", negative=False)
	#f2nimage.showcutoffs(redblue = True)
	f2nimage.writetitle("MF " + night["nightdatetimestr"])
	
	"""
	infolist = ["", "", image["datetime"].strftime("  %Y-%m-%d   %H:%M")]
	infolist.extend(["%12s : %s" % (key, image[key]) for key in keywords])
	infolist.append("%12s : %5i %5i " % ("Cuts", f2nimage.z1, f2nimage.z2))
	#infolist.append(image["datetime"].strftime("%Y-%m-%d %H:%M"))
	
	f2nimage.writeinfo(infolist)
	"""
	
	destpath = os.path.join(nightdir, "masterflat.png")
	f2nimage.tonet(destpath)
	
	
	
