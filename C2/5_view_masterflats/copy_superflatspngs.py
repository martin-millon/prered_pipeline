"""
We copy the superflat png from one html directory into another


"""



execfile("../global.py")

print "superflatname : %s" % superflatname

sourcesuperflatname = "RGjoined10"
print "source superflat : %s" % sourcesuperflatname

auxfcts.proquest(askquestions)



flatviewdirname = superflatname + "_html"
flatviewdir = os.path.join(productdir, flatviewdirname)

masterflatdb = auxfcts.readpickle(os.path.join(dbdir, "MF_" + superflatname + ".pkl"))
masterflatdbgo = [night for night in masterflatdb if night["nightdatetime"] > processfromhere]

sourceflatviewdirname = sourcesuperflatname + "_html"
sourceflatviewdir = os.path.join(productdir, sourceflatviewdirname)


for i, night in enumerate(masterflatdbgo):
		
	print "============ %4i / %4i : %s ==========" % (i+1, len(masterflatdbgo), night["nightdatetimestr"])

	sourcenightdir = os.path.join(sourceflatviewdir, night["nightdatetimestr"])
	sourcesuperflatpath = os.path.join(sourcenightdir, "superflat.png")
	
	if not os.path.isfile(sourcesuperflatpath):
		print "No superflat png, skipping this one ..."
		continue

	nightdir = os.path.join(flatviewdir, night["nightdatetimestr"])
	superflatpath = os.path.join(nightdir, "superflat.png")
	
	
	shutil.copy(sourcesuperflatpath, superflatpath)
	
	


