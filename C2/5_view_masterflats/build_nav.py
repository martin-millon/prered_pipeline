execfile("../global.py")

print "masterflatname : %s" % masterflatname

flatviewdirname = masterflatname + "_html"
flatviewdir = os.path.join(productdir, flatviewdirname)

if not os.path.isdir(flatviewdir):
	print "The flatviewdir does not exist"
	sys.exit()


masterflatdb = auxfcts.readpickle(os.path.join(dbdir, "MF_" + masterflatname + ".pkl"))
masterflatdbgo = [night for night in masterflatdb if night["nightdatetime"] > processfromhere]
# This time we can safely use this subset for our loop, as we will not update the database !

bsflatdir = os.path.join(biassubdir, flatname)
print "bsflatdir : %s" % bsflatdir
print "processfromhere : %s" % processfromhere

print "%i nights in total" % len(masterflatdb)
print "I will build the nav for %i nights" % (len(masterflatdbgo))

auxfcts.proquest(askquestions)

html = ["""
<html><head>
<title>nav</title>
<meta http-equiv="content-type" content="text/html;charset=utf-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<link rel="stylesheet" type="text/css" media="screen" href="style.css">
</head>
<body>
<table border="1" frame="void" width="%i">
<tr>

""" % (101 * len(masterflatdbgo))]


for i, night in enumerate(masterflatdbgo):

	tdstyle = ""
	
	bgcolor = "#44CC44" # green
	if len(night["thisnightflatsok"]) < 3:
		bgcolor = "#DDAA22" # yellow
	if len(night["flats"]) < 5:
		bgcolor = "#CC4444" # red
	if len(night["flats"]) == 0:
		bgcolor = "#675143"
	
	
	mfpklpath = os.path.join(masterflatdir, "MF_%s.pkl" % night["nightdatetimestr"])
	
	if os.path.isfile(mfpklpath): # So that you can run it even without masterflats.
		irafinfo = auxfcts.readpickle(mfpklpath)
		
		if irafinfo["fullrejpixels"] > 0:
			tdstyle = """style="text-decoration:overline;" """
		elif irafinfo["halfrejpixels"] > 0:
			tdstyle = """style="text-decoration:underline;" """
		
	html.append("""
	<td align="center" bgcolor=%s width="100" %s>
	<a href="%s/index.html" target="page">&nbsp;%s&nbsp;</a>
	</td>
	""" % (bgcolor, tdstyle, night["nightdatetimestr"], night["nightdatetimestr"]))

	

html.append("""
</tr>
</table>
</body>
</html>

""")

htmlpage = open(os.path.join(flatviewdir, "nav.html"), "w")
htmlpage.write("".join(html))
htmlpage.close()

shutil.copy("frames_index.html", os.path.join(flatviewdir, "index.html"))
shutil.copy("style.css", flatviewdir)

