#
#	We simply make png files from the raw bias subtracted flat frames
#

execfile("../global.py")
import f2n


#######
pngname = flatname + "bs"

#######

db = auxfcts.readpickle(os.path.join(dbdir, flatname+".pkl"))
db = sorted(db, key=lambda item: item["datetime"])

bsdir = os.path.join(biassubdir, flatname)

db = db[-500:]

print "flatname : %s" % flatname
print "pngname : %s " % pngname
print "%i pngs to make." % len(db)
auxfcts.proquest(askquestions)


if os.path.isdir(os.path.join(pngdir, pngname)):
	print "The pngname %s exists. I will delete it." % pngname
	print os.path.join(pngdir, pngname)
	auxfcts.proquest(askquestions)
	shutil.rmtree(os.path.join(pngdir, pngname))
os.mkdir(os.path.join(pngdir, pngname))



keywords = ["format", "filter", "EXPTIME", "medclean", "overscanlevel"]


for i, image in enumerate(db):
	print "=== %4i / %4i : %s ===" % (i+1, len(db), image["filename"])
	
	
	f2nimage = f2n.fromfits(os.path.join(bsdir, image["filename"]), hdu=0, verbose=True)
	
	f2nimage.setzscale("flat", "flat")
	f2nimage.rebin(3)
	f2nimage.makepilimage(scale="lin", negative=False)
	#f2nimage.showcutoffs(redblue = True)
	f2nimage.writetitle(image["filename"])
	
	infolist = ["", "", image["datetime"].strftime("  %Y-%m-%d   %H:%M")]
	infolist.extend(["%12s : %s" % (key, image[key]) for key in keywords])
	infolist.append("%12s : %5i %5i " % ("Cuts", f2nimage.z1, f2nimage.z2))
	#infolist.append(image["datetime"].strftime("%Y-%m-%d %H:%M"))
	
	f2nimage.writeinfo(infolist)
	
	pngpath = os.path.join(pngdir, pngname, image["basename"] + ".png")
	pngpathlink = os.path.join(pngdir, pngname, "%05i.png" % (i+1))
	
	f2nimage.tonet(pngpath)
	os.symlink(pngpath, pngpathlink)
