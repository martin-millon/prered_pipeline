"""

This is the only script (ok, make masterflat also exists) that uses pyraf !
Run it on obssl1 or obssl2

"""


execfile("../global.py")
from pyraf import iraf


print "superflatname : %s" % superflatname


if os.path.isdir(superflatdir):
	print "The superflatdir exists. I will complete it."
	print superflatdir
	auxfcts.proquest(askquestions)
else:
	os.mkdir(superflatdir)


superflatdb = auxfcts.readpickle(os.path.join(dbdir, "MF_" + superflatname + ".pkl"))
superflatdbgo = [night for night in superflatdb if night["nightdatetime"] > processfromhere]
# This time we can safely use this subset for our loop, as we will not update the database !

bsflatdir = os.path.join(biassubdir, flatname)
print "bsflatdir : %s" % bsflatdir
print "processfromhere : %s" % processfromhere

print "%i nights in total" % len(superflatdb)
print "I will go for %i nights" % (len(superflatdbgo))

auxfcts.proquest(askquestions)


for i, night in enumerate(superflatdbgo):
	
	irafinfo = {} # this guy will be written into a dedicated pickle aside of the superflat.
	
	print "============ %4i / %4i : %s ==========" % (i+1, len(superflatdbgo), night["nightdatetimestr"])
	
	if len(night["sciexps"]) == 0:
		print "No sci exps, error"
		sys.exit()
	
	print "Combination of %i images" % len(night["sciexps"])
	
	inputfiles = '\n'.join([os.path.join(biassubdir, img["dbname"], img["filename"]) for img in night["sciexps"]]) + '\n'
	txt_file = open('SFirafinput.txt', 'w') # It is important to choose another name here then for the masterflats, so that you can run the scripts in parallel !
	txt_file.write(inputfiles)
	txt_file.close()
	
	iraf.unlearn(iraf.images.immatch.imcombine)
	"""
	input   =                       List of images to combine
	output  =                       List of output images
	(headers=                     ) List of header files (optional)
	(bpmasks=                     ) List of bad pixel masks (optional)
	(rejmask=                     ) List of rejection masks (optional)
	(nrejmas=                     ) List of number rejected masks (optional)
	(expmask=                     ) List of exposure masks (optional)
	(sigmas =                     ) List of sigma images (optional)
	(logfile=               STDOUT) Log file
	
	(combine=              average) Type of combine operation
	(reject =                 none) Type of rejection
	(project=                   no) Project highest dimension of input images?
	(outtype=                 real) Output image pixel datatype
	(outlimi=                     ) Output limits (x1 x2 y1 y2 ...)
	(offsets=                 none) Input image offsets
	(masktyp=                 none) Mask type
	(maskval=                    0) Mask value
	(blank  =                   0.) Value if there are no pixels
	
	(scale  =                 none) Image scaling
	(zero   =                 none) Image zero point offset
	(weight =                 none) Image weights
	(statsec=                     ) Image section for computing statistics
	(expname=                     ) Image header exposure time keyword
	
	(lthresh=                INDEF) Lower threshold
	(hthresh=                INDEF) Upper threshold
	(nlow   =                    1) minmax: Number of low pixels to reject
	(nhigh  =                    1) minmax: Number of high pixels to reject
	(nkeep  =                    1) Minimum to keep (pos) or maximum to reject (neg)
	(mclip  =                  yes) Use median in sigma clipping algorithms?
	(lsigma =                   3.) Lower sigma clipping factor
	(hsigma =                   3.) Upper sigma clipping factor
	(rdnoise=                   0.) ccdclip: CCD readout noise (electrons)
	(gain   =                   1.) ccdclip: CCD gain (electrons/DN)
	(snoise =                   0.) ccdclip: Sensitivity noise (fraction)
	(sigscal=                  0.1) Tolerance for sigma clipping scaling corrections
	(pclip  =                 -0.5) pclip: Percentile clipping parameter
	(grow   =                   0.) Radius (pixels) for neighbor rejection
	(mode   =                   ql)
	"""
	
	iraf.images.immatch.imcombine.combine = "median"
	iraf.images.immatch.imcombine.scale = "mode"
	iraf.images.immatch.imcombine.reject = "sigclip"
	iraf.images.immatch.imcombine.lthresh = 10.0
	iraf.images.immatch.imcombine.hthresh = 60000.0
	iraf.images.immatch.imcombine.lsigma = 3.0
	iraf.images.immatch.imcombine.hsigma = 3.0
	iraf.images.immatch.imcombine.grow = 1.0
	iraf.images.immatch.imcombine.nkeep = -2 # Reject at most 2 values for each pixel
	
	
	outputfilepath = os.path.join(superflatdir, "SF_%s_notnorm.fits" % night["nightdatetimestr"])
	
	if os.path.isfile(outputfilepath):
		print "Deleting previous non-normed file."
		os.remove(outputfilepath)
	
	try:
		txt_output = iraf.images.immatch.imcombine("@SFirafinput.txt", output = outputfilepath, Stdout=1)
	except:
		print "I crashed :-("
		txt_output = "I crashed :-("
		os.remove("SFirafinput.txt")
		irafinfo["combitxtlines"] = txt_output
		irafinfo["combimedianval"] = 0.0
		auxfcts.writepickle(irafinfo, os.path.join(superflatdir, "SF_%s.pkl" % night["nightdatetimestr"]))
		continue
	
	os.remove("SFirafinput.txt")
	
	print "\n".join(txt_output)
	
	irafinfo["combitxtlines"] = txt_output
	
	# We normalize the superflat
	
	print "Normalization"
	inputfilepath = outputfilepath
	
	iraf.unlearn(iraf.imutil.imstatistics)
	"""
	images  =                       List of input images
	(fields = image,npix,mean,stddev,min,max) Fields to be printed
	(lower  =                INDEF) Lower limit for pixel values
	(upper  =                INDEF) Upper limit for pixel values
	(nclip  =                    0) Number of clipping iterations
	(lsigma =                   3.) Lower side clipping factor in sigma
	(usigma =                   3.) Upper side clipping factor in sigma
	(binwidt=                  0.1) Bin width of histogram in sigma
	(format =                  yes) Format output and print column labels ?
	(cache  =                   no) Cache image in memory ?
	(mode   =                   ql)

	"""
	iraf.imutil.imstatistics.fields = "npix,midpt,mean"
	iraf.imutil.imstatistics.lower = 10.0
	iraf.imutil.imstatistics.upper = 60000.0
	iraf.imutil.imstatistics.nclip = 0
	iraf.imutil.imstatistics.format = 0
	
	
		
	text_output = iraf.imutil.imstatistics(inputfilepath, Stdout=1)
	
	print text_output
	stats = text_output[0].split()
	medianval = stats[1]
	print "Median : ", medianval
	irafinfo["combimedianval"] = medianval
	
	
	iraf.unlearn(iraf.imutil.imarith)
	"""
	operand1=                       Operand image or numerical constant
	op      =                    +  Operator
	operand2=                       Operand image or numerical constant
	result  =                       Resultant image
	(title  =                     ) Title for resultant image
	(divzero=                   0.) Replacement value for division by zero
	(hparams=                     ) List of header parameters
	(pixtype=                     ) Pixel type for resultant image
	(calctyp=                     ) Calculation data type
	(verbose=                   no) Print operations?
	(noact  =                   no) Print operations without performing them?
	(mode   =                   ql)

	"""
	
	outputfilepath = os.path.join(superflatdir, "SF_%s.fits" % night["nightdatetimestr"])
	
	if os.path.isfile(outputfilepath):
		print "Deleting previous superflat."
		os.remove(outputfilepath)
	
	iraf.imutil.imarith(inputfilepath, '/', medianval, outputfilepath)
	
	if os.path.isfile(inputfilepath): # removing non-normalized superflat
		os.remove(inputfilepath)

	# we write the pickle :

	auxfcts.writepickle(irafinfo, os.path.join(superflatdir, "SF_%s.pkl" % night["nightdatetimestr"]))
	
	print "Done."
