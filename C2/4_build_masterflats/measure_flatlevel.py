"""
We measure the level of the flat images (post-bias-subtraction)
This is obviously useful, at least to check that they are not saturated etc.
"""


execfile("../global.py")
targetname = flatname

cleanregion = (50, 1950, 50, 1950)

dbpath = os.path.join(dbdir, targetname + ".pkl")
db = auxfcts.readpickle(dbpath)
db = sorted(db, key=lambda item: item["datetime"])

print "%i files in db" % len(db)

if not redofromscratch:
	treatdb = [img for img in db if "medclean" not in img.keys()]
	print "I will only treat %i not-yet-done files" % len(treatdb)

else:
	print "I will treat all of them."
auxfcts.proquest(askquestions)


bsdir = os.path.join(biassubdir, targetname)

outdb = [] # will put the measured data here + everything needed for the plot
	
for i, image in enumerate(db):
	if not redofromscratch:
		if "medclean" in image.keys():
			continue
			
	print "=== %4i/%4i : format %i : %s ===" % (i+1, len(db), image["format"], image["filename"])

	infilepath = os.path.join(bsdir, image["filename"])
	(a, h) = auxfcts.fromfits(infilepath, hdu = 0, verbose = False)
		
	exec "clean = a[%i:%i,%i:%i]" % cleanregion
	image["medclean"] = np.median(clean.ravel())
	image["stdclean"] = np.std(clean.ravel())
	image["meanclean"] = np.mean(clean.ravel())

	outdb.append(image)

outdbpath = dbpath
if os.path.isfile(outdbpath):
	auxfcts.backupfile(outdbpath, backupdir, "flatlevel")
auxfcts.writepickle(db, outdbpath, verbose=True)
		
		
	
	
