"""

Uses pyraf imcombine, median with masked stars
Run it on obssl1 or obssl2

"""


execfile("../global.py")
from pyraf import iraf


print "masterflatname : %s" % masterflatname


if os.path.isdir(masterflatdir):
	print "The masterflatdir exists. I will complete it."
	print masterflatdir
	auxfcts.proquest(askquestions)
else:
	os.mkdir(masterflatdir)


masterflatdb = auxfcts.readpickle(os.path.join(dbdir, "MF_" + masterflatname + ".pkl"))
masterflatdbgo = [night for night in masterflatdb if night["nightdatetime"] > processfromhere]
# This time we can safely use this subset for our loop, as we will not update the database !

bsflatdir = os.path.join(biassubdir, flatname)
print "bsflatdir : %s" % bsflatdir
print "processfromhere : %s" % processfromhere

print "%i nights in total" % len(masterflatdb)
print "I will go for %i nights" % (len(masterflatdbgo))

auxfcts.proquest(askquestions)

# Now we move to the directory where the flats and masks are.
os.chdir(bsflatdir)
# so for iraf input we will use relative paths !
# The input image list will also be traeted as relative path located in teh bsflatdir.


# We prepare the IRAF functions

iraf.unlearn(iraf.images.immatch.imcombine)
iraf.unlearn(iraf.imutil.imcopy)

# Default params of imcombine :
"""
	input   =                       List of images to combine
	output  =                       List of output images
	(headers=                     ) List of header files (optional)
	(bpmasks=                     ) List of bad pixel masks (optional)
	(rejmask=                     ) List of rejection masks (optional)
	(nrejmas=                     ) List of number rejected masks (optional)
	(expmask=                     ) List of exposure masks (optional)
	(sigmas =                     ) List of sigma images (optional)
	(logfile=               STDOUT) Log file
	
	(combine=              average) Type of combine operation
	(reject =                 none) Type of rejection
	(project=                   no) Project highest dimension of input images?
	(outtype=                 real) Output image pixel datatype
	(outlimi=                     ) Output limits (x1 x2 y1 y2 ...)
	(offsets=                 none) Input image offsets
	(masktyp=                 none) Mask type
	(maskval=                    0) Mask value
	(blank  =                   0.) Value if there are no pixels
	
	(scale  =                 none) Image scaling
	(zero   =                 none) Image zero point offset
	(weight =                 none) Image weights
	(statsec=                     ) Image section for computing statistics
	(expname=                     ) Image header exposure time keyword
	
	(lthresh=                INDEF) Lower threshold
	(hthresh=                INDEF) Upper threshold
	(nlow   =                    1) minmax: Number of low pixels to reject
	(nhigh  =                    1) minmax: Number of high pixels to reject
	(nkeep  =                    1) Minimum to keep (pos) or maximum to reject (neg)
	(mclip  =                  yes) Use median in sigma clipping algorithms?
	(lsigma =                   3.) Lower sigma clipping factor
	(hsigma =                   3.) Upper sigma clipping factor
	(rdnoise=                   0.) ccdclip: CCD readout noise (electrons)
	(gain   =                   1.) ccdclip: CCD gain (electrons/DN)
	(snoise =                   0.) ccdclip: Sensitivity noise (fraction)
	(sigscal=                  0.1) Tolerance for sigma clipping scaling corrections
	(pclip  =                 -0.5) pclip: Percentile clipping parameter
	(grow   =                   0.) Radius (pixels) for neighbor rejection
	(mode   =                   ql)
"""
	
iraf.images.immatch.imcombine.combine = "median"
iraf.images.immatch.imcombine.scale = "mode"
iraf.images.immatch.imcombine.reject = "sigclip" # we still want to reject the usual cosmic rays ...nothing to do with masks
iraf.images.immatch.imcombine.lthresh = 10.0
iraf.images.immatch.imcombine.hthresh = 60000.0
iraf.images.immatch.imcombine.lsigma = 3.0 # pixels kicked by sigclip do show up as well as the masks in nrejmask.
iraf.images.immatch.imcombine.hsigma = 3.0
iraf.images.immatch.imcombine.grow = 1.0
iraf.images.immatch.imcombine.nkeep = 2 # Keep min 2 pixels
iraf.images.immatch.imcombine.masktype = "!OBJMASK" # the keyword that contains the filename of the pl mask file.
iraf.images.immatch.imcombine.blank = "10" # lower then the background -> would produce "cosmics" in the reduced images.



for i, night in enumerate(masterflatdbgo):
	
	irafinfo = {} # this guy will be written into a dedicated pickle aside of the masterflat.
	
	print "============ %4i / %4i : %s ==========" % (i+1, len(masterflatdbgo), night["nightdatetimestr"])
	
	nbrcombi = len(night["flats"])
	irafinfo["nbrcombi"] = nbrcombi
	
	if nbrcombi == 0:
		# We cannot do anything. Just to be sure, we erase previous flats...
		previousmasterflatpath = os.path.join(masterflatdir, "MF_%s.fits" % night["nightdatetimestr"])
		if os.path.isfile(previousmasterflatpath):
			# Just to be sure
			print "No flats, so at least deleting previous masterflat."
			os.remove(previousmasterflatpath)
		continue
	
	print "Combination of %i flats" % nbrcombi
	
	
	#inputfiles = '\n'.join([os.path.join(bsflatdir, flat["filename"]) for flat in night["flats"]]) + '\n'
	inputfiles = '\n'.join([flat["filename"] for flat in night["flats"]]) + '\n'
	
	
	txt_file = open('MFirafinput.txt', 'w')
	txt_file.write(inputfiles)
	txt_file.close()
	
	
	
	outputfilepath = os.path.join(masterflatdir, "MF_%s_notnorm.fits" % night["nightdatetimestr"])
	nrejmaskfilepath = os.path.join(masterflatdir, "MFnrejmask_%s.pl" % night["nightdatetimestr"])
	nrejmaskfitsfilepath = os.path.join(masterflatdir, "MFnrejmask_%s.fits" % night["nightdatetimestr"])
	
	if os.path.isfile(outputfilepath):
		print "Deleting previous non-normed file."
		os.remove(outputfilepath)
	
	
	if os.path.isfile(nrejmaskfilepath):
		os.remove(nrejmaskfilepath)
	if os.path.isfile(nrejmaskfitsfilepath):
		os.remove(nrejmaskfitsfilepath)
	# COMBINATION 
	
	txt_output = iraf.images.immatch.imcombine("@MFirafinput.txt", output = outputfilepath, nrejmask = nrejmaskfilepath, Stdout=1)
	print "\n".join(txt_output)
	irafinfo["combitxtlines"] = txt_output
	
	# We convert the nrejmask from pl to fits :
	iraf.imutil.imcopy(nrejmaskfilepath, nrejmaskfitsfilepath)
	
	if os.path.isfile(nrejmaskfilepath):
		os.remove(nrejmaskfilepath)
	
	
	os.remove("MFirafinput.txt")
	
	
	# NORMALIZATION
	
	print "Normalization"
	inputfilepath = outputfilepath
	
	iraf.unlearn(iraf.imutil.imstatistics)
	"""
	images  =                       List of input images
	(fields = image,npix,mean,stddev,min,max) Fields to be printed
	(lower  =                INDEF) Lower limit for pixel values
	(upper  =                INDEF) Upper limit for pixel values
	(nclip  =                    0) Number of clipping iterations
	(lsigma =                   3.) Lower side clipping factor in sigma
	(usigma =                   3.) Upper side clipping factor in sigma
	(binwidt=                  0.1) Bin width of histogram in sigma
	(format =                  yes) Format output and print column labels ?
	(cache  =                   no) Cache image in memory ?
	(mode   =                   ql)

	"""
	iraf.imutil.imstatistics.fields = "npix,midpt,mean"
	iraf.imutil.imstatistics.lower = 10.0
	iraf.imutil.imstatistics.upper = 60000.0
	iraf.imutil.imstatistics.nclip = 0
	iraf.imutil.imstatistics.format = 0
	
	
		
	text_output = iraf.imutil.imstatistics(inputfilepath, Stdout=1)
	
	print text_output
	stats = text_output[0].split()
	medianval = stats[1]
	print "Median : ", medianval
	irafinfo["combimedianval"] = medianval
	
	
	iraf.unlearn(iraf.imutil.imarith)
	"""
	operand1=                       Operand image or numerical constant
	op      =                    +  Operator
	operand2=                       Operand image or numerical constant
	result  =                       Resultant image
	(title  =                     ) Title for resultant image
	(divzero=                   0.) Replacement value for division by zero
	(hparams=                     ) List of header parameters
	(pixtype=                     ) Pixel type for resultant image
	(calctyp=                     ) Calculation data type
	(verbose=                   no) Print operations?
	(noact  =                   no) Print operations without performing them?
	(mode   =                   ql)

	"""
	
	outputfilepath = os.path.join(masterflatdir, "MF_%s.fits" % night["nightdatetimestr"])
	
	if os.path.isfile(outputfilepath):
		print "Deleting previous masterflat."
		os.remove(outputfilepath)
	
	iraf.imutil.imarith(inputfilepath, '/', medianval, outputfilepath)
	
	if os.path.isfile(inputfilepath): # removing non-normalized masterflat
		os.remove(inputfilepath)


	# Now we want to analyse the rejection, see if we have problems with the mask.
	# For this we read the nrejmask and do some numpy stats on it.
	
	#if os.path.isfile(nrejmaskfitsfilepath): # should always be the case
		
	(pixelarray, hdr) = auxfcts.fromfits(nrejmaskfitsfilepath, hdu = 0, verbose = True)
	irafinfo["nbrpixels"] = pixelarray.size
	irafinfo["fullrejpixels"] = len(pixelarray[pixelarray > nbrcombi - 0.5].ravel()) # all images are rejected for these pixels !
	print "Number of full rejection pixels : %i" % irafinfo["fullrejpixels"]
	irafinfo["halfrejpixels"] = len(pixelarray[pixelarray > int((nbrcombi/2.0) - 0.1)].ravel()) # more then half are rejected !
	print "Number of half rejection pixels : %i" % irafinfo["halfrejpixels"]	
	irafinfo["twoormorerejpixels"] = len(pixelarray[pixelarray > 1.1].ravel()) # 2 or more pixels are rejected
	print "Number of twoormore rejection pixels : %10i / %10i (%6.3f %%)" % (irafinfo["twoormorerejpixels"], irafinfo["nbrpixels"], (float(irafinfo["twoormorerejpixels"]) / float(irafinfo["nbrpixels"])) * 100.0)
	
	irafinfo["partrejpixels"] = len(pixelarray[pixelarray > 0.1].ravel()) # some pixels are rejected
	print "Number of part rejection pixels      : %10i / %10i (%6.3f %%)" % (irafinfo["partrejpixels"], irafinfo["nbrpixels"], (float(irafinfo["partrejpixels"]) / float(irafinfo["nbrpixels"])) * 100.0)
	

	# we write the pickle :
	mfpicklepath = os.path.join(masterflatdir, "MF_%s.pkl" % night["nightdatetimestr"])
	if os.path.isfile(mfpicklepath):
		print "Deleting previous mfpickle."
		os.remove(mfpicklepath)
	
	auxfcts.writepickle(irafinfo, mfpicklepath)
	
	print "Done."
