# The heart of the prereuction : we choose how the flats are combined.
# At the end we write a dedicated database to be used by the following scripts.
# Boy am I proud of this one...

# We do modify the databases, but we do not save them. Only a new one.
# We check the filter of the images, but not of the flats !

execfile("../global.py")




print "Science db : %s" % allsciexpdbname
print "filtername : %s" % filtername
print "Flats  : %s" % flatname
print "masterflatname : %s" % masterflatname
print "minflats : %i" % minflats
print "maxsearch : %i" % maxsearch

print "flatskiplist : %s" % flatskiplistfilename
print "flatcutlist  : %s" % flatcutlistfilename



flatdb = auxfcts.readpickle(os.path.join(dbdir, flatname + ".pkl"))
flatdb = sorted(flatdb, key=lambda item: item["datetime"])

print "I've read %i flats from database." % len(flatdb)


# We read the science object database :
db = auxfcts.readpickle(os.path.join(dbdir, allsciexpdbname + ".pkl"))
db = sorted(db, key=lambda item: item["datetime"])

print "I've read %i science images from database." % len(db)

db = [img for img in db if img["filter"] == filtername]

print "Among those %i have the right filter." % len(db)

auxfcts.proquest(askquestions)

masterflatdbpath = os.path.join(dbdir, "MF_" + masterflatname + ".pkl")
if os.path.isfile(masterflatdbpath):
	print "This masterflatdatabase exists. I will make a backup and overwrite it."
	auxfcts.proquest(askquestions)


#	TAKING CARE OF BAD FLATS
#	We simply flag them as ok = False ...

flatskip = auxfcts.readimagelist(os.path.join(configdir, flatskiplistfilename))
flatskiplist = [image["imgname"] for image in flatskip]
flatskipcomments = [image["comment"] for image in flatskip]
# HA, THIS IS SO UGLY ... remember to use dict keys as identifiers, much more flexible !!!

for flat in flatdb:
	flat["ok"] = True
	#flat["kocomment"] = None
	if flat["filename"] in flatskiplist:
		flat["ok"] = False
		flat["kocomment"] = flatskipcomments[flatskiplist.index(flat["filename"])]
		#print '%s : "%s"' % (flat["filename"], flat["kocomment"])

print "I will disregard %i bad flats." % len([flat for flat in flatdb if flat["ok"] == False])		


#	TAKING CARE OF THE FLAT "CUTS" (aka grouping the flats)
#	We add some fields to each flat

flatcuts = auxfcts.readdatelist(os.path.join(configdir, flatcutlistfilename))
# We add extreme dates :
flatcuts.append({"pythondt":datetime.datetime(1900, 01, 01), "comment":"-Inf"})
flatcuts.append({"pythondt":datetime.datetime(2100, 01, 01), "comment":"+Inf"})
# And sort this :
flatcuts = sorted(flatcuts, key=lambda item: item["pythondt"])

# flatcut is now a list of dicts  {"pythondt", "comment"}

starts = flatcuts[:-1]
ends = flatcuts[1:]

#for s,e in zip(starts, ends):
#	print s["pythondt"], e["pythondt"]


for flat in flatdb:
	#print flat["filename"], flat["datetime"]
	flat["groupstart"] = None
	for (start, end) in zip(starts, ends):
		if flat["datetime"] > start["pythondt"] and flat["datetime"] < end["pythondt"]:
			# Then we have found the right group.
			flat["groupstart"] = start
			flat["groupend"] = end
			break # get out of the for loop
	
	if flat["groupstart"] == None:
		raise RuntimeError("Could not find a group for this flat !")
	
	

"""
# We will now reorganize the flatdb into groups according to the cutlist.
# We simply build a list of dicts, where each dict is such a group and contains :
#  - "flats" : the list of flat exposres
#  - "start" : {"pythondt", "comment"} of the beginning of the group
#  - "end"   : {"pythondt", "comment"} of the end of the group

# We add extreme dates :
flatcut.append({"pythondt":datetime.datetime(1900, 01, 01), "comment":"-Inf"})
flatcut.append({"pythondt":datetime.datetime(2100, 01, 01), "comment":"+Inf"})
# And sort this :
flatcut = sorted(flatcut, key=lambda item: item["pythondt"])

starts = flatcut[:-1]
ends = flatcut[1:]
groupindexes = range(len(starts))

flatgroups = []
for (start, end, i) in zip(starts, ends, groupindexes):

	flatgroup = [flat for flat in flatdb if flat["datetime"] > start["pythondt"] and flat["datetime"] < end["pythondt"]]
	flatgroups.append({"flats":flatgroup, "start":start, "end":end})
	okflats = [flat for flat in flatgroup if flat["ok"] == True]
	
	# Just for indication, we find how many objects are in these groups
	scienceingroup = [img for img in db if img["datetime"] > start["pythondt"] and img["datetime"] < end["pythondt"]]
	
	print "Group %3i : %s -> %s : %4i OK & %4i KO : %4i sci. exp." % (i+1, start["pythondt"], end["pythondt"], len(okflats), len(flatgroup) - len(okflats), len(scienceingroup))
	
"""


# We go through the science exposures, night by night (only those of the right filter)
# Probably we could directrly use the datetime objects here, but I find it safer to work on date strings, in case there are "rounding errors"...

nights = sorted(list(set([img["nightdatetimestr"] for img in db])))
print "%i masterflats to assemble." % len(nights) 

# So night is a list of strings like 2009-12-24.

auxfcts.proquest(askquestions)

# Old, assymetric :
# we build a list that looks like [1, -1, 2, -2, 3, -3, ...] for searching adjacent dates
#searchlist = []
#for i in range(1,maxsearch):
#	searchlist.extend([i, -i])

searchlist = range(1, maxsearch) # symmetric search (we always include both the night before and after...

masterflatdb = [] # the final database to write

for night in nights:
	print "       ============================== Night %s ============================ " % night
	
	sciexps = [img for img in db if img["nightdatetimestr"] == night]
	print "%i science exposures" % len(sciexps)
	
	# A datetime "in the middle" of the night : we take the one from the first exposure :
	firstexpdt = sciexps[0]["datetime"]
	lastexpdt = sciexps[-1]["datetime"]
	nightdt = sciexps[0]["nightdatetime"] # just to get back to a python datetime object
	
	# We find the flats of this group (of course including those flagged as ko...)
	thisgroupflats = [flat for flat in flatdb if flat["groupstart"]["pythondt"] < firstexpdt and flat["groupend"]["pythondt"] > firstexpdt]
	thisgroupflatsok = [flat for flat in thisgroupflats if flat["ok"] == True]
	print "%4i/%4i flats in this group" % (len(thisgroupflatsok), len(thisgroupflats))
	
	
	# For plotting purposes, we also want to find the group of this night, even if no flats are available:
	# (yes, this is a bit messy...)
	thisgroupstart = None
	for (start, end) in zip(starts, ends):
		if firstexpdt > start["pythondt"] and firstexpdt < end["pythondt"]:
			# Then we have found the right group.
			thisgroupstart = start
			thisgroupend = end
			break # get out of the for loop
	
	if thisgroupstart == None:
		raise RuntimeError("Could not find a group for this night !")
	
	"""
	thisflatgroup = [fg for fg in flatgroups if fg["start"]["pythondt"] < firstexpdt and fg["end"]["pythondt"] > lastexpdt]
	if len(thisflatgroup) != 1:
		print "Error with flatgroups or with the dates of the images of this night... solve this !"
		sys.exit()
	thisflatgroup = thisflatgroup[0]
	print "Flatgroup : %s -> %s (%s -> %s)" % (thisflatgroup["start"]["pythondt"], thisflatgroup["end"]["pythondt"], thisflatgroup["start"]["comment"], thisflatgroup["end"]["comment"])
	"""
	
	# Ok, now that we have everything together we can start looking the the flats we want to use.
	
	thisnightflats = [flat for flat in thisgroupflats if flat["nightdatetimestr"] == night]
	thisnightflatsok = [flat for flat in thisnightflats if flat["ok"] == True]
	print "%4i/%4i flats in this night" % (len(thisnightflatsok), len(thisnightflats))
	
	flatstouse = thisnightflats[:] # these copies are important, we want to keep these list of flats separated
	flatstouseok = thisnightflatsok[:]
	
	if len(flatstouseok) < minflats:
		print "This is not enough. I will look around ..."
		
		# Old, asymmetric :
		
#		for i in searchlist:
#			thatnightdt = nightdt + datetime.timedelta(i) # i is in days
#			thatnightstr = thatnightdt.strftime('%Y-%m-%d')
#			
#			thatnightflats = [flat for flat in thisgroupflats if flat["nightdatetimestr"] == thatnightstr]
#			thatnightflatsok = [flat for flat in thatnightflats if flat["ok"] == True]
#			
#			# As a bonus, we have a look also at all the flats, not only of this group
#			thatnightallgroupflats = [flat for flat in flatdb if flat["nightdatetimestr"] == thatnightstr]
#			thatnightallgroupflatsok = [flat for flat in thatnightallgroupflats if flat["ok"] == True]
#			
#			if len(thatnightallgroupflats) != len(thatnightflats):
#				outofgroupmessage = "(Out of group)"
#			else:
#				outofgroupmessage = ""
#			
#			print "%+2i days : %s : %2i/%2i/%2i/%2i flats %s" % (i, thatnightstr, len(thatnightflatsok), len(thatnightflats), len(thatnightallgroupflatsok), len(thatnightallgroupflats), outofgroupmessage)
#			flatstouse.extend(thatnightflats)
#			flatstouseok.extend(thatnightflatsok)
#			if len(flatstouseok) >= minflats:
#				print "Ok, I am happy, I have %i flats to use." % len(flatstouseok)
#				break # get out of the for loop


		for i in searchlist:
			
			# The night ahead :
			
			thatnightdt = nightdt + datetime.timedelta(i) # i is in days
			thatnightstr = thatnightdt.strftime('%Y-%m-%d')
			
			thatnightflats = [flat for flat in thisgroupflats if flat["nightdatetimestr"] == thatnightstr]
			thatnightflatsok = [flat for flat in thatnightflats if flat["ok"] == True]
			
			# As a bonus, we have a look also at all the flats, not only of this group
			thatnightallgroupflats = [flat for flat in flatdb if flat["nightdatetimestr"] == thatnightstr]
			thatnightallgroupflatsok = [flat for flat in thatnightallgroupflats if flat["ok"] == True]
			
			if len(thatnightallgroupflats) != len(thatnightflats):
				outofgroupmessage = "(Out of group)"
			else:
				outofgroupmessage = ""
			
			print "%+2i days : %s : %2i/%2i/%2i/%2i flats %s" % (i, thatnightstr, len(thatnightflatsok), len(thatnightflats), len(thatnightallgroupflatsok), len(thatnightallgroupflats), outofgroupmessage)
			
			
			# We do the same for the night in the past :
			
			thatpastnightdt = nightdt + datetime.timedelta(-i) # i is in days
			thatpastnightstr = thatpastnightdt.strftime('%Y-%m-%d')
			
			thatpastnightflats = [flat for flat in thisgroupflats if flat["nightdatetimestr"] == thatpastnightstr]
			thatpastnightflatsok = [flat for flat in thatpastnightflats if flat["ok"] == True]
			
			# As a bonus, we have a look also at all the flats, not only of this group
			thatpastnightallgroupflats = [flat for flat in flatdb if flat["nightdatetimestr"] == thatpastnightstr]
			thatpastnightallgroupflatsok = [flat for flat in thatpastnightallgroupflats if flat["ok"] == True]
			
			if len(thatpastnightallgroupflats) != len(thatpastnightflats):
				outofgroupmessage = "(Out of group)"
			else:
				outofgroupmessage = ""
			
			print "%+2i days : %s : %2i/%2i/%2i/%2i flats %s" % (-i, thatpastnightstr, len(thatpastnightflatsok), len(thatpastnightflats), len(thatpastnightallgroupflatsok), len(thatpastnightallgroupflats), outofgroupmessage)
			
			
			flatstouse.extend(thatnightflats)
			flatstouseok.extend(thatnightflatsok)
			flatstouse.extend(thatpastnightflats)
			flatstouseok.extend(thatpastnightflatsok)
			
			if len(flatstouseok) >= minflats:
				print "Ok, I am happy, I have %i flats to use." % len(flatstouseok)
				break # get out of the for loop
	

	
	
	flatstouse = sorted(flatstouse, key=lambda item: item["datetime"])
	flatstouseok = sorted(flatstouseok, key=lambda item: item["datetime"])
	
	for flat in flatstouse:
		delta = (flat["datetime"] - firstexpdt)
		deltadays = (3600*24*delta.days + delta.seconds)/(3600.0*24.0)
		
		if flat["ok"] == False:
			nookmessage = "(X)"
		else:
			nookmessage = ""
		
		print "%s : %s : %+7.2f days %s" % (flat["filename"], flat["datetime"].strftime("%Y-%m-%dT%H:%M:%S"), deltadays, nookmessage)
		
	
	
	masterflatinfo = {"nightdatetime": nightdt, "nightdatetimestr":night, "flats":flatstouseok,
	"thisnightflats":thisnightflats, "thisnightflatsok":thisnightflatsok, "flatsinclnotok": flatstouse,
	"firstexpdt":firstexpdt, "lastexpdt": lastexpdt, "sciexps": sciexps, "groupstart": thisgroupstart, "groupend": thisgroupend
	} 
	
		
	masterflatdb.append(masterflatinfo)
	

if os.path.isfile(masterflatdbpath):
	auxfcts.backupfile(masterflatdbpath, backupdir, "groupflats")
auxfcts.writepickle(masterflatdb, masterflatdbpath)
	



















