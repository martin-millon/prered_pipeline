"""

We associate a mask to each flat frame, to mask out stars in the combination.
This is independent of the flat_grouping...

Uses IRAF objmasks task to produce a .pl file ... ah the good old times !
The mask file is stored aside of the flat frames.
The IRAF task edits the header or the flat frame, adding the keyword
OBJMASK that points to that mask file.

"""


execfile("../global.py")
from pyraf import iraf


print "I will associate a mask to every flat of"
print flatname
print "from"
print processfromhere
print "on."
print "biassubdir : "
print biassubdir

db = auxfcts.readpickle(os.path.join(dbdir, flatname+".pkl"))
db = sorted(db, key=lambda item: item["datetime"])

dbgo = [flat for flat in db if flat["nightdatetime"] > processfromhere]

print "This means I will run on %i flats." % len(dbgo)

auxfcts.proquest(askquestions)


bsdir = os.path.join(biassubdir, flatname)


iraf.nproto() # loading that package
iraf.unlearn(iraf.nproto.objmasks)
iraf.unlearn(iraf.imutil.hedit)

# Default params of objmask :
"""
PACKAGE = nproto
   TASK = objmasks

images  =                       List of images or MEF files
objmasks=                       List of output object masks
(omtype =              numbers) Object mask type
(skys   =                     ) List of input/output sky maps
(sigmas =                     ) List of input/output sigma maps
(masks  =                 !BPM) List of input bad pixel masks
(extname=                     ) Extension names
(logfile=               STDOUT) List of log files

(blkstep=                    1) Line step for sky sampling
(blksize=                  -10) Sky block size (+=pixels, -=blocks)
(convolv=            block 3 3) Convolution kernel
(hsigma =                   3.) Sigma threshold above sky
(lsigma =                  10.) Sigma threshold below sky
(hdetect=                  yes) Detect objects above sky?
(ldetect=                   no) Detect objects below sky?
(neighbo=                    8) Neighbor type"
(minpix =                    6) Minimum number of pixels in detected objects
(ngrow  =                    2) Number of grow rings
(agrow  =                   2.) Area grow factor
(mode   =                   ql)
"""

"""
iraf.nproto.objmasks.omtype = "boolean"
iraf.nproto.objmasks.hsigma = 10.0 # We only want to mask stars after all, nothing else.
iraf.nproto.objmasks.hdetect = "no" # WARNING : SHOULD BE YES
iraf.nproto.objmasks.ldetect = "no"
iraf.nproto.objmasks.minpix = 5
iraf.nproto.objmasks.ngrow = 3
iraf.nproto.objmasks.agrow = 5.0
"""

iraf.nproto.objmasks.omtype = "boolean"
iraf.nproto.objmasks.hsigma = 10.0 # We only want to mask stars after all, nothing else.
iraf.nproto.objmasks.hdetect = "yes" # WARNING : SHOULD BE YES
iraf.nproto.objmasks.ldetect = "no"
iraf.nproto.objmasks.minpix = 30
iraf.nproto.objmasks.ngrow = 4
iraf.nproto.objmasks.agrow = 10.0

for i, image in enumerate(dbgo):
	print "=== %4i / %4i : %s ===" % (i+1, len(dbgo), image["filename"])

	
	flatpath = os.path.join(bsdir, image["filename"])
	maskfilename = "mask_" + image["basename"] + ".pl"
	maskpath = os.path.join(bsdir, maskfilename)
	
	if os.path.isfile(maskpath):
		print "Deleting existing mask ..."
		os.remove(maskpath)
	
	# We erase any previous header OBJMASK
	#iraf.imutil.hedit(images=flatpath, fields="OBJMASK", delete="yes", show="yes", verify="no", update="yes")
	
	iraf_output = iraf.nproto.objmasks(flatpath, maskpath, Stdout=1)
	#print "\n".join(iraf_output)
	
	nstars = int([line for line in iraf_output if "objects detected" in line][0].split()[0])
	print "I have made a mask for %i stars." % nstars
	
	# We erase the header entry as the path name get cropped ... and by principle... this is such a stupide thing !
	iraf.imutil.hedit(images=flatpath, fields="OBJMASK", delete="yes", show="yes", verify="no", update="yes")
	
	# But unfortunately we a forced to use this sh... so we add the header again, but only the filename
	# This will force us to run the imcombine task inside the directory where the masks are ...
	
	iraf.imutil.hedit(images=flatpath, fields="OBJMASK", value=maskfilename, add="yes", show="yes", verify="no", update="yes")
	
	
	


