execfile("../global.py")


masterflatdb = auxfcts.readpickle(os.path.join(dbdir, "MF_" + masterflatname + ".pkl"))


print "masterflatname : %s" % masterflatname
print "%i masterflats" % len(masterflatdb)

print "Histogram of masterflat ncombi :"


l = sorted([len(night["flats"]) for night in masterflatdb])
h = ["%4i times %3i flats per masterflat" % (l.count(c), c) for c in set(l)]
for l in h:
	print l


noflatnigths = [night for night in masterflatdb if len(night["flats"]) == 0]
print "Nights without flats :"
for night in noflatnigths:
	print night["nightdatetimestr"]





