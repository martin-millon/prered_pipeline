execfile("../global.py")

import matplotlib.pyplot as plt
import matplotlib.dates as mpldates

"""
toplot =[
{"dbname":"flatRG", "colour":"blue", "marker":"+"},
{"dbname":"bias", "colour":"red", "marker":"."},
{"dbname":"HE0435-1223", "colour":"green", "marker":"."}
]
"""
"""
toplot =[
{"dbname":"dark", "colour":"blue", "marker":","},
{"dbname":"bias", "colour":"red", "marker":","},
{"dbname":"joined", "colour":"green", "marker":","},
]
"""
toplot =[
{"dbname":targetname, "colour":"blue", "marker":","}
]


for item in toplot:
	db = auxfcts.readpickle(os.path.join(dbdir, item["dbname"] + ".pkl"))
	dates = np.array([image["datetime"] for image in db])
	noyeardates = np.array([image["datetime"].replace(2000) for image in db])
	# yes, 2000 was a leap year.


	item["dates"] = dates
	item["noyeardates"] = noyeardates




fig = plt.figure()
ax = fig.add_subplot(111)

for item in toplot:
	ax.plot(item["dates"], item["noyeardates"], marker = item["marker"], color = item["colour"], linestyle="None")


# format the ticks
years    = mpldates.YearLocator()   # every year
months   = mpldates.MonthLocator()  # every month
days   = mpldates.DayLocator()  # every day
yearsFmt = mpldates.DateFormatter('%Y')
monthsFmt = mpldates.DateFormatter('%b') # Jan Feb ...

ax.xaxis.set_major_locator(years)
ax.xaxis.set_major_formatter(yearsFmt)
ax.xaxis.set_minor_locator(months)

ax.yaxis.set_major_locator(months)
ax.yaxis.set_minor_locator(days)
ax.yaxis.set_major_formatter(monthsFmt)


ymin = datetime.date(1999, 12, 31)
ymax = datetime.date(2000, 12, 31)
ax.set_ylim(ymin, ymax)


# format the coords message box
ax.format_xdata = mpldates.DateFormatter('%Y-%m')
ax.format_ydata = mpldates.DateFormatter('%m-%d')
ax.grid(True)

plt.xlabel("Year")
plt.ylabel("Month")

plt.show()

