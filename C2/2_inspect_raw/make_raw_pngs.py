#
#	We make png files from the raw bias frames
#

execfile("../global.py")
import f2n

#######

dbname = "bias"
pngname = "bias"

#dbname = "flatRG"
#pngname = "flatRG"

crop = False

#######


db = auxfcts.readpickle(os.path.join(dbdir, dbname + ".pkl"))
db = sorted(db, key=lambda item: item["datetime"])

#db = db[100:105]

print "%i files in db." % len(db)
print "redofromscratch : %s" % redofromscratch
print "dbname : %s" % dbname
print "pngname : %s" % pngname
auxfcts.proquest(askquestions)


if os.path.isdir(os.path.join(pngdir, pngname)):
	print "The pngname %s exists. I will complete it." % pngname
	print os.path.join(pngdir, pngname)
	auxfcts.proquest(askquestions)
else:	
	os.mkdir(os.path.join(pngdir, pngname))



keywords = ["NAXIS1", "NAXIS2", "format", "filter", "EXPTIME", "OBJECT", "CCD_GAIN", "CCD_TYPE"]


for i, image in enumerate(db):
	
	print "=== %4i / %4i : %s ===" % (i+1, len(db), image["filename"])
	
	pngpath = os.path.join(pngdir, pngname, image["basename"] + ".png")
	pngpathlink = os.path.join(pngdir, pngname, "%05i.png" % (i+1))
	
	if redofromscratch:
		if os.path.isfile(pngpath):
			print "File exists, I delete it."
			os.remove(pngpath)
		if os.path.islink(pngpathlink):
			os.unlink(pngpathlink)
		
	else:
		if os.path.isfile(pngpath):
			print "File exists, I skip it."
			continue
		
		
	
	

	f2nimage = f2n.fromfits(image["filepath"], hdu=0, verbose=True)
	
	f2nimage.setzscale("flat", "flat")
	if crop:
		f2nimage.crop(0, 2080, 0, 2040)
	f2nimage.rebin(3)
	f2nimage.makepilimage(scale="lin", negative=False)
	#f2nimage.showcutoffs(redblue = True)
	f2nimage.writetitle(image["filename"])
	
	infolist = ["", "", image["datetime"].strftime("  %Y-%m-%d   %H:%M")]
	infolist.extend(["%12s : %s" % (key, image[key]) for key in keywords])
	infolist.append("%12s : %5i %5i (span = %i) [ADU]" % ("Cuts", f2nimage.z1, f2nimage.z2, f2nimage.z2 - f2nimage.z1))
	#infolist.append(image["datetime"].strftime("%Y-%m-%d %H:%M"))
	
	f2nimage.writeinfo(infolist)
	
	pngpath = os.path.join(pngdir, pngname, image["basename"] + ".png")
	pngpathlink = os.path.join(pngdir, pngname, "%05i.png" % (i+1))
	
	f2nimage.tonet(pngpath)
	os.symlink(pngpath, pngpathlink)

