"""
"Skips" the dates, and plots the acutal times in UTC when observations where done.
Useful to decide how to cut observing nights (attribute_formats.py), and to check for errors in the way headers are read.

"""


execfile("../global.py")

import matplotlib.pyplot as plt
import matplotlib.dates as mpldates


# You can make the plot for all objects mixed :

targetdb = []
for targetname in alltargetnames:
	thisdb = auxfcts.readpickle(os.path.join(dbdir, targetname + ".pkl"))
	targetdb.extend(thisdb)

targetdates = np.array([image["datetime"] for image in targetdb])
targetonlytimes = np.array([image["datetime"].replace(2000, 1, 1) for image in targetdb])

flatdb = auxfcts.readpickle(os.path.join(dbdir, "flatRG.pkl"))
flatdates = np.array([image["datetime"] for image in flatdb])
flatonlytimes = np.array([image["datetime"].replace(2000, 1, 1) for image in flatdb])


fig = plt.figure()
ax = fig.add_subplot(111)

ax.plot(targetdates, targetonlytimes, marker = ".", color = "red", linestyle="None")
ax.plot(flatdates, flatonlytimes, marker = ".", color = "blue", linestyle="None")


# format the ticks
years    = mpldates.YearLocator()   # every year
months   = mpldates.MonthLocator()  # every month
days   = mpldates.DayLocator()  # every day
yearsFmt = mpldates.DateFormatter('%Y')
monthsFmt = mpldates.DateFormatter('%b') # Jan Feb ...

ax.xaxis.set_major_locator(years)
ax.xaxis.set_major_formatter(yearsFmt)
ax.xaxis.set_minor_locator(months)

#ax.yaxis.set_major_locator(months)
#ax.yaxis.set_minor_locator(days)
#ax.yaxis.set_major_formatter(monthsFmt)


#ymin = datetime.date(1999, 12, 31)
#ymax = datetime.date(2000, 12, 31)
#ax.set_ylim(ymin, ymax)


# format the coords message box
ax.format_xdata = mpldates.DateFormatter('%Y-%m')
#ax.format_ydata = mpldates.DateFormatter('%m-%d')
ax.grid(True)

#plt.xlabel("Year")
#plt.ylabel("Month")

plt.show()
