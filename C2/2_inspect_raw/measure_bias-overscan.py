"""
We measure the level at two locations on bias images
"""



execfile("../global.py")
dbname = "bias"

overscanregions={	# format is xmin xmax, ymin, ymax
"1":(2070, 2085, 5, 2040),
"2":(2075, 2135, 5, 2040),
"3":(2069, 2084, 5, 2040),
"4":(10,16,5,2040),			# This is a prescan, as no overscan is available.
"5":(2100, 2200, 5, 2040),
"6":(2080, 2140, 5, 2040),
}

cleanregion = (500, 1500, 500, 1500)

dbpath = os.path.join(dbdir, dbname + ".pkl")

db = auxfcts.readpickle(dbpath)
db = sorted(db, key=lambda item: item["datetime"])


print "%i files in db" % len(db)

if not redofromscratch:
	treatdb = [img for img in db if "medoverscan" not in img.keys()]
	
	print "I will only treat %i not-yet-done files" % len(treatdb)

else:
	print "I will treat all of them."
auxfcts.proquest(askquestions)



	
for i, image in enumerate(db):

	if not redofromscratch:
		if "medoverscan" in image.keys():
			continue

	print "=== %4i/%4i : format %i : %s ===" % (i+1, len(db), image["format"], image["filename"])

	infilepath = image["filepath"]
	(a, h) = auxfcts.fromfits(infilepath, hdu = 0, verbose = False)
	
	
	#print overscanregions[str(image["format"])]
	exec "overscan = a[%i:%i,%i:%i]" % overscanregions[str(image["format"])]
	image["medoverscan"] = np.median(overscan.ravel())
	image["stdoverscan"] = np.std(overscan.ravel())
	image["meanoverscan"] = np.mean(overscan.ravel())
		
	exec "clean = a[%i:%i,%i:%i]" % cleanregion
	image["medclean"] = np.median(clean.ravel())
	image["stdclean"] = np.std(clean.ravel())
	image["meanclean"] = np.mean(clean.ravel())
	
	print image["medoverscan"], image["medclean"]
	
	
outdbpath = dbpath
if os.path.isfile(outdbpath):
	auxfcts.backupfile(outdbpath, backupdir, "bias-overscan")
auxfcts.writepickle(db, outdbpath, verbose=True)
	
		
		
	
	
