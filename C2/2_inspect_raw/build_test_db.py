# We read the joined db, and write only a small subset of it in a test db.


execfile("../global.py")

testimagetable = auxfcts.readimagelist(os.path.join(configdir, "testlist.txt"))
testimagelist = [image["imgname"] for image in testimagetable]

auxfcts.proquest(askquestions)
joineddb = auxfcts.readpickle(os.path.join(dbdir, "joined.pkl"))

testdb = [image for image in joineddb if image["filename"] in testimagelist]


print "We have %i test images" % len(testdb)

auxfcts.writepickle(testdb, os.path.join(dbdir, "test.pkl"))
