

execfile("../global.py")
import matplotlib.pyplot as plt
import matplotlib.dates as mpldates

db = auxfcts.readpickle(os.path.join(dbdir, "bias.pkl"))

db = sorted(db, key=lambda item: item["datetime"])

#db = [image for image in db if image["format"] == 2]

#for image in db:
#	if image["medclean"] > 12000:
#		print image["filename"], image["datetime"]


dates = np.array([image["datetime"] for image in db])
medoverscans = np.array([image["medoverscan"] for image in db])
medcleans = np.array([image["medclean"] for image in db])
meanoverscans = np.array([image["meanoverscan"] for image in db])
meancleans = np.array([image["meanclean"] for image in db])
stdcleans = np.array([image["stdclean"] for image in db])
stdoverscans = np.array([image["stdoverscan"] for image in db])


fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(dates, medcleans - medoverscans, marker = ",", linestyle="None", color="blue")
ax.plot(dates, meancleans - meanoverscans, marker = ",", linestyle="None", color="red")

#ax.plot(dates, stdcleans, marker = ".", linestyle="None", color="blue")
#ax.plot(dates, stdoverscans, marker = ".", linestyle="None", color="green")
	

# format the ticks
years    = mpldates.YearLocator()   # every year
months   = mpldates.MonthLocator()  # every month
days   = mpldates.DayLocator()  # every day
yearsFmt = mpldates.DateFormatter('%Y')
monthsFmt = mpldates.DateFormatter('%b') # Jan Feb ...

ax.xaxis.set_major_locator(years)
ax.xaxis.set_major_formatter(yearsFmt)
ax.xaxis.set_minor_locator(months)

#ax.yaxis.set_major_locator(months)
#ax.yaxis.set_minor_locator(days)
#ax.yaxis.set_major_formatter(monthsFmt)


#ymin = datetime.date(1999, 12, 31)
#ymax = datetime.date(2000, 12, 31)
#ax.set_ylim(ymin, ymax)


# format the coords message box
#ax.format_xdata = mpldates.DateFormatter('%Y-%m')
#ax.format_ydata = mpldates.DateFormatter('%m-%d')
ax.grid(True)

#plt.xlabel("Year")
#plt.ylabel("Month")

#plt.legend()
plt.show()
	


