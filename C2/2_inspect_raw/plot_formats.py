execfile("../global.py")

import matplotlib.pyplot as plt
import matplotlib.dates as mpldates


dbnames = ["flatRG", "HE0047-1756", "J1226-0006", "RXJ1131-123", "HE0435-1223", "bias"]
#dbnames = ["bias"]
#dbnames = alltargetnames + ["flatRG"]
#dbnames = ["flatRG"]




fig = plt.figure()
ax = fig.add_subplot(111)


for dbname in dbnames :
	
	db = auxfcts.readpickle(os.path.join(dbdir, dbname + ".pkl"))
	dates = np.array([image["datetime"] for image in db])
	formats = np.array([image["format"] for image in db])
	ax.plot(dates, formats, marker = ".", linestyle="None", label=dbname)
	

# format the ticks
years    = mpldates.YearLocator()   # every year
months   = mpldates.MonthLocator()  # every month
days   = mpldates.DayLocator()  # every day
yearsFmt = mpldates.DateFormatter('%Y')
monthsFmt = mpldates.DateFormatter('%b') # Jan Feb ...

ax.xaxis.set_major_locator(years)
ax.xaxis.set_major_formatter(yearsFmt)
ax.xaxis.set_minor_locator(months)

#ax.yaxis.set_major_locator(months)
#ax.yaxis.set_minor_locator(days)
#ax.yaxis.set_major_formatter(monthsFmt)


#ymin = datetime.date(1999, 12, 31)
#ymax = datetime.date(2000, 12, 31)
#ax.set_ylim(ymin, ymax)


# format the coords message box
#ax.format_xdata = mpldates.DateFormatter('%Y-%m')
#ax.format_ydata = mpldates.DateFormatter('%m-%d')
ax.grid(True)

#plt.xlabel("Year")
#plt.ylabel("Month")

plt.legend()
plt.show()
	
