import sys
import os
import shutil
import pyfits
import pickle
import glob
import datetime
import numpy as np


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#		GENERAL CONFIGURATION
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# The scripts and configuration :
scriptsdir = "/archive/vbonvin/disk1/PRERED/C2"

# The producs (will get large !) :
#productdir = "/Users/mtewes/Desktop/products/"
productdir = "/obs/lenses_EPFL/PRERED/C2"

# There is no directory for the raw input images, as those might be spread.
# We take this info from the databases.

askquestions = False

# The mecanism used for bias subtraction, png + measures :
redofromscratch = False 


targetname = "J0158-4325"

# flatRG

alltargetnames = [
"HE0047-1756",
"HE0230-2130",
"HE0435-1223",
"HE2149-274",
"HS0818+1227",
"HS2209+1914",
"J0158-4325",
"J0246-0825",
"J0924+0219",
"J1226-0006",
"J1332+0347",
"J1335+0118",
"LBQS1429-008",
"Q1120+0195",
"Q1355-2257",
"Q2237+030",
"RXJ1131-123",
"UM673",
"WFI2026-4536",
"WFI2033-4723",
"J0832+0404",
"J1138+03",
"J1322+1052",
"J1349+1227",
"J1455+1447",
"J1620+1203",
"J2343-0050"
]

alldbnames = alltargetnames + ["flatRG", "bias", "dark"]

# This is used only from 4_build_masterflats on

flatredofromscratch = False
flatname = "flatRG"
flatskiplistfilename = "skiplist_flatRG.txt"
flatcutlistfilename = "cutlist_flatRG.txt"
allsciexpdbname = "joined"
#allsciexpdbname = "test"
filtername = "RG"
minflats = 10
maxsearch = 30

masterflatname = "RGjoined12" # choose it to reflect the filter...
#masterflatname = "RGtest1" # choose it to reflect the filter..

# The mecanism for masterflat construction and flatfielding
# This is always compared to the nightdatetime. Be safe, redo a few nights.
#processfromhere = datetime.datetime(2005, 5, 29)
#processfromhere = datetime.datetime(2009, 12, 24)
#processfromhere = datetime.datetime(2010, 4, 1)
processfromhere = datetime.datetime(2000, 1, 1)





superflatname = masterflatname # don't change this

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


sys.path.append(os.path.join(scriptsdir, "modules"))
import auxfcts
configdir = os.path.join(scriptsdir, "config")
dbdir = os.path.join(scriptsdir, "dbs")
backupdir = os.path.join(scriptsdir, "backups")
pngdir = os.path.join(productdir, "pngs")
biassubdir = os.path.join(productdir, "biassub")
masterflatdir = os.path.join(productdir, "MF_" + masterflatname)
superflatdir = os.path.join(productdir, "SF_" + superflatname)
reducdir = os.path.join(productdir, "reduc")
#reducdir = "/obs/EPFL/tewes/pypr_reduc/"

if not os.path.isdir(configdir):
	raise RuntimeError("Cannot find configdir : %s" % configdir)

if not os.path.isdir(dbdir):
	raise RuntimeError("Cannot find dbdir : %s" % dbdir)

if not os.path.isdir(backupdir):
	raise RuntimeError("Cannot find backupdir : %s" % backupdir)

if not os.path.isdir(productdir):
	raise RuntimeError("Cannot find productdir : %s" % productdir)

if not os.path.isdir(pngdir):
	os.mkdir(pngdir)

if not os.path.isdir(biassubdir):
	os.mkdir(biassubdir)

if not os.path.isdir(reducdir):
	os.mkdir(reducdir)



#print "MMMMMEEEEEEGGGGGGAAAAAA WARNING !!!!!!!!!"
#biassubdir = os.path.join(productdir, "wrongbiassub")
