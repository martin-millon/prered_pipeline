

execfile("../global.py")
import matplotlib.pyplot as plt
import matplotlib.dates as mpldates

#db = auxfcts.readpickle(os.path.join(dbdir, targetname + ".pkl"))

db = auxfcts.readpickle(os.path.join(dbdir, "flatRG.pkl"))

biasdb = auxfcts.readpickle(os.path.join(dbdir, "bias.pkl"))

#db = [image for image in db if image["format"] == 2]
#for image in db:
#	if image["medclean"] > 12000:
#		print image["filename"], image["datetime"]

formats = np.array([image["format"] for image in db])


biasdates = np.array([image["datetime"] for image in biasdb])
medoverscans = np.array([image["medoverscan"] for image in biasdb])
medcleans = np.array([image["medclean"] for image in biasdb])
meanoverscans = np.array([image["meanoverscan"] for image in biasdb])
meancleans = np.array([image["meanclean"] for image in biasdb])
stdcleans = np.array([image["stdclean"] for image in biasdb])
stdoverscans = np.array([image["stdoverscan"] for image in biasdb])



fig = plt.figure()
ax = fig.add_subplot(111)

ax.plot(biasdates, medcleans, marker = ",", linestyle="None", color="grey")
	

formatcolors = {"1":"black", "2":"green", "3":"purple", "4":"brown", "5":"blue", "6":"red"}

for format in set(formats):
	print format
	dates = np.array([image["datetime"] for image in db if image["format"] == format])
	biaslevels = np.array([image["biaslevel"] for image in db if image["format"] == format])
	overscanlevels = np.array([image["overscanlevel"] for image in db if image["format"] == format])
	overscanstds = np.array([image["overscanstd"] for image in db if image["format"] == format])
	overscanmeans = np.array([image["overscanmean"] for image in db if image["format"] == format])
	
	ax.plot(dates, biaslevels, marker = ".", linestyle="None", color=formatcolors[str(format)])
	

"""
# format the ticks
years    = mpldates.YearLocator()   # every year
months   = mpldates.MonthLocator()  # every month
days   = mpldates.DayLocator()  # every day
yearsFmt = mpldates.DateFormatter('%Y')
monthsFmt = mpldates.DateFormatter('%b') # Jan Feb ...

#ax.xaxis.set_major_locator(years)
#ax.xaxis.set_major_formatter(yearsFmt)
#ax.xaxis.set_minor_locator(months)

#ax.yaxis.set_major_locator(months)
#ax.yaxis.set_minor_locator(days)
#ax.yaxis.set_major_formatter(monthsFmt)


#ymin = datetime.date(1999, 12, 31)
#ymax = datetime.date(2000, 12, 31)
#ax.set_ylim(ymin, ymax)

"""
# format the coords message box
#ax.format_xdata = mpldates.DateFormatter('%Y-%m')
#ax.format_ydata = mpldates.DateFormatter('%m-%d')
#ax.grid(False)

#ax.set_ylim(-200, 2000)

plt.xlabel("Date")
plt.ylabel("Bias estimated from overscan")

plt.title(targetname)
#plt.legend()
plt.show()
	


