execfile("../global.py")

##########

dbnames = alldbnames
#dbnames = ["bias", "flatRG"]
#dbnames = ["dark"]

askquestions = False

##########


print "Bias subtraction"
print "dbnames : %s " % dbnames

print "This could be massive..."
print "From here on askquestions is set to %s" % askquestions

auxfcts.proquest(True)

for dbname in dbnames:

 print "################### %s ###############" % dbname


 dbpath = os.path.join(dbdir, dbname + ".pkl")
 db = auxfcts.readpickle(dbpath)
 db = sorted(db, key=lambda item: item["datetime"])

 print "%i files in db" % len(db)

 if not redofromscratch:
	 treatdb = [img for img in db if "biaslevel" not in img.keys()]
	 print "I will only treat %i not-yet-done files" % len(treatdb)

 else:
	 print "I will treat all of them."
 auxfcts.proquest(askquestions)

 destdir = os.path.join(biassubdir, dbname)

 if os.path.isdir(destdir):
	 if not redofromscratch:
		 print "I will add my images in the directory :"
		 print destdir
		 auxfcts.proquest(askquestions)

	 else:
		 print "Some stuff already exists ! I will DELETE it !"
		 print destdir
		 auxfcts.proquest(askquestions)
		 shutil.rmtree(destdir)
		 os.mkdir(destdir)
 else : 
	 os.mkdir(destdir)



 overscanregions={	# format is xmin xmax, ymin, ymax
 "1":(2070, 2085, 5, 2040),
 "2":(2075, 2135, 5, 2040),
 "3":(2069, 2084, 5, 2040),
 "4":(10,16,5,2040),			# This is a prescan, as no overscan is available.
 "5":(2075, 2224, 0, 2047),	# Special, we do a median filtering along the lines of the overscan
 "6":(2080, 2140, 5, 2040),
 }



 for i, image in enumerate(db):
	 if not redofromscratch:
		 if "biaslevel" in image.keys():
			 continue

	 print "=== %4i/%4i : format %i : %s ===" % (i+1, len(db), image["format"], image["filename"])

	 infilepath = image["filepath"]
	 (a, h) = auxfcts.fromfits(infilepath, hdu = 0, verbose = True)

	 format = image["format"]


	 # Now we subtract the bias as estimated form the overscan.
	 # each if block defines :
	 # bsa, biaslevel, overscanlevel, "overscanmean, overscanstd
	 # bsa is the bias subtracted array


	 if format == 1: # only few bias images, but (median) bias - overscan seems to be around 2.5
		 exec "overscan = a[%i:%i,%i:%i]" % overscanregions[str(format)]
		 overscanlevel = np.median(overscan.ravel())
		 biaslevel = overscanlevel + 2.5
		 overscanmean = np.mean(overscan.ravel())
		 overscanstd = np.std(overscan.ravel())
		 bsa = a - biaslevel 

	 if format == 2: # Many images, median bias - overscan at 1.8
		 exec "overscan = a[%i:%i,%i:%i]" % overscanregions[str(format)]
		 overscanlevel = np.median(overscan.ravel())
		 biaslevel = overscanlevel + 1.8
		 overscanmean = np.mean(overscan.ravel())
		 overscanstd = np.std(overscan.ravel())
		 bsa = a - biaslevel 

	 if format == 3: # no bias frames, let's assume bias = overscan...
		 exec "overscan = a[%i:%i,%i:%i]" % overscanregions[str(format)]
		 overscanlevel = np.median(overscan.ravel())
		 biaslevel = overscanlevel
		 overscanmean = np.mean(overscan.ravel())
		 overscanstd = np.std(overscan.ravel())
		 bsa = a - biaslevel 

	 if format == 4: # dec 2004 - jan 2005. no overscan, but some bias frames of format 2 ..."prouesse from Geneva"
		 # 5 of these biases have levels above 12000, so something is strange here.
		 # The science exposures of this time look fine (not at 12000)
		 # I will use the prescan, it looks fine. Good bias exposures of format 2 also look fine. I add 12.0 to the prescan
		 exec "overscan = a[%i:%i,%i:%i]" % overscanregions[str(format)]
		 overscanlevel = np.median(overscan.ravel())
		 biaslevel = overscanlevel + 12.0
		 overscanmean = np.mean(overscan.ravel())
		 overscanstd = np.std(overscan.ravel())
		 bsa = a - biaslevel 

	 if format == 5: # Many images, new controler, bias look flat.
		 # Clearly, bias - overscan is around 5.5 and pretty stable.
		 # Here we have a large overscan region and stable conditions, so we do a more sophisticated bias subtraction.
		 # median along the lines of the overscan, then subtract these lines.


		 exec "overscan = a[%i:%i,%i:%i]" % overscanregions[str(format)]
		 overscanlevelvec = np.median(overscan)

		 # smoothing ?
		 #kernel = np.array([1,1,1,1,1])/5.0
		 #overscanlevelvec = np.convolve(overscanlevelvec, kernel, mode='same')

		 overscanlevelarray = np.ones(a.shape)

		 #overscanlevelarray = overscanlevelarray * overscanlevelvec
		 # This works, but seems unsafe
		 # So I prefer this :
		 overscanlevelarray = np.apply_along_axis(lambda v: v * overscanlevelvec, 1, overscanlevelarray)


		 biaslevelarray = overscanlevelarray + 5.5

		 # Just to fill out the return values
		 overscanmean = np.mean(overscan.ravel())
		 overscanstd = np.std(overscan.ravel())
		 overscanlevel = np.median(overscan.ravel())
		 biaslevel = overscanlevel + 5.5

		 bsa = a - biaslevelarray


	 if format == 6: # We do the same as for format 5, with a smaller overscan region.
		 # Again, bias-overscan is quite clean at 5.5
		 exec "overscan = a[%i:%i,%i:%i]" % overscanregions[str(format)]
		 overscanlevel = np.median(overscan.ravel())
		 biaslevel = overscanlevel + 5.5
		 overscanmean = np.mean(overscan.ravel())
		 overscanstd = np.std(overscan.ravel())
		 bsa = a - biaslevel	


	 print "Bias level : %8.2f" % biaslevel

	 # We get back to 32 bit arrays :
	 bsa = bsa.astype(np.float32)



	 # Now we crop the image, cutting away pre and overscan

	 if format == 1:
		 cropa = bsa[25:2045,5:2040]

	 if format == 2:
		 cropa = bsa[25:2045,5:2040]

	 if format == 3:
		 cropa = bsa[24:2044,5:2040]

	 if format == 4:
		 cropa = bsa[25:2045,5:2040]

	 if format == 5:
		 cropa = bsa[28:2048,5:2040]

	 if format == 6:
		 cropa = bsa[28:2048,5:2040]

	 if cropa.shape != (2020, 2035):
		 print "wrong cropa shape =", cropa.shape
		 sys.exit()	

	 # If you want to test the bias subtraction only ...
	 #cropa = bsa

	 # We add some infos to the header
	 h.update('PR_FORMA', format, "pypr image format")
	 h.update('PR_BIASL', biaslevel, "pypr subtracted bias level in ADU")
	 h.update('PR_OVERL', overscanlevel, "pypr median overscan in ADU")
	 h.update('PR_OVERM', overscanmean, "pypr mean overscan in ADU")
	 h.update('PR_OVERS', overscanstd, "pypr stddev overscan in ADU")

	 # We write the fits file
	 outfilepath = os.path.join(destdir, image["filename"])
	 auxfcts.tofits(outfilepath, cropa, hdr = h, verbose = True)

	 # And we also add this info to the database

	 image["biaslevel"] = biaslevel
	 image["overscanlevel"] = overscanlevel
	 image["overscanmean"] = overscanmean
	 image["overscanstd"] = overscanstd


 # We overwrite the pickle with the new one.
 outdbpath = dbpath
 if os.path.isfile(outdbpath):
	 auxfcts.backupfile(outdbpath, backupdir, "biassub")
 auxfcts.writepickle(db, outdbpath, verbose=True)


