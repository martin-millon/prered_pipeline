

execfile("../global.py")
import matplotlib.pyplot as plt
import matplotlib.dates as mpldates

db = auxfcts.readpickle(os.path.join(dbdir, "bias.pkl"))


dates = np.array([image["datetime"] for image in db])
#exptimes = np.array([image["EXPTIME"] for image in db])
#meancleans = np.array([image["meanclean"] for image in db])
#stdcleans = np.array([image["stdclean"] for image in db])
bsmedcleans = np.array([image["bsmedclean"] for image in db])
bsmeancleans = np.array([image["bsmeanclean"] for image in db])



fig = plt.figure()
ax = fig.add_subplot(111)

ax.plot(dates, bsmedcleans, marker = ",", linestyle="None", color="blue")
ax.plot(dates, bsmeancleans, marker = ",", linestyle="None", color="red")
	


"""
# format the ticks
years    = mpldates.YearLocator()   # every year
months   = mpldates.MonthLocator()  # every month
days   = mpldates.DayLocator()  # every day
yearsFmt = mpldates.DateFormatter('%Y')
monthsFmt = mpldates.DateFormatter('%b') # Jan Feb ...

#ax.xaxis.set_major_locator(years)
#ax.xaxis.set_major_formatter(yearsFmt)
#ax.xaxis.set_minor_locator(months)

#ax.yaxis.set_major_locator(months)
#ax.yaxis.set_minor_locator(days)
#ax.yaxis.set_major_formatter(monthsFmt)


#ymin = datetime.date(1999, 12, 31)
#ymax = datetime.date(2000, 12, 31)
#ax.set_ylim(ymin, ymax)

"""
# format the coords message box
#ax.format_xdata = mpldates.DateFormatter('%Y-%m')
#ax.format_ydata = mpldates.DateFormatter('%m-%d')
#ax.grid(False)

#ax.set_ylim(-200, 2000)

#plt.xlabel("Date")
#plt.ylabel("Bias estimated from overscan")

#plt.title(targetname)
#plt.legend()
plt.show()
	


