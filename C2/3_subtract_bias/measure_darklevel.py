"""
We measure the level of the dark images (post-bias-subtraction)
"""



execfile("../global.py")
dbname = "dark"




cleanregion = (500, 1500, 500, 1500)

dbpath = os.path.join(dbdir, dbname + ".pkl")
db = auxfcts.readpickle(dbpath)
db = sorted(db, key=lambda item: item["datetime"])
bsdir = os.path.join(biassubdir, dbname)


print "%i files in db" % len(db)

if not redofromscratch:
	treatdb = [img for img in db if "medclean" not in img.keys()]
	
	print "I will only treat %i not-yet-done files" % len(treatdb)

else:
	print "I will treat all of them."
auxfcts.proquest(askquestions)



	
for i, image in enumerate(db):

	if not redofromscratch:
		if "medclean" in image.keys():
			continue

	print "=== %4i/%4i : format %i : %s ===" % (i+1, len(db), image["format"], image["filename"])

	infilepath = os.path.join(bsdir, image["filename"])
	(a, h) = auxfcts.fromfits(infilepath, hdu = 0, verbose = False)
		
	exec "clean = a[%i:%i,%i:%i]" % cleanregion
	image["medclean"] = np.median(clean.ravel())
	image["stdclean"] = np.std(clean.ravel())
	image["meanclean"] = np.mean(clean.ravel())


	if image["EXPTIME"] != None:	
		print "Level : %8.2f ADU, exptime %i -> %8.6f ADU/s" % (image["medclean"], image["EXPTIME"], image["medclean"]/image["EXPTIME"])
	



outdbpath = dbpath
if os.path.isfile(outdbpath):
	auxfcts.backupfile(outdbpath, backupdir, "darklevel")
auxfcts.writepickle(db, outdbpath, verbose=True)
		
	
	
