"""
we add a constant on images, to see the effect on light curves

First you run biassub, then you fill out this scirpt, and run it.
Images in the biassubdir are now overwritten, I will put these test images elsewhere...

"""
execfile("../global.py")

dbname = "HE0047-1756"

wrongbiassubdir = os.path.join(productdir, "wrongbiassub")
destdir = os.path.join(wrongbiassubdir, dbname)

if os.path.isdir(destdir):
	print "Some stuff already exists ! I will DELETE it !"
	print destdir
	auxfcts.proquest(askquestions)
	shutil.rmtree(destdir)

os.mkdir(destdir)

biassubdir = os.path.join(productdir, "biassub")
origdir = os.path.join(biassubdir, dbname)


dbpath = os.path.join(dbdir, dbname + ".pkl")
db = auxfcts.readpickle(dbpath)
db = sorted(db, key=lambda item: item["datetime"])

print "%i files in db" % len(db)

for i, image in enumerate(db):
	print "=== %4i/%4i : format %i : %s ===" % (i+1, len(db), image["format"], image["filename"])
	
	inputfilepath = os.path.join(origdir, image["filename"])
	if not os.path.isfile(inputfilepath):
		print "Cannot find input file..."
		sys.exit()
		
	(a, h) = auxfcts.fromfits(inputfilepath, hdu = 0, verbose = True)
	
	h.update('PR_WARNI', "Wrong bias !!!!", "pypr intentionally tweaked the bias !")
	
	
	atweaked = a + 20.0
	
	outfilepath = os.path.join(destdir, image["filename"])
	auxfcts.tofits(outfilepath, atweaked, hdr = h, verbose = True)



