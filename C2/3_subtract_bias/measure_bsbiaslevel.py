"""
We measure the level of the dark images (post-bias-subtraction)
"""



execfile("../global.py")
dbname = "bias"


cleanregion = (500, 1500, 500, 1500)

dbpath = os.path.join(dbdir, dbname + ".pkl")
db = auxfcts.readpickle(dbpath)
db = sorted(db, key=lambda item: item["datetime"])
bsdir = os.path.join(biassubdir, dbname)


print "%i files in db" % len(db)

if not redofromscratch:
	treatdb = [img for img in db if "bsmedclean" not in img.keys()]
	
	print "I will only treat %i not-yet-done files" % len(treatdb)

else:
	print "I will treat all of them."
auxfcts.proquest(askquestions)



	
for i, image in enumerate(db):

	if not redofromscratch:
		if "bsmedclean" in image.keys():
			continue

	print "=== %4i/%4i : format %i : %s ===" % (i+1, len(db), image["format"], image["filename"])

	infilepath = os.path.join(bsdir, image["filename"])
	(a, h) = auxfcts.fromfits(infilepath, hdu = 0, verbose = False)
		
	exec "clean = a[%i:%i,%i:%i]" % cleanregion
	image["bsmedclean"] = np.median(clean.ravel())
	image["bsstdclean"] = np.std(clean.ravel())
	image["bsmeanclean"] = np.mean(clean.ravel())


	print "Level : %8.2f ADU" % (image["bsmedclean"])
	



outdbpath = dbpath
if os.path.isfile(outdbpath):
	auxfcts.backupfile(outdbpath, backupdir, "darklevel")
auxfcts.writepickle(db, outdbpath, verbose=True)
		
	
	
