"""
various auxiliary functions for the prereduction tools

"""

import sys
import os
import pickle
import pyfits
import numpy as np
import datetime


def proquest(askquestions):
	"""
	Asks the user if he wants to proceed. If not, exits python.
	askquestions is a switch, True or False, that allows to skip the 
	questions.
	"""
	import sys
	if askquestions:
		answer = raw_input("Tell me, do you want to go on ? (yes/no) ")
		if answer[:3] != "yes":
			sys.exit("Ok, bye.")
		print ""	# to skip one line after the question.


def fromfits(infilename, hdu = 0, verbose = True):
	"""
	Reads a FITS file and returns a 2D numpy array of the data.
	Use hdu to specify which HDU you want (default = primary = 0)
	"""
	
	pixelarray, hdr = pyfits.getdata(infilename, hdu, header=True)
	pixelarray = np.asarray(pixelarray).transpose()
	
	pixelarrayshape = pixelarray.shape
	if verbose :
		print "FITS import shape : (%i, %i)" % (pixelarrayshape[0], pixelarrayshape[1])
		print "FITS file BITPIX : %s" % (hdr["BITPIX"])
		# Note that this is highly misleading -- as we touched the data, the 16 bit is already transformed to -32.
		print "Internal array type :", pixelarray.dtype.name
	
	return pixelarray, hdr

def tofits(outfilename, pixelarray, hdr = None, verbose = True):
	"""
	Takes a 2D numpy array and write it into a FITS file.
	If you specify a header (pyfits format, as returned by fromfits()) it will be used for the image.
	You can give me boolean numpy arrays, I will convert them into 8 bit integers.
	"""
	pixelarrayshape = pixelarray.shape
	if verbose :
		print "FITS export shape : (%i, %i)" % (pixelarrayshape[0], pixelarrayshape[1])

	if pixelarray.dtype.name == "bool":
		pixelarray = np.cast["uint8"](pixelarray)

	if os.path.isfile(outfilename):
		os.remove(outfilename)
	
	if hdr == None: # then a minimal header will be created 
		hdu = pyfits.PrimaryHDU(pixelarray.transpose())
	else: # this if else is probably not needed but anyway ...
		hdu = pyfits.PrimaryHDU(pixelarray.transpose(), hdr)

	hdu.writeto(outfilename)
	
	if verbose :
		print "Wrote %s" % outfilename
	


def writepickle(obj, filepath, verbose=True):
	"""
	I write your python object into a pickle file at filepath.
	"""
	outputfile = open(filepath, 'wb')
	pickle.dump(obj, outputfile)
	outputfile.close()
	if verbose:
		print "Wrote %s" % filepath


def readpickle(filepath, verbose=True):
	"""
	I read a pickle file and return whatever object it contains.
	"""
	pkl_file = open(filepath, 'rb')
	obj = pickle.load(pkl_file)
	pkl_file.close()
	if verbose:
		print "Read %s" % filepath
	return obj


def readimagelist(txtfile):
	"""
	Reads a textfile with comments like # and possible blank lines
	Actual data lines look like
	name [possible comment]
	"""
	
	if not os.path.isfile(txtfile):
		print "File does not exist :"
		print txtfile
		print "Line format to write : imgname [comment]"
		sys.exit()
	
	myfile = open(txtfile, "r")
	lines = myfile.readlines()
	myfile.close
	table=[]
	for line in lines:
		#print line[0]
		if line[0] == '#' or len(line) < 4:
			continue
		
		if len(line.split()) > 1:
			imgname = line.split()[0]
			comment = line.split(None, 1)[1:][0].rstrip('\n')
		else:
			imgname = line.split()[0]
			comment = "na"
		
		#print "imgname :", imgname
		#print "comment :", comment
		table.append({"imgname":imgname, "comment":comment})
	
	print "I've read", len(table), "images from", txtfile.split("/")[-1]
	return table


def readdatelist(txtfile):
	"""
	Reads a list of dates, converts this into a pyhton list of datetime objects.
	As above, you can use comments by starting a line with #, and leave blank lines.
	
	"""
	if not os.path.isfile(txtfile):
		print "File does not exist :"
		print txtfile
		print "Line format to write : 2010-01-22T10:00:00 [comment]"
		sys.exit()
		
	myfile = open(txtfile, "r")
	lines = myfile.readlines()
	myfile.close
	table=[]
	for line in lines:
		if line[0] == '#' or len(line) < 4:
			continue
		
		if len(line.split()) > 1:
			datetimestring = line.split()[0]
			comment = line.split(None, 1)[1:][0].rstrip('\n')
		else:
			datetimestring = line.split()[0]
			comment = "na"
		
		pythondt = datetime.datetime.strptime(datetimestring, "%Y-%m-%dT%H:%M:%S")
		
		
		table.append({"pythondt":pythondt, "comment":comment})
	
	print "I've read", len(table), "datetimes from", txtfile.split("/")[-1]
	return table
	
	
def backupfile(filepath, backupdir, code=""):
	"""
	This function backups a file into the backupdir
	and changes the filename to contain the date, time,
	and the "code", if provided.
	"""
	import os
	import sys
	import shutil
	import datetime
	import time
	
	now = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")

	filename = os.path.split(filepath)[-1]
	
	if len(code) == 0:
		backupfilename = now + "_" + filename
	else :
		backupfilename = now + "_" + code + "_" + filename

	#cmd = "cp " + filepath + " " + os.path.join(backupdir, filename)
	#print cmd
	#os.system(cmd)
	
	if not os.path.isdir(backupdir):
		raise RuntimeError("Cannot find the backupdir %s" % backupdir)
	
	shutil.copy(filepath, os.path.join(backupdir, backupfilename))
	
	filesize = os.path.getsize(filepath)
	
	print "Backup of %s : %s (%.1f kB)" % (filename, backupfilename, filesize/1024.0)

