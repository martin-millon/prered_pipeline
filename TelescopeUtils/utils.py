import astropy.io.fits as pyfits
from astropy.time import Time
from . import f2n
import os
import numpy as np
import math
import datetime
import ephem

def remove_bad_margins(image, num_pixels=20):
    """remove num_pixels from each side of the image"""
    return image[num_pixels:-num_pixels, num_pixels:-num_pixels]


def check_flat(data, header=None, min_level=5000, max_level = 60000):
    mean_level = np.median(data)
    if mean_level > max_level or mean_level < min_level:
        return False

    return True


def check_bias(data):
    # TODO : write this
    pass


def get_all_dates(start="2018-01-01", stop="2018-01-01"):
    '''
    return a list containing all the date from the start date
    :param start: string containing the start date
    :param stop: string containing the stop date
    :return: list of all the date between start and end date.
    '''

    starttime = Time(start, format='iso', scale='utc').mjd
    endtime = Time(stop, format='iso', scale='utc').mjd

    days = np.arange(starttime, endtime + 1, 1)
    dates = [Time(d, format='mjd', scale='utc').iso[:10] for d in days]
    return dates

def make_MF_png(date, masterflat_dir, png_dir, redo=False):
    '''Create nice pngs of the masterflats'''

    destpath = os.path.join(png_dir, "masterflat_%s.png" % date)
    if os.path.isfile(destpath) and redo is True :
        print('Deleting previous png')
        os.remove(destpath)

    masterflatpath = os.path.join(masterflat_dir,date, "masterflat.fits")
    if os.path.isfile(masterflatpath):
        flat_type = 'Flat'
    else :
        print("Masterflat does not exist.")
        return "No masterflats for %s"%date

    h = pyfits.getheader(masterflatpath)
    destpath = os.path.join(png_dir, "masterflat_%s.png" % date)

    if os.path.isfile(destpath) and redo is False:
        print("Masterflat png already exists.")
        return "%s : Masterflat png already exists."%date

    f2nimage = f2n.fromfits(masterflatpath, hdu=0, verbose=True)
    f2nimage.setzscale(0.9, 1.1)
    f2nimage.rebin(8)
    f2nimage.makepilimage(scale="lin", negative=False)
    f2nimage.writetitle("MF " + date)
    if 'NCOMBINE' in h.keys() :
        f2nimage.writeinfo(['Number of combined flats : %i'%(h['NCOMBINE']), flat_type])
    else :
        f2nimage.writeinfo(['Number of combined flats : ? ', flat_type])
    f2nimage.tonet(destpath)
    return "Png done for %s"%masterflatpath

def make_stack_png(file, date, stack_png_dir, redo=False, rebin =2):
    '''Create nice pngs of the stack'''

    if os.path.isfile(file) and redo is False:
        print("Stack png already exists.")
        return "Stack png %s already exists."%file

    basename = os.path.basename(file).split('.')[0]
    h = pyfits.getheader(file)
    destpath = os.path.join(stack_png_dir, basename + ".png")

    f2nimage = f2n.fromfits(file, hdu=0, verbose=True)
    f2nimage.setzscale(-15, 100)
    f2nimage.rebin(rebin)
    f2nimage.makepilimage(scale="lin", negative=False)
    f2nimage.writetitle("Stack " + date)
    line = []
    if 'NCOMBINE' in h.keys() :
        line.append('Number of combined images : %i \n'%(h['NCOMBINE']))
    else :
        line.append('Number of combined images : ? \n')

    if 'SEEING' in h.keys() :
        line.append('Mean seeing : %2.2f \n'%(h['SEEING']))
    else :
        line.append('Mean seeing : ? \n' )

    f2nimage.writeinfo(line)
    f2nimage.tonet(destpath)

    return "Png done for %s"%file

def subtract_sky():
    pass

def airmass(radalt):
    #	We calculate the airmass (radalt is altitude in radians)
    #	Rozenberg's empirical relation :
    #	X = 1 / [sin ho + 0.025 exp(-11 sin ho)]
    #	where ho is the apparent altitude of the object. This formula can be used all down to the horizon (where it gives X = 40).

    if radalt < 0.0:
        return -1.0
    elif radalt > math.pi / 2.0:
        return -2.0
    else:
        return 1.0 / (math.sin(radalt) + 0.025 * math.exp(-11.0 * math.sin(radalt)))

def datetimefromjd(JD):
	"""
	Copy and past from cosmouline.
	Can be of use here to plot lightcurves with nice dates.

	Returns the Gregorian calendar (i.e. our "normal" calendar)
	Based on wikipedia:de and the interweb :-)


	:type JD: float
	:param JD: julian date

	:rtype: datetime object
	:returns: corresponding datetime

	"""

	if JD < 0:
		raise ValueError('Julian Day must be positive')

	dayofwk = int(math.fmod(int(JD + 1.5),7))
	(F, Z) = math.modf(JD + 0.5)
	Z = int(Z)

	if JD < 2299160.5:
		A = Z
	else:
		alpha = int((Z - 1867216.25)/36524.25)
		A = Z + 1 + alpha - int(alpha/4)


	B = A + 1524
	C = int((B - 122.1)/365.25)
	D = int(365.25 * C)
	E = int((B - D)/30.6001)

	day = B - D - int(30.6001 * E) + F
	nday = B-D-123
	if nday <= 305:
		dayofyr = nday+60
	else:
		dayofyr = nday-305
	if E < 14:
		month = E - 1
	else:
		month = E - 13

	if month > 2:
		year = C - 4716
	else:
		year = C - 4715


	# a leap year?
	leap = 0
	if year % 4 == 0:
		leap = 1

	if year % 100 == 0 and year % 400 != 0:
		leap = 0
	if leap and month > 2:
		dayofyr = dayofyr + leap

	# Convert fractions of a day to time
	(dfrac, days) = math.modf(day/1.0)
	(hfrac, hours) = math.modf(dfrac * 24.0)
	(mfrac, minutes) = math.modf(hfrac * 60.0)
	seconds = round(mfrac * 60.0) # seconds are rounded

	if seconds > 59:
		seconds = 0
		minutes = minutes + 1
	if minutes > 59:
		minutes = 0
		hours = hours + 1
	if hours > 23:
		hours = 0
		days = days + 1

	return datetime.datetime(year,month,int(days),int(hours),int(minutes),int(seconds))


def find_object_from_coordinate(RA, DEC, date_obs,xephemlens_list, tolerance=1):
    """

    :param RA: right ascension of the target
    :param DEC: declination ascension of the target
    :param date_obs: string date format %YYY-MM-DD, corresponds to the header keyword 'DATE-OBS'
    :param tolerance: tolerance for the identification in degree
    :return:
    """
    obs = ephem.Observer()
    obs.long = "32:42:05.00"  # VATT longitude
    obs.lat = "-109:53:31.00"  # VATT latitude
    obs.elevation = 3178.0  # VATT altitude
    obs.epoch = ephem.J2000

    # DJD = JD - 2415020 is the Dublin Julian Date used by pyephem.
    djd = float(Time(date_obs, format='isot', scale='utc').jd - 2415020.0)  # jd is a string !
    obs.date = djd

    field = ephem.readdb("unknown,f|Q,%s,%s,19.0,2000" % (RA, DEC))
    field.compute(obs)

    found = False
    for xephemlens in xephemlens_list:
        lens = ephem.readdb(xephemlens)
        lens.compute(obs)

        sep = ephem.separation(field, lens)  # class ephem Angle
        if math.degrees(float(sep)) < tolerance:
            print("Found lens :", lens.name)
            found = True
            name = lens.name
            break

    if found is False:
        print("Unknown system !")
        name = "Unknown"

    return name

def get_best_seeing_index(list) :
    seeing_list = []
    for im in list :
        s = pyfits.getheader(im)['SEEING']
        if s < 0.1 :
            s = 99999
        seeing_list.append(s)
    return np.argmin(seeing_list)