"""
We re-organized the raw data by type, i.e calibration or science image.
"""

#########################################
from __future__ import print_function
import os, sys
from glob import glob
from astropy.io.fits import getheader
from astropy.time import Time
import numpy as np


def main(myrawdir_science, myrawdir_calib, prereddir, logfile,command='cp', log_stream=None, redo=False):

    if redo == False:
        command = command + ' -n'  # no clobber command (do not overwrite)
    else :
        command = command + ' -f' #do not prompt before overwriting

    # if log stream == None : we print in the terminal.
    if log_stream is None:
        outstream = sys.stdout
    else:
        outstream = log_stream

    loglines = ['\n', '------ Restructure data -----', '\ n']
    success = True

    try :
        for myrawdir in [myrawdir_science, myrawdir_calib] :
            fitsfiles = sorted(glob(os.path.join(myrawdir, "*.fits")))
            if len(fitsfiles) != 0:
                print("=" * 15, 'Sorting by date', "=" * 15, file=outstream)
                dates = []
                for ind, fitsfile in enumerate(fitsfiles):

                    basename = os.path.basename(fitsfile)
                    header = getheader(fitsfile)

                    date = str(header["DATE-OBS"])
                    datedir = os.path.join(prereddir, date[:10])
                    dates.append(date[:10])
                    if not os.path.isdir(datedir):
                        os.makedirs(datedir)
                    destpath = os.path.join(datedir, basename)

                    os.system("cp %s %s" % (fitsfile, destpath))
                dates = list(set(dates))
            else:
                dates = os.listdir(prereddir)
            for ind, date in enumerate(dates):
                print("=" * 15, date, "=" * 15, file=outstream)
                datedir = os.path.join(prereddir, date)

                fitsfiles = sorted(glob(os.path.join(datedir, "*.fits")))
                dirs = [os.path.join(datedir, o) for o in os.listdir(datedir) if os.path.isdir(os.path.join(datedir, o))]

                if len(fitsfiles) == 0 and len(dirs) == 0:
                    print("I have %i images for this night" % len(fitsfiles), file=outstream)
                    loglines.append("No images for this night \n")
                #
                elif len(dirs) != 0 and len(fitsfiles) == 0:
                    print("Folder was already restructured", file=outstream)
                    loglines.append("Folder already restructured. \n")
                    for directory in dirs:
                        files = glob(os.path.join(directory, "*.fits"))
                        loglines.append("I have %i images in folder %s \n" % (len(files), directory))
                elif len(dirs) != 0 and len(fitsfiles) != 0:
                    print("Folder was already restructured but I have new files", file=outstream)
                    loglines.append("Folder already restructured but I have new files. \n")
                    for directory in dirs:
                        files = glob(os.path.join(directory, "*.fits"))
                        loglines.append("I have %i images in folder %s \n" % (len(files), directory))
                for fitsfile in fitsfiles:

                    basename = os.path.basename(fitsfile)
                    header = getheader(fitsfile)

                    obstype = str(header["IMAGETYP"])
                    exptime = str(header["EXPTIME"])
                    filter = str(header["STFLTNM"])
                    target = str(header["OBJECT"])

                    if filter == "R_Bes641_148":
                        obsfilter = 'R'
                    # We build a directory name
                    destdir = "unknown"

                    if obstype == "BIAS":
                        destdir = "bias"
                    elif obstype == 'OBJECT' and "+" in target:

                        destdir = "science_%s" % (obsfilter)
                        line = "%s\t%s\t%s\t%s\t%7.2f\n" % ("NOT", target, obsfilter, date, float(exptime))
                        loglines.append(line)
                    else:
                        destdir = "flat_%s" % (obsfilter)
                        line = "%s\t%s\t%s\t%s\t%7.2f\n" % ("NOT", obstype, obsfilter, date, float(exptime))
                        loglines.append(line)

                    destdir = os.path.join(prereddir, date, destdir)
                    if not os.path.isdir(destdir):
                        os.makedirs(destdir)

                    destpath = os.path.join(destdir, basename)

                    os.system("%s %s %s" % (command, fitsfile, destpath))

    except Exception as e :
        print('Error during restructuration : %s '%e, file = outstream)
        loglines.append('Error during restructuration : %s '%e)
        sucess = False

    logtxt = "".join(loglines)
    log_night = open(logfile, "a")
    log_night.write('Exposures taken during the night : \n')
    log_night.write(logtxt)
    log_night.close()

    print("Done with restructuration ! \n", file=outstream)
    print("#"*10, file=outstream)

    return success


if __name__ == '__main__':
    myrawdir_science = "/obs/lenses_EPFL/RAW/NOT/science"
    myrawdir_calib = "/obs/lenses_EPFL/RAW/NOT/calib"

    # myrawdir = ["/home/ericpaic/Documents/PHD/Monitoring/NOT/RAW/target/","/home/ericpaic/Documents/PHD/Monitoring/NOT/RAW/calibrations/"]
    # prereddir = "/home/ericpaic/Documents/PHD/Monitoring/NOT/PRERED/"
    prereddir = "/obs/lenses_EPFL/PRERED/NOT/sorted"

    logfile = "/home/astro/millon/Desktop/PRERED/NOT/pipe_log/log_test.log"

    main(myrawdir_science, myrawdir_calib,prereddir, logfile, command='cp', log_stream=None, redo=True)
