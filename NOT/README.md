## Pipeline for NOT reduction.
This pipeline will querry the ftp server of the NOT, download the data and reduce them. 

The pipeline is composed of 7 steps :

## 0_copy_raw
We copy the images from the NOT server and save a copy locally

## 1_restruct
We sort the images by type and objects : calibration, flats, science, ect...

## 2_biassub
Remove the bias using the masterbias.

## 3_prepare_mask
We build the mask to mask star in the flats. 

## 4_build_masterflats
We group flats from the previous and coming nights. I will search flats between "radius_future" and "radius_past" around the observation date until I have
"n_max_flats". I have find less than "n_min_flats", I will double the searching windows and print a warning message. I will build one masterflat per night and per filter and ampmode. I will reject flats with ADU above 30000 or below 8000 or exptime < 6.5s.

## 5_apply_masterflats
I divide the science images by the masterflats.

    
## 6_cleanup 
Remove all the intermediate product to save disk space
It also create a summary of all the logs in the pipe_log directory. All the prints are saved in the 'print_out' directory in the pipe_log folder. 

## run_all 
You can also run all the scrit at once, by specifying the start and end date with the --start and --end optionnal argument. Date format is YYYY-MM-DD. Without argument the pipeline will run from 3 days ago until today for the 3 first steps. It will prepare the flats until today but construct the masterflats only for the day, 3 days ago. This is meant to wait for the flats taken up to 3 day after the observation date. The reduction is in fact done 3 days after the observation

## execute_all.sh 
This is wrapper in bash to call for run_all.py. It loads all the necessary module on lesta and execute the python scripts. It can be call with crontab for automatic run everyday. To edit the crontab command : 
    
    export EDITOR=nano
    crontab -e 
    

 The image on which the pipeline has failed are written in error_flatfielding.log in the pipe_log directory. We are not using bias callibration, the overscan is doing the job.
 
 ## Outputs : 
  __Reduced files__ : /obs/lenses_EPFL/PRERED/NOT/reduc/
__Raw images__ : /obs/lenses_EPFL/RAW/NOT/science and /obs/lenses_EPFL/RAW/NOT/calib
 __Masterflats pngs__ : /obs/lenses_EPFL/PRERED/ECAM/diagnostics/png_flat_RG_UL/
 
 








