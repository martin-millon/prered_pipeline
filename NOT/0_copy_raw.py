"""
We query the NOT database with ftp protocol
Existing file are overwritten, if redo == True

No problem to launch it again and again over the same directories.
"""

from __future__ import print_function
import sys, os
import glob
from ftplib import FTP
from datetime import date


def mkdir_recursive(path):
    sub_path = os.path.dirname(path)
    if not os.path.exists(sub_path):
        mkdir_recursive(sub_path)
    if not os.path.exists(path):
        os.mkdir(path)


def main(myrawdir_science,myrawdir_calib, userlist, dirlist, calibrationlist, logfile, redo=False, log_stream=None, verbose =False):

    if log_stream is None:
        outstream = sys.stdout
    else:
        outstream = log_stream

    if not os.path.exists(myrawdir_calib):
        mkdir_recursive(myrawdir_calib)
    if not os.path.exists(myrawdir_science):
        mkdir_recursive(myrawdir_science)

    existing_files = glob.glob(os.path.join(myrawdir_calib, '*.fits')) + glob.glob(os.path.join(myrawdir_science, '*.fits'))
    loglines = ['------ Retrieval from the NOT server ----- ', '\ n']
    success = True

    for user,dir,calib in zip(userlist,dirlist, calibrationlist) :
        if calib:
            print('#'*3, ' Retrieving calibration from %s'%dir, '#'*3, file=outstream)
            loglines.append('### Retrieving calib from %s ### \n' % dir)
            myrawdir = myrawdir_calib
        else :
            print('#'*3, ' Retrieving data from program %s'%dir, '#'*3, file=outstream)
            loglines.append('### Retrieving data from program %s ### \n' % dir)
            myrawdir = myrawdir_science

        ftp = FTP('ftp.not.iac.es')
        ftp.login(user = 'anonymous', passwd = user)

        ftp.cwd(dir)

        filenames = ftp.nlst()  # get filenames within the directory

        print('I have %i files in the folder %s' %(len(filenames), dir), file=outstream)
        retrieved = []
        already_existing = []
        failed = []


        for filename in filenames:
            if not "STE" in filename : #this is not a stancam calibration
                continue

            local_filename = os.path.join(myrawdir, filename)
            if local_filename in existing_files :
                if redo :
                    if verbose :
                        print('Files %s already exists ! I will delete it and download it again !' % filename, file=outstream)
                    os.system('rm %s'%local_filename)
                else :
                    if verbose :
                        print('Files %s already exists ! Skipping this one !'%filename, file=outstream)
                    already_existing.append(filename)
                    continue


            try :
                file = open(local_filename, 'wb')
                ftp.retrbinary('RETR ' + filename, file.write)
                file.close()
                retrieved.append(filename)
            except Exception as e :
                print('Error could not retrieve %s : %s'%(filename, e), file=outstream)
                failed.append(filename)
                success = False


        loglines.append('Already existing : %i images\n'%len(already_existing))
        print('Already existing : %i images' % len(already_existing), file=outstream)
        loglines.append('Successfully retrieved : %i images\n'%len(retrieved))
        print('Successfully retrieved : %i images'%len(retrieved), file=outstream)
        loglines.append('Failed : %i images \n'%len(failed))
        print('Failed : %i images'%len(failed), file=outstream)

    logtxt = "".join(loglines)
    log_night = open(logfile, "w")
    log_night.write(logtxt)
    log_night.close()

    return success


if __name__ == '__main__':
    myrawdir_science = "/home/ericpaic/Documents/PHD/Monitoring/NOT/RAW/target"
    myrawdir_calib = "/home/ericpaic/Documents/PHD/Monitoring/NOT/RAW/calibrations"

    # myrawdir_science = "/Users/martin/Desktop/lc_run/data_raw/J1817_NOT/science"
    # myrawdir_calib = "/Users/martin/Desktop/lc_run/data_raw/J1817_NOT/calib"

    logdir = "/home/ericpaic/Documents/PHD/Monitoring/NOT/prered_pipeline/NOT/pipe_log/retrieving_log"
    # logdir = "/Users/martin/Desktop/prered_pipeline/NOT/pipe_log/retrieving_log"

    today = date.today()
    logfile = os.path.join(logdir, 'log_retrieving_%s.log'%today)
    userlist = ['adriano.agnello@nbi.ku.dk', 'frederic.courbin@epfl.ch', 'frederic.courbin@epfl.ch']
    dirlist = ['/pub/service/63/501', '/pub/service/63/804', '/pub/service/calib']

    calibrationlist = [False, False, True] #boolean vector to indicate if the above list are calibration.

    main(myrawdir_science, myrawdir_calib, userlist, dirlist, calibrationlist, logfile, redo=False, log_stream=None)

