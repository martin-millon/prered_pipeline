#!/usr/bin/python3

import os, datetime, sys
from astropy.time import Time
import numpy as np
import argparse as ap
sys.path.append('../VST/')
from emails_func import send_email
copy_raw = __import__('0_copy_raw')
restruct_raw = __import__('1_restruct')
biassub = __import__('2_biassub')
prepare_mask = __import__('3_prepare_mask_flats')
build_flats = __import__('4_build_masterflats')
apply_flats = __import__('5_apply_masterflats')
cleanup= __import__('6_cleanup')

def write_log(logfile, msg, mode='a'):
    log_night = open(logfile, mode)
    log_night.write(msg)
    log_night.close()

def main(dates):
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    #           GENERAL CONFIGURATION
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

    # The scripts and configuration :
    scriptsdir = "/home/astro/millon/Desktop/PRERED/NOT"

    #Raw data :
    myrawdir_science = "/obs/lenses_EPFL/RAW/NOT/science"
    myrawdir_calib = "/obs/lenses_EPFL/RAW/NOT/calib"

    #Define the location of the data to download from the NOT server.
    userlist = ['adriano.agnello@nbi.ku.dk', 'frederic.courbin@epfl.ch', 'frederic.courbin@epfl.ch']
    dirlist = ['/pub/service/63/501', '/pub/service/63/804', '/pub/service/calib']
    calibrationlist = [False, False, True] #boolean vector to indicate if the above list are calibration.

    # The products (will get large !) :
    myrawdirrestruct = "/obs/lenses_EPFL/PRERED/NOT/sorted"
    productsciencedir = "/obs/lenses_EPFL/PRERED/NOT/reduced"
    productdir = "/obs/lenses_EPFL/PRERED/NOT/"

    #The diagnostics :
    diagnostics_dir = "/obs/lenses_EPFL/PRERED/NOT/diagnostics"
    png_dir = os.path.join(diagnostics_dir, "png_flat_RG_UL")

    #The log file :
    now = datetime.datetime.now()
    log_directory = "/home/astro/millon/Desktop/PRERED/NOT/pipe_log/"
    print_log_file = os.path.join(log_directory, 'print_out/printlog_%s.txt' % now.strftime("%Y-%m-%d_%H:%M")) #where to save the prints
    log_file = os.path.join(log_directory, 'log_%s.txt' % now.strftime("%Y-%m-%d_%H:%M")) # will keep the important informations on a log file
    log_file_global = os.path.join(log_directory, 'global_log_NOT.txt') # A global log to keep track of everything
    f = open(print_log_file, 'a')
    f.write('################# \n')
    f.write('Reduction starting on %s: \n'%now.strftime("%Y-%m-%d %H:%M"))

    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    #   Flat setting
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    #parameters for the flat construction, radius in days around the observation date to look for flats
    filtername = ["R"]
    radius_future = 0
    radius_past = 5
    n_min_flats = 6
    n_max_flats = 20

    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    #           Pipeline
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    redo = False # redo already reduced files ?
    verbose = False # Verbosity ?

    print ('Running from %s to %s' % (dates[0], dates[-1]))

    try :
    	# copy the raw image from repository :
    	success_retrieved = copy_raw.main(myrawdir_science,myrawdir_calib, userlist, dirlist, calibrationlist, log_file, redo=redo, log_stream=f, verbose =verbose)

    	if not success_retrieved :
    	    write_log(log_file, 'Error during the retrieval from the NOT server ! Please chek the log %s'%log_file)
    	    raise RuntimeError('Error during the retrieval from the NOT server ! Please chek the log %s'%log_file)

    	#restructure the raw directory by object, calibrations,ect... :
    	success_restruc = restruct_raw.main(myrawdir_science, myrawdir_calib, myrawdirrestruct, log_file, command='cp', log_stream=f, redo=redo)
    	if not success_restruc:
    	    write_log(log_file, 'Error during the restructuration ! Please check the log %s'%log_file)
    	    raise RuntimeError('Error during the restructuration ! Please check the log %s'%log_file)

    	#Subtract the bias from the flats and science images :
    	success_bias = biassub.main(date, myrawdirrestruct, log_file, redo=redo, log_stream=f)
    	if not success_bias:
    	    write_log(log_file, 'Error during bias subtraction ! Please check the log %s'%log_file)
    	    raise RuntimeError('Error during bias subtraction ! Please check the log %s'%log_file)

    	#prepare the flat masks :
    	success_mask = prepare_mask.main(date, myrawdirrestruct, log_file, redo=redo, log_stream=f)
    	if not success_mask:
    	    write_log(log_file, 'Error during mask preparation ! Please check the log %s'%log_file)
    	    raise RuntimeError('Error during mask preparation ! Please check the log %s'%log_file)

    	#stack flats to create the masterflat :
    	success_mfflats = build_flats.main(date, myrawdirrestruct, productdir, filtername, log_file, radius_future=radius_future,
     radius_past=radius_past, n_max_flats=n_max_flats, n_min_flats=n_min_flats, redo=redo, log_stream=f)
    	if not success_mfflats:
    	    write_log(log_file, 'Error during masterflats preparation ! Please check the log %s' % log_file)
    	    raise RuntimeError('Error during masterflats preparation ! Please check the log %s' % log_file)

    	#apply the masterflats to the science images :
    	success_applyflats  = apply_flats.main(date, myrawdirrestruct, productdir, log_file, redo=redo, log_stream=f)
    	if not success_applyflats:
    	    write_log(log_file, 'Error masterflat calibration ! Please check the log %s' % log_file)
    	    raise RuntimeError('Error masterflat calibration ! Please check the log %s' % log_file)

    except Exception :

    	write_log(log_file_global, '%s : FAILURE \n'%date)
    	path_pwd = os.path.join('/home/astro/millon/Desktop/PRERED', 'pwd.txt')
    	message = """From: From reduction COSMOGRAIL <reduction.cosmograil@gmail.com>
    	To: To Eric Paic <eric.paic@epfl.ch>
    	Subject: NOT reduction failure.
    
    	The NOT reduction pipeline has failed on one of those dates %s.
    	""" % dates

            send_email('reduction.cosmograil@gmail.com', 'eric.paic@epfl.ch', path_pwd, message)

        else :
            write_log(log_file_global, '%s : SUCCESS \n'%date)

    f.close()


if __name__ == '__main__':
    parser = ap.ArgumentParser(prog="python {}".format(os.path.basename(__file__)),
                               description="Run the NOT pipeline. No argument will run for the previous nigth.",
                               formatter_class=ap.RawTextHelpFormatter)
    help_startnight = "first night to reduce in the format YYYY-MM-DD"
    help_endnight = "last night to reduce in the format YYYY-MM-DD"

    parser.add_argument('--start', dest='startnight', type=str,
                        metavar='', action='store', default=None,
                        help=help_startnight)
    parser.add_argument('--end', dest='endnight', type=str,
                        metavar='', action='store', default=None,
                        help=help_endnight)

    args = parser.parse_args()
    if args.startnight is None or args.endnight is None:
        now = datetime.datetime.now()
        startnight = now.strftime("%Y-%m-%d")
        starttime = Time(startnight, format='iso', scale='utc').mjd - 3  # we reduce until the three days before, to use the flats taken after observation date.
        endtime = starttime  # only one night

    else:
        args.startnight = args.startnight + " 01:00:00"
        args.endnight = args.endnight + " 01:00:00"
        starttime = Time(args.startnight, format='iso', scale='utc').mjd
        endtime = Time(args.endnight, format='iso', scale='utc').mjd

    days = np.arange(starttime, endtime + 1, 1)
    dates = [Time(d, format='mjd', scale='utc').iso[:10] for d in days]

    main(dates)
