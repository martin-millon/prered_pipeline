"""
We now prepare the flats. This requires to compensate for the shutter, compute the flat levels and mask the stars
"""
from __future__ import print_function
import os
import sys
import glob
from astropy.io.fits import getheader
from astropy.time import Time
import numpy as np
from utils import auxfcts
from photutils.segmentation import make_source_mask

def main(dates, myrawdirrestruct, logfile, redo=False, log_stream=None):
    if log_stream is None:
        outstream = sys.stdout
    else:
        outstream = log_stream

    logline = ['\n', '------ Mask preparation -----', '\ n']
    success = True

    try :
        for ind, date in enumerate(dates):
            print("=" * 15, date, "=" * 15, file=outstream)
            datedir = os.path.join(myrawdirrestruct, date)
            if os.path.isdir(datedir):
                flat_dirs = [os.path.join(datedir, o) for o in os.listdir(datedir) if
                             os.path.isdir(os.path.join(datedir, o)) and 'flat' in os.path.join(datedir, o)]

                for flat_dir in flat_dirs:
                    print("Shutter compensation for %s" % os.path.basename(flat_dir), file=outstream)
                    flats = glob.glob(os.path.join(flat_dir, '*bs.fits'))

                    for i, fl in enumerate(flats):
                        print("=== %4i / %4i : %s ===" % (i + 1, len(flats), fl), file=outstream)

                        flat_basename = os.path.basename(fl).split('.fits')[0]  # get only the image name, without extension
                        h = getheader(fl)
                        (cora, h) = auxfcts.fromfits(fl, hdu=0, verbose=False)

                        # now we measure the flat level

                        if "medclean" in h.keys():
                            print("Flat levels already computed", file=outstream)
                            logline.append("%s has flat levels already computed \n" % fl)
                        else:
                            clean = cora

                            h["HIERARCH medclean"] = np.median(clean.ravel())
                            h["stdclean"] = np.std(clean.ravel())
                            h["meanclean"] = np.mean(clean.ravel())

                            logline.append("%s : adding flat levels to the header \n" % fl)

                        # And we overwrite the bias subtracted flat :
                        auxfcts.tofits(fl, cora, h)

                        # now we make the mask

                        # flatpath = os.path.join(bsdir, image["filename"])
                        maskfilename = "mask_" + flat_basename + ".pl"
                        maskpath = os.path.join(flat_dir, maskfilename)

                        if os.path.isfile(maskpath) and redo is False :
                            print("Mask is already existing for %s, I am not redoing it." % flat_basename, file=outstream)
                            logline.append("Mask is already existing for %s, I am not redoing it. \n" % fl)
                            continue

                        elif os.path.isfile(maskpath) and redo is True :
                            print("Mask is already existing for %s, I will delete it and redo it. " % flat_basename, file=outstream)
                            logline.append("Mask is already existing for %s, I will delete it and redo it. \n" % fl)
                            os.remove(maskpath)

                        mask = make_source_mask(cora, nsigma=5, npixels=5)

                        auxfcts.tofits(maskpath, mask)
                        logline.append("Mask created for %s \n" % fl)
    except Exception as e:
        print('Error mask preparation : %s ' % e, file=outstream)
        logline.append('Error during restructuration : %s ' % e)
        sucess = False

    logtxt = "".join(logline)
    log_night = open(logfile, "a")
    log_night.write(logtxt)
    log_night.close()
    print("Done with mask preparation.", file=outstream)
    print("#" * 10, file=outstream)

    return success


if __name__ == '__main__':
    myrawdirrestruct = "/obs/lenses_EPFL/PRERED/NOT/sorted"
    # myrawdirrestruct = "/home/ericpaic/Documents/PHD/Monitoring/NOT/PRERED/"

    logfile = "/home/astro/millon/Desktop/PRERED/NOT/pipe_log/log_test.log"

    datemin = "2021-03-10"
    datemax = "2021-04-16"
    starttime = Time(datemin, format='iso', scale='utc').mjd
    endtime = Time(datemax, format='iso', scale='utc').mjd

    days = np.arange(starttime, endtime + 1, 1)
    dates = [Time(d, format='mjd', scale='utc').iso[:10] for d in days]

    main(dates, myrawdirrestruct, logfile, redo=True, log_stream=None)

