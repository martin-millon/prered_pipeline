"""
We now build the master flats. This requires to group the flat from the previous nights. You can control how namy flats are stacked to create the master flats by giving 4 parameters :
    radius_future : number of days to look for flat after the present date
    radius_past : number of days to look for flat before the present date
    n_min_flats : minimal number of flats to stack, if this not matched I will double the radius of search until I match this criterion
    n_max_flats : maximal number of flats. I will take the flats the closest to the present date.
"""
from __future__ import print_function
import os
import sys
import glob
from astropy.time import Time
import numpy as np
from utils import auxfcts
from astropy.io.fits import getheader, getdata
from stsci.image.numcombine import num_combine
from utils import f2n

def make_MF_png(date, masterflat_dir, png_dir, redo=False):
    '''Create nice pngs of the masterflats and return '''

    destpath = os.path.join(png_dir, "masterflat_%s.png" % date)
    if os.path.isfile(destpath) and redo is True :
        print('Deleting previous png')
        os.remove(destpath)
    masterflatpath = os.path.join(masterflat_dir, "MF_%s.fits" % date)
    if os.path.isfile(masterflatpath):


        h = getheader(masterflatpath)
        destpath = os.path.join(png_dir, "masterflat_%s.png" % date)

        if os.path.isfile(destpath) and redo is False:
            print("Masterflat png already exists.")
            return "%s : Masterflat png already exists."%date

        f2nimage = f2n.fromfits(masterflatpath, hdu=0, verbose=True)
        f2nimage.setzscale(0.9, 1.1)
        f2nimage.rebin(2)
        f2nimage.makepilimage(scale="lin", negative=False)
        f2nimage.writetitle("MF " + date)
        if 'NCOMBINE' in h.keys() :
            f2nimage.writeinfo(['Number of combined flats : %i'%(h['NCOMBINE'])])
        else :
            f2nimage.writeinfo(['Number of combined flats : ? '])
        f2nimage.tonet(destpath)
        return "Png done for %s"%masterflatpath
    else :
        print("Masterflat does not exist.")
        return "No masterflats for %s"%date

def search_flat(date, rawdir, filtername, radius_future,
                radius_past):
    '''Return all flats in the given radius for the required amp mode and filter.
     I will add the flats from the closest to the reference date to the furthest.'''

    night = {}
    flats = []
    flats_name = []
    masks = []
    masks_name = []

    r_max = max(radius_past, radius_future)
    starttime = Time(date, format='iso', scale='utc').mjd
    for i in range(2 * r_max):
        index = int(i / 2)
        if i == 0:
            continue
        if i % 2 == 0:
            # we look backward
            d = Time(starttime - index, format='mjd', scale='utc').iso[:10]
            if index > radius_past:
                continue
        else:
            # we look forward
            d = Time(starttime + index, format='mjd', scale='utc').iso[:10]
            if index > radius_future:
                continue

        flat_dir = os.path.join(rawdir, d, "flat_%s" % (filtername))
        flat_night = glob.glob(os.path.join(flat_dir, "*bs.fits"))
        for f in flat_night:
            flat_base = os.path.basename(f).split('.fits')[0]
            m = os.path.join(flat_dir, 'mask_' + flat_base + '.pl')
            if not os.path.isfile(m):
                print('mask %s is not there ! Skipping it...'%m)
                continue
            mean_flatlevel = getheader(f)["medclean"]
            exptime = getheader(f)["EXPTIME"]
            data = getdata(f)
            mask = getdata(m)
            if mean_flatlevel < 8000 or mean_flatlevel > 50000 or exptime < 1:
                print("%s : Bad flat level, rejecting." % f)
            else:
                flats_name.append(f)
                flats.append(data)
                masks_name.append(m)
                masks.append(mask)


    night["flats_name"] = flats_name
    night["flats"] = flats
    night["masks_name"] = masks_name
    night["masks"] = masks
    return night


def main(dates, myrawdirrestruct, productdir, filtername, logfile, radius_future=0,
         radius_past=3, n_max_flats=10, n_min_flats=6, redo=False, log_stream=None):

    if log_stream is None:
        outstream = sys.stdout
    else:
        outstream = log_stream

    logline = ['\n', '------ Master flat preparation -----', '\ n']
    success = True

    for ind, date in enumerate(dates):
        print("=" * 15, date, "=" * 15, file=outstream)
        logline.append("==== %s ====="%date)
        datedir = os.path.join(myrawdirrestruct, date)
        if os.path.isdir(datedir):

            for fm in filtername:
                print("=" * 15, fm, "=" * 15, file=outstream)

                masterflatdir = os.path.abspath(os.path.join(productdir, "MF_" + fm + "4"))
                if not os.path.isdir(masterflatdir):
                    print("I'll create the directory %s" % masterflatdir, file=outstream)
                    os.mkdir(masterflatdir)

                previousmasterflatpath = os.path.join(masterflatdir, "MF_%s.fits" % date)
                if os.path.isfile(previousmasterflatpath) and redo is True:
                    # Just to be sure
                    print("%s : masterflat %s already exists, deleting this one." % (date, fm), file=outstream)
                    logline.append("%s : masterflat %s already exists, deleting this one. \n" % (date, fm))
                    os.remove(previousmasterflatpath)

                elif os.path.isfile(previousmasterflatpath) and redo is False:
                    print("%s : masterflat %s already exists, skipping this one." % (date, fm), file=outstream)
                    logline.append("%s : masterflat %s already exists, skipping this one. \n" % (date, fm))
                    continue

                try:
                    night = search_flat(date, myrawdirrestruct, fm, radius_future, radius_past)

                    nbrcombi = len(night["flats_name"])

                    rf = radius_future
                    rp = radius_past
                    while nbrcombi < n_min_flats:
                        # We will extent the radius until we have enough flats.
                        rf += 2
                        rp += 2
                        night = search_flat(date, myrawdirrestruct, fm, rf, rp)
                        nbrcombi = len(night["flats_name"])
                        if (rp >= 60 or rf >= 60) and nbrcombi < n_min_flats:
                            raise RuntimeError(
                                "I did not find %i flats within +%i days and -%i days around %s, there is a problem here ! \n I have only : %s \n" % (
                                    n_min_flats, rf, rp, date, '\n'.join(night["flats_name"] + '\n')))
                        elif nbrcombi > n_min_flats:
                            print(
                                "WARNING : I had to increase the radius of search to +%i -%i days around %s but I found %i flats" % (
                                    rf, rp, date, nbrcombi), file=outstream)
                            logline.append(
                                "WARNING : I had to increase the radius of search to +%i -%i days around %s %s but I found %i flats \n" % (
                                    rf, rp, date, fm, nbrcombi))

                    if nbrcombi > n_max_flats:
                        # we reduce the number of flats we account for :
                        print("I found %i flats, I will reduce it to %i." % (nbrcombi, n_max_flats), file=outstream)
                        logline.append("I found %i flats for %s %s, I will reduce it to %i. \n" % (
                            nbrcombi, date, fm, n_max_flats))
                        night["flats"] = night["flats"][:n_max_flats]
                        night["flats_name"] = night["flats_name"][:n_max_flats]
                        night["masks"] = night["masks"][:n_max_flats]
                        night["masks_name"] = night["masks_name"][:n_max_flats]

                    nbrcombi = len(night["flats_name"])
                    logline.append('Combining these %i flats : \n' % nbrcombi)
                    logline.append('\n'.join(night["flats_name"]) + '\n')

                    a = num_combine(night["flats"], masks=night["masks"], combination_type="median")
                    _, h = auxfcts.fromfits(night["flats_name"][0], hdu=0, verbose=False)

                    h['NCOMBINE']=nbrcombi
                    for i,tocombine in enumerate(night["flats_name"]):
                        k = "IMCMB%i"%(i+1)
                        h[k]=tocombine

                    # # We cannot do anything. Just to be sure, we erase previous flats...
                    outputfilepath = os.path.join(masterflatdir, "MF_%s.fits" % date)


                    print("Combination of %i flats" % nbrcombi, file=outstream)

                    # NORMALIZATION

                    suba = a[300:800, 300:800]
                    subamed = np.median(suba.ravel())


                    a = a / subamed

                    #for some reason the masterflat is now transposed...
                    a = a.T

                    auxfcts.tofits(outputfilepath, a, h)
                    s = make_MF_png(date, masterflatdir, masterflatdir, redo=redo)
                    print(s, file=outstream)

                    print("Done for %s." % (fm), file=outstream)
                    logline.append("Successfully created master flat %s in %s \n" % (outputfilepath, fm))
                except Exception as e:
                    print("Masterflat %s %s failed !!!" % (date, fm), file=outstream)
                    print(e, file=outstream)
                    logline.append("ERROR : Masterflat %s %s failed !!! \n" % (date, fm))
                    logline.append(str(e) + "\n")
                    success = False

    logtxt = "".join(logline)
    log_night = open(logfile, "a")
    log_night.write(logtxt)
    log_night.close()

    print('Done with masterflats construction.', file=outstream)
    print("#" * 10, file=outstream)

    return success


if __name__ == '__main__':
    myrawdirrestruct = "/obs/lenses_EPFL/PRERED/NOT/sorted"
    # myrawdirrestruct = "/home/ericpaic/Documents/PHD/Monitoring/NOT/PRERED/"

    logfile = "/home/astro/millon/Desktop/PRERED/NOT/pipe_log/log_test.log"

    # myrawdirrestruct = "/home/ericpaic/Documents/PHD/Monitoring/NOT/PRERED/"
    # productdir = "/home/ericpaic/Documents/PHD/Monitoring/NOT/PRERED/reduced/"
    productdir = "/obs/lenses_EPFL/PRERED/NOT/reduced"

    datemin = "2021-03-10"
    datemax = "2021-04-16"
    starttime = Time(datemin, format='iso', scale='utc').mjd
    endtime = Time(datemax, format='iso', scale='utc').mjd

    filtername = ["R"]

    days = np.arange(starttime, endtime + 1, 1)
    dates = [Time(d, format='mjd', scale='utc').iso[:10] for d in days]

    radius_future = 0
    radius_past = 5
    n_min_flats = 3
    n_max_flats = 20

    main(dates, myrawdirrestruct, productdir, filtername, logfile, radius_future=radius_future,
         radius_past=radius_past, n_max_flats=n_max_flats, n_min_flats=n_min_flats, redo=True, log_stream=None)
