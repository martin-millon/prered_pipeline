"""
We use masterbias to subtract bias from images

"""
from __future__ import print_function
import os
import sys
import glob
from astropy.io.fits import getheader
from astropy.time import Time
import numpy as np
from astropy.io import fits

def get_all_dates(start="2018-01-01", stop="2018-01-01"):
    '''
    return a list containing all the date from the start date
    :param start: string containing the start date
    :param stop: string containing the stop date
    :return: list of all the date between start and end date.
    '''

    starttime = Time(start, format='iso', scale='utc').mjd
    endtime = Time(stop, format='iso', scale='utc').mjd

    days = np.arange(starttime, endtime + 1, 1)
    dates = [Time(d, format='mjd', scale='utc').iso[:10] for d in days]
    return dates

def check_bias(data,header, sizex_min = 1000, sizey_min = 1000 ):
    if np.shape(data)[0] < sizex_min :
        return False
    if np.shape(data)[1] < sizey_min :
        return False
    return True

def main(dates, myrawdirrestruct, logfile, redo=False, log_stream=None):
    if log_stream is None:
        outstream = sys.stdout
    else:
        outstream = log_stream

    print("Bias subtraction", file=outstream)
    print("My redofromscratch is set to %s" % redo, file=outstream)

    logline = ['\n', '------ Bias subtraction -----', '\ n']
    success = True

    for ind, date in enumerate(dates):
        datedir = os.path.join(myrawdirrestruct, date)

        if os.path.isdir(datedir):
            print("=" * 15, date, "=" * 15, file=outstream)
            dirs = [os.path.join(datedir, o) for o in os.listdir(datedir) if
                    os.path.isdir(os.path.join(datedir, o)) and not 'bias' in os.path.join(datedir, o)]
            nobiassub = []
            logline = []
            failed_date = []
            for directory in dirs:
                images = sorted(glob.glob(os.path.join(directory, '*.fits')))
                basename_images = [os.path.basename(ima) for ima in images]

                for i, image in enumerate(images):
                    basename = os.path.basename(image).split('.fits')[
                        0]  # get only the name of the file without extension
                    if "_bs" in basename:
                        continue
                    if basename + "_bs.fits" in basename_images and redo is False:
                        print("bias-subtracted image exists already for %s !" % image, file=outstream)
                        logline.append("bias-subtracted image exists already for %s ! \n" % image)
                        continue
                    elif basename + "_bs.fits" in basename_images and redo is True:
                        os.remove(os.path.join(directory, basename + "_bs.fits"))
                        print("Removed the previous bias subtraction %s !" % (basename + "_bs.fits"),
                              file=outstream)
                        logline.append("Removed the previous bias subtraction %s ! \n" % (basename + "_bs.fits"))

                    header = getheader(image)

                    print("=== %4i/%4i : %s ===" % (i + 1, len(images), basename), file=outstream)

                    all_dates = get_all_dates(start='2021-04-03', stop=date)
                    ### MASTERBIASES ###
                    print("Create masterbias...", file=outstream)
                    biasesdir = os.path.join(datedir, 'bias/')
                    flatsdir = os.path.join(datedir, 'flat_R/')
                    sciencesdir = os.path.join(datedir, 'science_R/')

                    biasespaths = sorted(glob.glob(biasesdir + "*.fits"))
                    flatspaths = sorted(glob.glob(flatsdir + "*.fits"))
                    sciencespaths = sorted(glob.glob(sciencesdir + "*.fits"))

                    biasarrays = []
                    masterbiasdata = None

                    if len(biasespaths) >= 6:
                        masterbiasheader = {}
                        biascount = 0
                        for i, biaspath in enumerate(biasespaths):
                            try:
                                bias = fits.open(biaspath)
                            except Exception as e:
                                print("Problem with bias %s : %s" % (datedir, e), file=outstream)
                                continue
                            if check_bias(bias[1].data, bias[1].header):
                                biasarrays.append(bias[1].data)
                                biascount += 1
                                if biascount == 1:
                                    masterbiasheader = bias[1].header
                                    masterbiasheader['BIAS1'] = os.path.basename(biaspath)
                                else:
                                    masterbiasheader['BIAS%i' % (i + 1)] = os.path.basename(biaspath)

                        masterbiasheader['NCOMBINE'] = biascount
                        masterbiasheader['OBJECT'] = 'MASTERBIAS'

                        if biascount >= 6:
                            masterbiasdata = np.median(biasarrays, axis=0)
                            masterbias = fits.PrimaryHDU(data=masterbiasdata, header=masterbiasheader)
                            if os.path.exists(os.path.join(datedir, 'masterbias.fits')):
                                os.remove(os.path.join(datedir, 'masterbias.fits'))
                            masterbias.writeto(os.path.join(datedir, 'masterbias.fits'))
                        else:
                            print("\tNot enough exposures !", file=outstream)
                    else:
                        print("\tNot enough exposures !", file=outstream)

                    print("Apply masterbias...", file=outstream)
                    if os.path.isfile(os.path.join(biasesdir, 'masterbias.fits')):
                        pass
                    else:
                        print("\tNo biases for tonight !", file=outstream)
                        print("\tI look in previous nights folders...", file=outstream)
                        for pdate in all_dates[::-1]:
                            if os.path.isfile(os.path.join(myrawdirrestruct, pdate, 'masterbias.fits')):
                                masterbiasdata = \
                                fits.open(os.path.join(myrawdirrestruct, pdate, 'masterbias.fits'))[0].data
                                print("\t\tI found a masterbias in %s" % pdate, file=outstream)
                                break

                    if masterbiasdata is None:
                        print("No masterbias was found. Skipping %s" % date, file=outstream)

                        failed_date.append(date)
                        continue
                    for filepath in flatspaths:
                        try:
                            file = fits.open(filepath)  # we use readonly mode because it's much faster.
                            newdata = file[1].data - masterbiasdata
                            header = file[0].header

                            fits.writeto(filepath.split('.')[0] + "_bs.fits", newdata, header)
                        except Exception as e:
                            print("ERROR in fbias subtraction : %s" % e, file=outstream)
                            failed_date.append(date)

                    for filepath in sciencespaths:
                        try:
                            file = fits.open(filepath)  # we use readonly mode because it's much faster.
                            newdata = file[1].data - masterbiasdata
                            header = file[0].header

                            fits.writeto(filepath.split('.')[0] + "_bs.fits", newdata, header)
                        except Exception as e:
                            print("ERROR in fbias subtraction : %s" % e, file=outstream)
                            failed_date.append(date)

            # Printing faulty formats:
            if not len(nobiassub) == 0:
                print("I failed to subtract the bias on the following images : ", file=outstream)
                for e in nobiassub:
                    print("=" * 5, e, "=" * 5, file=outstream)
                    logline.append("=" * 5 + e + "=" * 5 + "\n")
                print("You might want to do something with these images...", file=outstream)
                success = False
            else:
                print("I successfully subtracted the bias of all images.", file=outstream)
                logline.append("I successfully subtracted the bias of all images. \n")

    logtxt = "".join(logline)
    log_night = open(logfile, "a")
    log_night.write(logtxt)
    log_night.close()

    print("Done with bias subtraction", file=outstream)
    print("#" * 10, file=outstream)

    return success


if __name__ == '__main__':
    myrawdirrestruct = "/obs/lenses_EPFL/PRERED/NOT/sorted"

    logfile = "/home/astro/millon/Desktop/PRERED/NOT/pipe_log/log_test.log"

    datemin = "2021-03-10"
    datemax = "2021-04-16"
    starttime = Time(datemin, format='iso', scale='utc').mjd
    endtime = Time(datemax, format='iso', scale='utc').mjd

    days = np.arange(starttime, endtime + 1, 1)
    dates = [Time(d, format='mjd', scale='utc').iso[:10] for d in days]

    main(dates, myrawdirrestruct, logfile, redo=True, log_stream=None)
