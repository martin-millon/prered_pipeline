'''
We finally apply the masterflats to the science images
'''

from __future__ import print_function
import os
import sys
import glob
from astropy.time import Time
from astropy.io.fits import getheader
import numpy as np
from utils import auxfcts


def main(dates, myrawdirrestruct, productdir, logfile, redo=True, log_stream=None):
    if log_stream is None:
        outstream = sys.stdout
    else:
        outstream = log_stream

    failed_images = []
    logline = ['\n', '------ Master flat application -----', '\ n']
    success = True

    for ind, date in enumerate(dates):

        print("=" * 15, date, "=" * 15, file=outstream)
        datedir = os.path.join(myrawdirrestruct, date)
        if os.path.isdir(datedir):

            science_dirs = [os.path.join(datedir, o) for o in os.listdir(datedir) if
                            os.path.isdir(os.path.join(datedir, o)) and 'science' in os.path.join(datedir,
                                                                                                  o)]  # select the science directories

            for sdir in science_dirs:
                print("=" * 15, sdir.split('science')[1], "=" * 15, file=outstream)
                images = glob.glob(os.path.join(sdir, "*_bs.fits"))  # select the bias subtracted images.

                couldnotflatfield = []
                for i, im in enumerate(images):
                    basename = os.path.basename(im)
                    basename_out = basename.split("_bs.fits")[0] + ".fits"
                    header = getheader(im)
                    target = str(header["OBJECT"])
                    filt = str(header["STFLTNM"]).split('_')[0]


                    flattype = filt  + "4"  # historical naming convention, to make the pipeline compatible with the previous file system
                    destdir = os.path.join(productdir, target + "_" + filt)

                    if os.path.isdir(destdir):
                        print("The destination %s directory exists. I will complete it." % destdir, file=outstream)
                    else:
                        os.mkdir(destdir)

                    print("=== %4i/%4i : %s ===" % (i + 1, len(images), target), file=outstream)

                    # We find the masterflat correpsonding to the night :
                    masterflatfilepath = os.path.join(productdir, "MF_" + flattype, "MF_%s.fits" % date)

                    inputfilepath = im
                    outputfilepath = os.path.join(destdir, basename_out)

                    if os.path.isfile(outputfilepath) and redo is False:
                        print('Already flatfielded, going on...', file=outstream)
                        continue
                    elif os.path.isfile(outputfilepath) and redo is True:
                        print('Already flatfielded, I will remove it', file=outstream)
                        os.remove(outputfilepath)

                    if not os.path.isfile(inputfilepath):
                        print("WARNING : Cannot find input file %s" % inputfilepath, file=outstream)
                        logline.append("WARNING : Cannot find input file %s \n" % inputfilepath)
                        couldnotflatfield.append(im)
                        continue

                    (a, h) = auxfcts.fromfits(inputfilepath, hdu=0, verbose=False)

                    if not os.path.isfile(masterflatfilepath):
                        print("WARNING : No masterflat available for this night... I will not flatfield the image %s" % im,
                              file=outstream)
                        logline.append(
                            "WARNING : No masterflat available for this night... I will not flatfield the image %s \n" % im)
                        couldnotflatfield.append(im)
                        continue

                    else:
                        (masterflat, mh) = auxfcts.fromfits(masterflatfilepath, hdu=0, verbose=False)
                        n_flats = mh['NCOMBINE']
                        mf_filter = mh['STFLTNM'].split('_')[0]
                        flatfieldeda = a / masterflat
                        h['PR_FLATF'] = ("Yes", "pypr has flatfielded this image.")

                        # We add some infos to the header
                        h['PR_NIGHT'] = (date, "pypr night")
                        h['PR_NFLAT'] = (n_flats, "pypr number of flatfields in masterflat")
                        h['PR_MFNAM'] = (os.path.basename(masterflatfilepath), "pypr masterflatname")
                        h['PR_MFFIL'] = (mf_filter, "pypr masterflat filter")
                        h['PR_PROUD'] = (":-)", "pypr proudly prereduced this image !")

                        flatfieldeda = flatfieldeda[100:-100, 100:-100].clip(min=-66000, max=66000)  # border of 300 pixels

                        # We write the fits file
                        auxfcts.tofits(outputfilepath, flatfieldeda, hdr=h, verbose=True)
                        print("Done !", file=outstream)

                print("I could not flatfield %i images in %s:" % (len(couldnotflatfield), sdir), file=outstream)
                logline.append("I could not flatfield %i images in %s: \n" % (len(couldnotflatfield), sdir))
                for img in couldnotflatfield:
                    print(img, file=outstream)
                    logline.append(img + '\n')
                    failed_images.append(date + ' ' + img)

    if not len(failed_images) == 0 :
        success = False

    logtxt = "".join(logline)
    log_night = open(logfile, "a")
    log_night.write(logtxt)
    log_night.close()

    print('Done with masterflat correction.', file=outstream)
    print("#" * 10, file=outstream)

    return success

if __name__ == '__main__':
    # myrawdirrestruct = "./test_data_ECAM/"
    myrawdirrestruct = "/home/ericpaic/Documents/PHD/Monitoring/NOT/PRERED/"
    # productdir = "./test_data_ECAM/reduced/"
    productdir = "/home/ericpaic/Documents/PHD/Monitoring/NOT/PRERED/reduced/"

    logfile = "/home/astro/millon/Desktop/PRERED/NOT/pipe_log/log_test.log"

    datemin = "2021-03-10"
    datemax = "2021-04-16"
    starttime = Time(datemin, format='iso', scale='utc').mjd
    endtime = Time(datemax, format='iso', scale='utc').mjd

    days = np.arange(starttime, endtime + 1, 1)
    dates = [Time(d, format='mjd', scale='utc').iso[:10] for d in days]

    failure = main(dates, myrawdirrestruct, productdir, logfile, redo=True, log_stream=None)
