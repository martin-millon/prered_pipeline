'''We now remove the bias and bias subtracted intermediate product to save disk space
We keep the flats as they are usefull to construct masterflats from the previous and following nights'''

import os, sys
import glob
from astropy.time import Time
import numpy as np
import shutil

def gather_files(outputfile, files_list):
    out = open(outputfile, 'w')

    for f in files_list :
        out.write('### ' + os.path.basename(f) +' ### \n \n')
        with open(f, 'r') as lines :
            for line in lines :
                out.write(line)
            out.write('\n')


def main(dates, myrawdirrestruct, log_directory, log_stream=None):
    if log_stream is None:
        outstream = sys.stdout
    else:
        outstream = log_stream

    for date in dates:
        print("=" * 15, date, "=" * 15, file=outstream)
        datedir = os.path.join(myrawdirrestruct, date)
        logline = ['Starting cleaning up... \n']

        #removing all bias
        bias_dirs = [os.path.join(datedir, o) for o in os.listdir(datedir) if
                     os.path.isdir(os.path.join(datedir, o)) and 'bias' in os.path.join(datedir,
                                                                                        o)]
        for dir in bias_dirs:
            logline.append('Removing bias : %s \n' % dir)
            shutil.rmtree(dir)

        #removing the bias subtracted images in science directories
        science_dirs = [os.path.join(datedir, o) for o in os.listdir(datedir) if
                os.path.isdir(os.path.join(datedir, o)) and 'science' in os.path.join(datedir,
                                                                                        o)]

        for dir in science_dirs:
            bias_subfiles = glob.glob(os.path.join(dir, "*_bs.fits"))
            for file in bias_subfiles:
                os.remove(file)
                logline.append("Removing science bias-sub : Removing %s \n" % file)

        logtxt = "".join(logline)
        log_night = open(os.path.join(datedir, "NOT_cleaning.log"), "w")
        log_night.write(logtxt)
        log_night.close()

        #Reorganizing all log files in one single summary :
        summary = os.path.join(log_directory, 'night_log_%s.log'%date)
        log_files = ['NOT_night_restruct.log','NOT_biassub.log', 'NOT_flats_preparation.log',
                     'NOT_masterflat_construction.log', 'NOT_flatfielding.log' , 'NOT_diagnostics.log',
                     'NOT_cleaning.log']
        log_files = [os.path.join(datedir, l) for l in log_files]
        gather_files(summary, log_files)



    print("Done with cleaning.", file=outstream)
    print("#" * 10, file=outstream)


if __name__ == '__main__':
    # myrawdirrestruct = "./test_data_ECAM/"
    myrawdirrestruct = "/obs/lenses_EPFL/RAW/NOT/"
    # log_directory = './pipe_log/'
    log_directory = '/home/astro/paic/Monitoring/NOT/pipe_log/'
    datemin = "2021-03-10"
    datemax = "2021-04-17"
    starttime = Time(datemin, format='iso', scale='utc').mjd
    endtime = Time(datemax, format='iso', scale='utc').mjd

    days = np.arange(starttime, endtime + 1, 1)
    dates = [Time(d, format='mjd', scale='utc').iso[:10] for d in days]

    main(dates, myrawdirrestruct, log_directory, log_stream=None)
