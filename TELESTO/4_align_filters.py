import os
import alipy
import glob
from astropy.io import fits

def main(date,reducpath, overwrite_non_aligned=False, showplot=False):

    stackeddir = os.path.join(reducpath, date, 'STACKED')
    # Align the stack between filters :
    for binning in os.listdir(stackeddir):
        if not os.path.isdir(os.path.join(stackeddir, binning)): continue
        for object in os.listdir(os.path.join(stackeddir, binning)):
            print(object)
            if not os.path.isdir(os.path.join(stackeddir, binning, object)): continue

            files_object = sorted(glob.glob(os.path.join(stackeddir, binning, object, '*.fits')))
            files_object = [f for f in files_object if not '_align' in f] # rmoved the already aligned images

            if len(files_object) <= 1:
                print('Nothing to align for %s' % os.path.join(stackeddir, binning, object))
                continue

            identifications = alipy.ident.run(files_object[0], files_object, visu=False)
            outputshape = alipy.align.shape(files_object[0])
            for id in identifications:
                if id.ok == True:
                    alipy.align.affineremap(id.ukn.filepath, id.trans, shape=outputshape, makepng=showplot,
                                            outdir=os.path.join(stackeddir, binning, object, "ALIGNED"))


            #overwrite the previous non-aligned image
            for i,file_aligned in enumerate(sorted(glob.glob(os.path.join(stackeddir, binning, object, "ALIGNED", "*.fits")))):
                data = fits.open(file_aligned)[0].data
                header = fits.open(files_object[i])[0].header

                aligned_image = fits.PrimaryHDU(data=data, header=header)

                if overwrite_non_aligned :
                    aligned_image.writeto(files_object[i],overwrite=True)
                else :
                    basename = os.path.basename(files_object[i])
                    basename = str(basename.split('.fits')[0]) + '_align.fits'
                    aligned_image.writeto(os.path.join(stackeddir, binning, object,basename), overwrite=True)

            os.system('rm -r %s'%os.path.join(stackeddir, binning, object, "ALIGNED"))




if __name__ == '__main__':
    reducpath = '/Users/martin/Desktop/lc_run/data_raw/TELESTO/reduced'

    date = 'August-23-2021'
    main(date, reducpath)