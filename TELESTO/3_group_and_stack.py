import os, glob,sys
from astropy.io import fits
from util import mkdir_recursive
import numpy as np
import alipy
sys.path.append('../TelescopeUtils')


def main(date,reducpath, redo=True, command = "cp", showplot=False):
    if redo == False:
        command += ' -n'  # no clobber command (do not overwrite)

    sciencedir = os.path.join(reducpath, date, 'SCIENCE')
    stackeddir = os.path.join(reducpath, date, 'STACKED')

    for binning in os.listdir(sciencedir):
        if not os.path.isdir(os.path.join(sciencedir, binning)): continue
        for filter in os.listdir(os.path.join(sciencedir, binning)):
            if not os.path.isdir(os.path.join(sciencedir, binning,filter)): continue

            files = glob.glob(os.path.join(sciencedir, binning, filter, '*fits'))
            #rmov ethe already existing stack :
            for f in files :
                if '_stack' in f :
                    os.system('rm %s'%f)
            files = glob.glob(os.path.join(sciencedir, binning, filter, '*fits'))

            object_list = []
            #sort by object
            for f in files :
                hdulist = fits.open(f)
                header = hdulist[0].header
                object = header["OBJECT"]
                object_name = object.replace(" ", "")
                if not object_name in object_list:
                    object_list.append(object_name)

                tmp_dir = os.path.join(sciencedir, binning, filter,object_name)
                mkdir_recursive(tmp_dir)
                os.system("%s %s %s"%(command, f, tmp_dir))

            #align for object
            for o in object_list:
                files_object = sorted(glob.glob(os.path.join(sciencedir, binning, filter,o, '*.fits')))
                if len(files_object) == 0 :
                    print('No fits files for %s'%o)
                    continue


                header = fits.open(files_object[0])[0].header
                dataarray = []

                mkdir_recursive(os.path.join(stackeddir, binning, o))

                print(files_object)
                identifications = alipy.ident.run(files_object[0], files_object, visu=False)
                outputshape = alipy.align.shape(files_object[0])
                for id in identifications:
                    if id.ok == True:
                        alipy.align.affineremap(id.ukn.filepath, id.trans, shape=outputshape, makepng=showplot,
                                                outdir=os.path.join(sciencedir, binning, filter,o, "ALIGNED"))

                totexptime=0
                nimage =0
                for i, file_aligned in enumerate(sorted(glob.glob(os.path.join(sciencedir, binning, filter,o, "ALIGNED", "*.fits")))):
                    aligned_image = fits.open(file_aligned)[0].data
                    aligned_header = fits.open(files_object[i])[0].header
                    totexptime+=aligned_header['EXPTIME']
                    dataarray.append(aligned_image)
                    nimage+=1

                dataarray = np.median(dataarray, axis=0)
                header['EXPTIME']=totexptime
                header['NCOMBINE']=nimage
                stacked = fits.PrimaryHDU(data=dataarray, header=header)
                stacked.writeto(os.path.join(stackeddir, binning, o, o + '_stack_%s.fits'%filter),
                                overwrite=redo)


if __name__ == '__main__':
    reducpath = '/Users/martin/Desktop/lc_run/data_raw/TELESTO/reduced'

    date = 'August-23-2021'
    main(date, reducpath)
