import os, glob
from astropy.io import fits
import numpy as np
from util import mkdir_recursive

def main(date,sortpath, reducpath, redo=True, margin = 100, skysub=True):
    #make masterdark :
    darkdir = os.path.join(sortpath,date,'DARK')

    for binning in os.listdir(darkdir):
        if not os.path.isdir(os.path.join(darkdir,binning)): continue
        for exptime in os.listdir(os.path.join(darkdir,binning)):
            if not os.path.isdir(os.path.join(darkdir,binning,exptime)):continue
            masterdark_file = os.path.join(darkdir,binning,exptime,'masterdark.fits')
            if os.path.isfile(masterdark_file):
                if redo :
                    os.system('rm %s'%masterdark_file)
                else :
                    continue

            files = glob.glob(os.path.join(darkdir,binning,exptime,'*fits'))

            darkarray = []
            masterdarkheader = {}
            for f in files :
                hdulist = fits.open(f)
                header = hdulist[0].header
                type = header['IMAGETYP']
                xbinning = header['XBINNING']
                ybinning = header['YBINNING']
                expt = header['EXPTIME']
                darkarray.append(hdulist[0].data)

            masterdarkheader['IMAGETYP']=type
            masterdarkheader['XBINNING']=xbinning
            masterdarkheader['YBINNING']=ybinning
            masterdarkheader['EXPTIME']=expt
            masterdarkheader['NCOMBINE'] = len(files)

            masterdarkdata = np.median(darkarray, axis=0)

            masterdark = fits.PrimaryHDU(data=masterdarkdata, header=fits.Header(masterdarkheader))
            masterdark.writeto(masterdark_file, overwrite=redo)


    #make masterflat:
    flatdir = os.path.join(sortpath,date, 'FLAT')

    for binning in os.listdir(flatdir):
        if not os.path.isdir(os.path.join(flatdir, binning)): continue
        for filter in os.listdir(os.path.join(flatdir, binning)):
            if not os.path.isdir(os.path.join(flatdir, binning, filter)): continue
            for exptime in os.listdir(os.path.join(flatdir, binning, filter)):
                if not os.path.isdir(os.path.join(flatdir, binning, filter,exptime)): continue
                masterflat_file = os.path.join(flatdir, binning, filter, 'masterflat.fits')
                if os.path.isfile(masterflat_file):
                    if redo:
                        os.system('rm %s' % masterflat_file)
                    else:
                        continue

                files = glob.glob(os.path.join(flatdir, binning, filter, exptime, '*fits'))

                flatarray = []
                masterflatheader = {}

                try :
                    masterdark_c = fits.open(os.path.join(darkdir, binning, exptime, 'masterdark.fits'))[0].data
                except FileNotFoundError as e :
                    print('WARNING : no masterdark for the flats ! I am not remove the darks to the flats....')
                    print(e)
                    no_masterdark_c = True
                else :
                    no_masterdark_c = False

                for f in files:
                    hdulist = fits.open(f)
                    header = hdulist[0].header
                    type = header['IMAGETYP']
                    xbinning = header['XBINNING']
                    ybinning = header['YBINNING']
                    expt = header['EXPTIME']
                    if no_masterdark_c:
                        masterdark_c = np.zeros_like(hdulist[0].data)
                    flatarray.append((hdulist[0].data -masterdark_c) / np.median(hdulist[0].data -masterdark_c))

                masterflatheader['IMAGETYP'] = type
                masterflatheader['XBINNING'] = xbinning
                masterflatheader['YBINNING'] = ybinning
                masterflatheader['EXPTIME'] = expt
                masterflatheader['NCOMBINE'] = len(files)

                masterflatdata = np.median(flatarray, axis=0)

                masterflat = fits.PrimaryHDU(data=masterflatdata, header=fits.Header(masterflatheader))
                masterflat.writeto(os.path.join(flatdir, binning, filter, 'masterflat.fits'),overwrite=redo)

    #reduce the science image :
    sciencedir = os.path.join(sortpath,date, 'SCIENCE')

    for binning in os.listdir(sciencedir):
        if not os.path.isdir(os.path.join(sciencedir, binning)): continue
        for filter in os.listdir(os.path.join(sciencedir, binning)):
            if not os.path.isdir(os.path.join(sciencedir, binning, filter)): continue
            for exptime in os.listdir(os.path.join(sciencedir, binning, filter)):
                if not os.path.isdir(os.path.join(sciencedir, binning, filter, exptime)): continue
                files = glob.glob(os.path.join(sciencedir, binning, filter, exptime, '*fits'))
                hdulist = fits.open(files[0])

                try :
                    masterdark_c = fits.open(os.path.join(darkdir, binning, exptime, 'masterdark.fits'))[0].data
                except FileNotFoundError as e :
                    print('WARNING : No masterdark for the science image. I am not removing the darks... ')
                    print(e)
                    masterdark_c = np.zeros_like(hdulist[0].data)

                try:
                    masterflat_c = fits.open(os.path.join(flatdir, binning, filter, 'masterflat.fits'))[0].data
                except FileNotFoundError as e :
                    print('WARNING : No masterflats for the science image. I am not removing the flats... ')
                    print(e)
                    masterflat_c = np.ones_like(hdulist[0].data)

                outdir = os.path.join(reducpath,date, 'SCIENCE',binning, filter)
                mkdir_recursive(outdir)

                for f in files:
                    if '_sky' in f:
                        os.system('rm %s'%f)
                        print('Deleting existing sky.fits')
                        continue

                    hdulist = fits.open(f)
                    basename = os.path.basename(f)
                    basename = basename.replace(",", "-")
                    header = hdulist[0].header
                    binning_science = header['XBINNING']
                    print(f)
                    data = (hdulist[0].data - masterdark_c) / masterflat_c

                    #convert in ADU/s-1 :
                    data = data / header['EXPTIME']

                    #cut the border of the image
                    margin_c = int(margin/binning_science)
                    data = data[margin_c:-margin_c, margin_c:-margin_c]
                    x,y = np.shape(data)
                    header['NAXIS1']=x
                    header['NAXIS2']=y

                    science_im = fits.PrimaryHDU(data=data, header=header)
                    science_file = os.path.join(outdir, basename)
                    science_im.writeto(science_file,
                                       overwrite=True)

                    #subtract sky
                    if skysub:
                        saturlevel = 65000/ header['EXPTIME']
                        pixsize = 0.8 #for TELESTO

                        skyimage = os.path.join(sciencedir, binning, filter, exptime,basename.split('.')[0] + '_sky.fits')
                        cmd = "%s %s -c default_sky_template_smooth.sex -PIXEL_SCALE %.3f -SATUR_LEVEL %.3f -CHECKIMAGE_NAME %s" % (
                        'sex', science_file, pixsize, saturlevel, skyimage)
                        os.system(cmd)

                        sky = fits.getdata(skyimage, header=False)
                        skysubimage = data - sky
                        header['SKYSUB']='YES'
                        header['SKYMEAN']=np.median(sky)
                        science_im = fits.PrimaryHDU(data=skysubimage, header=header)
                        science_im.writeto(science_file,
                                           overwrite=True)



    #TODO : implement bias reduction

if __name__ == '__main__':
    sortpath = '/Users/martin/Desktop/lc_run/data_raw/TELESTO/sorted'
    reducpath = '/Users/martin/Desktop/lc_run/data_raw/TELESTO/reduced'

    date = 'August-23-2021'
    main(date,sortpath, reducpath, redo=False)
