#!/usr/bin/python3

sort_data = __import__('1_sort_data')
reduce = __import__('2_reduce')
group = __import__('3_group_and_stack')
alignfilter = __import__('4_align_filters')
cleanup = __import__('5_cleanup')

import argparse as ap
import os

def main(date):

    #Place the output folder from the Telesto telescope with all the calibration in it in the 'rawpath' folder.

    rawpath = '/Users/martin/Desktop/lc_run/data_raw/TELESTO/raw'
    reducpath = '/Users/martin/Desktop/lc_run/data_raw/TELESTO/reduced'
    sortedpath = '/Users/martin/Desktop/lc_run/data_raw/TELESTO/sorted'

    #option
    redo = False #redo the reduction and overwrite existing files
    clean = False #clean all the intermediate product
    skysub=True #use sextractor to remove the sky
    cutmargin = 50 #resize the frames with the given margin
    showplot=False #print png of the align image

    sort_data.main(date,rawpath,sortedpath, command='cp', redo=redo)
    reduce.main(date,sortedpath, reducpath, redo=redo, margin = cutmargin, skysub=skysub)
    group.main(date,reducpath, redo=redo, command = "cp", showplot=showplot)
    alignfilter.main(date,reducpath, overwrite_non_aligned=False, showplot=showplot)

    if clean:
        cleanup.main(date,reducpath,sortedpath, clean_sorted = True, keeponly_stack= False)

if __name__ == '__main__':
    parser = ap.ArgumentParser(prog="python {}".format(os.path.basename(__file__)),
                               description="Run the VST pipeline. No argument will run for the previous nigth.",
                               formatter_class=ap.RawTextHelpFormatter)
    help_startnight = "first night to reduce in the format YYYY-MM-DD"

    parser.add_argument(dest='night', type=str,
                        metavar='', action='store',
                        help=help_startnight)

    args = parser.parse_args()

    main(args.night)

