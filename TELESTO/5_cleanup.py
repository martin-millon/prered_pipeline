import os
import glob

def main(date,reducpath,sortedpath, clean_sorted = True, keeponly_stack= False):
    if clean_sorted:
        print('Cleaning intermediate product in %s'%os.path.join(sortedpath,date))
        for dir in os.listdir(os.path.join(sortedpath,date)) :
            os.system('rm -r %s'%os.path.join(sortedpath,date,dir))

    if keeponly_stack :
        print('Keeping only the stacked image in %s' % os.path.join(reducpath, date, 'STACKED'))
        for dir in os.listdir(os.path.join(reducpath, date, 'SCIENCE')):
            os.system('rm -r %s' %os.path.join(sortedpath,date,dir))

    else :
        print('Keeping only reduced science frame')
        sciencedir = os.path.join(reducpath, date, 'SCIENCE')
        for binning in os.listdir(sciencedir):
            if not os.path.isdir(os.path.join(sciencedir, binning)): continue
            for filter in os.listdir(os.path.join(sciencedir, binning)):
                if not os.path.isdir(os.path.join(sciencedir, binning, filter)): continue

                files = glob.glob(os.path.join(sciencedir, binning, filter, '*.fits'))
                for f in files :
                    os.system('rm %s'%f)

                for object in os.listdir(os.path.join(sciencedir, binning, filter)):
                    if not os.path.isdir(os.path.join(sciencedir, binning, filter, object)): continue

                    os.system('rm -r %s'%os.path.join(os.path.join(sciencedir, binning, filter, object, 'ALIGNED')))




if __name__ == '__main__':
    reducpath = '/Users/martin/Desktop/lc_run/data_raw/TELESTO/reduced'
    sortedpath = '/Users/martin/Desktop/lc_run/data_raw/TELESTO/sorted'

    date = 'February-10-2021'
    main(date, reducpath, sortedpath)