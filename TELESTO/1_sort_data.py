import os, glob
from astropy.io import fits
from util import mkdir_recursive


def main(date,rawpath,sortpath, command='cp', redo=True):

    if redo == False:
        command += ' -n'  # no clobber command (do not overwrite)

    print(os.path.join(rawpath,date))
    #remove space in filename
    for f in glob.glob(os.path.join(rawpath,date,'*.fits')):
        r = f.replace(" ", "")
        if (r != f):
            os.rename(f, r)

    filepaths = glob.glob(os.path.join(rawpath,date,'*.fits'))
    for filepath in filepaths:
        filename = os.path.basename(filepath)

        print(filename)

        hdulist = fits.open(filepath)
        header = hdulist[0].header
        type = header['IMAGETYP']
        filter = header['FILTER'].split(' ')[0]
        binning = header['XBINNING']
        expt = header['EXPTIME']
        if type == 'Flat Field':
            folder = os.path.join(sortpath,date,'FLAT','binning'+ str(binning),filter, 'exptime'+str(expt))
        elif type == 'Dark Frame':
            folder = os.path.join(sortpath, date, 'DARK', 'binning' + str(binning), 'exptime'+str(expt))
        elif type == 'Light Frame':
            folder = os.path.join(sortpath, date, 'SCIENCE', 'binning' + str(binning),filter, 'exptime' + str(expt))
        elif type == 'Bias':
            folder = os.path.join(sortpath, date, 'BIAS', 'binning' + str(binning))
        else :
            raise RuntimeError('Unknown type of image %s'%filename)

        mkdir_recursive(folder)
        os.system('%s %s %s'%(command,filepath,folder))


if __name__ == '__main__':
    rawpath = '/Users/martin/Desktop/lc_run/data_raw/TELESTO/raw'
    sortpath = '/Users/martin/Desktop/lc_run/data_raw/TELESTO/sorted'

    date = 'August-23-2021'
    main(date,rawpath,sortpath, command='cp', redo=True)

