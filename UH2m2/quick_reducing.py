import numpy as np
import glob, os
from astropy.io import fits
import matplotlib.pyplot as plt

dir = '/Users/martin/Desktop/COSMOULINE/data_raw/H0liCow_UH2p2m/'
flat_dir = dir + 'flats/'
bias_dir = dir + 'bias/'
science_dir = dir + 'science/'
output_dir = dir + 'prereduced/'
if not os.path.exists(output_dir):
    os.mkdir(output_dir)

display = True
overwrite = True

#Masterbias :
bias_filenames = glob.glob(os.path.join(bias_dir,'bias.*.fits'))
bias_list = []
for b in bias_filenames:
    print b
    bias, header = fits.getdata(b, header= True )
    bias = bias[:-20,:]
    print("New bias shape : ",np.shape(bias))
    bias_list.append(bias)

bias_list = np.asarray(bias_list)
masterbias = np.median(bias_list, axis=0)
hdu = fits.PrimaryHDU(masterbias)
hdul = fits.HDUList([hdu])
hdul.writeto(bias_dir + 'masterbias.fits', overwrite = overwrite)

if display :
    plt.figure(1)
    plt.imshow(masterbias)
    plt.title('Masterbias')
    plt.show()

#Masterflats :
flat_filenames = glob.glob(os.path.join(flat_dir,'skyflat.*.fits'))
flat_list = []
for f in flat_filenames:
    print f
    flat, header = fits.getdata(f, header= True )
    flat = flat[4:,:-36]
    flat_list.append(flat)

masterbias = masterbias[4:,4:-32]
flat_list = np.asarray(flat_list)
masterflat = np.median(flat_list, axis=0)
masterflat = masterflat - masterbias

masterflat = masterflat/float(np.mean(masterflat))
print('Mean of the masterflat :', np.mean(masterflat))
for i in range(len(masterflat[:, 0])):
    for j in range(len(masterflat[0, :])):
        if masterflat[i, j] == 0:
            print "here", i, j
            masterflat [i, j] = 0.00001
hdu = fits.PrimaryHDU(masterflat)
hdul = fits.HDUList([hdu])
hdul.writeto(flat_dir + 'masterflat.fits', overwrite = overwrite)

if display :
    plt.figure(2)
    plt.imshow(masterflat)
    plt.title('Masterflat')
    plt.show()

#Reduce science images :
science_filenames = glob.glob(os.path.join(science_dir,'*.fits'))
for s in science_filenames:
    science, header = fits.getdata(s, header=True)
    science =science[4:,4:-32]
    science = (science - masterbias)/(masterflat)
    hdu = fits.PrimaryHDU(science, header=header)
    hdul = fits.HDUList([hdu])
    name = os.path.split(s)[-1]
    name = name.split('.')[0] + '_' + name.split('.')[1]
    print (name + '.fits')
    hdul.writeto(output_dir + name + '.fits', overwrite = overwrite)



